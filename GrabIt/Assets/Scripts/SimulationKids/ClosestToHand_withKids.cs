﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ClosestToHand_withKids : MonoBehaviour
{
	// private Mesh objMesh;
	// private Vector3[] objVertices; 
	private Vector3 closestPointPerVertex;
	// public Color[] colors;

	public bool phalanxContact;
	public GameObject OOI;

	public Vector3 closest_distance, closestPoint;

	public float REAL_MIN_DISTANCE;

	// private Shader shader;
	private Vector3 worldVertex;
	private int nbKids;


    void Start()
    {
  		// shader = Shader.Find("Legacy Shaders/Particles/VertexLit Blended");
      	OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");
      	nbKids = OOI.transform.childCount;
    }

    // Update is called once per frame
    void Update()
    {
  		OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");

  		nbKids = OOI.transform.childCount;
  		for(int i = 0; i < nbKids; i++)
      {
    	
// SIMPLIFIED FOR CALCULATION TIME
      	closestPoint = OOI.transform.GetChild(i).gameObject.GetComponent<Collider>().ClosestPointOnBounds(this.transform.position);
      	closest_distance = OOI.transform.GetChild(i).gameObject.GetComponent<Collider>().ClosestPoint(closestPoint);
        REAL_MIN_DISTANCE = Vector3.Distance(closest_distance, this.transform.position);

// REAL
    // 		objVertices = OOI.transform.GetChild(i).gameObject.GetComponent<MeshFilter>().mesh.vertices;
    		// objMesh = OOI.transform.GetChild(i).gameObject.GetComponent<MeshFilter>().mesh;

    // 		for(int k = 0; k < objVertices.Length; k++)
     //  	{
	    //     worldVertex = transform.TransformPoint(objVertices[k]);
	    //   	// closestPoint = OOI.GetComponent<Collider>().ClosestPointOnBounds(this.transform.position);
	    //     closestPoint = this.GetComponent<Collider>().ClosestPointOnBounds(worldVertex);

	    //   	closest_distance = OOI.transform.GetChild(i).gameObject.GetComponent<Collider>().ClosestPoint(closestPoint);
	    //     REAL_MIN_DISTANCE = Vector3.Distance(closest_distance, this.transform.position);
	    // }
	    }
    }

    void OnCollisionStay(Collision collisionInfo)
    {
      if(collisionInfo.collider.gameObject.tag == "ObjectOfInterest")
      {
        // Debug.Log(this.gameObject.name + " PhalanxContact");
        this.phalanxContact = true;
      }
    }

    void OnCollisionExit(Collision collisionInfo)
    {
      if(collisionInfo.collider.gameObject.tag == "ObjectOfInterest")
      {
        // Debug.Log(this.gameObject.name + " PhalanxContact");
        this.phalanxContact = false;
      }
    }
}

