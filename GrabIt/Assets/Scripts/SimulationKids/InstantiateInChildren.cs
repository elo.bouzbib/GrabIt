using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class InstantiateInChildren : MonoBehaviour
{

	[Serializable]	
 	public struct TasksChoice {
	    public string nameTask;
	    public int nbTask;
 	}
 	[Header("Tasks")]	
 	public TasksChoice[] declaredTasks;

	[Serializable]	
 	public struct ObjectChoice {
	    public string objectName;
	    public int nbObject;
 	}
 	[Header("Objects")]
 	public ObjectChoice[] declaredObjects;

	[Header("Scales")]
	public Vector3[] scales;

	private string[] tasks;
    [HideInInspector]
	public GameObject[] objectTypes;
	private GameObject[] phantomObject;
	// private Vector3[] scales;
	public List<int> configException;

    // private Transform[] posPhantoms;
    private Vector3[] posPhantoms;
    private Vector3 posOrigin;

    [Range(0, 127)]
    public int config;
    public Vector3 scaleObj;
    public GameObject objToCatch;
    public string taskToDo;
    private int taskNumber, objId;

    public int nbBloc = 0;
    public int nbBlocMax = 1;
    public int state = -1;

    public Material phantomMaterialRed, phantomMaterialGreen;

    private Transform[] walls;
    private TextMesh[] otherwalls;
    [HideInInspector]
	public GameObject Text3D;

	public bool realGame;
	private GameObject resetButton, nextButton, startButton;
	private GameObject[] hands;

	private Vector3 previousAngles;

	public int nbTouch = 0;
	public bool grasp;
	private bool[] graspOne;


	private int correctBug = 0;
	private bool startOver = false;
	private int secondBug = 0;


    // Start is called before the first frame update
    void Start()
    {
    	// posOrigin = new Vector3(-0.25f, -0.63f, 0.335f);
    	posOrigin = new Vector3(-0.25f, -0.4f, 0.335f);

		// TASKS
		tasks = new string[declaredTasks.Length];
		for(int i = 0; i < declaredTasks.Length; i++)
		{
			tasks[declaredTasks[i].nbTask] = declaredTasks[i].nameTask;
		}
		

		// OBJECTS
		objectTypes = new GameObject[declaredObjects.Length];
		phantomObject = new GameObject[declaredObjects.Length];
		for(int i = 0; i < declaredObjects.Length; i++)
		{
			objectTypes[declaredObjects[i].nbObject] = GameObject.Find("ObjectsOfInterest/" + declaredObjects[i].objectName);
			phantomObject[declaredObjects[i].nbObject] = GameObject.Find("PhantomObjects/" + declaredObjects[i].objectName);
		}
		// ATTENTION : IF ADDING MORE TASKS, GO LINE 113 of COLORCHANGEKids.CS
    	//0:Hold, 1:Push: 2:Pull: 3:MoveOver; 4:Raise; 5:PushDown; 6:Fit; 7:Move
// REAL
    	// objectTypes = new GameObject[]{GameObject.Find("ObjectsOfInterest/Cylinder"), GameObject.Find("ObjectsOfInterest/Cube"), GameObject.Find("ObjectsOfInterest/Sphere")};//, GameObject.Find("ObjectsOfInterest/Capsule")};//, GameObject.Find("ObjectsOfInterest/Cylinder90")}; // j
// PILOT
    	// objectTypes = new GameObject[]{GameObject.Find("ObjectsOfInterest/Sphere"), GameObject.Find("ObjectsOfInterest/Cube"), GameObject.Find("ObjectsOfInterest/Cylinder")};//, GameObject.Find("ObjectsOfInterest/Capsule")};//, GameObject.Find("ObjectsOfInterest/Cylinder90")}; // j

    	//0:Cylinder, 1:Cube, 2:Sphere, 3:Capsule //, 4:LyingCylinder
//REAL		
		// phantomObject = new GameObject[]{GameObject.Find("PhantomObjects/Cylinder"), GameObject.Find("PhantomObjects/Cube"), GameObject.Find("PhantomObjects/Sphere")};//, GameObject.Find("PhantomObjects/Capsule")};//, GameObject.Find("ObjectsOfInterest/Cylinder90")};
// PILOT
    	// phantomObject = new GameObject[]{GameObject.Find("PhantomObjects/Sphere"), GameObject.Find("PhantomObjects/Cube"), GameObject.Find("PhantomObjects/Cylinder")};
    	for(int i = 0; i < objectTypes.Length; i++)
    	{
	    	objectTypes[i].SetActive(false);
	    	phantomObject[i].SetActive(false);
	    	// 0: cylinder; 1: cube; 2: sphere; 3: capsule
    	}

    	// scales = new Vector3[3];
    	// scales[0] = new Vector3(0.03f, 0.03f, 0.03f);
    	// scales[1] = new Vector3(0.05f, 0.05f, 0.05f);
    	// scales[2] = new Vector3(0.1f, 0.1f, 0.1f);



// REAL
		posPhantoms = new Vector3[tasks.Length];
		for(int i = 0; i < tasks.Length; i++)
		{
			if(declaredTasks[i].nameTask == "Touch")
			{
				posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z); // TOUCH
			}
			if(declaredTasks[i].nameTask == "Push")
			{
				posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z + 0.2f); // PUSH
			}
			if(declaredTasks[i].nameTask == "Pull")
			{
				posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z - 0.2f); // PULL
			}
			if(declaredTasks[i].nameTask == "Move Over")
			{
				posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x + 0.6f, posOrigin.y, posOrigin.z - 0.15f); // MOVEOVER
			}
			if(declaredTasks[i].nameTask == "Raise")
			{
				posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x, posOrigin.y + 0.5f, posOrigin.z); // RAISE
			}
			if(declaredTasks[i].nameTask == "Push Down")
			{
				posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x, posOrigin.y - 0.1f, posOrigin.z); // PUSHDOWN
			}
			if(declaredTasks[i].nameTask == "Fit")
			{
				posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x - 0.6f, posOrigin.y, posOrigin.z + 0.35f); // FIT
			}
			if(declaredTasks[i].nameTask == "Roll")
			{
				posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x + 0.8f, posOrigin.y, posOrigin.z); // MOVE
			}
		}
		
  //   	posPhantoms_defined[0] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z); // TOUCH
  //   	posPhantoms_defined[1] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z + 0.2f); // PUSH
  //   	posPhantoms_defined[2] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z - 0.2f); // PULL
  //   	posPhantoms_defined[3] = new Vector3(posOrigin.x + 0.6f, posOrigin.y, posOrigin.z - 0.15f); // MOVEOVER
		// posPhantoms_defined[4] = new Vector3(posOrigin.x, posOrigin.y + 0.5f, posOrigin.z); // RAISE
		// posPhantoms_defined[5] = new Vector3(posOrigin.x, posOrigin.y - 0.1f, posOrigin.z); // PUSHDOWN
		// posPhantoms_defined[6] = new Vector3(posOrigin.x - 0.6f, posOrigin.y, posOrigin.z + 0.35f); // FIT
		// posPhantoms_defined[7] = new Vector3(posOrigin.x + 0.8f, posOrigin.y, posOrigin.z); // MOVE
// PILOT
		// posPhantoms = new Vector3[tasks.Length];
		// for(int i = 0; i < tasks.Length; i++)
		// {
		// 	posPhantoms[i] = posPhantoms_defined[i];
		// }
    	// posPhantoms[0] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z + 0.2f);
    	// posPhantoms[0] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z);
    	// posPhantoms[1] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z + 0.2f); // PUSH
    	// posPhantoms[2] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z - 0.2f); // PULL
    	// posPhantoms[1] = new Vector3(posOrigin.x + 0.5f, posOrigin.y, posOrigin.z);


		configException = new List<int>();
		config = UnityEngine.Random.Range(0, tasks.Length*objectTypes.Length*scales.Length);

    	walls = GameObject.Find("Walls").GetComponentsInChildren<Transform>();
		Text3D = GameObject.Find("Text3D");
		otherwalls = new TextMesh[walls.Length];

    	for (int i = 2; i < otherwalls.Length; i++)
    	{
	    	otherwalls[i] = (TextMesh)Instantiate(Text3D, walls[i].gameObject.transform).GetComponent<TextMesh>();
	    	otherwalls[i].transform.position = walls[i].gameObject.transform.position;
	    	otherwalls[i].transform.eulerAngles = new Vector3(walls[i].gameObject.transform.eulerAngles.x, walls[i].gameObject.transform.eulerAngles.y - 90f, walls[i].gameObject.transform.eulerAngles.z);

	    	otherwalls[i].characterSize = 0.02f;
    	}

    	resetButton = GameObject.Find("ResetButton");
    	nextButton = GameObject.Find("NextButton");
    	hands = new GameObject[]{GameObject.Find("OVRCustomHandPrefab_L"), GameObject.Find("OVRCustomHandPrefab_R")};
    	startButton = GameObject.Find("StartPosition");
    	grasp = false;

    }

    // Update is called once per frame
    void Update()
    {
    	// Going through i, j, k and assigning tasks/objects/scales to configurations numbers.
		for(int k = 0; k < scales.Length; k++)
		{
			if((config < tasks.Length*objectTypes.Length*(k+1)) && (config >= tasks.Length*objectTypes.Length*k))
			{
				taskToDo = tasks[config%tasks.Length];
				taskNumber = config%tasks.Length;
				for(int j = 0; j < objectTypes.Length; j++)
				{
					if((config < tasks.Length*(objectTypes.Length*k + j+1)) && (config >= tasks.Length*(objectTypes.Length*k + j)))
					{
						objToCatch = objectTypes[j];
						objId = j;
					}
				}
				scaleObj = scales[k];
			}
		}

		Debug.Log("Etat: " + state);

		// FOR ANALYSIS, WaitUntil Grasp and FreezeAll in the meantime;
    	// ATTENTION: GRAVITY OF OOI REMOVED IN COLORCHANGEKids.CS FOR ANALYSIS OF GRASP BEHAVIOUR
		for(int k = 0; k < objectTypes[objId].transform.childCount; k++)
		{
			graspOne = new bool[objectTypes[objId].transform.childCount];
			// graspOne[k] = false;
			objectTypes[objId].transform.GetChild(k).GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
			if(realGame)
			{
				graspOne[k] = objectTypes[objId].transform.GetChild(k).GetComponent<ColorChangeKids>().graspContact;	
				if(graspOne[k])
				{
					grasp = true;
				}
			}
			else
			{
				grasp = true;
			}
		}
		
		if(correctBug >= 1)
		{
			state = 3;
		}

		if(grasp)
		{
			GameObject.Find("Distractor").GetComponent<MeshRenderer>().material.color = Color.green;
		}
		else
		{
			GameObject.Find("Distractor").GetComponent<MeshRenderer>().material.color = Color.red;
		}

		startOver = false;


		switch(state)
		{
			case -2:

				nbTouch = nbTouch + 1;
				if(configException.Count >= (tasks.Length*objectTypes.Length*scales.Length))
				{
					nbBloc = nbBloc + 1;
					if(nbBloc >= nbBlocMax)
					{
						state = 2;
					}
					else
					{
						startOver = true;
					}
				}
				else
				{
					Debug.Log("NBTOUCH +1");
					state = -1;
				}

				if(startOver)
				{
					configException.Clear();
					configException = new List<int>();
					Debug.Log("NBTOUCH -1");
					nbTouch = -1;
				}
			break;
		
			case -1:
				config = UnityEngine.Random.Range(0, tasks.Length*objectTypes.Length*scales.Length);
				while(configException.Contains(config))
				{
					config = UnityEngine.Random.Range(0, tasks.Length*objectTypes.Length*scales.Length);
				}
				for(int i = 0; i < objectTypes.Length; i++)
				{
					for(int k = 0; k < objectTypes[i].transform.childCount; k++)
					{
						objectTypes[i].transform.GetChild(k).gameObject.GetComponent<ColorChangeKids>().collision = false;
					}
					objectTypes[i].SetActive(false);
					phantomObject[i].SetActive(false);
				}
				state = 0;

			break;

			case 0:
				grasp = false;
				configException.Add(config);
				
                objectTypes[objId].SetActive(true);
                objectTypes[objId].transform.position = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z);
                for(int k = 0; k < objectTypes[objId].transform.childCount; k++)
				{
	                objectTypes[objId].transform.GetChild(k).gameObject.GetComponent<ColorChangeKids>().collision = false;
    			}
                // objectTypes[objId].GetComponent<ObjectOfInterest>().toBePicked = true;
                objectTypes[objId].transform.localScale = new Vector3(scaleObj.x, scaleObj.y, scaleObj.z);
                // ATTENTION ANGLES de BASE
                if((objectTypes[objId].name == "S") || (objectTypes[objId].name == "S"))
                {
                	objectTypes[objId].transform.eulerAngles = new Vector3(0f, 0f, -90f);
                }
                else if((objectTypes[objId].name == "T"))
                {
                	objectTypes[objId].transform.eulerAngles = new Vector3(180f, 0f, 0f);
                }
                else
                {
	                objectTypes[objId].transform.eulerAngles = new Vector3(0f, 0f, 0f);
                }

              //   if((objectTypes[objId].name == "2") || (objectTypes[objId].name == "2 (1)") )
              //   {
              //   	if(scales[objId] == scales[1])
              //   	{
              //   		posOrigin = new Vector3(-0.375f, 0.412f, 0.335f);
              //   		objectTypes[objId].transform.position = new Vector3(-0.375f, 0.412f, 0.335f);
              //   	}
                	
              //   }
              //   else if((objectTypes[objId].name == "UIST21"))
              //   {
              //   	posOrigin = new Vector3(-0.375f, 0.412f, 0.335f);
            		// objectTypes[objId].transform.position = new Vector3(-0.375f, 0.412f, 0.335f);
              //   }
              //   else
              //   {
        	    	// posOrigin = new Vector3(-0.25f, -0.4f, 0.335f);
              //   }

              //   if((objectTypes[objId].name == "UIST21"))
              //   {
              //   	posOrigin = new Vector3(-0.375f, 0.077f, 0.335f);
            		// objectTypes[objId].transform.position = new Vector3(-0.375f, 0.412f, 0.335f);
              //   }
              //   else
              //   {
        	    	// posOrigin = new Vector3(-0.25f, -0.4f, 0.335f);
              //   }
                // objectTypes[objId].transform.eulerAngles = new Vector3(0f, 0f, 0f);
                //CHANGE ORIENTATION
                // if(objId == 1)
                // {
                // 	objectTypes[objId].transform.eulerAngles = new Vector3(0f, -90f, 0f);
                // }
                // objectTypes[objId].transform.eulerAngles = new Vector3(0f, 0f, 0f);

                phantomObject[objId].SetActive(true);
                phantomObject[objId].transform.position = new Vector3(posPhantoms[taskNumber].x, posPhantoms[taskNumber].y, posPhantoms[taskNumber].z);
                phantomObject[objId].transform.localScale = new Vector3(scaleObj.x, scaleObj.y, scaleObj.z);


                //Vector3(posPhantoms[taskNumber].position.x, posPhantoms[taskNumber].position.y, posPhantoms[taskNumber].position.z);
                previousAngles = new Vector3(objectTypes[objId].transform.eulerAngles.x, objectTypes[objId].transform.eulerAngles.y, objectTypes[objId].transform.eulerAngles.z);
                for(int k = 0; k < objectTypes[objId].transform.childCount; k++)
				{
                    phantomObject[objId].transform.GetChild(k).gameObject.GetComponent<MeshRenderer>().material = phantomMaterialRed;
               		phantomObject[objId].transform.GetChild(k).gameObject.GetComponent<Collider>().enabled = false;

	                objectTypes[objId].transform.GetChild(k).gameObject.GetComponent<Renderer>().enabled = true;
	                phantomObject[objId].transform.GetChild(k).gameObject.GetComponent<Renderer>().enabled = true;
    			

    			}

    			previousAngles = new Vector3(objectTypes[objId].transform.eulerAngles.x, objectTypes[objId].transform.eulerAngles.y, objectTypes[objId].transform.eulerAngles.z);
                StartCoroutine(WaitForFirstGrasp());
                state = 1;
			break;

			case 1:
				correctBug = 0;
				// grasp = false;
				StartCoroutine(Consignes());
				// Debug.Log("State = 1");
				// resetButton.GetComponent<MeshRenderer>().material.color = Color.yellow;
				// nextButton.GetComponent<MeshRenderer>().material.color = Color.yellow;

				// objectTypes[objId].GetComponentInChildren<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
				if((GameObject.Find("OVRCustomHandPrefab_L") != null) && (GameObject.Find("OVRCustomHandPrefab_R") != null))
				{
					for(int i = 0; i < hands.Length; i++)
					{
						if((Mathf.Abs(hands[i].transform.position.x - resetButton.transform.position.x) < 0.07f) && (Mathf.Abs(hands[i].transform.position.y - resetButton.transform.position.y) < 0.07f) && (Mathf.Abs(hands[i].transform.position.z - resetButton.transform.position.z) < 0.07f))
						{
							// resetButton.GetComponent<MeshRenderer>().material.color = Color.green;
							objectTypes[objId].transform.position = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z);
							objectTypes[objId].transform.eulerAngles = new Vector3(previousAngles.x, previousAngles.y, previousAngles.z);
						}
					}

					for(int i = 0; i < hands.Length; i++)
					{
						if((Mathf.Abs(hands[i].transform.position.x - nextButton.transform.position.x) < 0.07f) && (Mathf.Abs(hands[i].transform.position.y - nextButton.transform.position.y) < 0.07f) && (Mathf.Abs(hands[i].transform.position.z - nextButton.transform.position.z) < 0.07f))
						{
							// nextButton.GetComponent<MeshRenderer>().material.color = Color.green;
							secondBug = secondBug + 1;
							if(secondBug == 1)
							{
								StartCoroutine(WaitingTask());
							}
						}
					}
				}
				if(Input.GetKeyDown(KeyCode.Space))
				{
					StartCoroutine(WaitForNext());
				}
				
				for(int k = 0; k < objectTypes[objId].transform.childCount; k++)
				{
                    phantomObject[objId].transform.GetChild(k).gameObject.GetComponent<MeshRenderer>().material = phantomMaterialRed;
    			
                    if((Mathf.Abs(objectTypes[objId].transform.position.x - phantomObject[objId].transform.position.x) < 0.07f) && (Mathf.Abs(objectTypes[objId].transform.position.y - phantomObject[objId].transform.position.y) < 0.07f) && (Mathf.Abs(objectTypes[objId].transform.position.z - phantomObject[objId].transform.position.z) < 0.07f))
					{
						phantomObject[objId].transform.GetChild(k).gameObject.GetComponent<MeshRenderer>().material = phantomMaterialGreen;
						
					}
    			}

				
				if((GameObject.Find("OVRCustomHandPrefab_L") != null) && (GameObject.Find("OVRCustomHandPrefab_R") != null))
				{
					if(grasp) 
					{
						secondBug = secondBug + 1;
						// for(int k = 0; k < objectTypes[objId].transform.childCount; k++)
						// {
						// 	objectTypes[objId].transform.GetChild(k).gameObject.GetComponent<ColorChangeKids>().collision = true;
						// 	if(secondBug == 1)
						// 	{
						// 		StartCoroutine(WaitingTask());
						// 	}
						// }
					}
					if(secondBug == 1)
					{
						StartCoroutine(WaitingTask());
					}
				}
				
			break;

			case 2:
				StartCoroutine(EndOfTheGame());
			break;

			case 3:
				correctBug = 0;
				secondBug = 0;
				grasp = false;
				Debug.Log("Just waiting for Coroutine to end...");
				Debug.Log("NbTouch: " + nbTouch + "; ConfigCount: " + configException.Count);

				for (int i = 2; i < otherwalls.Length; i++)
		    	{
			    	otherwalls[i].text = "Press Start Button to Get Started Again. " + "\n NbTouch: " + configException.Count;
		    	}
				if((GameObject.Find("OVRCustomHandPrefab_L") != null) && (GameObject.Find("OVRCustomHandPrefab_R") != null))
				{
					for(int i = 0; i < hands.Length; i++)
					{
						if((Mathf.Abs(hands[i].transform.position.x - startButton.transform.position.x) < 0.07f) && (Mathf.Abs(hands[i].transform.position.y - startButton.transform.position.y) < 0.07f) && (Mathf.Abs(hands[i].transform.position.z - startButton.transform.position.z) < 0.07f))
						{
							for (int j = 2; j < otherwalls.Length; j++)
					    	{
						    	otherwalls[j].text = "";
					    	}
					    	StartCoroutine(CollisionWait());
							state = -2;
						}
					}
				}

				if(Input.GetKeyDown(KeyCode.Space))
				{
					for (int j = 2; j < otherwalls.Length; j++)
			    	{
				    	otherwalls[j].text = "";
			    	}
			    	StartCoroutine(CollisionWait());
					state = -2;
				}
			break;
		}

		// ADD CYLINDER 90DEGRES?
    }

    IEnumerator Consignes()
    {
    	// yield return new WaitForSeconds(1);
    	for (int i = 2; i < otherwalls.Length; i++)
    	{
	    	otherwalls[i].text = taskToDo + " the WHITE " + objToCatch.gameObject.name + "\n into the RED " + objToCatch.gameObject.name + " until it becomes GREEN. \n Task #" + (configException.Count + tasks.Length*objectTypes.Length*scales.Length*nbBloc) + "/" + (tasks.Length*objectTypes.Length*scales.Length*nbBlocMax);
    	}
    	// yield return new WaitForSeconds(5);
    	yield return new WaitUntil (() => state == 3);
    }

    IEnumerator CollisionWait()
    {
    	for (int i = 2; i < otherwalls.Length; i++)
    	{
	    	otherwalls[i].text = "";
    	}
        // yield return new WaitForSeconds(1);
        yield return new WaitUntil (() => state == 1);
    }

    IEnumerator WaitForFirstGrasp()
    { // FOR ANALYSIS, WaitUntil Grasp and FreezeAll in the meantime;
    	// ATTENTION: GRAVITY OF OOI REMOVED IN COLORCHANGEKids.CS FOR ANALYSIS OF GRASP BEHAVIOUR
		// objectTypes[objId].GetComponentInChildren<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
		// yield return new WaitUntil(() => (objectTypes[objId].GetComponentInChildren<ColorChangeKids>().graspContact == true));
		for(int k = 0; k < objectTypes[objId].transform.childCount; k++)
		{
			//0:Hold, 1:Push: 2:Pull: 3:MoveOver; 4:Raise; 5:PsuhDown; 6:Fit; 7:Roll 
			if(declaredTasks[objId].nameTask == "Touch")//if(taskNumber == 0) // All
			{
				objectTypes[objId].transform.GetChild(k).gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
			}
			if(declaredTasks[objId].nameTask == "Push" || declaredTasks[objId].nameTask == "Pull")//if(taskNumber == 1 || taskNumber == 2)//Push/Pull -> FreeZ
			{
				objectTypes[objId].transform.GetChild(k).gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY & RigidbodyConstraints.FreezePositionX & RigidbodyConstraints.FreezeRotationX & RigidbodyConstraints.FreezeRotationY & RigidbodyConstraints.FreezeRotationZ;
			}
			if(declaredTasks[objId].nameTask == "Fit" || declaredTasks[objId].nameTask == "Move Over")//if(taskNumber == 3 || taskNumber == 6) // None
			{
				objectTypes[objId].transform.GetChild(k).gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;

			}
			if(declaredTasks[objId].nameTask == "Raise" || declaredTasks[objId].nameTask == "PushDown")//if(taskNumber == 4 || taskNumber == 5)//Raise/PushDown -> FreeY
			{
				objectTypes[objId].transform.GetChild(k).gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ & RigidbodyConstraints.FreezePositionX & RigidbodyConstraints.FreezeRotationX & RigidbodyConstraints.FreezeRotationY & RigidbodyConstraints.FreezeRotationZ;
			}
			if(declaredTasks[objId].nameTask == "Roll")//if(taskNumber == 7) // FreeX | RotZ | RotY
			{
				objectTypes[objId].transform.GetChild(k).gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ & RigidbodyConstraints.FreezePositionY & RigidbodyConstraints.FreezeRotationX;// | RigidbodyConstraints.FreezeRotationY;
			}
		}
		yield return new WaitUntil(() => (grasp == true)); //(objectTypes[objId].GetComponentInChildren<ColorChangeKids>().graspContact == true));
	}
    	

    IEnumerator WaitForNext()
    {
    	// MAYBE ADD NEED TO GO START FOR NEXT STEP
    	Debug.Log("Coroutine");
    	// yield return new WaitForSeconds(1);
    	// state = 3;
    	yield return new WaitForSeconds(3); // NORMALLY 1 SEC (prior 17feb)
    	correctBug = correctBug + 1; // This instead of having state = 3 in coroutine.
    	for(int k = 0; k < objectTypes[objId].transform.childCount; k++)
		{
	    	objectTypes[objId].transform.GetChild(k).gameObject.GetComponent<Renderer>().enabled = false;
			phantomObject[objId].transform.GetChild(k).gameObject.GetComponent<Renderer>().enabled = false;
			objectTypes[objId].transform.GetChild(k).gameObject.GetComponent<ColorChangeKids>().collision = false;
			graspOne[k] = false;
		}
        objectTypes[objId].transform.position = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z);
    }

    IEnumerator WaitingTask()
    {
    	// CAN INCREASE HERE AND HAVE PEOPLE TOUCH WITHIN 5SECS INSTEAD!
    	yield return new WaitForSeconds(2);
		StartCoroutine(WaitForNext());
    }

    IEnumerator EndOfTheGame()
    {
    	for(int i = 0; i < objectTypes.Length; i++)
    	{
	    	objectTypes[i].SetActive(false);
	    	phantomObject[i].SetActive(false);
    	}
    	for (int i = 2; i < otherwalls.Length; i++)
    	{
	    	otherwalls[i].text = "The Game is Over. Thank you!";
    	}

    	Debug.Log("Fin du Game!");
    	yield return new WaitForSeconds(2);
    	if(realGame)
    	{
	    	Application.Quit();
    	}
    	else
    	{
	    	Debug.Break();
    	}
    }
}

