﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class ColorChangeKids : MonoBehaviour
{
	public GameObject rightHand, leftHand;
	private float distanceL, distanceR;

	// private float[] distancePerCapsL, distancePerCapsR;
	private Vector3[] closestPointPerVertexHandLeft, closestPointPerVertexHandRight;

	public float distance;
	// private Mesh thisMesh;
	private Vector3[] thisVertices, closestPointPerVertexRight, closestPointPerVertexLeft;
	// private Color[] colors;
	private Vector3 worldVertex;

	public bool graspContact, collision;

	private Collider handCollidersRight, handCollidersLeft;
	// private Collider[] handCollidersRight, handCollidersLeft;

	// private Shader shader;

	public IList<OVRBoneCapsule> Capsules { get; private set; }
	public IList<OVRBoneCapsule> CapsulesR { get; private set; }

	public InstantiateInChildren gameManager;

	// private Vector3[] closestPointR, closestPointL, closest_distanceL, closest_distanceR;

    void Start()
    {
    	leftHand = GameObject.Find("OVRCustomHandPrefab_L");
    	rightHand = GameObject.Find("OVRCustomHandPrefab_R");
    	gameManager = GameObject.Find("GameManager").GetComponent<InstantiateInChildren>();
		// shader = Shader.Find("Legacy Shaders/Particles/VertexLit Blended");
		// this.GetComponent<MeshRenderer>().material.shader = shader;
		// thisMesh = this.GetComponent<MeshFilter>().mesh;
		thisVertices = this.GetComponent<MeshFilter>().mesh.vertices;	
		// colors = new Color[thisVertices.Length];
		closestPointPerVertexRight = new Vector3[thisVertices.Length];	
		closestPointPerVertexLeft = new Vector3[thisVertices.Length];
		graspContact = false;

		Capsules = GameObject.Find("OVRCustomHandPrefab_L").GetComponent<OVRCustomSkeleton>().Capsules;
		CapsulesR = GameObject.Find("OVRCustomHandPrefab_R").GetComponent<OVRCustomSkeleton>().Capsules;
    	
    	// closestPointR = new Vector3[19];
    	// closestPointL = new Vector3[19];
    	// closest_distanceL = new Vector3[19];
    	// closest_distanceR = new Vector3[19];


    }

    void Update()
    {
    	collision = false;
    	graspContact = false;
    	this.transform.parent.transform.SetParent(GameObject.Find("ObjectsOfInterest").transform);
    	this.GetComponent<Rigidbody>().isKinematic = false;
		this.GetComponent<Rigidbody>().useGravity = false;
		// this.GetComponent<Rigidbody>().useGravity = true;

		
		for(int j = 0; j < leftHand.GetComponentsInChildren<Collider>().Length; j++)
    	{
    		handCollidersRight= rightHand.GetComponentsInChildren<Collider>()[j];
    		handCollidersLeft = leftHand.GetComponentsInChildren<Collider>()[j];
    		// handCollidersRight[j] = rightHand.GetComponentsInChildren<Collider>()[j];
    		// handCollidersLeft[j] = leftHand.GetComponentsInChildren<Collider>()[j];
    	}
		if((GameObject.Find("OVRCustomHandPrefab_L") != null) && (GameObject.Find("OVRCustomHandPrefab_R") != null))
		{

// DID NOT GRASP
			// closestPointR[j] = handCollidersRight[j].ClosestPointOnBounds(this.transform.position);
			// closestPointL[j] = handCollidersLeft[j].ClosestPointOnBounds(this.transform.position);

   //      	closest_distanceR[j] = handCollidersRight[j].ClosestPoint(closestPointR[j]);
   //      	closest_distanceL[j] = handCollidersLeft[j].ClosestPoint(closestPointL[j]);

	  //       distanceR = Vector3.Distance(closest_distanceR[j], this.transform.position);
	  //       distanceL = Vector3.Distance(closest_distanceL[j], this.transform.position);

			for(int k = 0; k < thisVertices.Length; k++)
        	{
	        	worldVertex = transform.TransformPoint(thisVertices[k]);
		  
		        closestPointPerVertexRight[k] = handCollidersRight.ClosestPoint(worldVertex);
				closestPointPerVertexLeft[k] = handCollidersLeft.ClosestPoint(worldVertex);
				distanceL = Vector3.Distance(closestPointPerVertexLeft[k], worldVertex);
				distanceR = Vector3.Distance(closestPointPerVertexRight[k], worldVertex);

				if(distanceL < distanceR)
				{
					distance = distanceL;

				}
				else
				{
					distance = distanceR;
				}

				// if(distance >= 0.15f)
				// {
				// 	colors[k] = Color.Lerp(Color.white, Color.yellow, (1-distance));
				// }
				// else
				// {
				// 	colors[k] = Color.Lerp(Color.yellow, Color.red, (1-distance));
				// }
				
				
					

				if((distance < 0.02f) && (gameManager.state == 1)) // 0.01
				{
					graspContact = true;
					
					// IMPORTANT: PUT THAT BACK IN FOR OTHER INTERACTIONS!!!!
					this.GetComponent<Rigidbody>().useGravity = false;

					if(distance == distanceR)
					{
						this.transform.parent.transform.SetParent(rightHand.transform);
					}
					else
					{
						this.transform.parent.transform.SetParent(leftHand.transform);
					}
				}

			}
			
        }
    }
}
