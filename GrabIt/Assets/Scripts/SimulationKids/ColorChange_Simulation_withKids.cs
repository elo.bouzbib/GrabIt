﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class ColorChange_Simulation_withKids : MonoBehaviour
{
	public GameObject rightHand, leftHand;
	private float distanceL, distanceR;

	private float[] distancePerCapsL, distancePerCapsR;
	private Vector3[] closestPointPerVertexHandLeft, closestPointPerVertexHandRight;

	public float distance;
	// private Mesh thisMesh;
	private Vector3[] thisVertices, closestPointPerVertexRight, closestPointPerVertexLeft;
	// private Color[] colors;
	private Vector3 worldVertex;

	public bool graspContact;

	private Collider handCollidersRight, handCollidersLeft;
	// private Shader shader;

	public IList<OVRBoneCapsule> Capsules { get; private set; }
	public IList<OVRBoneCapsule> CapsulesR { get; private set; }

	public ClosestToHand_withKids[] closestToHand;

	public int numberVertex;


    void Start()
    {
		// shader = Shader.Find("Legacy Shaders/Particles/VertexLit Blended");
		// this.GetComponent<MeshRenderer>().material.shader = shader;
		// thisMesh = this.GetComponent<MeshFilter>().mesh;
		thisVertices = this.GetComponent<MeshFilter>().mesh.vertices;	
		// colors = new Color[thisVertices.Length];
		closestPointPerVertexRight = new Vector3[thisVertices.Length];	
		closestPointPerVertexLeft = new Vector3[thisVertices.Length];
		graspContact = false;

		rightHand = GameObject.Find("RightHandColl");
		leftHand = GameObject.Find("LeftHandColl");
		Capsules = GameObject.Find("OVRCustomHandPrefab_L").GetComponent<OVRCustomSkeleton>().Capsules;
		CapsulesR = GameObject.Find("OVRCustomHandPrefab_R").GetComponent<OVRCustomSkeleton>().Capsules;

		closestToHand = FindObjectsOfType<ClosestToHand_withKids>();
		numberVertex = 0;
    }

    void Update()
    {

    	graspContact = false;
    	this.transform.parent.transform.SetParent(GameObject.Find("ObjectsOfInterest").transform);
    	this.GetComponent<Rigidbody>().isKinematic = false;
		this.GetComponent<Rigidbody>().useGravity = false;
		this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;

    	for(int j = 0; j < leftHand.GetComponentsInChildren<Collider>().Length; j++)
    	{
    		handCollidersRight = rightHand.GetComponentsInChildren<Collider>()[j];
    		handCollidersLeft = leftHand.GetComponentsInChildren<Collider>()[j];
    	}
  //   	numberVertex = thisVertices.Length;
  //       for(int k = 0; k < thisVertices.Length; k++)
  //       {
  //       	worldVertex = transform.TransformPoint(thisVertices[k]);
		// 	closestPointPerVertexRight[k] = handCollidersRight.ClosestPoint(worldVertex);
		// 	closestPointPerVertexLeft[k] = handCollidersLeft.ClosestPoint(worldVertex);
			
		// 	distanceL = Vector3.Distance(closestPointPerVertexLeft[k], worldVertex);
		// 	distanceR = Vector3.Distance(closestPointPerVertexRight[k], worldVertex);

		// 	if(distanceL < distanceR)
		// 	{
		// 		distance = distanceL;
		// 	}
		// 	else
		// 	{
		// 		distance = distanceR;
		// 	}

		// 	if(distance < 0.02f)
		// 	{
		// 		graspContact = true;
		// 	}

		// 	// for(int i = 0; i < closestToHand.Length; i++)
		// 	// {
		// 	// 	if(closestToHand[i].REAL_MIN_DISTANCE < 0.05f)
		// 	// 	{
		// 	// 		// graspContact = true;
		// 	// 	}

		// 	// }
		// // thisMesh.colors = colors;
		// }
		for(int i = 0; i < closestToHand.Length; i++)
		{
			if(closestToHand[i].REAL_MIN_DISTANCE < 0.02f)
			{
				graspContact = true; // CHANGE TO
			}
		}
	}

}

