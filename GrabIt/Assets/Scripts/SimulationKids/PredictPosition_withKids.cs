﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PredictPosition_withKids : MonoBehaviour
{
	private GameObject handR, handL;
	// public GameObject skeletonR;

    [HideInInspector]
    public Vector3[] oldPos;
    [HideInInspector]
	public bool recordNow;


	public int slidingWindow = 45;
	private int j;

    [HideInInspector]
    public ClosestToHand_withKids[] closest;
    private Vector3[] toClosestPoint;
    [HideInInspector]
    public Vector3[] onGoingVector; 
    [HideInInspector]
    public float[] onGoingAngle, error, errorThumb, errorIndex, errorMiddle;
    [HideInInspector]
    public GameObject[] handChildR, handChildL;
    [HideInInspector]
    public Vector3[] projection;

    private GameObject OOI;
    
    public Vector3 virtualFinger;
    [HideInInspector]
    public float projVFX, projVFY, projVFZ;

    private GameObject palm, thumb, index;
    [HideInInspector]
    public SimulationHand readFromData;

    public bool[] inZone, contactPerPhal, inZone_fromPlane;//, inZone_fromProbsPred;
    [HideInInspector]
    public Vector3[] errorPredict;
    [HideInInspector]
    public GameObject[] zone;
    [HideInInspector]
    public Vector3[] posToPredict;

    // [HideInInspector]
    // public AlgoHints algoHint;

    public bool oneContact, clampGrip;
    // public string[] touchedBy, probsTouchedBy, touchByPlane;
    public List<string> gonnaTouch;
    private int nbKids;

    void Start()
    {
    	handR = GameObject.Find("RightHandColl");
    	handL = GameObject.Find("LeftHandColl");
    	// childR = new GameObject[19];
    	handChildR = new GameObject[19];
    	// childL = new GameObject[19];
    	handChildL = new GameObject[19];
    	// skeletonR = GameObject.Find("OVRCustomHandPrefab_R");
        closest = new ClosestToHand_withKids[19];
        toClosestPoint = new Vector3[19];
        onGoingVector = new Vector3[19];
        // onGoingAngle = new float[19];
        error = new float[19];

        readFromData = FindObjectOfType<SimulationHand>();

        for(int i = 0; i < 19; i++)
        {
        	handChildR[i] = handR.transform.GetChild(i).transform.gameObject;
        	handChildL[i] = handL.transform.GetChild(i).transform.gameObject;
            closest[i] = handChildR[i].GetComponent<ClosestToHand_withKids>();
        }

		oldPos = new Vector3[19];
        projection = new Vector3[19];

        for(int i = 0; i < 19; i++)
        {
            oldPos[i] = handChildR[i].transform.position;
        }
		j = 0;

        OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");

        // virtualFinger = handChildR[9].transform.position - handChildR[6].transform.position;
        
        palm = GameObject.Find("r_palm_center_marker");
        index = GameObject.Find("r_index_finger_pad_marker");
        thumb = GameObject.Find("r_thumb_finger_pad_marker");
        
        virtualFinger = (index.transform.position - thumb.transform.position);
		projVFX = Vector3.Project(virtualFinger, Vector3.right).x;
        projVFY = Vector3.Project(virtualFinger, Vector3.up).y;
        projVFZ = Vector3.Project(virtualFinger, Vector3.forward).z;


        posToPredict = new Vector3[19];
        
        // inZone_fromProbsPred = new bool[6];

        errorPredict = new Vector3[19];
        contactPerPhal = new bool[19];

        // algoHint = FindObjectOfType<AlgoHints>();
		zone = new GameObject[OOI.transform.childCount];
        inZone = new bool[OOI.transform.childCount];

        for(int k = 0; k < OOI.transform.childCount; k++)
        {
            zone[k] = OOI.transform.GetChild(k).gameObject;
    		inZone_fromPlane = new bool[OOI.transform.childCount];
            inZone[k] = false;
            inZone_fromPlane[k] = false;
            // inZone_fromProbsPred[k] = false;
        }
        oneContact = false;
        clampGrip = false;

        // touchedBy = new string[6];
        // probsTouchedBy = new string[6];
        // touchByPlane = new string[6];
        gonnaTouch = new List<string>();
        nbKids = OOI.transform.childCount;
    }

    void Update()
    {
        recordNow = false;
    	StartCoroutine(RecordPos());

        oneContact = false;
        clampGrip = false;

        OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");
        nbKids = OOI.transform.childCount;
        zone = new GameObject[nbKids];
        inZone = new bool[nbKids];
		inZone_fromPlane = new bool[nbKids];

		// Debug.Log("CHILDCOUNT " + nbKids);
        for(int k = 0; k < nbKids; k++)
        {
            zone[k] = OOI.transform.GetChild(k).gameObject;
            inZone[k] = false;
            inZone_fromPlane[k] = false;
            // inZone_fromProbsPred[k] = false;
        }

        for(int i = 0; i < 19; i++)
        {
            toClosestPoint[i] = -closest[i].closest_distance + handChildR[i].transform.position; // WHITE RAY

            onGoingVector[i] = handChildR[i].transform.position - oldPos[i]; // RED RAY
            // onGoingAngle[i] = Vector3.Angle(-toClosestPoint[i], onGoingVector[i]);

            // PROJECTION WHITE ONTO RED: white.magnitude * red.magnitude * cos(angle) * direction(red)
            projection[i] = handChildR[i].transform.position + Vector3.Project(-toClosestPoint[i], onGoingVector[i]);
            // error[i] = Vector3.Distance(closest[i].closest_distance, projection[i]);
        	// Debug.DrawRay(handChildR[i].transform.position, onGoingVector[i], Color.red);
            
            posToPredict[i] = readFromData.alpha*projection[i] + (1-readFromData.alpha)*closest[i].closest_distance;
            contactPerPhal[i] = closest[i].phalanxContact;

            errorPredict[i] = posToPredict[i] - handChildR[i].transform.position;
            for(int k = 0; k < zone.Length; k++)
            {
                if(zone[k].GetComponent<Collider>().bounds.Contains(posToPredict[i]))
                {
                    // ALL CHILDREN's COLLIDER -> JENGA/COLOR FACE / "UIST21!""
                    inZone[k] = true;
                    zone[k].GetComponent<MeshRenderer>().material.color = Color.green;
                    // touchedBy[k] = handChildR[i].name;
                    if(!gonnaTouch.Contains(handChildR[i].name))
                    {
                        gonnaTouch.Add(handChildR[i].name);
                    }
                    

                    // SI LA LISTE NE CONTIENT PAS LE NOM, AJOUTER LE NOM, PUIS COMPTER NB Future contact Points
                }
                else
                {
                    // touchedBy[k] = " ";
                    gonnaTouch.Remove(handChildR[i].name);
                    // zone[k].GetComponent<MeshRenderer>().material.color = Color.white;
                    // FLUSH THE LIST
                }

                inZone_fromPlane[k] = zone[k].GetComponent<CollideCutSections>().collidingCutSections;
            	if(inZone_fromPlane[k])
            	{
            		zone[k].GetComponent<MeshRenderer>().material.color = Color.cyan;
            	}

            	if((!inZone_fromPlane[k]) && (!zone[k].GetComponent<Collider>().bounds.Contains(posToPredict[i])))
            	{
                    zone[k].GetComponent<MeshRenderer>().material.color = Color.white;
            	}
            }
        }
        
        j = j+1;
    	if(j%slidingWindow == 0)
    	{
    		recordNow = true;
    	}
    	
        palm = GameObject.Find("r_palm_center_marker");
        index = GameObject.Find("r_index_finger_pad_marker");
        thumb = GameObject.Find("r_thumb_finger_pad_marker");
        virtualFinger = (index.transform.position - thumb.transform.position);

        if(Mathf.Abs(Vector3.Dot(thumb.transform.right.normalized, palm.transform.right.normalized)) > 0.70f)
        {
            if(Mathf.Abs(Vector3.Dot(thumb.transform.right.normalized, index.transform.right.normalized)) > 0.85f)
            {
                oneContact = true;
            }
            else
            {
                clampGrip = true;
            }

        }

        projVFX = Vector3.Project(virtualFinger, Vector3.right).x;
        projVFY = Vector3.Project(virtualFinger, Vector3.up).y;
        projVFZ = Vector3.Project(virtualFinger, Vector3.forward).z;
    }

    IEnumerator RecordPos()
    {
    	yield return new WaitUntil (() => recordNow == true);
    	for(int i = 0; i < 19; i++)
        {
            oldPos[i] = handChildR[i].transform.position;

        }

    }

    // void OnDrawGizmos()
    // {
    // 	for(int i = 0; i < 19; i++)
    //     {	
    // 		Gizmos.DrawWireSphere(projection[i], 0.01f);
    // 	}
    // }
}

