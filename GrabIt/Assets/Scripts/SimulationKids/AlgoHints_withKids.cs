﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class AlgoHints_withKids : MonoBehaviour
{
  	private GameObject[] childR, childL, handChildL, handChildR; 
  	private GameObject handR, handL;
    [HideInInspector]
    public ClosestToHand_withKids[] closest;
    private Vector3[] onGoingVector; 
    [HideInInspector]
    public PredictPosition_withKids predictPos;

    // private GameObject OOI;
    [HideInInspector]
    public SimulationHand readFromData;
    [HideInInspector]
    public Vector3[] projPhalOnPlanes, errorPerPhal;
    [HideInInspector]
    public Vector3 virtualFinger;
    private Vector3 closestDist;
    private GameObject thumb, palm, index;
    public Vector3 probsA, probsB, debugProbsA, debugProbsB;
    [HideInInspector]
    public Vector3[] probsP, probsQ, probsR;
    [HideInInspector]
    public int nbOfPlanes;
    [HideInInspector]
    public GameObject[] plane;
    
    private string[] name;

    void Start()
    {

    	predictPos = FindObjectOfType<PredictPosition_withKids>();
    	readFromData = FindObjectOfType<SimulationHand>();
      	handR = GameObject.Find("RightHandColl");
    	handL = GameObject.Find("LeftHandColl");
    	childR = new GameObject[19];
    	handChildR = new GameObject[19];
    	childL = new GameObject[19];
    	handChildL = new GameObject[19];
	    closest = new ClosestToHand_withKids[19];
	    onGoingVector = new Vector3[19];

		// planes = GameObject.Find("Planes");
		plane = new GameObject[3];
		nbOfPlanes = GameObject.FindGameObjectWithTag("CutSections").GetComponent<CollisionPlanes_withKids>().nbOfPlanes;

		for(int i = 0; i < GameObject.FindGameObjectsWithTag("CutSections").Length; i++)
		{
			plane[i] = GameObject.FindGameObjectsWithTag("CutSections")[i];
		}

		probsB = new Vector3();
		probsA = new Vector3();

		probsP = new Vector3[19];
		probsQ = new Vector3[19];
		probsR = new Vector3[19];
		errorPerPhal = new Vector3[19*nbOfPlanes];

		debugProbsB = new Vector3();
		debugProbsA = new Vector3();

		for(int i = 0; i < 19; i++)
		{
			childR[i] = this.transform.GetChild(i).transform.gameObject;
			handChildR[i] = handR.transform.GetChild(i).transform.gameObject;
			childL[i] = this.transform.GetChild(i+19).transform.gameObject;
			handChildL[i] = handL.transform.GetChild(i).transform.gameObject;
			closest[i] = handChildR[i].GetComponent<ClosestToHand_withKids>();
		}

		// OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");

		palm = GameObject.Find("r_palm_center_marker");
        index = GameObject.Find("r_index_finger_pad_marker");
        thumb = GameObject.Find("r_thumb_finger_pad_marker");
        name = new string[19];
	}

    void FixedUpdate()
    {
        // SETUP 20 FPS - 0.05 // 
		Time.fixedDeltaTime = 0.05f;
		for(int i = 0; i < GameObject.FindGameObjectsWithTag("CutSections").Length; i++)
		{
			plane[i] = GameObject.FindGameObjectsWithTag("CutSections")[i];
		}

		// OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");

    	palm = GameObject.Find("r_palm_center_marker");
        index = GameObject.Find("r_index_finger_pad_marker");
        thumb = GameObject.Find("r_thumb_finger_pad_marker");
        // closestFromVF = whosClosest(handChildR[6].transform.position + (handChildR[9].transform.position - handChildR[6].transform.position)/2);

    	// NEED TO CREATE PLANE, from l to this point. GetContactPoints of VFMagnitude
    	// virtualFinger = (handChildR[9].transform.position - handChildR[6].transform.position);

    	virtualFinger = (index.transform.position - thumb.transform.position);
    	// closestFromVF = whosClosest(thumb.transform.position + virtualFinger/2);

    	// Vector3 lengthToPalm = (handChildR[6].transform.position + virtualFinger/2) - handChildR[0].transform.position;
    	
    	Vector3 lengthToPalm = (thumb.transform.position + virtualFinger/2) - palm.transform.position;
    	
        for(int i = 0; i < 19; i++)
        {
			childR[i] = this.transform.GetChild(i).transform.gameObject;
			childL[i] = this.transform.GetChild(i+19).transform.gameObject;

            childR[i].transform.position = GameObject.Find("OVRCustomHandPrefab_R").GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position;
            childR[i].transform.eulerAngles = GameObject.Find("OVRCustomHandPrefab_R").GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles;
            
            childR[i].name = GameObject.Find("OVRCustomHandPrefab_R").GetComponent<OVRCustomSkeleton>().CustomBones[i].name;
            childL[i].name = GameObject.Find("OVRCustomHandPrefab_L").GetComponent<OVRCustomSkeleton>().CustomBones[i].name;
            
            childL[i].transform.position = GameObject.Find("OVRCustomHandPrefab_L").GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position;            
            childL[i].transform.eulerAngles = GameObject.Find("OVRCustomHandPrefab_L").GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles;            
        }

        for(int k = 0; k < 19; k++)
        {
            for(int m = 0; m < 19; m++)
            {
                if(m != k)
                {
                    if( (Mathf.Abs(Vector3.Distance(plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhal[m], plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[m])) < 0.05f) && (Mathf.Abs(Vector3.Distance(plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhal[k], plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k])) < 0.05f ) )
                    {
                        if( (Mathf.Abs( Vector3.Distance(plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[m], plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k]) - (plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhal[m] - plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhal[k]).magnitude) < 0.5f*(plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhal[m] - plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhal[k]).magnitude ))
                        {
                            probsP[k] = plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k];
                            errorPerPhal[k] = probsP[k] - childR[k].transform.position;
                            name[k] = this.transform.GetChild(k).gameObject.name;
                        }
                        else
                        {
                            // probsP[k] = Vector3.zero;
                            // errorPerPhal[k] = Vector3.zero;
                            name[k] = "";
                        }

                    }
                    if( (Mathf.Abs(Vector3.Distance(plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhal[m], plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[m])) < 0.05f) && (Mathf.Abs(Vector3.Distance(plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhal[k], plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k])) < 0.05f ) )
                    {
                        if( (Mathf.Abs( Vector3.Distance(plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[m], plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k]) - (plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhal[m] - plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhal[k]).magnitude) < 0.5f*(plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhal[m] - plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhal[k]).magnitude ))
                        {
                            probsQ[k] = plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k];
                            errorPerPhal[k*2] = probsP[k] - childR[k].transform.position;
                        }
                        else
                        {
                            // probsP[k] = Vector3.zero;
                            // errorPerPhal[k] = Vector3.zero;
                        }

                    }
                    if( (Mathf.Abs(Vector3.Distance(plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhal[m], plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[m])) < 0.05f) && (Mathf.Abs(Vector3.Distance(plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhal[k], plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k])) < 0.05f ) )
                    {
                        if( (Mathf.Abs( Vector3.Distance(plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[m], plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k]) - (plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhal[m] - plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhal[k]).magnitude) < 0.5f*(plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhal[m] - plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhal[k]).magnitude ))
                        {
                            probsR[k] = plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k];
                            errorPerPhal[k*3] = probsP[k] - childR[k].transform.position;
                        }
                        else
                        {
                            // probsP[k] = Vector3.zero;
                            // errorPerPhal[k] = Vector3.zero;
                        }

                    }
                }
            }
        }

        if( (Mathf.Abs(Vector3.Distance(plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhal[5], plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[5])) < 0.05f) && (Mathf.Abs(Vector3.Distance(plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhal[8], plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[8])) < 0.05f) )
        {
            debugProbsA = plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhal[5];
            debugProbsB = plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhal[8];
            if((Mathf.Abs(Vector3.Distance(plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[5], plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[8]) - virtualFinger.magnitude) < 0.5f*virtualFinger.magnitude) )
            {
                probsA = plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[5];
                probsB = plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[8];
            }
            else
            {
                // probsA = Vector3.zero;
                // probsB = Vector3.zero;
            }
        }
        else
        {
            // debugProbsA = Vector3.zero;
            // debugProbsB = Vector3.zero;
        }
    }

    public void OnDrawGizmos()
    {
        GUIStyle style = new GUIStyle();
        style.normal.textColor = Color.blue; 
        // Gizmos.DrawWireSphere(closest_distance, 0.005f);
        // for(int k = 0; k < 19; k++)
        // {
        //     // Handles.Label(probsP[k], name[k], style);
        //     Gizmos.color = Color.blue;
        //     Gizmos.DrawSphere(probsP[k], 0.005f);
        // }
        
    }
}
