﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollideCutSections_inKids : MonoBehaviour
{
    // public bool[] collidingCutSections;
    // public bool[] collidingCutSectionsLeft;

    [HideInInspector]
    public string[] name, mesh;
    [HideInInspector]
    public float[] scale;
    [HideInInspector]
    public Vector3[] scaleC;
    [HideInInspector]
    public PredictionStop predictStop;
    [HideInInspector]
    public PredictionStopL predictStopL;

    public GameObject[] spheres, warpSpheres;
    [HideInInspector]
    public RedirectMe_Sim_Index warp;
    [HideInInspector]
    public GameObject warpedRightHand, realRightHand;
    [HideInInspector]
    public GameObject warpedLeftHand, realLeftHand;
    [HideInInspector]
    public GameObject[] warpPhal, realPhal;


    void Start()
    {
        predictStop = GameObject.FindObjectOfType<PredictionStop>().GetComponent<PredictionStop>();
        predictStopL = GameObject.FindObjectOfType<PredictionStopL>().GetComponent<PredictionStopL>();

    	// collidingCutSections = new bool[this.transform.childCount];
     //    collidingCutSectionsLeft = new bool[this.transform.childCount];
        if(GameObject.FindObjectOfType<RedirectMe_Sim_Index>() != null)
        {
            warp = GameObject.FindObjectOfType<RedirectMe_Sim_Index>();
            realRightHand = GameObject.Find("RightHandColl");
            warpedRightHand = GameObject.Find("WarpedRightHandColl");
            realLeftHand = GameObject.Find("LeftHandColl");
            warpedLeftHand = GameObject.Find("WarpedLeftHandColl");

            spheres = new GameObject[38];
            warpSpheres = new GameObject[38];
            warpPhal = new GameObject[38];
            realPhal = new GameObject[38];

            scaleC = new Vector3[38];

            for(int i = 0; i < 38; i++)
            {
                spheres[i] = GameObject.Find("Projection ("+i.ToString()+")");
                // spheres[i+19] = GameObject.Find("Projection ("+(i+19).ToString()+")");

                warpSpheres[i] = GameObject.Find("WarpProjection ("+i.ToString()+")");
                // warpSpheres[i+19] = GameObject.Find("WarpProjection  ("+(i+19).ToString()+")");

            }
            for(int i = 0; i < 19; i++)
            {
                warpPhal[i] = warpedRightHand.transform.GetChild(i).transform.gameObject;
                warpPhal[i+19] = warpedLeftHand.transform.GetChild(i).transform.gameObject;

                realPhal[i] = realRightHand.transform.GetChild(i).transform.gameObject;
                realPhal[i+19] = realLeftHand.transform.GetChild(i).transform.gameObject;


            }
        }
    	for(int k = 0; k < this.transform.childCount; k++)
    	{
    		if(this.transform.GetChild(k).gameObject.GetComponent<Rigidbody>() == null)
    		{
    			this.transform.GetChild(k).gameObject.AddComponent<Rigidbody>();
    			this.transform.GetChild(k).gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
    			this.transform.GetChild(k).gameObject.GetComponent<Rigidbody>().isKinematic = false;
    			this.transform.GetChild(k).gameObject.GetComponent<Rigidbody>().useGravity = false;
    		}
            else
            {
                this.transform.GetChild(k).gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                this.transform.GetChild(k).gameObject.GetComponent<Rigidbody>().isKinematic = false;
                this.transform.GetChild(k).gameObject.GetComponent<Rigidbody>().useGravity = false;
            }

    	}

        name = new string[38];
        mesh = new string[38];
        scale = new float[38];
        
    }

    // Update is called once per frame
    void Update()
    {

        name = new string[38];
        mesh = new string[38];
        scale = new float[38];


        for(int k = 0; k < this.transform.childCount; k++)
        {   

            if(GameObject.FindObjectOfType<RedirectMe_Sim_Index>() != null)
            {
                 // warpedRightHand.SetActive(true);
                warpedRightHand.transform.parent = null;
                realRightHand.transform.parent = null;
                warpedRightHand.transform.localPosition = realRightHand.transform.localPosition;
                scaleC = new Vector3[38];

                for(int i = 0; i < 19; i++)
                {
                    spheres[i].transform.parent = GameObject.Find("Projections").transform;
                    spheres[i+19].transform.parent = GameObject.Find("Projections").transform;
                    warpSpheres[i].transform.parent = GameObject.Find("WarpProjections").transform;
                    warpSpheres[i+19].transform.parent = GameObject.Find("WarpProjections").transform;
                    
                    warpPhal[i] = warpedRightHand.transform.GetChild(i).transform.gameObject;
                    warpPhal[i+19] = warpedLeftHand.transform.GetChild(i).transform.gameObject;

                    realPhal[i] = realRightHand.transform.GetChild(i).transform.gameObject;
                    realPhal[i+19] = realLeftHand.transform.GetChild(i).transform.gameObject;


                    if(realRightHand.transform.parent == null)
                    {
                        warpedRightHand.transform.parent = null;
                        warpedRightHand.transform.localPosition = realRightHand.transform.localPosition;

                    }
                
                    if(this.transform.GetChild(k).gameObject.GetComponent<Collider>().bounds.Contains(spheres[i].transform.position))
                    {
                        name[i] = this.transform.GetChild(k).name;
                        spheres[i].transform.parent = this.transform.GetChild(k).gameObject.transform;
                        // this.transform.GetChild(k).gameObject.GetComponent<MeshRenderer>().material.color = Color.green;
                        
                        scaleC[i] = new Vector3(this.transform.GetChild(k).gameObject.transform.localScale.x, this.transform.GetChild(k).gameObject.transform.localScale.y, this.transform.GetChild(k).gameObject.transform.localScale.z);
                        // scale[i] = (100*Vector3.Scale(this.transform.GetChild(k).transform.localScale, this.transform.localScale).x * 100*Vector3.Scale(this.transform.GetChild(k).transform.localScale, this.transform.localScale).y * 100*Vector3.Scale(this.transform.GetChild(k).transform.localScale, this.transform.localScale).z);
                        // mesh[i] = this.transform.GetChild(k).gameObject.GetComponent<MeshFilter>().mesh.name;
                        // scale[i] = this.transform.GetChild(k).transform.localScale.magnitude;
                        
                        if(GameObject.FindObjectOfType<RedirectMe_Sim_Index>() != null)
                        {
                            warpSpheres[i].transform.parent = warp.targetToTouch[i].gameObject.transform;
                            Vector3 ratio = new Vector3(warp.targetToTouch[i].transform.localScale.x/scaleC[i].x, warp.targetToTouch[i].transform.localScale.y/scaleC[i].y, warp.targetToTouch[i].transform.localScale.z/scaleC[i].z);
                            warpSpheres[i].transform.localPosition = new Vector3(spheres[i].transform.localPosition.x*ratio.x, spheres[i].transform.localPosition.y*ratio.y, spheres[i].transform.localPosition.z*ratio.z);
                            
                            Vector3 ratioM = new Vector3(this.transform.GetChild(k).transform.localScale.x/warp.targetToTouch[i].transform.localScale.x, this.transform.GetChild(k).transform.localScale.y/warp.targetToTouch[i].transform.localScale.y, this.transform.GetChild(k).transform.localScale.z/warp.targetToTouch[i].transform.localScale.z);
                            
                            if(warp.startWarp)
                            {
                                warpedRightHand.transform.parent = this.transform.GetChild(k).gameObject.transform;
                                realRightHand.transform.parent = warp.targetToTouch[i].gameObject.transform;

                                warpedRightHand.transform.localPosition = Vector3.Scale(realRightHand.transform.localPosition, ratio);
                                warpPhal[i].transform.localPosition = Vector3.Scale(realPhal[i].transform.localPosition, ratio);
                                Debug.Log(realRightHand.transform.localPosition + ";" + warpedRightHand.transform.localPosition);
                            }
                            else
                            {
                                Debug.Log("GET OUT");
                                warpedRightHand.transform.parent = null;
                                realRightHand.transform.parent = null;
                                warpedRightHand.transform.position = realRightHand.transform.position;

                            }
                            
                            
                        }
                    }

                    if(this.transform.GetChild(k).gameObject.GetComponent<Collider>().bounds.Contains(spheres[i+19].transform.position))
                    {
                        name[i+19] = this.transform.GetChild(k).name;
                        spheres[i+19].transform.parent = this.transform.GetChild(k).gameObject.transform;
                        // this.transform.GetChild(k).gameObject.GetComponent<MeshRenderer>().material.color = Color.magenta;

                        scaleC[i+19] = new Vector3(this.transform.GetChild(k).gameObject.transform.localScale.x, this.transform.GetChild(k).gameObject.transform.localScale.y, this.transform.GetChild(k).gameObject.transform.localScale.z);

                        if(GameObject.FindObjectOfType<RedirectMe_Sim_Index>() != null)
                        {
                            warpSpheres[i+19].transform.parent = warp.targetToTouch[i+19].gameObject.transform;

                            Vector3 ratio = new Vector3(warp.targetToTouch[i+19].transform.localScale.x/scaleC[i+19].x, warp.targetToTouch[i+19].transform.localScale.y/scaleC[i+19].y, warp.targetToTouch[i+19].transform.localScale.z/scaleC[i+19].z);
                            warpSpheres[i+19].transform.localPosition = new Vector3(spheres[i+19].transform.localPosition.x*ratio.x, spheres[i+19].transform.localPosition.y*ratio.y, spheres[i+19].transform.localPosition.z*ratio.z);
                        }
                        // scale[i+19] = (100*Vector3.Scale(this.transform.GetChild(k).transform.localScale, this.transform.localScale).x * 100*Vector3.Scale(this.transform.GetChild(k).transform.localScale, this.transform.localScale).y * 100*Vector3.Scale(this.transform.GetChild(k).transform.localScale, this.transform.localScale).z);
                        // mesh[i+19] = this.transform.GetChild(k).gameObject.GetComponent<MeshFilter>().mesh.name;
                    }

                    spheres[i].transform.parent = GameObject.Find("Projections").transform;
                    spheres[i+19].transform.parent = GameObject.Find("Projections").transform;
                    warpSpheres[i].transform.parent = GameObject.Find("WarpProjections").transform;
                    warpSpheres[i+19].transform.parent = GameObject.Find("WarpProjections").transform;

                }
            }
            else
            {
                for(int i = 0; i < 19; i++)
                {
                    if(this.transform.GetChild(k).gameObject.GetComponent<Collider>().bounds.Contains(predictStop.finalPred[i]))
                    {
                        name[i] = this.transform.GetChild(k).name;
                        // this.transform.GetChild(k).gameObject.GetComponent<MeshRenderer>().material.color = Color.green;

                        scale[i] = (100*Vector3.Scale(this.transform.GetChild(k).transform.localScale, this.transform.localScale).x * 100*Vector3.Scale(this.transform.GetChild(k).transform.localScale, this.transform.localScale).y * 100*Vector3.Scale(this.transform.GetChild(k).transform.localScale, this.transform.localScale).z);
                        mesh[i] = this.transform.GetChild(k).gameObject.GetComponent<MeshFilter>().mesh.name;
                    }

                    if(this.transform.GetChild(k).gameObject.GetComponent<Collider>().bounds.Contains(predictStopL.finalPred[i]))
                    {
                        name[i+19] = this.transform.GetChild(k).name;
                        // this.transform.GetChild(k).gameObject.GetComponent<MeshRenderer>().material.color = Color.magenta;

                        scale[i+19] = (100*Vector3.Scale(this.transform.GetChild(k).transform.localScale, this.transform.localScale).x * 100*Vector3.Scale(this.transform.GetChild(k).transform.localScale, this.transform.localScale).y * 100*Vector3.Scale(this.transform.GetChild(k).transform.localScale, this.transform.localScale).z);
                        mesh[i+19] = this.transform.GetChild(k).gameObject.GetComponent<MeshFilter>().mesh.name;
                    }
                }
                
            }
            
        }


    }
}
