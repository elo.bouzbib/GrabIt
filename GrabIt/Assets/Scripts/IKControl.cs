﻿using UnityEngine;
using System;
using System.Collections;

public class IKControl : MonoBehaviour
{
    protected Animator animator;

    public bool ikActive = false;
    public bool useMovements = false;
    public Transform rootObj = null;
    public Transform rightHandObj = null;
    public Transform leftHandObj = null;
    public Transform leftFootObj = null;
    public Transform rightFootObj = null;
    public Vector3 rightHandOffset = Vector3.zero;

    public Transform lookObj = null;
    public Transform targetBody = null;

    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        //animator.SetLayerWeight(0, 0f);
    }
    //a callback for calculating IK
    void OnAnimatorIK()
    {
        if (animator)
        {
            //if the IK is active, set the position and rotation directly to the goal. 
            if (ikActive)
            {
                // Set the look target position, if one has been assigned
                if (lookObj != null)
                {
                    animator.SetLookAtWeight(1);
                    animator.SetLookAtPosition(lookObj.position);
                }

                if(rootObj != null)
                {
                    animator.rootPosition = rootObj.position;
                    animator.rootRotation = rootObj.rotation;

                    if(targetBody != null) {
                    transform.position = rootObj.position;
                    transform.rotation = rootObj.rotation;

                        } else {
                                                targetBody.position = rootObj.position;
                    targetBody.rotation = rootObj.rotation;

                        }

                }

                if (useMovements)
                {
                    // Set the right hand target position and rotation, if one has been assigned
                    if (rightHandObj != null)
                    {
                        animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
                        animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
                        animator.SetIKPosition(AvatarIKGoal.RightHand, rightHandObj.position + rightHandOffset);
                        animator.SetIKRotation(AvatarIKGoal.RightHand, rightHandObj.rotation);
                    }
                    if (leftHandObj != null)
                    {
                        animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
                        animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1);
                        animator.SetIKPosition(AvatarIKGoal.LeftHand, leftHandObj.position);
                        animator.SetIKRotation(AvatarIKGoal.LeftHand, leftHandObj.rotation);
                    }



                    if (rightFootObj != null)
                    {
                        animator.SetIKPositionWeight(AvatarIKGoal.RightFoot, 1);
                        animator.SetIKRotationWeight(AvatarIKGoal.RightFoot, 1);
                        animator.SetIKPosition(AvatarIKGoal.RightFoot, rightFootObj.position);
                        animator.SetIKRotation(AvatarIKGoal.RightFoot, rightFootObj.rotation);
                    }
                    if (leftFootObj != null)
                    {
                        animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot, 1);
                        animator.SetIKRotationWeight(AvatarIKGoal.LeftFoot, 1);
                        animator.SetIKPosition(AvatarIKGoal.LeftFoot, leftFootObj.position);
                        animator.SetIKRotation(AvatarIKGoal.LeftFoot, leftFootObj.rotation);
                    }
                }
            }

            //if the IK is not active, set the position and rotation of the hand and head back to the original position
            else
            {
                animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 0);
                animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 0);
                animator.SetLookAtWeight(0);
            }
        }
    }
}
