﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System.IO;
using System.Globalization;

public class WritePhalanxContact : MonoBehaviour
{

	public SimulationHand simulationHand;
	public GameObject rightHand, leftHand;

	public int frame;
	public string time;

	private string path;
	private StreamWriter writer;

	private int state = 0;

	public ClosestToHand_Live[] phalanxInfo;



    // Start is called before the first frame update
    void Start()
    {
        simulationHand = GameObject.FindObjectOfType<SimulationHand>().GetComponent<SimulationHand>();
        rightHand = GameObject.Find("RightHandColl");
        leftHand = GameObject.Find("LeftHandColl");

        path = "Assets/Resources/DataSimulation/" + simulationHand.FileName + "-contact.csv";
        writer = new StreamWriter(path, true);
        
        writer.Write("Frame;Time");
		for(int i = 0; i < rightHand.gameObject.transform.childCount; i++)
        {
			writer.Write(";ID" + i.ToString() + ";" + "PhalanxContact-L;ClosestOnObject-LX;ClosestOnObject-LY;ClosestOnObject-LZ;ClosestDistance-L");
			writer.Write(";PhalanxContact-R;ClosestOnObject-RX;ClosestOnObject-RY;ClosestOnObject-RZ;ClosestDistance-R");

        }

		writer.Close();

		phalanxInfo = new ClosestToHand_Live[2*rightHand.gameObject.transform.childCount];


    }

    // Update is called once per frame

    void Update()
    {
    	   for(int i = 0; i < rightHand.gameObject.transform.childCount; i++)
	    {
	    	phalanxInfo[i] = leftHand.gameObject.transform.GetChild(i).gameObject.GetComponent<ClosestToHand_Live>();
	    	phalanxInfo[i+rightHand.gameObject.transform.childCount] = rightHand.gameObject.transform.GetChild(i).gameObject.GetComponent<ClosestToHand_Live>();
	    }
    }
    void FixedUpdate()
    {
        
        frame = simulationHand.frame;
        time = simulationHand.timeRead;

       

     //    for(int i = 0; i < rightHand.gameObject.transform.childCount; i++)
	    // {
	    // 	phalanxInfo[i] = leftHand.gameObject.transform.GetChild(i).gameObject.GetComponent<ClosestToHand_Live>();
	    // 	phalanxInfo[i+rightHand.gameObject.transform.childCount] = rightHand.gameObject.transform.GetChild(i).gameObject.GetComponent<ClosestToHand_Live>();
	    // }

	    path = "Assets/Resources/DataSimulation/" + simulationHand.FileName + "-contact.csv";
        writer = new StreamWriter(path, true);
        writer.WriteLine();
        writer.Write(frame.ToString() + ";" + time);
        Debug.Log("open");
		for(int i = 0; i < rightHand.gameObject.transform.childCount; i++)
        {
        	 Debug.Log("yes" + i);
        	 // writer.Write(";yes" + i);
	    	writer.Write(";ID" + i.ToString() + ";" +  leftHand.gameObject.transform.GetChild(i).gameObject.GetComponent<ClosestToHand_Live>().phalanxContact + ";" + leftHand.gameObject.transform.GetChild(i).gameObject.GetComponent<ClosestToHand_Live>().shareMeTooX  + ";" + leftHand.gameObject.transform.GetChild(i).gameObject.GetComponent<ClosestToHand_Live>().shareMeTooY + ";" + leftHand.gameObject.transform.GetChild(i).gameObject.GetComponent<ClosestToHand_Live>().shareMeTooZ + ";" + leftHand.gameObject.transform.GetChild(i).gameObject.GetComponent<ClosestToHand_Live>().shareMe);
            writer.Write(";" +  rightHand.gameObject.transform.GetChild(i).gameObject.GetComponent<ClosestToHand_Live>().phalanxContact + ";" + rightHand.gameObject.transform.GetChild(i).gameObject.GetComponent<ClosestToHand_Live>().shareMeTooX  + ";" + rightHand.gameObject.transform.GetChild(i).gameObject.GetComponent<ClosestToHand_Live>().shareMeTooY + ";" + rightHand.gameObject.transform.GetChild(i).gameObject.GetComponent<ClosestToHand_Live>().shareMeTooZ + ";" + rightHand.gameObject.transform.GetChild(i).gameObject.GetComponent<ClosestToHand_Live>().shareMe);


        }
		writer.Close();

    }
}
