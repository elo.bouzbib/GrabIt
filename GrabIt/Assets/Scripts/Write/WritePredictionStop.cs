using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System.IO;
using System.Globalization;


public class WritePredictionStop : MonoBehaviour
{

	public WriteHand handWriting;

	private static float time0;
	private string path, time1;
	private StreamWriter writer;
	private int state = 0;

	
	// public IList<OVRBoneCapsule> Capsules { get; private set; }
	// public IList<OVRBoneCapsule> CapsulesR { get; private set; }

	// private OVRCustomSkeleton handSkelLeft, handSkelRight;

	public PredictionStop predictionStop;
	public PredictionStopL predictionStopL;

	public Instantiate_Live objectInstance;
	private GameObject handCollRight, handCollLeft;

	private float gamma, beta;


    void Start()
    {
    	objectInstance = FindObjectOfType<Instantiate_Live>();
    	predictionStop = GameObject.FindObjectOfType<PredictionStop>();
    	predictionStopL = GameObject.FindObjectOfType<PredictionStopL>();
		
        time0 = Time.time;
		time1 = System.DateTime.Now.ToString("ddMMyyyy-HHmm");

        // handSkelLeft = GameObject.Find("OVRCustomHandPrefab_L").GetComponent<OVRCustomSkeleton>();
        // handSkelRight = GameObject.Find("OVRCustomHandPrefab_R").GetComponent<OVRCustomSkeleton>();
        
        // CapsulesR = handSkelRight.GetComponent<OVRCustomSkeleton>().Capsules;
        // Capsules = handSkelLeft.GetComponent<OVRCustomSkeleton>().Capsules;

        handCollRight = GameObject.Find("RightHandColl");
		handCollLeft = GameObject.Find("LeftHandColl");
		beta = 1.05f;
		gamma = 0f;

    }

    // Update is called once per frame
    void Update()
    {
    	if(handWriting.inSDCard)
		{
			path = $"/mnt/sdcard/" + time1 + "-predictionsStop.csv";

		}
		else
		{
			path = "Assets/Resources/DataCollection/" + time1 + "-predictionsStop.csv";
		}

        writer = new StreamWriter(path, true);
    	// objectInstance = FindObjectOfType<Instantiate_Live>();

        // handSkelRight = GameObject.Find("OVRCustomHandPrefab_R").GetComponent<OVRCustomSkeleton>();
        // handSkelLeft = GameObject.Find("OVRCustomHandPrefab_L").GetComponent<OVRCustomSkeleton>();

        // CapsulesR = handSkelRight.GetComponent<OVRCustomSkeleton>().Capsules;
        // Capsules = handSkelLeft.GetComponent<OVRCustomSkeleton>().Capsules;


    switch(state)
		{
			case 0:
				writer.Write("Time;Beta;Gamma;Config;NbBloc;Grasp;TimePrediction;StopPredBool;TimePredictionL;StopPredBoolL");

				for(int i = 0; i < 19; i++)
				{
					writer.Write(";BoneID;BoneName;BonePos;PosPred;DistToPred;BoneRadius;BoneContact;ClosestPoint;DistToClosest");
					writer.Write(";BoneIDL;BoneNameL;BonePosL;PosPredL;DistToPredL;BoneRadiusL;BoneContactL;ClosestPointL;DistToClosestL");
				}
				writer.Close();
				state = 1;
			break;

			case 1:
				writer.WriteLine();		
				
				if((Time.unscaledTime - time0) > 2.0f)
				{
					writer.Write((Time.unscaledTime - time0) + ";" + beta.ToString() + ";" + gamma.ToString() + ";" + objectInstance.config + ";" + objectInstance.nbBloc + ";" + objectInstance.grasp + ";" + predictionStop.timePrediction.ToString() + ";" + predictionStop.stopPredictions + ";" + predictionStopL.timePrediction.ToString() + ";" + predictionStopL.stopPredictions);
				
					for(int i = 0; i < 19 ; i++)
			    	{
						writer.Write(";" + predictionStop.phalanxPrediction[i].namePhalanx + ";" + predictionStop.phalanxPrediction[i].posPhalanx.ToString() + ";" + predictionStop.finalPred[i].magnitude.ToString() +";" + predictionStop.finalDistToPred[i].ToString() + ";" + handCollRight.transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().radius.ToString() + ";" + predictionStop.phalanxPrediction[i].phalanxContact + ";" + predictionStop.phalanxPrediction[i].closestPoint.ToString() + ";" + predictionStop.phalanxPrediction[i].distToClosest.ToString());
						writer.Write(";" + predictionStopL.phalanxPrediction[i].namePhalanx + ";" + predictionStopL.phalanxPrediction[i].posPhalanx.ToString() + ";" + predictionStopL.finalPred[i].magnitude.ToString() + ";" + predictionStopL.finalDistToPred[i].ToString() + ";" + handCollLeft.transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().radius.ToString() + ";" + predictionStopL.phalanxPrediction[i].phalanxContact + ";" + predictionStopL.phalanxPrediction[i].closestPoint.ToString() + ";" + predictionStopL.phalanxPrediction[i].distToClosest.ToString());
					}
				}
				
				// for(int i = 0; i < 19 ; i++)
		  //   	{
				// 	writer.Write(";" + predictionStop.phalanxPrediction[i].namePhalanx + ";" + predictionStop.phalanxPrediction[i].posPhalanx.ToString() + ";" + predictionStop.phalanxPrediction[i].posPred.ToString() + ";" + predictionStop.phalanxPrediction[i].posPred1.ToString() + ";" + predictionStop.phalanxPrediction[i].posPred2.ToString() +";" + predictionStop.phalanxPrediction[i].distToPred.ToString() + ";" + predictionStop.phalanxPrediction[i].distToPred1.ToString() + ";" + predictionStop.phalanxPrediction[i].distToPred2.ToString() + ";" + CapsulesR[i].CapsuleCollider.radius.ToString() + ";" + predictionStop.phalanxPrediction[i].phalanxContact + ";" + predictionStop.phalanxPrediction[i].closestPoint.ToString() + ";" + predictionStop.phalanxPrediction[i].distToClosest.ToString());
				// 	// writer.Write(";" + predictionStopL.phalanxPrediction[i].namePhalanx + ";" + handCollLeft.transform.GetChild(i).gameObject.transform.position.magnitude.ToString() + ";" + predictionStopL.phalanxPrediction[i].posPred.ToString() + ";" + predictionStopL.phalanxPrediction[i].posPred1.ToString() + ";" + predictionStopL.phalanxPrediction[i].posPred2.ToString() + ";" + predictionStopL.phalanxPrediction[i].distToPred.ToString() + ";" + predictionStopL.phalanxPrediction[i].distToPred1.ToString() + ";" + predictionStopL.phalanxPrediction[i].distToPred2.ToString() + ";" + handCollLeft.transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().radius.ToString() + ";" + predictionStopL.phalanxPrediction[i].phalanxContact + ";" + predictionStopL.phalanxPrediction[i].closestPoint.ToString() + ";" + predictionStopL.phalanxPrediction[i].distToClosest.ToString());
				
				// }

		    	writer.Close();
			break;
		}

	}


}
