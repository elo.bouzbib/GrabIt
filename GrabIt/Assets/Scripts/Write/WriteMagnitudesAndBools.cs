﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System.IO;
using System.Globalization;

public class WriteMagnitudesAndBools : MonoBehaviour
{
	private static float time0, time1;
	private string path, pathME, fileName;
	private string[] dataNames, nameSkel;
	private GameObject handCollLeft, handCollRight;
	private OVRCustomSkeleton handSkelLeft, handSkelRight;
	public SimulationHand fromCollection;
	public SimulationObjOfInterest_withKids ooiCollection;
	// private StreamWriter writer, writerNames;
	private StreamWriter writer;
	public int state = 0;

	public Predict_Live predictPos;
	public AlgoHints_Live algoHint;

    private GameObject palm, thumb, index;
    public string addWord;

    // Start is called before the first frame update
    void Start()
    {
    	handCollLeft = GameObject.Find("LeftHandColl");
		handCollRight = GameObject.Find("RightHandColl");
		fromCollection = FindObjectOfType<SimulationHand>();
		ooiCollection = FindObjectOfType<SimulationObjOfInterest_withKids>();
		fileName = fromCollection.FileName;
		time0 = Time.time;
		time1 = Time.time;
		dataNames = new string[19];
		nameSkel = new string[19];
		path = "Assets/Resources/DataSimulation/AlgoHints/" + addWord + "-" + fileName + "-Bools-Magnitudes.csv";
		// pathME = "Assets/Resources/DataSimulation/AlgoHints/" + addWord + "-" + fileName + "-TouchPhalanx.csv";

		predictPos = FindObjectOfType<Predict_Live>();
        algoHint = FindObjectOfType<AlgoHints_Live>();
        handSkelLeft = GameObject.Find("OVRCustomHandPrefab_L").GetComponent<OVRCustomSkeleton>();
        handSkelRight = GameObject.Find("OVRCustomHandPrefab_R").GetComponent<OVRCustomSkeleton>();
        palm = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/r_palm_center_marker").gameObject;
        index = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/b_r_index1/b_r_index2/b_r_index3/r_index_finger_pad_marker").gameObject;
        thumb = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/b_r_thumb0/b_r_thumb1/b_r_thumb2/b_r_thumb3/r_thumb_finger_pad_marker").gameObject;
       
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        writer = new StreamWriter(path, true);

        // writerNames = new StreamWriter(pathME, true);

        switch(state)
		{
			case 0:
				for(int i = 0; i < 19; i++)
		    	{
		    		dataNames[i] = handCollLeft.transform.GetChild(i).name;
		    		nameSkel[i] = handSkelLeft.CustomBones[i].name;		    		
		    	}

				writer.Write("Time;Time2;Time0;Config;NbBloc;Grasp;alpha;epsilon");
				// writerNames.Write("Time;Time0;Config;NbBloc;Grasp;alpha;nbFutureTouch");
				// for(int i = 0; i < 6; i++)
				// {
				// 	// writer.Write(";inZone;touchedBy;nbFutureTouch;inZone_fromPlane;touchedByPlane");//;inZone_fromProbsPred;probsTouchedBy");
				// 	writer.Write(";inZone;inZone_fromPlane");//;inZone_fromProbsPred;probsTouchedBy");

				// }
				writer.Write(";nbFutureTouch;oneContact;clampGrip");
				writer.Write(";fromTop;fromSideX;fromSideZ");

				for(int i = 0; i < 19; i++)
				{
					writer.Write(";BoneID;Name;CollPos;PosPredict;ClosestDist;ProjFromTraj;PhalanxContact");
					writer.Write(";PosSkel;DebugProbs0;Probs0;DebugProbs1;Probs1;DebugProbs2;Probs2");

					writer.Write(";BoneID;Name;CollPosL;PosPredictL;ClosestDistL;ProjFromTrajL;PhalanxContactL");
					writer.Write(";PosSkelL;DebugProbs0L;Probs0L;DebugProbs1L;Probs1L;DebugProbs2L;Probs2L");

				}

				// writer.Write(";PosSkelThumb;DebugProbsA;ProbsA;PosSkelIndex;DebugProbsB;ProbsB;Dot(T/P);Dot(T/I)");
				// // FINISH WITHOUT ;
				// writer.Write(";BoneID;Name;CollPos;PosPredict;ClosestDist;ProjFromTraj;PhalanxContact");
	   //  		writer.Write(";SkelID;PosSkel;ProjOnPlane0;ProbsPositionPlane0;ProjOnPlane1;ProbsPositionPlane1;ProjOnPlane2;ProbsPositionPlane2");
	   //  		writer.Write(";BoneID;Name;CollPos;PosPredict;ClosestDist;ProjFromTraj;PhalanxContact");
				// writer.Write(";SkelID;PosSkel;ProjOnPlane0;ProbsPositionPlane0;ProjOnPlane1;ProbsPositionPlane1;ProjOnPlane2;ProbsPositionPlane2");
				writer.Close();
				// writerNames.Close();
				state = 1;
				
			break;

			case 1:
				writer.WriteLine();		
				// writerNames.WriteLine();		

				if(ooiCollection.changeConfig)
				{
					time0 = Time.unscaledTime;
				}
				
				writer.Write(fromCollection.timeRead + ";" + (Time.unscaledTime - time1) + ";" + (Time.unscaledTime - time0) + ";" + ooiCollection.config + ";" + ooiCollection.nbBlocFromData + ";" + ooiCollection.grasp + ";" + fromCollection.alpha + ";" + fromCollection.epsilon);

				// writerNames.Write((Time.unscaledTime - time1) + ";" + (Time.unscaledTime - time0) + ";" + ooiCollection.config + ";" + ooiCollection.nbBlocFromData + ";" + ooiCollection.grasp + ";" + fromCollection.alpha + ";" + fromCollection.epsilon);
				// writerNames.Write(";" + algoHint.gonnaTouch.Count);

				// if(algoHint.gonnaTouch.Count != 0)
				// {
				// 	for(int k = 0; k < algoHint.gonnaTouch.Count; k++)
				// 	{
						// writerNames.Write(";" + algoHint.gonnaTouch[k]);
				// 	}

				// }
				
				// for(int i = 0; i < 6; i++)
				// {
				// 	// writer.Write(";" + predictPos.inZone[i] + ";" + predictPos.touchedBy[i] + ";" + algoHint.gonnaTouch.Count + ";" + predictPos.inZone_fromPlane[i] + ";" + predictPos.touchByPlane[i]);// + ";" + predictPos.inZone_fromProbsPred[i] + ";" + predictPos.probsTouchedBy[i]);
				// 	writer.Write(";" + predictPos.inZone[i] + ";" + predictPos.inZone_fromPlane[i]);// + ";" + predictPos.inZone_fromProbsPred[i] + ";" + predictPos.probsTouchedBy[i]);

				// }
				// writer.Write(";" + algoHint.gonnaTouch.Count + ";" + predictPos.oneContact + ";" + predictPos.clampGrip);
				// writer.Write(";" + algoHint.plane[0].GetComponent<CollisionPlanes_withKids>().fromTop + ";" + algoHint.plane[0].GetComponent<CollisionPlanes_withKids>().fromSideX + ";" + algoHint.plane[0].GetComponent<CollisionPlanes_withKids>().fromSideZ);


				// for(int i = 0; i < 19; i++)
				// {
				// 	writer.Write(";" + dataNames[i] + ";" + handCollRight.transform.GetChild(i).transform.position.magnitude.ToString("F4") + ";" + predictPos.posToPredict[i].magnitude.ToString("F4") + ";" + predictPos.closest[i].closest_distance.magnitude.ToString("F4") + ";" + predictPos.projection[i].magnitude.ToString("F4") + ";" + predictPos.contactPerPhal[i]);
				// 	writer.Write(";" + handSkelRight.CustomBones[i].transform.position.magnitude.ToString() + ";" + algoHint.debugProbsP[i].magnitude.ToString() + ";" + algoHint.probsP[i].magnitude.ToString() + ";" + algoHint.debugProbsQ[i].magnitude.ToString() + ";" + algoHint.probsQ[i].magnitude.ToString() + ";" + algoHint.debugProbsR[i].magnitude.ToString() + ";" + algoHint.probsR[i].magnitude.ToString());
					
				// 	writer.Write(";" + dataNames[i] + ";" + handCollLeft.transform.GetChild(i).transform.position.magnitude.ToString("F4") + ";" + predictPos.posToPredict[i+19].magnitude.ToString("F4") + ";" + predictPos.closest[i+19].closest_distance.magnitude.ToString("F4") + ";" + predictPos.projection[i+19].magnitude.ToString("F4") + ";" + predictPos.contactPerPhal[i+19]);
				// 	writer.Write(";" + handSkelLeft.CustomBones[i].transform.position.magnitude.ToString() + ";" + algoHint.debugProbsS[i].magnitude.ToString() + ";" + algoHint.probsS[i].magnitude.ToString() + ";" + algoHint.debugProbsT[i].magnitude.ToString() + ";" + algoHint.probsT[i].magnitude.ToString() + ";" + algoHint.debugProbsU[i].magnitude.ToString() + ";" + algoHint.probsU[i].magnitude.ToString());


				// }
				
				// writer.Write(";" + handSkelRight.CustomBones[5].transform.position.magnitude + ";" + algoHint.debugProbsA.magnitude + ";" + algoHint.probsA.magnitude + ";" + handSkelRight.CustomBones[8].transform.position.magnitude + ";" + algoHint.debugProbsB.magnitude + ";" + algoHint.probsB.magnitude);
				// writer.Write(";" + Vector3.Dot(thumb.transform.right.normalized, palm.transform.right.normalized).ToString("F4") + ";" + Vector3.Dot(thumb.transform.right.normalized, index.transform.right.normalized).ToString("F4"));
				// writer.Write(";" + dataNames[6]);
	   //  		writer.Write(";" + handCollRight.transform.GetChild(6).transform.position.magnitude.ToString("F4") + ";" + predictPos.posToPredict[6].magnitude.ToString("F4") + ";" + predictPos.closest[6].closest_distance.magnitude.ToString("F4") + ";" + predictPos.projection[6].magnitude.ToString("F4") + ";" + predictPos.contactPerPhal[6]);	
	   //  		writer.Write(";" + nameSkel[5] + ";" + handSkelRight.CustomBones[5].transform.position.magnitude + ";" + algoHint.plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[5].magnitude.ToString("F4") + ";" + algoHint.probsP[5].magnitude.ToString("F4") + ";" + algoHint.plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[5].magnitude.ToString("F4") + ";" + algoHint.probsQ[5].magnitude.ToString("F4") + ";" + algoHint.plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[5].magnitude.ToString("F4") + ";" + algoHint.probsR[5].magnitude.ToString("F4"));// + ";" + algoHint.projPhalOnPlanes[i*2].magnitude.ToString("F4") + ";" + algoHint.probsP[i*2].magnitude.ToString("F4") + ";" + algoHint.errorPerPhal[i*2].ToString("F4"));
	   // 	    	writer.Write(";" + dataNames[9]);
	   //  		writer.Write(";" + handCollRight.transform.GetChild(9).transform.position.magnitude.ToString("F4") + ";" + predictPos.posToPredict[9].magnitude.ToString("F4") + ";" + predictPos.closest[9].closest_distance.magnitude.ToString("F4") + ";" + predictPos.projection[9].magnitude.ToString("F4") + ";" + predictPos.contactPerPhal[9]);	
	   //  		writer.Write(";" + nameSkel[8] + ";" + handSkelRight.CustomBones[8].transform.position.magnitude + ";" + algoHint.plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[8].magnitude.ToString("F4") + ";" + algoHint.probsP[8].magnitude.ToString("F4") + ";" + algoHint.plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[8].magnitude.ToString("F4") + ";" + algoHint.probsQ[8].magnitude.ToString("F4") + ";" + algoHint.plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[8].magnitude.ToString("F4") + ";" + algoHint.probsR[8].magnitude.ToString("F4"));// + ";" + algoHint.projPhalOnPlanes[i*2].magnitude.ToString("F4") + ";" + algoHint.probsP[i*2].magnitude.ToString("F4") + ";" + algoHint.errorPerPhal[i*2].ToString("F4"));

		    	writer.Close();
		    	// writerNames.Close();
			break;
		}
    
    }
    
}
