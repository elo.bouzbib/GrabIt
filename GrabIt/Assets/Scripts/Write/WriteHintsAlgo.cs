﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System.IO;
using System.Globalization;

public class WriteHintsAlgo : MonoBehaviour
{
	private static float time0, time1;
	private string path, fileName;
	private string[] dataNames, nameSkel;
	private GameObject handCollLeft, handCollRight;
	private OVRCustomSkeleton handSkelLeft, handSkelRight;
	public SimulationHand fromCollection;
	public SimulationObjOfInterest ooiCollection;
	private StreamWriter writer;
	public int state = 0;

	public PredictPosition predictPos;
	public AlgoHints algoHint;

    private GameObject palm, thumb, index;


    // Start is called before the first frame update
    void Start()
    {
		handCollLeft = GameObject.Find("LeftHandColl");
		handCollRight = GameObject.Find("RightHandColl");
		fromCollection = FindObjectOfType<SimulationHand>();
		ooiCollection = FindObjectOfType<SimulationObjOfInterest>();
		fileName = fromCollection.FileName;
		time0 = Time.time;
		time1 = Time.time;
		dataNames = new string[19];
		nameSkel = new string[19];
		path = "Assets/Resources/DataSimulation/AlgoHints/" + fileName + ".csv";
		predictPos = FindObjectOfType<PredictPosition>();
        algoHint = FindObjectOfType<AlgoHints>();
        handSkelLeft = GameObject.Find("OVRCustomHandPrefab_L").GetComponent<OVRCustomSkeleton>();
        handSkelRight = GameObject.Find("OVRCustomHandPrefab_R").GetComponent<OVRCustomSkeleton>();

        palm = GameObject.Find("r_palm_center_marker");
        index = GameObject.Find("r_index_finger_pad_marker");
        thumb = GameObject.Find("r_thumb_finger_pad_marker");
    }
    

    // Update is called once per frame
    void Update()
    {
        writer = new StreamWriter(path, true);

        switch(state)
		{
			case 0:
					//ONLY ONE HAND
			// Time, Config, NbBloc, Alpha, Grasp, PosSkelThumb, DebugProbsA, ProbsA, PosSkelIndex, DebugProbsB, ProbsB, VF, VFMagnitude, ProjVFX, ProjVFY, ProjVFZ, BoolZones, Dot(ThumbPalm), Dot(ThumbIndex)
				writer.Write("Time;Time0;Config;NbBloc;Alpha;Grasp;PosSkelThumb;DebugProbsA;ProbsA;PosSkelIndex;DebugProbsB;ProbsB;VF;VFMagnitude;ProjVFX;ProjVFY;ProjVFZ;InZone0;InZone1;InZone2;InZone3;InZone4;InZone5;Dot(T/P);Dot(T/I)");
				// FINISH WITHOUT ;
				for(int i = 0; i < 19; i++)
		    	{
		    		dataNames[i] = handCollLeft.transform.GetChild(i).name;
		    		// FINISH WITHOUT ;
		    		// BoneID, Name, Pos, PosMagnitude, 						ErrorPred, PosPredict, PosPredictMagnitude, ClosDist, ProjTraj, PhalanxContact
		    		writer.Write(";BoneID;Name;CollPosition;CollPosMagnitude;ErrorPredict;PosPredict;PosPredictMagnitude;ClosestDist;ProjFromTraj;PhalanxContact");
		    		// IDSkel, PosSkel, Proj0, Probs0, ErrorProj0, Proj1, Probs1, ErrorProj1
		    		nameSkel[i] = handSkelLeft.CustomBones[i].name;
					writer.Write(";SkelID;PosSkel;PosSkelMagnitude;ProjOnPlane0;ProbsPositionPlane0;ErrorProjOnPlane0");//;ProjOnPlane1;ProbsPositionPlane1;ErrorProjOnPlane1");
		    		
		    	}
				writer.Close();
				state = 1;
				
			break;

			case 1:
				writer.WriteLine();				

				if(ooiCollection.changeConfig)
				{
					time0 = Time.unscaledTime;
				}
													// Time, 			Config, 								NbBloc, 					Alpha, 					Grasp
				writer.Write((Time.unscaledTime - time1) + ";" + (Time.unscaledTime - time0) + ";" + ooiCollection.config + ";" + ooiCollection.nbBlocFromData + ";" + fromCollection.alpha + ";" + ooiCollection.grasp);
				// PosSkelThumb, DebugProbsA, ProbsA, PosSkelIndex, DebugProbsB, ProbsB, 
				// writer.Write(";" + handSkelRight.CustomBones[5].transform.position + ";" + algoHint.debugProbsA[0] + ";" + algoHint.probsA[0] + ";" + handSkelRight.CustomBones[8].transform.position + ";" + algoHint.debugProbsB[0] + ";" + algoHint.probsB[0]);
				writer.Write(";" + handSkelRight.CustomBones[5].transform.position + ";" + algoHint.debugProbsA + ";" + algoHint.probsA + ";" + handSkelRight.CustomBones[8].transform.position + ";" + algoHint.debugProbsB + ";" + algoHint.probsB);
								//	 VF, 									VFMagnitude, 													ProjVFX, ProjVFY, ProjVFZ, 	
				writer.Write(";" + predictPos.virtualFinger + ";" + predictPos.virtualFinger.magnitude.ToString("F4") + ";" + predictPos.projVFX.ToString("F4") + ";" + predictPos.projVFY.ToString("F4") + ";" + predictPos.projVFZ.ToString("F4"));
				// BoolZones, Dot(ThumbPalm), Dot(ThumbIndex)
				writer.Write(";" + predictPos.inZone[0] + ";" + predictPos.inZone[1] + ";" + predictPos.inZone[2] + ";" + predictPos.inZone[3] + ";" + predictPos.inZone[4] + ";" + predictPos.inZone[5]);
				writer.Write(";" + Vector3.Dot(thumb.transform.right.normalized, palm.transform.right.normalized).ToString("F4") + ";" + Vector3.Dot(thumb.transform.right.normalized, index.transform.right.normalized).ToString("F4"));
				for(int i = 0; i < 19; i++)
		    	{
		    		// BoneID, Name, Pos, PosMagnitude, 						ErrorPred, PosPredict, PosPredictMagnitude, ClosDist, ProjTraj, PhalanxContact
			    	writer.Write(";" + dataNames[i]);
		    		writer.Write(";" + handCollRight.transform.GetChild(i).transform.position + ";" + handCollRight.transform.GetChild(i).transform.position.magnitude.ToString("F4") + ";" + predictPos.errorPredict[i].magnitude.ToString("F4") + ";" + predictPos.posToPredict[i] + ";" + predictPos.posToPredict[i].magnitude.ToString("F4") + ";" + predictPos.closest[i].closest_distance.ToString("F4") + ";" + predictPos.projection[i] + ";" + predictPos.contactPerPhal[i]);	
		    		// IDSkel, PosSkel, Proj0, Probs0, ErrorProj0, Proj1, Probs1, ErrorProj1
		    		// writer.Write(";" + nameSkel[i] + ";" + handSkelRight.CustomBones[i].transform.position + ";" + algoHint.projPhalOnPlanes[i].magnitude.ToString("F4") + ";" + algoHint.probsP[i].magnitude.ToString("F4") + ";" + algoHint.errorPerPhal[i].ToString("F4"));// + ";" + algoHint.projPhalOnPlanes[i*2].magnitude.ToString("F4") + ";" + algoHint.probsP[i*2].magnitude.ToString("F4") + ";" + algoHint.errorPerPhal[i*2].ToString("F4"));
		    		writer.Write(";" + nameSkel[i] + ";" + handSkelRight.CustomBones[i].transform.position + ";" + handSkelRight.CustomBones[i].transform.position.magnitude + ";" + algoHint.plane[2].GetComponent<CollisionPlanes>().projPerPhalOnOOI[i].magnitude.ToString("F4") + ";" + algoHint.probsP[i].magnitude.ToString("F4") + ";" + algoHint.errorPerPhal[i].ToString("F4"));// + ";" + algoHint.projPhalOnPlanes[i*2].magnitude.ToString("F4") + ";" + algoHint.probsP[i*2].magnitude.ToString("F4") + ";" + algoHint.errorPerPhal[i*2].ToString("F4"));

		    	}
		    	writer.Close();
			break;
		}
    
    }
}
