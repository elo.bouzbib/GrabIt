﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System.IO;
using System.Globalization;

public class WriteSpeeds : MonoBehaviour
{
	private static float time0;
	private string path, fileName;
	private string[] dataNames;
	private GameObject handLeft, handRight;
	public SimulationHand fromCollection;
	private StreamWriter writer;
	public int state = 0;
	public bool proj = true;



    // Start is called before the first frame update
    void Start()
    {
        handLeft = GameObject.Find("LeftHandColl");
		handRight = GameObject.Find("RightHandColl");
		fromCollection = FindObjectOfType<SimulationHand>();
		fileName = fromCollection.FileName;
		time0 = Time.time;
		dataNames = new string[19];
		if(!proj)
		{
			path = "Assets/Resources/DataSimulation/Speeds/" + fileName + ".csv";
		}
		else
		{
			path = "Assets/Resources/DataSimulation/Speeds/" + fileName + "-angularSpeed.csv";
		}
        
        
    }

    // Update is called once per frame
    void Update()
    {
        writer = new StreamWriter(path, true);

        switch(state)
		{
			case 0:
				// writer.WriteLine("Time");
				// writer.WriteLine("Time;BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ");
				if(!proj)
				{
					//ONLY RIGHT HAND
					writer.WriteLine("Time;BoneID;Name;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ;BoneID;Name;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ");

					for(int i = 0; i < 19; i++)
			    	{
			    		dataNames[i] = handLeft.transform.GetChild(i).name;
			    		Debug.Log(dataNames[i]);
			    		// writer.Write(";BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ");
			    	}
					writer.Close();
					state = 1;
				}
				else
				{
					writer.WriteLine("Time;BoneID;Name;AngleVel;TangRight;TangUp;TangForward;AngVelX;AngVelY;AngVelZ;BoneID;Name;AngleVel;TangRight;TangUp;TangForward;AngVelX;AngVelY;AngVelZ;BoneID;Name;AngleVel;TangRight;TangUp;TangForward;AngVelX;AngVelY;AngVelZ;BoneID;Name;AngleVel;TangRight;TangUp;TangForward;AngVelX;AngVelY;AngVelZ;BoneID;Name;AngleVel;TangRight;TangUp;TangForward;AngVelX;AngVelY;AngVelZ;BoneID;Name;AngleVel;TangRight;TangUp;TangForward;AngVelX;AngVelY;AngVelZ;BoneID;Name;AngleVel;TangRight;TangUp;TangForward;AngVelX;AngVelY;AngVelZ;BoneID;Name;AngleVel;TangRight;TangUp;TangForward;AngVelX;AngVelY;AngVelZ;BoneID;Name;AngleVel;TangRight;TangUp;TangForward;AngVelX;AngVelY;AngVelZ;BoneID;Name;AngleVel;TangRight;TangUp;TangForward;AngVelX;AngVelY;AngVelZ;BoneID;Name;AngleVel;TangRight;TangUp;TangForward;AngVelX;AngVelY;AngVelZ;BoneID;Name;AngleVel;TangRight;TangUp;TangForward;AngVelX;AngVelY;AngVelZ;BoneID;Name;AngleVel;TangRight;TangUp;TangForward;AngVelX;AngVelY;AngVelZ;BoneID;Name;AngleVel;TangRight;TangUp;TangForward;AngVelX;AngVelY;AngVelZ;BoneID;Name;AngleVel;TangRight;TangUp;TangForward;AngVelX;AngVelY;AngVelZ;BoneID;Name;AngleVel;TangRight;TangUp;TangForward;AngVelX;AngVelY;AngVelZ;BoneID;Name;AngleVel;TangRight;TangUp;TangForward;AngVelX;AngVelY;AngVelZ;BoneID;Name;AngleVel;TangRight;TangUp;TangForward;AngVelX;AngVelY;AngVelZ;BoneID;Name;AngleVel;TangRight;TangUp;TangForward;AngVelX;AngVelY;AngVelZ");
					for(int i = 0; i < 19; i++)
			    	{
			    		dataNames[i] = handLeft.transform.GetChild(i).name;
			    		Debug.Log(dataNames[i]);
			    		// writer.Write(";BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ");
			    	}
			    	writer.Close();
					state = 1;
				}
				
			break;

			case 1:
				writer.WriteLine();				

				writer.Write((Time.unscaledTime - time0));
				if(!proj)
				{
					for(int i = 0; i < 19; i++)
			    	{
			    		float posLeft = Mathf.Sqrt((Mathf.Pow(handLeft.transform.GetChild(i).gameObject.GetComponent<Transform>().position.x, 2f)) + (Mathf.Pow(handLeft.transform.GetChild(i).gameObject.GetComponent<Transform>().position.y, 2f)) + (Mathf.Pow(handLeft.transform.GetChild(i).gameObject.GetComponent<Transform>().position.z, 2f)));
						float posRight = Mathf.Sqrt((Mathf.Pow(handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().position.x, 2f)) + (Mathf.Pow(handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().position.y, 2f)) + (Mathf.Pow(handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().position.z, 2f)));
				    	
						float speedLeft = Mathf.Sqrt((Mathf.Pow(handLeft.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().velocity.x, 2f)) + (Mathf.Pow(handLeft.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().velocity.y, 2f)) + (Mathf.Pow(handLeft.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().velocity.z, 2f)));
						float speedRight = Mathf.Sqrt((Mathf.Pow(handRight.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().velocity.x, 2f)) + (Mathf.Pow(handRight.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().velocity.y, 2f)) + (Mathf.Pow(handRight.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().velocity.z, 2f)));

						float setpoint = Mathf.Sqrt(Mathf.Pow(0.05f, 2f) + Mathf.Pow(0.63f, 2f) + Mathf.Pow(0.335f, 2f));
						float MJpos =  posRight + (setpoint - posRight);
			    		writer.Write(";" + dataNames[i]);
			    		// writer.Write(";" + posLeft + ";" + speedLeft + ";" + handLeft.transform.GetChild(i).gameObject.GetComponent<Transform>().position.x.ToString("F4") + ";" + handLeft.transform.GetChild(i).gameObject.GetComponent<Transform>().position.y.ToString("F4") + ";" + handLeft.transform.GetChild(i).gameObject.GetComponent<Transform>().position.z.ToString("F4") + ";" + handLeft.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().velocity.x.ToString("F4") + ";" + handLeft.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().velocity.y.ToString("F4") + ";" + handLeft.transform.GetChild(i).GetComponent<Rigidbody>().velocity.z.ToString("F4"));
			    		writer.Write(";" + posRight + ";" + speedRight + ";" + handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().position.x.ToString("F4") + ";" + handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().position.y.ToString("F4") + ";" + handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().position.z.ToString("F4") + ";" + handRight.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().velocity.x.ToString("F4") + ";" + handRight.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().velocity.y.ToString("F4") + ";" + handRight.transform.GetChild(i).GetComponent<Rigidbody>().velocity.z.ToString("F4"));
			    	}
				}
				else
				{
					for(int i = 0; i < 19; i++)
			    	{
			    		float angleInDegrees;
			    		Vector3 rotationAxis;

		    			handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().rotation.ToAngleAxis(out angleInDegrees, out rotationAxis);

		    			Vector3 angularDisplacement = rotationAxis*angleInDegrees * Mathf.PI/180f;
		    			Vector3 angularSpeed = angularDisplacement / Time.deltaTime;
		    			Vector3 relativeAngularSpeed = handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().rotation * (angularDisplacement/Time.deltaTime);
			   //  		float tangentRight = Vector3.Dot(handRight.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().angularVelocity, handRight.transform.GetChild(i).gameObject.transform.right);
						// float tangentUp = Vector3.Dot(handRight.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().angularVelocity, handRight.transform.GetChild(i).gameObject.transform.up);
						// float tangentForward = Vector3.Dot(handRight.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().angularVelocity, handRight.transform.GetChild(i).gameObject.transform.forward);
						// float tangentSpeed = handRight.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().angularVelocity.magnitude;

						// float tangentRight = Vector3.Dot(handRight.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().angularVelocity, handRight.transform.GetChild(i).gameObject.transform.right);
						// float tangentUp = Vector3.Dot(handRight.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().angularVelocity, handRight.transform.GetChild(i).gameObject.transform.up);
						// float tangentForward = Vector3.Dot(handRight.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().angularVelocity, handRight.transform.GetChild(i).gameObject.transform.forward);
						// float tangentSpeed = handRight.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().angularVelocity.magnitude;

		    			float angularDisplacement_magnitude = angularDisplacement.magnitude;
		    			float angularSpeed_magnitude = angularSpeed.magnitude;
		    			float relativeAngularSpeed_magnitude = relativeAngularSpeed.magnitude;

						// float tangentRight = Vector3.Dot(handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().position, handRight.transform.GetChild(i).gameObject.transform.right);
						// float tangentUp = Vector3.Dot(handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().position, handRight.transform.GetChild(i).gameObject.transform.up);
						// float tangentForward = Vector3.Dot(handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().position, handRight.transform.GetChild(i).gameObject.transform.forward);
						// float tangentSpeed = handRight.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().angularVelocity.magnitude;

						// float tangentRight = handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().position.x;
						// float tangentUp = handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().position.y;
						// float tangentForward = handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().position.z;
						// float tangentSpeed = handRight.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().angularVelocity.magnitude;

		    			float tangentForward = Vector3.Dot(handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().position, new Vector3(-0.2f, 0.73f, 0.3f));

			    		writer.Write(";" + dataNames[i]);
			    		// writer.Write(";" + tangentSpeed.ToString("F4") + ";" + tangentRight.ToString("F4") + ";" + tangentUp.ToString("F4") + ";" + tangentForward.ToString("F4"));
			    		writer.Write(";" + angularDisplacement_magnitude.ToString("F4") + ";" + angularSpeed_magnitude.ToString("F4") + ";" + relativeAngularSpeed_magnitude.ToString("F4") + ";" + tangentForward.ToString("F4"));

			    		// writer.Write(";" + handRight.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().angularVelocity.x.ToString("F4") + ";" + handRight.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().angularVelocity.y.ToString("F4") + ";" + handRight.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().angularVelocity.z.ToString("F4"));
			    		// writer.Write(";" + handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().eulerAngles.x.ToString("F4") + ";" + handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().eulerAngles.y.ToString("F4") + ";" + handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().eulerAngles.z.ToString("F4"));
			    		writer.Write(";" + angularDisplacement.x.ToString("F4") + ";" + angularDisplacement.y.ToString("F4") + ";" + angularDisplacement.z.ToString("F4"));

			    	}
				}
		    	writer.Close();
			break;
		}
    }
}
