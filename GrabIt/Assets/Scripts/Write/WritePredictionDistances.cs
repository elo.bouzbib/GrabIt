﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System.IO;
using System.Globalization;

public class WritePredictionDistances : MonoBehaviour
{
    private static float time0;
	private string path, fileName;
	private string[] dataNames;
	private GameObject handLeft, handRight;
	public SimulationHand fromCollection;
	public SimulationObjOfInterest ooiCollection;
	private StreamWriter writer;
	public int state = 0;

	public PredictPosition predictPos;



    // Start is called before the first frame update
    void Start()
    {
        handLeft = GameObject.Find("LeftHandColl");
		handRight = GameObject.Find("RightHandColl");
		fromCollection = FindObjectOfType<SimulationHand>();
		ooiCollection = FindObjectOfType<SimulationObjOfInterest>();
		fileName = fromCollection.FileName;
		time0 = Time.time;
		dataNames = new string[19];
		path = "Assets/Resources/DataSimulation/Distances/" + fileName + ".csv";
		predictPos = FindObjectOfType<PredictPosition>();
        
        
    }

    // Update is called once per frame
    void Update()
    {
        writer = new StreamWriter(path, true);

        switch(state)
		{
			case 0:
					//ONLY ONE HAND
					writer.WriteLine("Time;NbBloc;Config;Grasp;BoneID;Name;Position;ClosestPosition;RealMinDistance;Proj;Error;AngleOnGoingObj;BoneID;Name;Position;ClosestPosition;RealMinDistance;Proj;Error;AngleOnGoingObj;BoneID;Name;Position;ClosestPosition;RealMinDistance;Proj;Error;AngleOnGoingObj;BoneID;Name;Position;ClosestPosition;RealMinDistance;Proj;Error;AngleOnGoingObj;BoneID;Name;Position;ClosestPosition;RealMinDistance;Proj;Error;AngleOnGoingObj;BoneID;Name;Position;ClosestPosition;RealMinDistance;Proj;Error;AngleOnGoingObj;BoneID;Name;Position;ClosestPosition;RealMinDistance;Proj;Error;AngleOnGoingObj;BoneID;Name;Position;ClosestPosition;RealMinDistance;Proj;Error;AngleOnGoingObj;BoneID;Name;Position;ClosestPosition;RealMinDistance;Proj;Error;AngleOnGoingObj;BoneID;Name;Position;ClosestPosition;RealMinDistance;Proj;Error;AngleOnGoingObj;BoneID;Name;Position;ClosestPosition;RealMinDistance;Proj;Error;AngleOnGoingObj;BoneID;Name;Position;ClosestPosition;RealMinDistance;Proj;Error;AngleOnGoingObj;BoneID;Name;Position;ClosestPosition;RealMinDistance;Proj;Error;AngleOnGoingObj;BoneID;Name;Position;ClosestPosition;RealMinDistance;Proj;Error;AngleOnGoingObj;BoneID;Name;Position;ClosestPosition;RealMinDistance;Proj;Error;AngleOnGoingObj;BoneID;Name;Position;ClosestPosition;RealMinDistance;Proj;Error;AngleOnGoingObj;BoneID;Name;Position;ClosestPosition;RealMinDistance;Proj;Error;AngleOnGoingObj;BoneID;Name;Position;ClosestPosition;RealMinDistance;Proj;Error;AngleOnGoingObj;BoneID;Name;Position;ClosestPosition;RealMinDistance;Proj;Error;AngleOnGoingObj");

					for(int i = 0; i < 19; i++)
			    	{
			    		dataNames[i] = handLeft.transform.GetChild(i).name;
			    		// writer.Write(";BoneID;Name;LeftPos;LeftSpeed;LeftPosX;LeftPosY;LeftPosZ;LeftVelX;LeftVelY;LeftVelZ;RightPos;RightSpeed;RightPosX;RightPosY;RightPosZ;RightVelX;RightVelY;RightVelZ");
			    	}
					writer.Close();
					state = 1;
				
			break;

			case 1:
				writer.WriteLine();				
// ADDED GRASP BOOL
				writer.Write((Time.unscaledTime - time0) + ";" + ooiCollection.nbBlocFromData + ";" + ooiCollection.config + ";" + ooiCollection.grasp);
// ;BoneID;Name;Position;ClosestPosition;RealMinDistance;ProjMinDistOnGoingVector;ErrorProj
				for(int i = 0; i < 19; i++)
		    	{
					float posRight = Mathf.Sqrt((Mathf.Pow(handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().position.x, 2f)) + (Mathf.Pow(handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().position.y, 2f)) + (Mathf.Pow(handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().position.z, 2f)));
			    	
		    		writer.Write(";" + dataNames[i]);
		    		// writer.Write(";" + posLeft + ";" + speedLeft + ";" + handLeft.transform.GetChild(i).gameObject.GetComponent<Transform>().position.x.ToString("F4") + ";" + handLeft.transform.GetChild(i).gameObject.GetComponent<Transform>().position.y.ToString("F4") + ";" + handLeft.transform.GetChild(i).gameObject.GetComponent<Transform>().position.z.ToString("F4") + ";" + handLeft.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().velocity.x.ToString("F4") + ";" + handLeft.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().velocity.y.ToString("F4") + ";" + handLeft.transform.GetChild(i).GetComponent<Rigidbody>().velocity.z.ToString("F4"));
		    		writer.Write(";" + posRight.ToString("F4") + ";" + predictPos.closest[i].closest_distance.magnitude.ToString("F4") + ";" + predictPos.closest[i].REAL_MIN_DISTANCE.ToString("F4") + ";" + predictPos.projection[i].magnitude.ToString("F4") + ";" + predictPos.error[i].ToString("F4") + ";" + predictPos.onGoingAngle[i].ToString("F4"));
		    	}
				
				
		    	writer.Close();
			break;
		}
    }
}
