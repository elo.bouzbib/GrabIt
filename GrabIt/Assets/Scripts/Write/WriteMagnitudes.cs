﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System.IO;
using System.Globalization;

public class WriteMagnitudes : MonoBehaviour
{
	private static float time0, time1;
	private string path, fileName;
	private string[] dataNames, nameSkel;
	private GameObject handCollLeft, handCollRight;
	private OVRCustomSkeleton handSkelLeft, handSkelRight;
	public SimulationHand fromCollection;
	public SimulationObjOfInterest ooiCollection;
	private StreamWriter writer;
	public int state = 0;

	public PredictPosition predictPos;
	public AlgoHints algoHint;

    private GameObject palm, thumb, index;

    // WRITE: new bools inZone_fromPlane, inZone_fromProbsPred, oneContact, clampGrip, touchedBy, probsTouchedBy (predictPos)
    // WRITE: new bools fromTop, fromSideX, fromSideZ (CollisionPlanes - 3planes) 
    // Start is called before the first frame update
    void Start()
    {
    	handCollLeft = GameObject.Find("LeftHandColl");
		handCollRight = GameObject.Find("RightHandColl");
		fromCollection = FindObjectOfType<SimulationHand>();
		ooiCollection = FindObjectOfType<SimulationObjOfInterest>();
		fileName = fromCollection.FileName;
		time0 = Time.time;
		time1 = Time.time;
		dataNames = new string[19];
		nameSkel = new string[19];
		path = "Assets/Resources/DataSimulation/AlgoHints/" + fileName + "-Magnitudes.csv";
		predictPos = FindObjectOfType<PredictPosition>();
        algoHint = FindObjectOfType<AlgoHints>();
        handSkelLeft = GameObject.Find("OVRCustomHandPrefab_L").GetComponent<OVRCustomSkeleton>();
        handSkelRight = GameObject.Find("OVRCustomHandPrefab_R").GetComponent<OVRCustomSkeleton>();
        palm = GameObject.Find("r_palm_center_marker");
        index = GameObject.Find("r_index_finger_pad_marker");
        thumb = GameObject.Find("r_thumb_finger_pad_marker");
        
    }

    // Update is called once per frame
    void Update()
    {
        writer = new StreamWriter(path, true);

        switch(state)
		{
			case 0:
				for(int i = 0; i < 19; i++)
		    	{
		    		dataNames[i] = handCollLeft.transform.GetChild(i).name;
		    		nameSkel[i] = handSkelLeft.CustomBones[i].name;		    		
		    	}
					//ONLY ONE HAND
				writer.Write("Time;Time0;Config;NbBloc;Grasp;PosSkelThumb;DebugProbsA;ProbsA;PosSkelIndex;DebugProbsB;ProbsB;Dot(T/P);Dot(T/I)");
				// FINISH WITHOUT ;
				writer.Write(";BoneID;Name;CollPos;PosPredict;ClosestDist;ProjFromTraj;PhalanxContact");
	    		writer.Write(";SkelID;PosSkel;ProjOnPlane0;ProbsPositionPlane0;ProjOnPlane1;ProbsPositionPlane1;ProjOnPlane2;ProbsPositionPlane2");
	    		writer.Write(";BoneID;Name;CollPos;PosPredict;ClosestDist;ProjFromTraj;PhalanxContact");
				writer.Write(";SkelID;PosSkel;ProjOnPlane0;ProbsPositionPlane0;ProjOnPlane1;ProbsPositionPlane1;ProjOnPlane2;ProbsPositionPlane2");
				writer.Close();
				state = 1;
				
			break;

			case 1:
				writer.WriteLine();				

				if(ooiCollection.changeConfig)
				{
					time0 = Time.unscaledTime;
				}
				
				writer.Write((Time.unscaledTime - time1) + ";" + (Time.unscaledTime - time0) + ";" + ooiCollection.config + ";" + ooiCollection.nbBlocFromData + ";" + ooiCollection.grasp);
				writer.Write(";" + handSkelRight.CustomBones[5].transform.position.magnitude + ";" + algoHint.debugProbsA.magnitude + ";" + algoHint.probsA.magnitude + ";" + handSkelRight.CustomBones[8].transform.position.magnitude + ";" + algoHint.debugProbsB.magnitude + ";" + algoHint.probsB.magnitude);
				writer.Write(";" + Vector3.Dot(thumb.transform.right.normalized, palm.transform.right.normalized).ToString("F4") + ";" + Vector3.Dot(thumb.transform.right.normalized, index.transform.right.normalized).ToString("F4"));
				writer.Write(";" + dataNames[6]);
	    		writer.Write(";" + handCollRight.transform.GetChild(6).transform.position.magnitude.ToString("F4") + ";" + predictPos.posToPredict[6].magnitude.ToString("F4") + ";" + predictPos.closest[6].closest_distance.magnitude.ToString("F4") + ";" + predictPos.projection[6].magnitude.ToString("F4") + ";" + predictPos.contactPerPhal[6]);	
	    		writer.Write(";" + nameSkel[5] + ";" + handSkelRight.CustomBones[5].transform.position.magnitude + ";" + algoHint.plane[2].GetComponent<CollisionPlanes>().projPerPhalOnOOI[5].magnitude.ToString("F4") + ";" + algoHint.probsP[5].magnitude.ToString("F4") + ";" + algoHint.plane[1].GetComponent<CollisionPlanes>().projPerPhalOnOOI[5].magnitude.ToString("F4") + ";" + algoHint.probsQ[5].magnitude.ToString("F4") + ";" + algoHint.plane[0].GetComponent<CollisionPlanes>().projPerPhalOnOOI[5].magnitude.ToString("F4") + ";" + algoHint.probsR[5].magnitude.ToString("F4"));// + ";" + algoHint.projPhalOnPlanes[i*2].magnitude.ToString("F4") + ";" + algoHint.probsP[i*2].magnitude.ToString("F4") + ";" + algoHint.errorPerPhal[i*2].ToString("F4"));
	   	    	writer.Write(";" + dataNames[9]);
	    		writer.Write(";" + handCollRight.transform.GetChild(9).transform.position.magnitude.ToString("F4") + ";" + predictPos.posToPredict[9].magnitude.ToString("F4") + ";" + predictPos.closest[9].closest_distance.magnitude.ToString("F4") + ";" + predictPos.projection[9].magnitude.ToString("F4") + ";" + predictPos.contactPerPhal[9]);	
	    		writer.Write(";" + nameSkel[8] + ";" + handSkelRight.CustomBones[8].transform.position.magnitude + ";" + algoHint.plane[2].GetComponent<CollisionPlanes>().projPerPhalOnOOI[8].magnitude.ToString("F4") + ";" + algoHint.probsP[8].magnitude.ToString("F4") + ";" + algoHint.plane[1].GetComponent<CollisionPlanes>().projPerPhalOnOOI[8].magnitude.ToString("F4") + ";" + algoHint.probsQ[8].magnitude.ToString("F4") + ";" + algoHint.plane[0].GetComponent<CollisionPlanes>().projPerPhalOnOOI[8].magnitude.ToString("F4") + ";" + algoHint.probsR[8].magnitude.ToString("F4"));// + ";" + algoHint.projPhalOnPlanes[i*2].magnitude.ToString("F4") + ";" + algoHint.probsP[i*2].magnitude.ToString("F4") + ";" + algoHint.errorPerPhal[i*2].ToString("F4"));

		    	writer.Close();
			break;
		}
    
    }
    
}
