﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System.IO;
using System.Globalization;


public class WritePredictionStop_Simulation : MonoBehaviour
{
	private static float time0;
	private string path, fileName;
	private StreamWriter writer;
	private int state = 0;

	private GameObject handCollRight, handCollLeft;
	public PredictionStop predictionStop;
	public PredictionStopL predictionStopL;
	public SimulationObjOfInterest_withKids objectInstance;
	public SimulationHand fromCollection;

    public string addWord;



    void Start()
    {
		fromCollection = FindObjectOfType<SimulationHand>();
    	objectInstance = FindObjectOfType<SimulationObjOfInterest_withKids>();
    	predictionStop = GameObject.FindObjectOfType<PredictionStop>();
    	predictionStopL = GameObject.FindObjectOfType<PredictionStopL>();
		fileName = fromCollection.FileName;

		
        time0 = Time.time;

		handCollRight = GameObject.Find("RightHandColl");
		handCollLeft = GameObject.Find("LeftHandColl");
		path = "Assets/Resources/DataSimulation/AlgoHints/G-" + fromCollection.gamma + "-B-" + fromCollection.beta + "-" + addWord + fileName + ".csv";

    }

    // Update is called once per frame
    void Update()
    {
		path = "Assets/Resources/DataSimulation/AlgoHints/G-" + fromCollection.gamma + "-B-" + fromCollection.beta + "-" + addWord + fileName + ".csv";
        writer = new StreamWriter(path, true);

        if(fromCollection.startAgain)
        {
        	state = 0;
        }


    	switch(state)
		{
			case 0:

				writer.Write("Time;Beta;Gamma;Config;NbBloc;Grasp;TimePrediction;StopPredBool;TimePredictionL;StopPredBoolL");

				for(int i = 0; i < 19; i++)
				{
					writer.Write(";BoneID;BoneName;BonePos;PosPred0;PosPred1;PosPred2;DistToPred0;DistToPred1;DistToPred2;BoneRadius;BoneContact;ClosestPoint;DistToClosest");
					writer.Write(";BoneIDL;BoneNameL;BonePosL;PosPredL0;PosPredL1;PosPredL2;DistToPred0L;DistToPred1L;DistToPred2L;BoneRadiusL;BoneContactL;ClosestPointL;DistToClosestL");
				}
				writer.Close();

				state = 1;
			break;

// WRITE OBJECTS OF INTEREST / REMOVE RB/COLLIDER AND ADD ROT/POS?
			case 1:
				writer.WriteLine();		
				
				if(fromCollection.frame > 3)
				{
					writer.Write(fromCollection.timeRead + ";" + fromCollection.beta + ";" + fromCollection.gamma + ";" + objectInstance.config + ";" + objectInstance.nbBloc + ";" + objectInstance.grasp + ";" + predictionStop.timePrediction.ToString() + ";" + predictionStop.stopPredictions + ";" + predictionStopL.timePrediction.ToString() + ";" + predictionStopL.stopPredictions);
				
					for(int i = 0; i < 19 ; i++)
			    	{
						writer.Write(";" + predictionStop.phalanxPrediction[i].namePhalanx + ";" + handCollRight.transform.GetChild(i).gameObject.transform.position.magnitude.ToString() + ";" + predictionStop.phalanxPrediction[i].posPred.ToString() + ";" + predictionStop.phalanxPrediction[i].posPred1.ToString() + ";" + predictionStop.phalanxPrediction[i].posPred2.ToString() +";" + predictionStop.phalanxPrediction[i].distToPred.ToString() + ";" + predictionStop.phalanxPrediction[i].distToPred1.ToString() + ";" + predictionStop.phalanxPrediction[i].distToPred2.ToString() + ";" + handCollRight.transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().radius.ToString() + ";" + predictionStop.phalanxPrediction[i].phalanxContact + ";" + predictionStop.phalanxPrediction[i].closestPoint.ToString() + ";" + predictionStop.phalanxPrediction[i].distToClosest.ToString());
						writer.Write(";" + predictionStopL.phalanxPrediction[i].namePhalanx + ";" + handCollLeft.transform.GetChild(i).gameObject.transform.position.magnitude.ToString() + ";" + predictionStopL.phalanxPrediction[i].posPred.ToString() + ";" + predictionStopL.phalanxPrediction[i].posPred1.ToString() + ";" + predictionStopL.phalanxPrediction[i].posPred2.ToString() + ";" + predictionStopL.phalanxPrediction[i].distToPred.ToString() + ";" + predictionStopL.phalanxPrediction[i].distToPred1.ToString() + ";" + predictionStopL.phalanxPrediction[i].distToPred2.ToString() + ";" + handCollLeft.transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().radius.ToString() + ";" + predictionStopL.phalanxPrediction[i].phalanxContact + ";" + predictionStopL.phalanxPrediction[i].closestPoint.ToString() + ";" + predictionStopL.phalanxPrediction[i].distToClosest.ToString());
					}
				}
				

		    	writer.Close();
			break;
		}

	}


}
