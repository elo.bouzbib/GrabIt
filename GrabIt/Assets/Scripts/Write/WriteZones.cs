﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System.IO;
using System.Globalization;


public class WriteZones : MonoBehaviour
{
	private static float time0;
	private string path, fileName;
	private StreamWriter writer;
	private int state = 0;

	private GameObject handCollRight, handCollLeft;
	public PredictionStop predictionStop;
	public PredictionStopL predictionStopL;
	public SimulationObjOfInterest_withKids objectInstance;
	public SimulationHand fromCollection;
	public Predict_Live predictLive;

    public string addWord;

    void Start()
    {
		fromCollection = FindObjectOfType<SimulationHand>();
    	objectInstance = FindObjectOfType<SimulationObjOfInterest_withKids>();
    	predictionStop = GameObject.FindObjectOfType<PredictionStop>();
    	predictionStopL = GameObject.FindObjectOfType<PredictionStopL>();
    	predictLive = GameObject.FindObjectOfType<Predict_Live>();

		fileName = fromCollection.FileName;

		
        time0 = Time.time;

		handCollRight = GameObject.Find("RightHandColl");
		handCollLeft = GameObject.Find("LeftHandColl");
		path = "Assets/Resources/DataSimulation/AlgoHints/Zones-G-" + fromCollection.gamma + "-B-" + fromCollection.beta + "-" + addWord + fileName + ".csv";

    }

    // Update is called once per frame
    void Update()
    {

		path = "Assets/Resources/DataSimulation/AlgoHints/Zones-G-" + fromCollection.gamma + "-B-" + fromCollection.beta + "-" + addWord + fileName + ".csv";
        writer = new StreamWriter(path, true);

        if(fromCollection.startAgain)
        {
        	state = 0;
        }

    	switch(state)
		{
			case 0:
				// fileName = fromCollection.FileName;

				writer.Write("Time;Beta;Gamma;Config;NbBloc;Grasp");

				for(int i = 0; i < 19; i++)
				{
					writer.Write(";BoneID;BoneName;Speed;ZonePred;ZonePredScale;ZonePredMesh;BoneContact;realZone;realZoneScale;realZoneMesh");
					writer.Write(";BoneIDL;BoneNameL;SpeedL;ZonePredL;ZonePredScaleL;ZonePredMeshL;BoneContactL;realZoneL;realZoneScaleL;realZoneMeshL");
				}
				writer.Close();

				state = 1;
			break;

			case 1:
				writer.WriteLine();		

				if(fromCollection.frame > 3)
				{
					writer.Write(fromCollection.timeRead + ";" + fromCollection.beta + ";" + fromCollection.gamma + ";" + objectInstance.config + ";" + objectInstance.nbBloc + ";" + objectInstance.grasp);				

					for(int i = 0; i < 19 ; i++)
			    	{
						writer.Write(";" + predictionStop.phalanxPrediction[i].namePhalanx + ";" + predictLive.onGoingVector[i].magnitude.ToString("F2") + ";" + predictLive.namePred[i] + ";" + predictLive.scalePred[i] + ";" + predictLive.meshPred[i] + ";" + predictionStop.phalanxPrediction[i].phalanxContact + ";" + predictLive.name[i] + ";" + predictLive.scale[i] + ";" + predictLive.mesh[i]);
						writer.Write(";" + predictionStopL.phalanxPrediction[i].namePhalanx + ";" + predictLive.onGoingVector[i+19].magnitude.ToString("F2") + ";" + predictLive.namePred[i+19] + ";" + predictLive.scalePred[i+19] + ";" + predictLive.meshPred[i+19] + ";" + predictionStop.phalanxPrediction[i].phalanxContact + ";" + predictLive.name[i+19] + ";" + predictLive.scale[i+19] + ";" + predictLive.mesh[i+19]);
					}
				}
				
		    	writer.Close();
			break;
		}

	}


}
