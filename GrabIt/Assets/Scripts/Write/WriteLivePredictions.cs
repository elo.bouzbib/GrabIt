﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System.IO;
using System.Globalization;


public class WriteLivePredictions : MonoBehaviour
{
	private static float time0, time2;
	private string path, time1;
	private StreamWriter writer;
	private int state = 0;

	public Instantiate_Live objectInstance;

	private string pathME;
	private string[] dataNames, nameSkelL, nameSkel;
	private GameObject handCollLeft, handCollRight;
	private OVRCustomSkeleton handSkelLeft, handSkelRight;
	
	private StreamWriter writerNames;

	public Predict_Live predictPos;
	public AlgoHints_Live algoHint;

    void Start()
    {
    	objectInstance = FindObjectOfType<Instantiate_Live>();
		
        time0 = Time.time;
        time2 = Time.time;
		time1 = System.DateTime.Now.ToString("ddMMyyyy-HHmm");

		handCollLeft = GameObject.Find("LeftHandColl");
		handCollRight = GameObject.Find("RightHandColl");
		dataNames = new string[19];
		nameSkel = new string[19];
		nameSkelL = new string[19];

		predictPos = FindObjectOfType<Predict_Live>();
        algoHint = FindObjectOfType<AlgoHints_Live>();
        handSkelLeft = GameObject.Find("OVRCustomHandPrefab_L").GetComponent<OVRCustomSkeleton>();
        handSkelRight = GameObject.Find("OVRCustomHandPrefab_R").GetComponent<OVRCustomSkeleton>();
        
    }

    // Update is called once per frame
    void Update()
    {
        path = $"/mnt/sdcard/" + time1 + "-predictions.csv";
		pathME = $"/mnt/sdcard/" + time1 + "-TouchPhalanx.csv";

        writer = new StreamWriter(path, true);
        writerNames = new StreamWriter(pathME, true);
    	objectInstance = FindObjectOfType<Instantiate_Live>();

        dataNames = new string[19];
        nameSkel = new string[19];
        nameSkelL = new string[19];

    switch(state)
		{
			case 0:
				for(int i = 0; i < 19; i++)
		    	{
		    		dataNames[i] = handCollLeft.transform.GetChild(i).name;
		    		nameSkel[i] = handSkelRight.CustomBones[i].name;		    		
		    		nameSkelL[i] = handSkelLeft.CustomBones[i].name;		    		

		    	}
				writer.Write("Time;Time0;Config;NbBloc;Grasp");
				writerNames.Write("Time;Time0;Config;NbBloc;Grasp;alpha;nbFutureTouch");
				
				writer.Write(";nbFutureTouch;oneContact;clampGrip");
				writer.Write(";fromTop;fromSideX;fromSideZ");

				for(int i = 0; i < 19; i++)
				{
					writer.Write(";PosSkel;debugProbsP;ProbsP;debugProbsQ;ProbsQ;debugProbsR;ProbsR;PredictPosR;ClosestDist;PhalanxContact;PosSkelL;debugProbsS;ProbsS;debugProbsT;ProbsT;debugProbsU;ProbsU;PredictPosL;ClosestDistL;PhalanxContactL");
				}
				writer.Close();
				writerNames.Close();
				state = 1;
			break;

// WRITE OBJECTS OF INTEREST / REMOVE RB/COLLIDER AND ADD ROT/POS?
			case 1:
				writer.WriteLine();		
				writerNames.WriteLine();		

				if(objectInstance.state != 1)
				{
					time0 = Time.unscaledTime;
				}
				
				writer.Write((Time.unscaledTime - time2) + ";" + (Time.unscaledTime - time0) + ";" + objectInstance.config + ";" + objectInstance.nbBloc + ";" + objectInstance.grasp);

				// writerNames.Write((Time.unscaledTime - time2) + ";" + (Time.unscaledTime - time0) + ";" + objectInstance.config + ";" + objectInstance.nbBloc + ";" + objectInstance.grasp);
				// writerNames.Write(";" + algoHint.gonnaTouch.Count);

				// if(algoHint.gonnaTouch.Count != 0)
				// {
				// 	for(int k = 0; k < algoHint.gonnaTouch.Count; k++)
				// 	{
				// 		writerNames.Write(";" + algoHint.gonnaTouch[k]);
				// 	}

				// }
				// writer.Write(";" + algoHint.gonnaTouch.Count + ";" + predictPos.oneContact + ";" + predictPos.clampGrip);
				// writer.Write(";" + algoHint.plane[0].GetComponent<CollisionPlanes_withKids>().fromTop + ";" + algoHint.plane[0].GetComponent<CollisionPlanes_withKids>().fromSideX + ";" + algoHint.plane[0].GetComponent<CollisionPlanes_withKids>().fromSideZ);
				
				// for(int i = 0; i < 19; i++)
				// {
				// 	writer.Write(";" + handSkelRight.CustomBones[i].transform.position.magnitude.ToString("F4") + ";" + algoHint.debugProbsP[i].magnitude.ToString("F4") + ";" + algoHint.probsP[i].magnitude.ToString("F4") + ";" + algoHint.debugProbsQ[i].magnitude.ToString("F4") + ";" + algoHint.probsQ[i].magnitude.ToString("F4") + ";" + algoHint.debugProbsR[i].magnitude.ToString("F4") + ";" + algoHint.probsR[i].magnitude.ToString("F4") + ";" + predictPos.posToPredict[i].magnitude.ToString("F4") + ";" + predictPos.closest[i].closest_distance.magnitude.ToString("F4") + ";" + predictPos.contactPerPhal[i]);
				// 	writer.Write(";" + handSkelLeft.CustomBones[i].transform.position.magnitude.ToString("F4") + ";" + algoHint.debugProbsS[i].magnitude.ToString("F4") + ";" + algoHint.probsS[i].magnitude.ToString("F4") + ";" + algoHint.debugProbsT[i].magnitude.ToString("F4") + ";" + algoHint.probsT[i].magnitude.ToString("F4") + ";" + algoHint.debugProbsU[i].magnitude.ToString("F4") + ";" + algoHint.probsU[i].magnitude.ToString("F4") + ";" + predictPos.posToPredict[i+19].magnitude.ToString("F4") + ";" + predictPos.closest[i+19].closest_distance.magnitude.ToString("F4") + ";" + predictPos.contactPerPhal[i+19]);
					
				// }

		    	writer.Close();
		    	writerNames.Close();
			break;
		}

	}


}
