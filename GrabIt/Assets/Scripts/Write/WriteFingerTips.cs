﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Globalization;

public class WriteFingerTips : MonoBehaviour
{
private static float time0;
	private string path, fileName;
	private string[] dataNames;
	private GameObject handLeft, handRight;
	public SimulationHand fromCollection;
	private StreamWriter writer;
	public int state = 0;

	public PredictPosition predictPos;
	public SimulationObjOfInterest ooiCollection;



    // Start is called before the first frame update
    void Start()
    {
        handLeft = GameObject.Find("LeftHandColl");
		handRight = GameObject.Find("RightHandColl");
		fromCollection = FindObjectOfType<SimulationHand>();
		fileName = fromCollection.FileName;
		time0 = Time.time;
		dataNames = new string[19];
		path = "Assets/Resources/DataSimulation/Projections/" + fileName + "-SphereErrors-FingerTips.csv";
		predictPos = FindObjectOfType<PredictPosition>();
        ooiCollection = FindObjectOfType<SimulationObjOfInterest>();
        
    }

    // Update is called once per frame
    void Update()
    {
        writer = new StreamWriter(path, true);

        switch(state)
		{
			case 0:
					//ONLY ONE HAND
			// Time, Config, NbBloc, VF, VFMagnitude, ProjVFX, ProjVFY, ProjVFZ, ID, Name,
				// POS, ThumbTarget, ProjThumb, ErrorThumb, Closest, Proj, Error, IndexTarget, ProjIndex, ErrorIndex
					writer.Write("Time;Config;NbBloc;Grasp;VF;VFMagnitude;ProjVFX;ProjVFY;ProjVFZ");
					// writer.Write("BoneID;Name;Position;ThumbTarget;ProjThumb;ErrorThumb;ClosestPosition;ProjClosest;ErrorClosest;IndexTarget;ProjIndex;ErrorIndex");

					for(int i = 0; i < 19; i++)
			    	{
			    		dataNames[i] = handLeft.transform.GetChild(i).name;
			    		writer.Write(";BoneID;Name;Position;ThumbTarget;ProjThumb;ErrorThumb;ClosestPosition;ProjClosest;ErrorClosest;IndexTarget;ProjIndex;ErrorIndex;MiddleTarget;ProjMiddle;ErrorMiddle");//;ErrorSphere1;ErrorSphere2;ErrorSphere3;ErrorSphere4");
			    	}
					writer.Close();
					state = 1;
				
			break;

			case 1:
				writer.WriteLine();				

				if(ooiCollection.changeConfig)
				{
					time0 = Time.unscaledTime;
				}

				writer.Write((Time.unscaledTime - time0) + ";" + ooiCollection.config + ";" + ooiCollection.nbBlocFromData + ";" + ooiCollection.grasp);
				writer.Write(";" + predictPos.virtualFinger + ";" + predictPos.virtualFinger.magnitude.ToString("F4") + ";" + predictPos.projVFX.ToString("F4") + ";" + predictPos.projVFY.ToString("F4") + ";" + predictPos.projVFZ.ToString("F4"));
				for(int i = 0; i < 19; i++)
		    	{
					float posRight = Mathf.Sqrt((Mathf.Pow(handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().position.x, 2f)) + (Mathf.Pow(handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().position.y, 2f)) + (Mathf.Pow(handRight.transform.GetChild(i).gameObject.GetComponent<Transform>().position.z, 2f)));
// ADD THUMB AND INDEX TARGETS!			    	
			    	writer.Write(";" + dataNames[i]);
		    		// writer.Write(";" + posRight.ToString("F4") + ";" + predictPos.thumbTarget.magnitude.ToString("F4") + ";" + predictPos.projectionThumb[i].ToString("F4") + ";" + predictPos.errorThumb[i].ToString("F4") + ";" + predictPos.closest[i].closest_distance.magnitude.ToString("F4") + ";" + predictPos.projection[i].magnitude.ToString("F4") + ";" + predictPos.error[i].ToString("F4") + ";" + predictPos.indexTarget.magnitude.ToString("F4") + ";" + predictPos.projectionIndex[i].ToString("F4") + ";" + predictPos.errorIndex[i].ToString("F4") + ";" + predictPos.middleTarget.magnitude.ToString("F4") + ";" + predictPos.projectionMiddle[i].ToString("F4") + ";" + predictPos.errorMiddle[i].ToString("F4"));

		    		// writer.Write(";" + predictPos.errorSphere1[i].ToString("F4") + ";" + predictPos.errorSphere2[i].ToString("F4") + ";" + predictPos.errorSphere3[i].ToString("F4") + ";" + predictPos.errorSphere4[i].ToString("F4"));
			    	// if((i >= 4) && (i < 7))
			    	// { // POS, ThumbTarget, ProjThumb, ErrorThumb, Closest, Proj, Error
			    	// 	writer.Write(";" + dataNames[i]);
			    	// 	// writer.Write(";" + posLeft + ";" + speedLeft + ";" + handLeft.transform.GetChild(i).gameObject.GetComponent<Transform>().position.x.ToString("F4") + ";" + handLeft.transform.GetChild(i).gameObject.GetComponent<Transform>().position.y.ToString("F4") + ";" + handLeft.transform.GetChild(i).gameObject.GetComponent<Transform>().position.z.ToString("F4") + ";" + handLeft.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().velocity.x.ToString("F4") + ";" + handLeft.transform.GetChild(i).gameObject.GetComponent<Rigidbody>().velocity.y.ToString("F4") + ";" + handLeft.transform.GetChild(i).GetComponent<Rigidbody>().velocity.z.ToString("F4"));
			    	// 	writer.Write(";" + posRight.ToString("F4") + ";" + predictPos.thumbTarget.magnitude.ToString("F4") + ";" + predictPos.projectionThumb[i].ToString("F4") + ";" + predictPos.errorThumb[i].ToString("F4") + ";" + predictPos.closest[i].closest_distance.magnitude.ToString("F4") + ";" + predictPos.projection[i].magnitude.ToString("F4") + ";" + predictPos.error[i].ToString("F4"));
		    	
			    	// }
			    	// if(i >= 7)
			    	// {
			    	// 	writer.Write(";" + dataNames[i]);
			    	// 	writer.Write(";" + posRight.ToString("F4") + ";" + predictPos.indexTarget.magnitude.ToString("F4") + ";" + predictPos.projectionIndex[i].ToString("F4") + ";" + predictPos.errorIndex[i].ToString("F4") + ";" + predictPos.closest[i].closest_distance.magnitude.ToString("F4") + ";" + predictPos.projection[i].magnitude.ToString("F4") + ";" + predictPos.error[i].ToString("F4"));
			    	// }
		    		
		    	}
				
				
		    	writer.Close();
			break;
		}
    }
}
