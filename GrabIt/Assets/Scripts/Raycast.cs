﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycast : MonoBehaviour
{
	RaycastHit hit;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Debug.DrawRay(this.transform.position, this.transform.forward * 10, Color.green);
        // Debug.DrawLine(this.transform.position, this.transform.forward, Color.yellow);

        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit)){
        	// Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.blue);
            Debug.DrawLine(transform.position, hit.point, Color.yellow);
            if(hit.collider.gameObject.transform.parent.gameObject.tag == "ObjectOfInterest")
            {
                Debug.Log(hit.collider.gameObject.name);
            }

            // Debug.Log("Colliding");
        }
    }
}