﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Redirect_indices_coll : MonoBehaviour
{	
	// [HideInInspector]
	public GameObject rightHand, leftHand, warpedRightHand, warpedLeftHand;

	public IList<OVRBoneCapsule> Capsules { get; private set; }
	public IList<OVRBoneCapsule> CapsulesR { get; private set; }

	public IList<OVRBoneCapsule> CapsulesL_Replica { get; private set; }
	public IList<OVRBoneCapsule> CapsulesR_Replica { get; private set; }

	private Vector3 physicalTargetPosition, virtualTargetPosition;
	private float[] ds, dp;
	public Vector3[] warpVector, initialPosition, projectVector;

	public GameObject physicalTarget, virtualTarget;
	public bool startWarpR, startWarpL;

	// public ClosestToHand_Live[] phalanxData;
	// public GameObject initialPosGO;

	public Instantiate_Live gameManager;
    public bool reverse;

    public GameObject warpZone;
    private int stateLeft, stateRight = 0;
    private bool recordNow, recordNowLeft = false;


    // Start is called before the first frame update
    void Start()
    {
	    gameManager = GameObject.Find("GameManager").GetComponent<Instantiate_Live>();

        leftHand = GameObject.Find("OVRCustomHandPrefab_L"); // THIS IS HP = Hand Physical
        rightHand = GameObject.Find("OVRCustomHandPrefab_R");
        
        warpedRightHand = GameObject.Find("WarpedRightHand"); // THIS IS HV = Hand Virtual
        warpedLeftHand = GameObject.Find("WarpedLeftHand");

    	Capsules = leftHand.GetComponent<OVRCustomSkeleton>().Capsules;
		CapsulesR = rightHand.GetComponent<OVRCustomSkeleton>().Capsules;

		CapsulesL_Replica = warpedLeftHand.GetComponent<OVRCustomSkeleton>().Capsules;
		CapsulesR_Replica = warpedRightHand.GetComponent<OVRCustomSkeleton>().Capsules;

		warpVector = new Vector3[48];
		projectVector = new Vector3[48];

		physicalTarget = GameObject.FindGameObjectWithTag("PhysicalTotem"); // Look into kids to find primitives
		virtualTarget = GameObject.Find("VirtualTotem"); 

		// VIRTUAL TARGET = read from AlgoHints. Same to start Warp -> as soon as prediction starts,
		// warp can begin.

		physicalTargetPosition = physicalTarget.transform.position;
		virtualTargetPosition = virtualTarget.transform.position; // ATTENTION!! SWITCH HERE!
    	startWarpR = false;
        startWarpL = false;

    	// phalanxData = new ClosestToHand_Live[48];
		initialPosition = new Vector3[48];

		ds = new float[48];
		dp = new float[48];

    	// for(int k = 0; k < 19; k++)
    	// {
    	// 	phalanxData[k] = GameObject.Find("RightHandColl").transform.GetChild(k).gameObject.GetComponent<ClosestToHand_Live>();
     //        phalanxData[k+19] = GameObject.Find("LeftHandColl").transform.GetChild(k).gameObject.GetComponent<ClosestToHand_Live>();
     //    }

        warpZone = GameObject.FindGameObjectWithTag("WarpZone");
    }

    // Update is called once per frame
    void Update()
    {
	
        for(int i = 0; i < 24; i++)
        {
            // initialPosition[i] = GameObject.Find("StartPosition").transform.position;
            // initialPosition[i+24] = GameObject.Find("StartPosition").transform.position;

            if(warpZone.GetComponent<Collider>().bounds.Contains(rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position))
            {
                startWarpR = true;
                stateRight = -1;
            }
            else
            {
                startWarpR = false;
                stateRight = 0;
            }

            if(startWarpR)
            {
                // phalanxData[i].startWarp = true; // leftHand - phalanxData(k+24)
                // startWarp = true;
                // initialPosition[i] = phalanxData[i].initialWarpPos;

                if(reverse)
                {
                    projectVector[i] = (virtualTargetPosition - initialPosition[i]).normalized;

                    ds[i] = Vector3.Project((rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position - initialPosition[i]), projectVector[i]).magnitude;
                    dp[i] = Vector3.Project((virtualTargetPosition - rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position), projectVector[i]).magnitude;
                    warpVector[i] = (ds[i] / (ds[i] + dp[i])) * (physicalTargetPosition - virtualTargetPosition);
                }
                else
                {
                    projectVector[i] = (physicalTargetPosition - initialPosition[i]).normalized;

                    ds[i] = Vector3.Project((rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position - initialPosition[i]), projectVector[i]).magnitude;
                    dp[i] = Vector3.Project((physicalTargetPosition - rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position), projectVector[i]).magnitude;
                    
                    // ds[i] = Vector3.Project((rightHand.transform.position - initialPosition[i]), projectVector[i]).magnitude;
                    // dp[i] = Vector3.Project((physicalTargetPosition - rightHand.transform.position), projectVector[i]).magnitude;
                    
                    warpVector[i] = (ds[i] / (ds[i] + dp[i])) * (virtualTargetPosition - physicalTargetPosition);
                }
            }
            else
            {
                warpVector[i] = Vector3.zero;
                // phalanxData[i].startWarp = false;
                initialPosition[i] = new Vector3(rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.x, rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.y, rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.z);
                // startWarp = false;
            }

            if(warpZone.GetComponent<Collider>().bounds.Contains(leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position))
            {
                startWarpL = true;
                stateLeft = -1;
            }
            else
            {
                startWarpL = false;
                stateLeft = 0;
            }


            if(startWarpL)
            {
                // phalanxData[i+24].startWarp = true; // leftHand - phalanxData(k+24)
                // initialPosition[i+24] = phalanxData[i+24].initialWarpPos;

                // if(phalanxData[i+24].startWarp)
                // {
                if(reverse)
                {
                    projectVector[i+24] = (virtualTargetPosition - initialPosition[i+24]).normalized;

                    ds[i+24] = Vector3.Project((leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i+24].transform.position - initialPosition[i+24]), projectVector[i+24]).magnitude;
                    dp[i+24] = Vector3.Project((virtualTargetPosition - leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i+24].transform.position), projectVector[i+24]).magnitude;
                    warpVector[i+24] = (ds[i+24] / (ds[i+24] + dp[i+24])) * (physicalTargetPosition - virtualTargetPosition);
                
                }
                else
                {
                    projectVector[i+24] = (physicalTargetPosition - initialPosition[i+24]).normalized;

                    ds[i+24] = Vector3.Project((leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i+24].transform.position - initialPosition[i+24]), projectVector[i+24]).magnitude;
                    dp[i+24] = Vector3.Project((physicalTargetPosition - leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i+24].transform.position), projectVector[i+24]).magnitude;
                    
                    // ds[i+24] = Vector3.Project((leftHand.transform.position - initialPosition[i+24]), projectVector[i+24]).magnitude;
                    // dp[i+24] = Vector3.Project((physicalTargetPosition - leftHand.transform.position), projectVector[i+24]).magnitude;
                    
                    warpVector[i+24] = (ds[i+24] / (ds[i+24] + dp[i+24])) * (virtualTargetPosition - physicalTargetPosition);
                }
                // }
            }
            else
            {
                warpVector[i+24] = Vector3.zero;
                // phalanxData[i+24].startWarp = false;
                initialPosition[i+24] = new Vector3(leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.x, leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.y, leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.z);
                // startWarp = false;
            }

            warpedRightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position = new Vector3(rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.x + warpVector[i].x, rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.y + warpVector[i].y, rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.z + warpVector[i].z);
        	warpedRightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles = new Vector3(rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.x, rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.y, rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.z);

            warpedLeftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position = new Vector3(leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.x + warpVector[i+24].x, leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.y + warpVector[i+24].y, leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.z + warpVector[i+24].z);
        	warpedLeftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles = new Vector3(leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.x, leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.y, leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.z);
    	
			switch(stateRight)
		    {
		    	case -1:
		    		if(gameManager.state == 1)
		    		{
		    			if(!recordNow)
		    			{
		    				for(int j = 0; j < 24; j++)
		    				{
					    		initialPosition[j] = rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position;
		    				}
		    				recordNow = true;
		    			}
		    		}
		    		else
		    		{
		    			recordNow = false;
		    		}
		    	break;

		    	case 0:
		    		if((!startWarpR) && gameManager.state == 1)
		    		{
		    			recordNow = false;
		    		}
		    	break;

		    }

		    switch(stateLeft)
		    {
		    	case -1:
		    		if(gameManager.state == 1)
		    		{
		    			if(!recordNowLeft)
		    			{
		    				for(int j = 24; j < 48; j++)
		    				{
					    		initialPosition[j] = leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position;
		    				}
		    				recordNowLeft = true;
		    			}
		    		}
		    		else
		    		{
		    			recordNowLeft = false;
		    		}

		    	break;

		    	case 0:
		    		if((!startWarpL) && gameManager.state == 1)
		    		{
		    			recordNowLeft = false;
		    		}
		    	break;

		    }



    	}

    	for(int i = 0; i < 19; i++)
    	{
    		CapsulesL_Replica[i].CapsuleCollider.gameObject.transform.position = new Vector3(Capsules[i].CapsuleCollider.gameObject.transform.position.x, Capsules[i].CapsuleCollider.gameObject.transform.position.y, Capsules[i].CapsuleCollider.gameObject.transform.position.z);    		
    		CapsulesR_Replica[i].CapsuleCollider.gameObject.transform.position = new Vector3(CapsulesR[i].CapsuleCollider.gameObject.transform.position.x, CapsulesR[i].CapsuleCollider.gameObject.transform.position.y, CapsulesR[i].CapsuleCollider.gameObject.transform.position.z);

			CapsulesL_Replica[i].CapsuleCollider.gameObject.transform.eulerAngles = new Vector3(Capsules[i].CapsuleCollider.gameObject.transform.eulerAngles.x, Capsules[i].CapsuleCollider.gameObject.transform.eulerAngles.y, Capsules[i].CapsuleCollider.gameObject.transform.eulerAngles.z);
    		CapsulesR_Replica[i].CapsuleCollider.gameObject.transform.eulerAngles = new Vector3(CapsulesR[i].CapsuleCollider.gameObject.transform.eulerAngles.x, CapsulesR[i].CapsuleCollider.gameObject.transform.eulerAngles.y, CapsulesR[i].CapsuleCollider.gameObject.transform.eulerAngles.z);
        
        	CapsulesL_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().center = Capsules[i].CapsuleCollider.center;
        	CapsulesL_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().radius = Capsules[i].CapsuleCollider.radius;
        	CapsulesL_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().height = Capsules[i].CapsuleCollider.height;
        	CapsulesL_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().direction = Capsules[i].CapsuleCollider.direction;
        
        	CapsulesR_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().center = CapsulesR[i].CapsuleCollider.center;
        	CapsulesR_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().radius = CapsulesR[i].CapsuleCollider.radius;
        	CapsulesR_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().height = CapsulesR[i].CapsuleCollider.height;
        	CapsulesR_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().direction = CapsulesR[i].CapsuleCollider.direction;
    	}
    }
}

