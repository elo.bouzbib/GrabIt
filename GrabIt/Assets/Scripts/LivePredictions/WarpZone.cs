﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarpZone : MonoBehaviour
{
	public GameObject OOI;
    // Start is called before the first frame update
    void Start()
    {
        OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");
        // OOI = GameObject.Find("VirtualTotem"); 


    }

    // Update is called once per frame
    void Update()
    {
        OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");
        // OOI = GameObject.Find("VirtualTotem");

        this.transform.position = OOI.transform.position;
        this.transform.localScale = new Vector3(OOI.transform.localScale.x * 0.6f, OOI.transform.localScale.y * 1.5f, OOI.transform.localScale.z * 0.65f);
    }
}
