﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class AlgoHints_Live : MonoBehaviour
{
  	private GameObject[] childR, childL, handChildL, handChildR; 
  	private GameObject handR, handL;
    [HideInInspector]
    public ClosestToHand_Live[] closest;
    private Vector3[] onGoingVector; 
    
    [HideInInspector]
    public Vector3[] errorPerPhal;
    [HideInInspector]
    public Vector3 virtualFinger;
    private Vector3 closestDist;
    private GameObject thumb, palm, index;
    [HideInInspector]
    public Vector3 probsA, probsB, debugProbsA, debugProbsB;
    [HideInInspector]
    public Vector3[] probsQ, probsR;
    [HideInInspector]
    public Vector3[] probsP;
    public Vector3[] debugProbsQ, debugProbsR;
    [HideInInspector]
    public Vector3[] debugProbsP;
    [HideInInspector]
    public int nbOfPlanes;
    [HideInInspector]
    public GameObject[] plane;
    
    [HideInInspector]
    public Vector3[] errorPerPhalL;
    [HideInInspector]
    public Vector3 virtualFingerL;
    private GameObject thumbL, palmL, indexL;
    [HideInInspector]
    public Vector3 probsC, probsD;
    [HideInInspector]
    public Vector3[] probsS, probsT, probsU;
    [HideInInspector]
    public Vector3[] debugProbsS, debugProbsT, debugProbsU;
    [HideInInspector]
    public int nbOfPlanesL;
    [HideInInspector]
    public GameObject[] planeL;
    [HideInInspector]
    private string[] name;
    public List<string> gonnaTouch;
    private float firstThreshold;
    public bool varyEpsilon;

    void Start()
    {
      	handR = GameObject.Find("RightHandColl");
    	handL = GameObject.Find("LeftHandColl");
    	childR = new GameObject[19];
    	handChildR = new GameObject[19];
    	childL = new GameObject[19];
    	handChildL = new GameObject[19];
	    closest = new ClosestToHand_Live[38];
	    onGoingVector = new Vector3[39];

		plane = new GameObject[3];
		nbOfPlanes = GameObject.FindGameObjectWithTag("CutSections").GetComponent<CollisionPlanes_withKids>().nbOfPlanes;

		for(int i = 0; i < GameObject.FindGameObjectsWithTag("CutSections").Length; i++)
		{
            if(GameObject.FindGameObjectsWithTag("CutSections")[i].GetComponent<CollisionPlanes_withKids>().planeID == i)
            {
                plane[i] = GameObject.FindGameObjectsWithTag("CutSections")[i];
            }
		}

        planeL = new GameObject[3];
        nbOfPlanesL = GameObject.FindGameObjectWithTag("CutSectionsLeft").GetComponent<CollisionPlanes_Lefty>().nbOfPlanes;

        for(int i = 0; i < GameObject.FindGameObjectsWithTag("CutSectionsLeft").Length; i++)
        {
            if(GameObject.FindGameObjectsWithTag("CutSectionsLeft")[i].GetComponent<CollisionPlanes_Lefty>().planeID == i)
            {
                planeL[i] = GameObject.FindGameObjectsWithTag("CutSectionsLeft")[i];
            }
        }
		probsB = new Vector3();
		probsA = new Vector3();

		probsP = new Vector3[19];
		probsQ = new Vector3[19];
		probsR = new Vector3[19];
		errorPerPhal = new Vector3[19*nbOfPlanes];

        probsD = new Vector3();
        probsC = new Vector3();

        probsS = new Vector3[19];
        probsT = new Vector3[19];
        probsU = new Vector3[19];
        errorPerPhalL = new Vector3[19*nbOfPlanesL];

		debugProbsB = new Vector3();
		debugProbsA = new Vector3();

        debugProbsP = new Vector3[19];
        debugProbsQ = new Vector3[19];
        debugProbsR = new Vector3[19];
        debugProbsS = new Vector3[19];
        debugProbsT = new Vector3[19];
        debugProbsU = new Vector3[19];

		for(int i = 0; i < 19; i++)
		{
			childR[i] = this.transform.GetChild(i).transform.gameObject;
			handChildR[i] = handR.transform.GetChild(i).transform.gameObject;
			childL[i] = this.transform.GetChild(i+19).transform.gameObject;
			handChildL[i] = handL.transform.GetChild(i).transform.gameObject;
		}


		palm = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/r_palm_center_marker").gameObject;
        index = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/b_r_index1/b_r_index2/b_r_index3/r_index_finger_pad_marker").gameObject;
        thumb = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/b_r_thumb0/b_r_thumb1/b_r_thumb2/b_r_thumb3/r_thumb_finger_pad_marker").gameObject;
        

        palmL = GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L/OculusHand_L/b_l_wrist/l_palm_center_marker").gameObject;
        indexL = GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L/OculusHand_L/b_l_wrist/b_l_index1/b_l_index2/b_l_index3/l_index_finger_pad_marker").gameObject;
        thumbL = GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L/OculusHand_L/b_l_wrist/b_l_thumb0/b_l_thumb1/b_l_thumb2/b_l_thumb3/l_thumb_finger_pad_marker").gameObject;
        
        name = new string[38];
        gonnaTouch = new List<string>();

	}

    // void FixedUpdate()
    void FixedUpdate()
    {
        if(GameObject.FindObjectOfType<SimulationHand>() != null)
        {
            varyEpsilon = GameObject.FindObjectOfType<SimulationHand>().varyEpsilon;
            if(varyEpsilon)
            {
                firstThreshold = GameObject.FindObjectOfType<SimulationHand>().epsilon;
            }
            else
            {
                firstThreshold = 0.07f;
            }
        }
        else
        {
            firstThreshold = 0.07f;
        }

        // SETUP 20 FPS - 0.05 // 
		Time.fixedDeltaTime = 0.05f;

        if(GameObject.FindObjectOfType<Instantiate_Live>() != null)
        {
            if(GameObject.FindObjectOfType<Instantiate_Live>().state != 1)
            {
                probsB = new Vector3();
                probsA = new Vector3();

                probsP = new Vector3[19];
                probsQ = new Vector3[19];
                probsR = new Vector3[19];
                errorPerPhal = new Vector3[19*nbOfPlanes];

                probsD = new Vector3();
                probsC = new Vector3();

                probsS = new Vector3[19];
                probsT = new Vector3[19];
                probsU = new Vector3[19];
                errorPerPhalL = new Vector3[19*nbOfPlanesL];

                debugProbsB = new Vector3();
                debugProbsA = new Vector3();

                debugProbsP = new Vector3[19];
                debugProbsQ = new Vector3[19];
                debugProbsR = new Vector3[19];
                debugProbsS = new Vector3[19];
                debugProbsT = new Vector3[19];
                debugProbsU = new Vector3[19];

            }
        }

        if(GameObject.FindObjectOfType<SimulationObjOfInterest_withKids>() != null)
        {
            if(GameObject.FindObjectOfType<SimulationObjOfInterest_withKids>().changeConfig)
            {
                probsB = new Vector3();
                probsA = new Vector3();

                probsP = new Vector3[19];
                probsQ = new Vector3[19];
                probsR = new Vector3[19];
                errorPerPhal = new Vector3[19*nbOfPlanes];

                probsD = new Vector3();
                probsC = new Vector3();

                probsS = new Vector3[19];
                probsT = new Vector3[19];
                probsU = new Vector3[19];
                errorPerPhalL = new Vector3[19*nbOfPlanesL];

                debugProbsB = new Vector3();
                debugProbsA = new Vector3();
            }
        }
		for(int i = 0; i < GameObject.FindGameObjectsWithTag("CutSections").Length; i++)
		{
			plane[i] = GameObject.FindGameObjectsWithTag("CutSections")[i];
		}
	   	palm = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/r_palm_center_marker").gameObject;
        index = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/b_r_index1/b_r_index2/b_r_index3/r_index_finger_pad_marker").gameObject;
        thumb = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/b_r_thumb0/b_r_thumb1/b_r_thumb2/b_r_thumb3/r_thumb_finger_pad_marker").gameObject;
       
    	// NEED TO CREATE PLANE, from l to this point. GetContactPoints of VFMagnitude
    	virtualFinger = (index.transform.position - thumb.transform.position);

        for(int i = 0; i < GameObject.FindGameObjectsWithTag("CutSectionsLeft").Length; i++)
        {
            planeL[i] = GameObject.FindGameObjectsWithTag("CutSectionsLeft")[i];
        }
        palmL = GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L/OculusHand_L/b_l_wrist/l_palm_center_marker").gameObject;
        indexL = GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L/OculusHand_L/b_l_wrist/b_l_index1/b_l_index2/b_l_index3/l_index_finger_pad_marker").gameObject;
        thumbL = GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L/OculusHand_L/b_l_wrist/b_l_thumb0/b_l_thumb1/b_l_thumb2/b_l_thumb3/l_thumb_finger_pad_marker").gameObject;
        
        // NEED TO CREATE PLANE, from l to this point. GetContactPoints of VFMagnitude
        virtualFingerL = (indexL.transform.position - thumbL.transform.position);
    

        for(int k = 0; k < 19; k++)
        {
            for(int m = 0; m < 19; m++)
            {
                if(m != k)
                {
                    // RIGHT HAND
                    if(plane[2].GetComponent<Collider>().bounds.Contains(plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k]))
                    {
                        debugProbsR[k] = plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k];
                    }
                    else
                    {
                        debugProbsR[k] = new Vector3(0,0,0);
                    }
                    if( (Mathf.Abs(Vector3.Distance(plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhal[m], plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[m])) < firstThreshold/10) && (Mathf.Abs(Vector3.Distance(plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhal[k], plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k])) < firstThreshold/10 ) )
                    {
                        if( (Mathf.Abs( Vector3.Distance(plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[m], plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k]) - (plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhal[m] - plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhal[k]).magnitude) < firstThreshold*(plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhal[m] - plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhal[k]).magnitude ))
                        {
                            probsR[k] = plane[2].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k];
                            errorPerPhal[k*3] = probsR[k] - childR[k].transform.position;
                            name[k] = this.transform.GetChild(k).gameObject.name;
                            if(!gonnaTouch.Contains(name[k]))
                            {
                                gonnaTouch.Add(name[k]);
                            }
                        }
                        else
                        {
                            name[k] = "";
                            gonnaTouch.Remove(name[k]);
                            // probsP[k] = Vector3.zero;
                            // errorPerPhal[k] = Vector3.zero;
                        }

                    }
                    if(plane[1].GetComponent<Collider>().bounds.Contains(plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k]))
                    {
                        debugProbsQ[k] = plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k];
                    }
                    else
                    {
                        debugProbsQ[k] = new Vector3(0,0,0);
                    }
                    // debugProbsQ[k] = plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k];
                    if( (Mathf.Abs(Vector3.Distance(plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhal[m], plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[m])) < firstThreshold/10) && (Mathf.Abs(Vector3.Distance(plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhal[k], plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k])) < firstThreshold/10 ) )
                    {
                        if( (Mathf.Abs( Vector3.Distance(plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[m], plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k]) - (plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhal[m] - plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhal[k]).magnitude) < firstThreshold*(plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhal[m] - plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhal[k]).magnitude ))
                        {
                            probsQ[k] = plane[1].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k];
                            errorPerPhal[k*2] = probsQ[k] - childR[k].transform.position;
                            name[k] = this.transform.GetChild(k).gameObject.name;
                            if(!gonnaTouch.Contains(name[k]))
                            {
                                gonnaTouch.Add(name[k]);
                            }
                        }
                        else
                        {
                            name[k] = "";
                            gonnaTouch.Remove(name[k]);

                            // probsP[k] = Vector3.zero;
                            // errorPerPhal[k] = Vector3.zero;
                        }

                    }
                    if(plane[0].GetComponent<Collider>().bounds.Contains(plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k]))
                    {
                        debugProbsP[k] = plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k];
                    }
                    else
                    {
                        debugProbsP[k] = new Vector3(0,0,0);
                    }
                    // debugProbsP[k] = plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k];
                    if( (Mathf.Abs(Vector3.Distance(plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhal[m], plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[m])) < firstThreshold/10) && (Mathf.Abs(Vector3.Distance(plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhal[k], plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k])) < firstThreshold/10 ) )
                    {
                        if( (Mathf.Abs( Vector3.Distance(plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[m], plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k]) - (plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhal[m] - plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhal[k]).magnitude) < firstThreshold*(plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhal[m] - plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhal[k]).magnitude ))
                        {
                            probsP[k] = plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[k];
                            errorPerPhal[k] = probsP[k] - childR[k].transform.position;
                            name[k] = this.transform.GetChild(k).gameObject.name;
                            if(!gonnaTouch.Contains(name[k]))
                            {
                                gonnaTouch.Add(name[k]);
                            }
                        }
                        else
                        {
                            name[k] = "";
                            gonnaTouch.Remove(name[k]);
                            // probsP[k] = Vector3.zero;
                            // errorPerPhal[k] = Vector3.zero;
                        }

                    }


                    // LEFT HAND
                    debugProbsU[k] = planeL[2].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[k];
                    if( (Mathf.Abs(Vector3.Distance(planeL[2].GetComponent<CollisionPlanes_Lefty>().projPerPhal[m], planeL[2].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[m])) < firstThreshold/10) && (Mathf.Abs(Vector3.Distance(planeL[2].GetComponent<CollisionPlanes_Lefty>().projPerPhal[k], planeL[2].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[k])) < firstThreshold/10 ) )
                    {
                        if( (Mathf.Abs( Vector3.Distance(planeL[2].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[m], planeL[2].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[k]) - (planeL[2].GetComponent<CollisionPlanes_Lefty>().projPerPhal[m] - planeL[2].GetComponent<CollisionPlanes_Lefty>().projPerPhal[k]).magnitude) < firstThreshold*(planeL[2].GetComponent<CollisionPlanes_Lefty>().projPerPhal[m] - planeL[2].GetComponent<CollisionPlanes_Lefty>().projPerPhal[k]).magnitude ))
                        {
                            probsU[k] = planeL[2].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[k];
                            errorPerPhalL[k*3] = probsU[k] - childL[k].transform.position;
                        }
                        else
                        {
                            // probsP[k] = Vector3.zero;
                            // errorPerPhalL[k] = Vector3.zero;
                        }

                    }
                    debugProbsT[k] = planeL[1].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[k];
                    if( (Mathf.Abs(Vector3.Distance(planeL[1].GetComponent<CollisionPlanes_Lefty>().projPerPhal[m], planeL[1].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[m])) < firstThreshold/10) && (Mathf.Abs(Vector3.Distance(planeL[1].GetComponent<CollisionPlanes_Lefty>().projPerPhal[k], planeL[1].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[k])) < firstThreshold/10 ) )
                    {
                        if( (Mathf.Abs( Vector3.Distance(planeL[1].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[m], planeL[1].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[k]) - (planeL[1].GetComponent<CollisionPlanes_Lefty>().projPerPhal[m] - planeL[1].GetComponent<CollisionPlanes_Lefty>().projPerPhal[k]).magnitude) < firstThreshold*(planeL[1].GetComponent<CollisionPlanes_Lefty>().projPerPhal[m] - planeL[1].GetComponent<CollisionPlanes_Lefty>().projPerPhal[k]).magnitude ))
                        {
                            probsT[k] = planeL[1].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[k];
                            errorPerPhalL[k*2] = probsT[k] - childL[k].transform.position;
                        }
                        else
                        {
                            // probsP[k] = Vector3.zero;
                            // errorPerPhalL[k] = Vector3.zero;
                        }

                    }
                    debugProbsS[k] = planeL[0].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[k];
                    if( (Mathf.Abs(Vector3.Distance(planeL[0].GetComponent<CollisionPlanes_Lefty>().projPerPhal[m], planeL[0].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[m])) < firstThreshold/10) && (Mathf.Abs(Vector3.Distance(planeL[0].GetComponent<CollisionPlanes_Lefty>().projPerPhal[k], planeL[0].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[k])) < firstThreshold/10 ) )
                    {
                        if( (Mathf.Abs( Vector3.Distance(planeL[0].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[m], planeL[0].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[k]) - (planeL[0].GetComponent<CollisionPlanes_Lefty>().projPerPhal[m] - planeL[0].GetComponent<CollisionPlanes_Lefty>().projPerPhal[k]).magnitude) < firstThreshold*(planeL[0].GetComponent<CollisionPlanes_Lefty>().projPerPhal[m] - planeL[0].GetComponent<CollisionPlanes_Lefty>().projPerPhal[k]).magnitude ))
                        {
                            probsS[k] = planeL[0].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[k];
                            errorPerPhalL[k] = probsS[k] - childL[k].transform.position;
                            name[k+19] = this.transform.GetChild(k+19).gameObject.name;

                        }
                        else
                        {
                            // probsP[k] = Vector3.zero;
                            // errorPerPhal[k] = Vector3.zero;
                            name[k+19] = "";
                        }

                    }
                }
            }
        }

        // if( (Mathf.Abs(Vector3.Distance(plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhal[5], plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[5])) < firstThreshold/10) && (Mathf.Abs(Vector3.Distance(plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhal[8], plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[8])) < firstThreshold/10) )
        // {
        //     debugProbsA = plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhal[5];
        //     debugProbsB = plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhal[8];
        //     if((Mathf.Abs(Vector3.Distance(plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[5], plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[8]) - virtualFinger.magnitude) < firstThreshold*virtualFinger.magnitude) )
        //     {
        //         probsA = plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[5];
        //         probsB = plane[0].GetComponent<CollisionPlanes_withKids>().projPerPhalOnOOI[8];
        //     }
        //     else
        //     {
        //         // probsA = Vector3.zero;
        //         // probsB = Vector3.zero;
        //     }
        // }
        // else
        // {
        //     // debugProbsA = Vector3.zero;
        //     // debugProbsB = Vector3.zero;
        // }


        // if( (Mathf.Abs(Vector3.Distance(planeL[0].GetComponent<CollisionPlanes_Lefty>().projPerPhal[5], planeL[0].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[5])) < firstThreshold/10) && (Mathf.Abs(Vector3.Distance(planeL[0].GetComponent<CollisionPlanes_Lefty>().projPerPhal[8], planeL[0].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[8])) < firstThreshold/10) )
        // {
        //     if((Mathf.Abs(Vector3.Distance(planeL[0].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[5], planeL[0].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[8]) - virtualFingerL.magnitude) < firstThreshold*virtualFingerL.magnitude) )
        //     {
        //         probsC = planeL[0].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[5];
        //         probsD = planeL[0].GetComponent<CollisionPlanes_Lefty>().projPerPhalOnOOI[8];
        //     }
        //     else
        //     {
        //         // probsA = Vector3.zero;
        //         // probsB = Vector3.zero;
        //     }
        // }
        // else
        // {
        //     // debugProbsA = Vector3.zero;
        //     // debugProbsB = Vector3.zero;
        // }
    }

  	// public void OnDrawGizmos()
   //  {
   //      GUIStyle style = new GUIStyle();
   //      style.normal.textColor = Color.blue; 
   //      for(int k = 0; k < 19; k++)
   //      {
   //          // Handles.Label(probsP[k], name[k], style);
   //          Gizmos.color = Color.blue;
   //          Gizmos.DrawSphere(probsP[k], 0.005f);

   //          // Handles.Label(probsS[k], name[k+19], style);
   //          Gizmos.color = Color.blue;
   //          Gizmos.DrawSphere(probsS[k], 0.005f);
            
   //      }
   //      Gizmos.color = Color.cyan;
   //      Gizmos.DrawWireSphere(debugProbsA, 0.005f);
   //      Gizmos.DrawWireSphere(debugProbsB, 0.005f);
   //      Gizmos.DrawSphere(probsA, 0.02f);
   //      Gizmos.DrawSphere(probsB, 0.02f);

   //      Gizmos.DrawSphere(probsC, 0.02f);
   //      Gizmos.DrawSphere(probsD, 0.02f);
   //  }
}

