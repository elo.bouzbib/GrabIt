﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CollisionPlanes_withKids : MonoBehaviour
{
    private GameObject OOI;
    [HideInInspector]
    public Vector3 closestFromVF, virtualFinger;
	private Vector3 closestDist;
	private GameObject thumb, palm, index;

	public int planeID, nbOfPlanes;
    // [HideInInspector]
	// public AlgoHints algoHints;
    [HideInInspector]
    public Vector3[] projPerPhal, projPerPhalOnOOI;
    [HideInInspector]
    public GameObject[] childR;

    public bool fromTop, fromSideX, fromSideZ = false;

    // [HideInInspector]
    // public Vector3 oldPos;
    // [HideInInspector]
    // public bool recordNow;
    // public int slidingWindow = 45;
    // private int j;
    // [HideInInspector]
    // public Vector3 onGoingVector; 
    [HideInInspector]
    public bool stopPredictions;
    public bool varyBetaGamma, varyEpsilon;
    // [HideInInspector]
    // public string[] name, mesh;
    // [HideInInspector]
    // public float[] scale;

    public bool warped;

    void Start()
    {
      	OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");

        warped = GameObject.FindObjectOfType<Predict_Live>().warped;
        if(!warped)
        {
            palm = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/r_palm_center_marker").gameObject;
            index = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/b_r_index1/b_r_index2/b_r_index3/r_index_finger_pad_marker").gameObject;
            thumb = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/b_r_thumb0/b_r_thumb1/b_r_thumb2/b_r_thumb3/r_thumb_finger_pad_marker").gameObject;
       
        }
        else
        {
            palm = GameObject.Find("Warped_OculusHand_R/b_r_wrist/r_palm_center_marker").gameObject;
            index = GameObject.Find("Warped_OculusHand_R/b_r_wrist/b_r_index1/b_r_index2/b_r_index3/r_index_finger_pad_marker").gameObject;
            thumb = GameObject.Find("Warped_OculusHand_R/b_r_wrist/b_r_thumb0/b_r_thumb1/b_r_thumb2/b_r_thumb3/r_thumb_finger_pad_marker").gameObject;

        }
        childR = new GameObject[19];

        for(int i = 0; i < 19; i++)
        {
            if(!warped)
            {
                childR[i] = GameObject.Find("RightHandColl").transform.GetChild(i).transform.gameObject;
                childR[i].name = GameObject.Find("RightHandColl").transform.GetChild(i).name;
            }
            else
            {
                childR[i] = GameObject.Find("WarpedRightHandColl").transform.GetChild(i).transform.gameObject;
                childR[i].name = GameObject.Find("WarpedRightHandColl").transform.GetChild(i).name;
            }
            
        }
        projPerPhal = new Vector3[19];
        projPerPhalOnOOI = new Vector3[19];
        fromTop = false;
        fromSideX = false;
        fromSideZ = false;

        // name = new string[19];
        // mesh = new string[19];
        // scale = new float[19];

        // oldPos = new Vector3();

        // oldPos = this.transform.position;
        // j = 0;
    }

    void Update()
    {

        // recordNow = false;
        // StartCoroutine(RecordPos());

        // // name = new string[19];
        // // mesh = new string[19];
        // // scale = new float[19];

        // onGoingVector = this.transform.position - oldPos; 
        // Debug.Log(this.gameObject.name + ";" + onGoingVector.magnitude);

      	OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");
      	// int nbKids = OOI.transform.childCount;
        childR = new GameObject[19];

        for(int i = 0; i < 19; i++)
        {
            if(!warped)
            {
                childR[i] = GameObject.Find("RightHandColl").transform.GetChild(i).transform.gameObject;
                childR[i].name = GameObject.Find("RightHandColl").transform.GetChild(i).name;
            }
            else
            {
                childR[i] = GameObject.Find("WarpedRightHandColl").transform.GetChild(i).transform.gameObject;
                childR[i].name = GameObject.Find("WarpedRightHandColl").transform.GetChild(i).name;
            }
        }

    	if(!warped)
        {
            palm = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/r_palm_center_marker").gameObject;
            index = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/b_r_index1/b_r_index2/b_r_index3/r_index_finger_pad_marker").gameObject;
            thumb = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/b_r_thumb0/b_r_thumb1/b_r_thumb2/b_r_thumb3/r_thumb_finger_pad_marker").gameObject;
       
        }
        else
        {
            palm = GameObject.Find("Warped_OculusHand_R/b_r_wrist/r_palm_center_marker").gameObject;
            index = GameObject.Find("Warped_OculusHand_R/b_r_wrist/b_r_index1/b_r_index2/b_r_index3/r_index_finger_pad_marker").gameObject;
            thumb = GameObject.Find("Warped_OculusHand_R/b_r_wrist/b_r_thumb0/b_r_thumb1/b_r_thumb2/b_r_thumb3/r_thumb_finger_pad_marker").gameObject;
       
        }
    	// NEED TO CREATE PLANE, from l to this point. GetContactPoints of VFMagnitude
    	virtualFinger = (index.transform.position - thumb.transform.position);
    	closestFromVF = whosClosest(thumb.transform.position + virtualFinger/2);
   	
    	Vector3 lengthToPalm = (thumb.transform.position + virtualFinger/2) - palm.transform.position;
    	
        // if(GameObject.FindObjectOfType<SimulationHand>() != null)
        // {
        //     varyBetaGamma = GameObject.FindObjectOfType<SimulationHand>().varyBetaGamma;
        //     varyEpsilon = GameObject.FindObjectOfType<SimulationHand>().varyEpsilon;
        //     if(!varyEpsilon)
        //     {
        //         if(varyBetaGamma)
        //         {
        //            SimulationHand parameters = GameObject.FindObjectOfType<SimulationHand>();
        //             if (Vector3.Distance(whosClosest(palm.transform.position), palm.transform.position) < parameters.beta*lengthToPalm.magnitude)// && onGoingVector.magnitude < parameters.gamma)
        //             {
        //                 stopPredictions = true;
        //                 this.transform.position = this.transform.position;
        //                 // this.transform.position = new Vector3(closestFromVF.x - Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.right).x, closestFromVF.y - Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.up).y, closestFromVF.z - Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.forward).z);

        //             }
        //             else
        //             {
        //                 stopPredictions = false;
        //                 this.transform.position = new Vector3(closestFromVF.x + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.right).x, closestFromVF.y + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.up).y, closestFromVF.z + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.forward).z);
        //             } 
        //         }
        //         else
        //         {
        //             if (Vector3.Distance(whosClosest(palm.transform.position), palm.transform.position) <= 1.05f*lengthToPalm.magnitude)//2.5f*lengthToPalm.magnitude && onGoingVector.magnitude < 0.01f)
        //             {
        //                 stopPredictions = true;
        //                 this.transform.position = new Vector3(closestFromVF.x - Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.right).x, closestFromVF.y - Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.up).y, closestFromVF.z - Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.forward).z);
        //                 // this.transform.position = this.transform.position;
        //             }
        //             else
        //             {
        //                 stopPredictions = false;
        //                 this.transform.position = new Vector3(closestFromVF.x + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.right).x, closestFromVF.y + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.up).y, closestFromVF.z + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.forward).z);
        //             }
        //         }
        //     }
        //     else
        //     {
        //         this.transform.position = new Vector3(closestFromVF.x + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.right).x, closestFromVF.y + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.up).y, closestFromVF.z + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.forward).z);
        //     }
            
        // }
        // else
        // {
        //     if (Vector3.Distance(whosClosest(palm.transform.position), palm.transform.position) <= 1.05f*lengthToPalm.magnitude ) //2.5f*lengthToPalm.magnitude && onGoingVector.magnitude < 0.01f)
        //     {
        //         stopPredictions = true;
        //         this.transform.position = this.transform.position;
        //     }
        //     else
        //     {
        //         stopPredictions = false;
        //         this.transform.position = new Vector3(closestFromVF.x + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.right).x, closestFromVF.y + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.up).y, closestFromVF.z + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.forward).z);
        //     }

        // }

        if (Vector3.Distance(whosClosest(palm.transform.position), palm.transform.position) <= 1.05f*lengthToPalm.magnitude)//2.5f*lengthToPalm.magnitude && onGoingVector.magnitude < 0.01f)
        {
            stopPredictions = true;
            this.transform.position = new Vector3(closestFromVF.x - Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.right).x, closestFromVF.y - Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.up).y, closestFromVF.z - Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.forward).z);
            // this.transform.position = this.transform.position;
        }
        else
        {
            stopPredictions = false;
            this.transform.position = new Vector3(closestFromVF.x + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.right).x, closestFromVF.y + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.up).y, closestFromVF.z + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.forward).z);
        }
		
        if((Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.up).y) > Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.forward).z)) && (Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.up).y) > Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.right).x)) )
        {
            this.transform.forward = virtualFinger/virtualFinger.magnitude;
            fromTop = true;
            fromSideX = false;
            fromSideZ = false;
        }
        else
        {

            this.transform.forward = virtualFinger/virtualFinger.magnitude;
            this.transform.rotation = this.transform.rotation* Quaternion.FromToRotation(Vector3.right, Vector3.up);
            fromTop = false;
            
            if((Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.up).y) < Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.right).x)) && (Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.forward).z) < Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.right).x)))
            {
                fromSideX = true;
                fromSideZ = false;
            }
            else if ((Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.up).y) < Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.forward).z)) && (Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.forward).z) > Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.right).x)))
            {
                fromSideX = false;
                fromSideZ = true;
            }
            else
            {
                // Debug.Log("Kindof in the middle");
                fromSideX = false;
                fromSideZ = false;
            }
        }

    	this.transform.localScale = new Vector3(virtualFinger.magnitude + 0.05f, virtualFinger.magnitude + 0.05f, virtualFinger.magnitude + 0.05f)*virtualFinger.magnitude*2.0f;
        // this.transform.localScale = new Vector3(virtualFinger.magnitude, virtualFinger.magnitude, virtualFinger.magnitude)*virtualFinger.magnitude*2;

        // s// Debug.DrawRay(palm.transform.position, Vector3.Cross(index.transform.forward, virtualFinger.normalized), Color.green);
        // this.transform.up = Vector3.Cross(virtualFinger.normalized, palm.transform.forward);        
        
        // this.transform.up = Vector3.Cross(virtualFinger.normalized, Vector3.Cross(thumb.transform.right, index.transform.right));
        // Debug.Log("Y: "+ Mathf.Abs(Vector3.Project(Vector3.Cross(thumb.transform.position.normalized, index.transform.position.normalized), Vector3.up).y) + " Z: " + Mathf.Abs(Vector3.Project(Vector3.Cross(thumb.transform.position.normalized, index.transform.position.normalized), Vector3.forward).z) + "X: " +  Mathf.Abs(Vector3.Project(Vector3.Cross(thumb.transform.position.normalized, index.transform.position.normalized), Vector3.right).x));
        // Debug.Log("Y: "+ Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.up).y) + " Z: " + Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.forward).z) + "X: " +  Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.right).x));

    	// Debug.DrawRay(this.transform.position, this.transform.up, Color.blue);
    	// Debug.DrawRay(this.transform.position, Vector3.Cross(new Vector3(1,0,0), virtualFinger/virtualFinger.magnitude), Color.green);
    	// Debug.DrawRay(this.transform.position, Vector3.Cross(new Vector3(0,1,0), virtualFinger/virtualFinger.magnitude), Color.red);

    	// Debug.DrawRay(this.transform.position, Vector3.Cross(new Vector3(0,0,1), virtualFinger/virtualFinger.magnitude), Color.magenta);

        for(int i = 0; i < 19; i++)
        {
            if(!warped)
            {
                childR[i] = GameObject.Find("RightHandColl").transform.GetChild(i).transform.gameObject;

            }
            else
            {
                childR[i] = GameObject.Find("WarpedRightHandColl").transform.GetChild(i).transform.gameObject;

            }
            


            projPerPhal[i] = this.GetComponent<Collider>().ClosestPoint(this.GetComponent<Collider>().ClosestPointOnBounds(childR[i].transform.position));
            // projPerPhalOnOOI[i] = whosClosest(projPerPhal[i]);
            if(this.GetComponent<Collider>().bounds.Contains(whosClosest(projPerPhal[i])))
            {
                projPerPhalOnOOI[i] = whosClosest(projPerPhal[i]);
            }
            else
            {
                //projPerPhalOnOOI[i] = new Vector3(0,0,0);
            }
        }

        // for(int i = 0; i < OOI.transform.childCount; i++)
        // {
        //     // for(int k = 0; k < 19; k++)
        //     // {
        //     //     // if(OOI.transform.GetChild(i).gameObject.GetComponent<Collider>().bounds.Contains(projPerPhalOnOOI[k]))
        //     //     if(OOI.transform.GetChild(i).gameObject.GetComponent<Collider>().bounds.Contains(GameObject.FindObjectOfType<PredictionStop>().GetComponent<PredictionStop>().finalPred[k]))
        //     //     {
        //     //         name[k] = OOI.transform.GetChild(i).name;
        //     //         scale[k] = (100*Vector3.Scale(OOI.transform.GetChild(i).transform.localScale, OOI.transform.localScale).x * 100*Vector3.Scale(OOI.transform.GetChild(i).transform.localScale, OOI.transform.localScale).y * 100*Vector3.Scale(OOI.transform.GetChild(i).transform.localScale, OOI.transform.localScale).z);
        //     //         mesh[k] = OOI.transform.GetChild(i).gameObject.GetComponent<MeshFilter>().mesh.name;
        //     //     }

        //     // }

        // 	if(this.GetComponent<Collider>().bounds.Contains(OOI.transform.GetChild(i).gameObject.transform.position))
        // 	{
        //         OOI.GetComponent<CollideCutSections_inKids>().collidingCutSections[i] = true;
        // 	}
        // 	else
        // 	{
        // 		// OOI.transform.GetChild(i).gameObject.transform.GetComponent<CollideCutSections>().collidingCutSections = false;
        //         OOI.GetComponent<CollideCutSections_inKids>().collidingCutSections[i] = false;

        // 	}
        // }


        // j = j+1;
        // if(j%slidingWindow == 0)
        // {
        //     recordNow = true;
        // }
        

    }


    public Vector3 whosClosest(Vector3 fromThis)
    {
    	int nbKids = OOI.transform.childCount;
    	Vector3[] closestPoint = new Vector3[nbKids];
    	Vector3[] closest_distance = new Vector3[nbKids];
    	float[] REAL_MIN_DISTANCE = new float[nbKids];

    	for(int i = 0; i < nbKids; i++)
        {
	    	// Vector3[] objVertices = OOI.transform.GetChild(i).gameObject.GetComponent<MeshFilter>().mesh.vertices;
	    	// float REAL_MIN_DISTANCE;
        	closestPoint[i] = OOI.transform.GetChild(i).gameObject.GetComponent<Collider>().ClosestPointOnBounds(fromThis);
        	closest_distance[i] = OOI.transform.GetChild(i).gameObject.GetComponent<Collider>().ClosestPoint(closestPoint[i]);
	        REAL_MIN_DISTANCE[i] = Vector3.Distance(closest_distance[i], fromThis);
	    	// }
            if(nbKids > 1)
            {
                if(i>1)
                {
                    if(REAL_MIN_DISTANCE[i-1] <= REAL_MIN_DISTANCE[i])
                    {
                        closest_distance[i] = closest_distance[i-1];
                        closestDist = closest_distance[i];
                        // Debug.Log(OOI.transform.GetChild(i-1).gameObject.name);
                        REAL_MIN_DISTANCE[i] = REAL_MIN_DISTANCE[i-1];
                    }
                    else
                    {
                        closestDist = closest_distance[i];
                    }
                }
            }
	    	
	    	
	    }
    	return closestDist;

    }

    // IEnumerator RecordPos()
    // {
    //     yield return new WaitUntil (() => recordNow == true);
        
    //     oldPos = this.transform.position;


    // }

    // void OnDrawGizmos()
    // {
    // 	// Gizmos.color = Color.magenta;
    // 	// Gizmos.DrawSphere(closestFromVF, 0.01f);

    //  //    Debug.DrawRay(thumb.transform.position, index.transform.position - thumb.transform.position);


    //  //    Debug.DrawRay(palm.transform.position, Vector3.Project((palm.transform.up/palm.transform.up.magnitude), Vector3.up)/10, Color.green);
    //  //    Debug.DrawRay(palm.transform.position, Vector3.Project((palm.transform.up/palm.transform.up.magnitude), Vector3.right)/10, Color.red);
    //  //    Debug.DrawRay(palm.transform.position, Vector3.Project((palm.transform.up/palm.transform.up.magnitude), Vector3.forward)/10, Color.blue);

    //  //    GUIStyle style = new GUIStyle();
    //  //    style.normal.textColor = Color.red; 
    //  //    if(this.name == "Plane")
    //  //    {
    //  //       for(int k = 0; k < 19; k++)
    //  //        {
    //  //            // Debug.Log(projPerPhalOnOOI[k]);
    //  //            Handles.Label(projPerPhalOnOOI[k], GameObject.Find("RightHandColl").transform.GetChild(k).name, style);
    //  //            Gizmos.color = Color.red;
    //  //            Gizmos.DrawWireSphere(projPerPhalOnOOI[k], 0.005f);
    //  //            // Gizmos.DrawSphere(projPerPhal[k], 0.005f);
                
    //  //        } 
    //  //    }
        
    //     // Gizmos.color = Color.cyan;
    //     // Gizmos.DrawWireSphere(debugProbsA, 0.005f);
    //     // Gizmos.DrawWireSphere(debugProbsB, 0.005f);
    //     // Gizmos.DrawSphere(probsA, 0.005f);
    //     // Gizmos.DrawSphere(probsB, 0.005f);
        
    //     // Debug.DrawRay(this.transform.position, Vector3.Cross(Vector3.up, virtualFinger.normalized)/10, Color.green);

    //     // Debug.DrawRay(this.transform.position, Vector3.Cross(Vector3.forward, virtualFinger.normalized)/10, Color.blue);
    //     // Debug.DrawRay(this.transform.position, Vector3.Cross(Vector3.right, virtualFinger.normalized)/10, Color.red);

    //     // Debug.DrawRay(this.transform.position, this.transform.up/10, Color.red);
 
    //     // Debug.DrawRay(this.transform.position, Vector3.Cross(Vector3.Cross(Vector3.Cross(thumb.transform.right, index.transform.right), palm.transform.right), virtualFinger.normalized), Color.white);


    // }
}

