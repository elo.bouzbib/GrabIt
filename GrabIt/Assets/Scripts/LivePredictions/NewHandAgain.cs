﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System.IO;
using System.Globalization;

public class NewHandAgain : MonoBehaviour
{
	private Vector3[] rotApproxSkel, posApproxSkel;
	private OVRCustomSkeleton handSkelRight, handWarpRight;

	private string pathCollProperty, pathCollNames;

    private string[] dataCollProp, dataNames;
    private string[] collProperties;
    private StreamReader srCollProp, srNames;
    // Start is called before the first frame update
    void Start()
    {
        handSkelRight = GameObject.Find("OVRCustomHandPrefab_R").GetComponent<OVRCustomSkeleton>();
        if(GameObject.Find("WarpedRightHand") != null)
        {
            handWarpRight = GameObject.Find("WarpedRightHand").GetComponent<OVRCustomSkeleton>();
        }


        posApproxSkel = new Vector3[19];
        rotApproxSkel = new Vector3[19];


        for(int i = 0; i < 19; i++)
        {

            this.transform.GetChild(i).transform.gameObject.AddComponent<CapsuleCollider>();
            this.transform.GetChild(i).transform.gameObject.AddComponent<Rigidbody>();
    		this.transform.GetChild(i).transform.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            this.transform.GetChild(i).transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
            this.transform.GetChild(i).transform.gameObject.GetComponent<Rigidbody>().useGravity = false;
            this.transform.GetChild(i).transform.gameObject.GetComponent<Rigidbody>().mass = 1.0f;
            
        	pathCollNames = "Assets/Resources/DataCollection/namesColliders.csv";
	        pathCollProperty = "Assets/Resources/DataCollection/propsColliders.csv";

			StreamReader srNames = new StreamReader(pathCollNames, true);
	        if (srNames.Peek() > -1) 
	        {
	            string line = srNames.ReadToEnd();     
	            dataNames = line.Split('\n');
	        }

        	this.transform.GetChild(i).name = dataNames[i];

		    StreamReader srCollProp = new StreamReader(pathCollProperty, true);
		    
		    if (srCollProp.Peek() > -1) 
		    {
		        string line = srCollProp.ReadToEnd();     
		        dataCollProp = line.Split('\n');
		    }
	    	collProperties = dataCollProp[1].Split(';');

	    	
    		int idProp = int.Parse(collProperties[(i*13)]);
            this.transform.GetChild(idProp).transform.gameObject.GetComponent<CapsuleCollider>().center = new Vector3(float.Parse((collProperties[(i*13)+7]), CultureInfo.InvariantCulture), float.Parse((collProperties[(i*13)+8]), CultureInfo.InvariantCulture), float.Parse((collProperties[(i*13)+9]), CultureInfo.InvariantCulture));
            this.transform.GetChild(idProp).transform.gameObject.GetComponent<CapsuleCollider>().radius = float.Parse((collProperties[(i*13)+10]), CultureInfo.InvariantCulture);
            this.transform.GetChild(idProp).transform.gameObject.GetComponent<CapsuleCollider>().height = float.Parse((collProperties[(i*13)+11]), CultureInfo.InvariantCulture);
            this.transform.GetChild(idProp).transform.gameObject.GetComponent<CapsuleCollider>().direction = int.Parse(collProperties[(i*13)+12]);
            
    	}

    }

    // Update is called once per frame
    void Update()
    {
		if(this.gameObject.name == "WarpedRightHandColl")
	    {
	    	for(int i = 0; i < 19; i++)
	    	{
		        // posApproxSkel[i] = GameObject.Find("Projection ( "+i.ToString()+" )").transform.position;
		        posApproxSkel[i] = GameObject.FindObjectOfType<PredictionStop>().finalPred[i];
	    	}
	    }

	    // ADD RATIO CURRENT GRASP APERTURE, FINAL PRED 6 AND 9
	    // Consider as all kids.. Same stuff, localPosition from RightHandColl child
	    posApproxSkel[0] = Vector3.Lerp(handSkelRight.CustomBones[2].transform.position, GameObject.FindObjectOfType<PredictionStop>().finalPred[0], 0.7f);
        posApproxSkel[1] = Vector3.Lerp(handSkelRight.CustomBones[2].transform.position, GameObject.FindObjectOfType<PredictionStop>().finalPred[1], 0.7f);
        posApproxSkel[2] = Vector3.Lerp(handSkelRight.CustomBones[0].transform.position, GameObject.FindObjectOfType<PredictionStop>().finalPred[2], 0.7f);
        posApproxSkel[3] = Vector3.Lerp(handSkelRight.CustomBones[15].transform.position, GameObject.FindObjectOfType<PredictionStop>().finalPred[3], 0.7f);
        posApproxSkel[4] = Vector3.Lerp(handSkelRight.CustomBones[3].transform.position, GameObject.FindObjectOfType<PredictionStop>().finalPred[4], 0.7f);
        posApproxSkel[5] = Vector3.Lerp(handSkelRight.CustomBones[4].transform.position, GameObject.FindObjectOfType<PredictionStop>().finalPred[5], 0.7f);
        posApproxSkel[6] = Vector3.Lerp(handSkelRight.CustomBones[5].transform.position, GameObject.FindObjectOfType<PredictionStop>().finalPred[6], 0.7f);
        posApproxSkel[7] = Vector3.Lerp(handSkelRight.CustomBones[6].transform.position, GameObject.FindObjectOfType<PredictionStop>().finalPred[7], 0.7f);
        posApproxSkel[8] = Vector3.Lerp(handSkelRight.CustomBones[7].transform.position, GameObject.FindObjectOfType<PredictionStop>().finalPred[8], 0.7f);
        posApproxSkel[9] = Vector3.Lerp(handSkelRight.CustomBones[8].transform.position, GameObject.FindObjectOfType<PredictionStop>().finalPred[9], 0.7f);
        posApproxSkel[10] = Vector3.Lerp(handSkelRight.CustomBones[9].transform.position, GameObject.FindObjectOfType<PredictionStop>().finalPred[10], 0.7f);
        posApproxSkel[11] = Vector3.Lerp(handSkelRight.CustomBones[10].transform.position, GameObject.FindObjectOfType<PredictionStop>().finalPred[11], 0.7f);
        posApproxSkel[12] = Vector3.Lerp(handSkelRight.CustomBones[11].transform.position, GameObject.FindObjectOfType<PredictionStop>().finalPred[12], 0.7f);
        posApproxSkel[13] = Vector3.Lerp(handSkelRight.CustomBones[12].transform.position, GameObject.FindObjectOfType<PredictionStop>().finalPred[13], 0.7f);
        posApproxSkel[14] = Vector3.Lerp(handSkelRight.CustomBones[13].transform.position, GameObject.FindObjectOfType<PredictionStop>().finalPred[14], 0.7f);
        posApproxSkel[15] = Vector3.Lerp(handSkelRight.CustomBones[14].transform.position, GameObject.FindObjectOfType<PredictionStop>().finalPred[15], 0.7f);
        posApproxSkel[16] = Vector3.Lerp(handSkelRight.CustomBones[16].transform.position, GameObject.FindObjectOfType<PredictionStop>().finalPred[16], 0.7f);
        posApproxSkel[17] = Vector3.Lerp(handSkelRight.CustomBones[17].transform.position, GameObject.FindObjectOfType<PredictionStop>().finalPred[17], 0.7f);
        posApproxSkel[18] = Vector3.Lerp(handSkelRight.CustomBones[18].transform.position, GameObject.FindObjectOfType<PredictionStop>().finalPred[18], 0.7f);

        rotApproxSkel[0] = handSkelRight.CustomBones[2].transform.eulerAngles;
		rotApproxSkel[1] = handSkelRight.CustomBones[2].transform.eulerAngles;
        rotApproxSkel[2] = handSkelRight.CustomBones[0].transform.eulerAngles;
        rotApproxSkel[3] = handSkelRight.CustomBones[15].transform.eulerAngles;
        rotApproxSkel[4] = handSkelRight.CustomBones[3].transform.eulerAngles;
        rotApproxSkel[5] = handSkelRight.CustomBones[4].transform.eulerAngles;
        rotApproxSkel[6] = handSkelRight.CustomBones[5].transform.eulerAngles;
        rotApproxSkel[7] = handSkelRight.CustomBones[6].transform.eulerAngles;
        rotApproxSkel[8] = handSkelRight.CustomBones[7].transform.eulerAngles;
        rotApproxSkel[9] = handSkelRight.CustomBones[8].transform.eulerAngles;
        rotApproxSkel[10] = handSkelRight.CustomBones[9].transform.eulerAngles;
        rotApproxSkel[11] = handSkelRight.CustomBones[10].transform.eulerAngles;
        rotApproxSkel[12] = handSkelRight.CustomBones[11].transform.eulerAngles;
        rotApproxSkel[13] = handSkelRight.CustomBones[12].transform.eulerAngles;
        rotApproxSkel[14] = handSkelRight.CustomBones[13].transform.eulerAngles;
        rotApproxSkel[15] = handSkelRight.CustomBones[14].transform.eulerAngles;
        rotApproxSkel[16] = handSkelRight.CustomBones[16].transform.eulerAngles;
        rotApproxSkel[17] = handSkelRight.CustomBones[17].transform.eulerAngles;
        rotApproxSkel[18] = handSkelRight.CustomBones[18].transform.eulerAngles;
    	for(int k = 0; k < 19; k++)
    	{
    		this.transform.GetChild(k).gameObject.transform.position = posApproxSkel[k];
    		this.transform.GetChild(k).gameObject.transform.eulerAngles = rotApproxSkel[k];
    	}
		
    }
}


