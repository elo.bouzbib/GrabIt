﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System.IO;
using System.Globalization;

public class ReadPosAndWrite : MonoBehaviour
{
	public GameObject handLeft, handRight;

	public IList<OVRBoneCapsule> Capsules { get; private set; }
	public IList<OVRBoneCapsule> CapsulesR { get; private set; }

	public bool simulationDebug;
	private string pathCollProperty, pathCollNames;

    private string[] dataCollProp, dataNames;
    private string[] collProperties;
    private StreamReader srCollProp, srNames;

    public Instantiate_Live gameManager;

    // Start is called before the first frame update
    void Start()
    {
    	handLeft = GameObject.Find("OVRCustomHandPrefab_L");
    	handRight = GameObject.Find("OVRCustomHandPrefab_R");
    	Capsules = handLeft.GetComponent<OVRCustomSkeleton>().Capsules;
		CapsulesR = handRight.GetComponent<OVRCustomSkeleton>().Capsules;

		gameManager = GameObject.FindObjectOfType<Instantiate_Live>();
		
		simulationDebug = !gameManager.realGame;

		for(int i = 0; i < 19; i++)
        {
        	if(this.gameObject.name == "LeftHandColl")
	        {
	            this.transform.GetChild(i).transform.gameObject.AddComponent<CapsuleCollider>();
	            this.transform.GetChild(i).transform.gameObject.AddComponent<Rigidbody>();
	    		this.transform.GetChild(i).transform.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
	            this.transform.GetChild(i).transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
	            this.transform.GetChild(i).transform.gameObject.GetComponent<Rigidbody>().useGravity = false;
	            this.transform.GetChild(i).transform.gameObject.GetComponent<Rigidbody>().mass = 1.0f;
	            if(!simulationDebug)
	            {
					this.transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().center = Capsules[i].CapsuleCollider.center;
		        	this.transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().radius = Capsules[i].CapsuleCollider.radius;
		        	this.transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().height = Capsules[i].CapsuleCollider.height;
		        	this.transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().direction = Capsules[i].CapsuleCollider.direction;
	        
	            }
	            else
	            {
	            	pathCollNames = "Assets/Resources/DataCollection/namesColliders.csv";
			        pathCollProperty = "Assets/Resources/DataCollection/propsColliders.csv";

					StreamReader srNames = new StreamReader(pathCollNames, true);
			        if (srNames.Peek() > -1) 
			        {
			            string line = srNames.ReadToEnd();     
			            dataNames = line.Split('\n');
			        }

		        	this.transform.GetChild(i).name = dataNames[i];

				    StreamReader srCollProp = new StreamReader(pathCollProperty, true);
				    
				    if (srCollProp.Peek() > -1) 
				    {
				        string line = srCollProp.ReadToEnd();     
				        dataCollProp = line.Split('\n');
				    }
			    	collProperties = dataCollProp[1].Split(';');

			    	
		    		int idProp = int.Parse(collProperties[(i*13)]);
		            this.transform.GetChild(idProp).transform.gameObject.GetComponent<CapsuleCollider>().center = new Vector3(float.Parse((collProperties[(i*13)+1]), CultureInfo.InvariantCulture), float.Parse((collProperties[(i*13)+2]), CultureInfo.InvariantCulture), float.Parse((collProperties[(i*13)+3]), CultureInfo.InvariantCulture));
		            this.transform.GetChild(idProp).transform.gameObject.GetComponent<CapsuleCollider>().radius = float.Parse((collProperties[(i*13)+4]), CultureInfo.InvariantCulture);
		            this.transform.GetChild(idProp).transform.gameObject.GetComponent<CapsuleCollider>().height = float.Parse((collProperties[(i*13)+5]), CultureInfo.InvariantCulture);
		            this.transform.GetChild(idProp).transform.gameObject.GetComponent<CapsuleCollider>().direction = int.Parse(collProperties[(i*13)+6]);

	            }
	            
        	}
        	if(this.gameObject.name == "RightHandColl")
	        {
	            this.transform.GetChild(i).transform.gameObject.AddComponent<CapsuleCollider>();
	            this.transform.GetChild(i).transform.gameObject.AddComponent<Rigidbody>();
	    		this.transform.GetChild(i).transform.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
	            this.transform.GetChild(i).transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
	            this.transform.GetChild(i).transform.gameObject.GetComponent<Rigidbody>().useGravity = false;
	            this.transform.GetChild(i).transform.gameObject.GetComponent<Rigidbody>().mass = 1.0f;

	            this.transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().center = new Vector3(CapsulesR[i].CapsuleCollider.center.x, CapsulesR[i].CapsuleCollider.center.y, CapsulesR[i].CapsuleCollider.center.z);
	        	this.transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().radius = CapsulesR[i].CapsuleCollider.radius;
	        	this.transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().height = CapsulesR[i].CapsuleCollider.height;
	        	this.transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().direction = CapsulesR[i].CapsuleCollider.direction;
	        
	        	if(!simulationDebug)
	            {
					this.transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().center = Capsules[i].CapsuleCollider.center;
		        	this.transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().radius = Capsules[i].CapsuleCollider.radius;
		        	this.transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().height = Capsules[i].CapsuleCollider.height;
		        	this.transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().direction = Capsules[i].CapsuleCollider.direction;
	        
	            }
	            else
	            {
	            	
		        	this.transform.GetChild(i).name = dataNames[i];

		    		int idProp = int.Parse(collProperties[i*13]);
		            this.transform.GetChild(idProp).transform.gameObject.GetComponent<CapsuleCollider>().center = new Vector3(float.Parse((collProperties[(i*13)+7]), CultureInfo.InvariantCulture), float.Parse((collProperties[(i*13)+8]), CultureInfo.InvariantCulture), float.Parse((collProperties[(i*13)+9]), CultureInfo.InvariantCulture));
		            this.transform.GetChild(idProp).transform.gameObject.GetComponent<CapsuleCollider>().radius = float.Parse((collProperties[(i*13)+10]), CultureInfo.InvariantCulture);
		            this.transform.GetChild(idProp).transform.gameObject.GetComponent<CapsuleCollider>().height = float.Parse((collProperties[(i*13)+11]), CultureInfo.InvariantCulture);
		            this.transform.GetChild(idProp).transform.gameObject.GetComponent<CapsuleCollider>().direction = int.Parse(collProperties[(i*13)+12]);


	            }
        	}
        }        
    }

    // Update is called once per frame
    void Update()
    {
    	for(int k = 0; k < 19; k++)
    	{
    		if(this.gameObject.name == "LeftHandColl")
	        {
	        	this.transform.GetChild(k).gameObject.transform.position = new Vector3(Capsules[k].CapsuleCollider.gameObject.transform.position.x, Capsules[k].CapsuleCollider.gameObject.transform.position.y, Capsules[k].CapsuleCollider.gameObject.transform.position.z);
	        	this.transform.GetChild(k).gameObject.transform.eulerAngles = new Vector3(Capsules[k].CapsuleCollider.gameObject.transform.eulerAngles.x, Capsules[k].CapsuleCollider.gameObject.transform.eulerAngles.y, Capsules[k].CapsuleCollider.gameObject.transform.eulerAngles.z);
	        	// this.transform.GetChild(k).gameObject.name = GameObject.Find("OVRCustomHandPrefab_L").GetComponent<OVRCustomSkeleton>().CustomBones[k].name;
	        }
	        if(this.gameObject.name == "RightHandColl")
	        {
	        	this.transform.GetChild(k).gameObject.transform.position = new Vector3(CapsulesR[k].CapsuleCollider.gameObject.transform.position.x, CapsulesR[k].CapsuleCollider.gameObject.transform.position.y, CapsulesR[k].CapsuleCollider.gameObject.transform.position.z);
	        	this.transform.GetChild(k).gameObject.transform.eulerAngles = new Vector3(CapsulesR[k].CapsuleCollider.gameObject.transform.eulerAngles.x, CapsulesR[k].CapsuleCollider.gameObject.transform.eulerAngles.y, CapsulesR[k].CapsuleCollider.gameObject.transform.eulerAngles.z);
	        	// this.transform.GetChild(k).gameObject.name = CapsulesR[k].boneId;
	        	
	        }
    	}

        
    }
}
