﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedirectMe_index : MonoBehaviour
{	
	// [HideInInspector]
	public GameObject rightHand, leftHand, warpedRightHand, warpedLeftHand;

	public IList<OVRBoneCapsule> Capsules { get; private set; }
	public IList<OVRBoneCapsule> CapsulesR { get; private set; }

	public IList<OVRBoneCapsule> CapsulesL_Replica { get; private set; }
	public IList<OVRBoneCapsule> CapsulesR_Replica { get; private set; }

	private Vector3 physicalTargetPosition, virtualTargetPosition;
	private float[] ds, dp;
	private Vector3[] warpVector, initialPosition, projectVector;

	public GameObject physicalTarget, virtualTarget;
	public bool startWarpR, startWarpL;

	public ClosestToHand_Live[] phalanxData;
	// public GameObject initialPosGO;

	public Instantiate_Live gameManager;
    public bool reverse;

    public GameObject warpZone;


    // Start is called before the first frame update
    void Start()
    {
	    gameManager = GameObject.Find("GameManager").GetComponent<Instantiate_Live>();

        leftHand = GameObject.Find("OVRCustomHandPrefab_L"); // THIS IS HP = Hand Physical
        rightHand = GameObject.Find("OVRCustomHandPrefab_R");
        
        warpedRightHand = GameObject.Find("WarpedRightHand"); // THIS IS HV = Hand Virtual
        warpedLeftHand = GameObject.Find("WarpedLeftHand");

    	Capsules = leftHand.GetComponent<OVRCustomSkeleton>().Capsules;
		CapsulesR = rightHand.GetComponent<OVRCustomSkeleton>().Capsules;

		CapsulesL_Replica = warpedLeftHand.GetComponent<OVRCustomSkeleton>().Capsules;
		CapsulesR_Replica = warpedRightHand.GetComponent<OVRCustomSkeleton>().Capsules;

		warpVector = new Vector3[38];
		projectVector = new Vector3[38];

		physicalTarget = GameObject.FindGameObjectWithTag("PhysicalTotem"); // Look into kids to find primitives
		virtualTarget = GameObject.Find("VirtualTotem"); 

		// VIRTUAL TARGET = read from AlgoHints. Same to start Warp -> as soon as prediction starts,
		// warp can begin.

		physicalTargetPosition = physicalTarget.transform.position;
		virtualTargetPosition = virtualTarget.transform.position; // ATTENTION!! SWITCH HERE!
    	startWarpR = false;
        startWarpL = false;

    	phalanxData = new ClosestToHand_Live[38];
		initialPosition = new Vector3[38];

		ds = new float[38];
		dp = new float[38];

    	for(int k = 0; k < 19; k++)
    	{
    		phalanxData[k] = GameObject.Find("RightHandColl").transform.GetChild(k).gameObject.GetComponent<ClosestToHand_Live>();
            phalanxData[k+19] = GameObject.Find("LeftHandColl").transform.GetChild(k).gameObject.GetComponent<ClosestToHand_Live>();
        }

        warpZone = GameObject.FindGameObjectWithTag("WarpZone");
    }

    // Update is called once per frame
    void Update()
    {
	
        for(int i = 0; i < 19; i++)
        {
            // initialPosition[i] = GameObject.Find("StartPosition").transform.position;
            // initialPosition[i+19] = GameObject.Find("StartPosition").transform.position;

            if(warpZone.GetComponent<Collider>().bounds.Contains(rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position))
            {
                startWarpR = true;
            }
            else
            {
                startWarpR = false;
            }

            if(startWarpR)
            {
                phalanxData[i].startWarp = true; // leftHand - phalanxData(k+19)
                // startWarp = true;
                initialPosition[i] = phalanxData[i].initialWarpPos;

                if(reverse)
                {
                    projectVector[i] = (virtualTargetPosition - initialPosition[i]).normalized;

                    ds[i] = Vector3.Project((rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position - initialPosition[i]), projectVector[i]).magnitude;
                    dp[i] = Vector3.Project((virtualTargetPosition - rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position), projectVector[i]).magnitude;
                    warpVector[i] = (ds[i] / (ds[i] + dp[i])) * (physicalTargetPosition - virtualTargetPosition);
                }
                else
                {
                    projectVector[i] = (physicalTargetPosition - initialPosition[i]).normalized;

                    ds[i] = Vector3.Project((rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position - initialPosition[i]), projectVector[i]).magnitude;
                    dp[i] = Vector3.Project((physicalTargetPosition - rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position), projectVector[i]).magnitude;
                    
                    // ds[i] = Vector3.Project((rightHand.transform.position - initialPosition[i]), projectVector[i]).magnitude;
                    // dp[i] = Vector3.Project((physicalTargetPosition - rightHand.transform.position), projectVector[i]).magnitude;
                    
                    warpVector[i] = (ds[i] / (ds[i] + dp[i])) * (virtualTargetPosition - physicalTargetPosition);
                }
            }
            else
            {
                warpVector[i] = Vector3.zero;
                phalanxData[i].startWarp = false;
                initialPosition[i] = new Vector3(rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.x, rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.y, rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.z);
                // startWarp = false;
            }

            if(warpZone.GetComponent<Collider>().bounds.Contains(leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position))
            {
                startWarpL = true;
            }
            else
            {
                startWarpL = false;
            }


            if(startWarpL)
            {
                phalanxData[i+19].startWarp = true; // leftHand - phalanxData(k+19)
                initialPosition[i+19] = phalanxData[i+19].initialWarpPos;

                // if(phalanxData[i+19].startWarp)
                // {
                if(reverse)
                {
                    projectVector[i+19] = (virtualTargetPosition - initialPosition[i+19]).normalized;

                    ds[i+19] = Vector3.Project((leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i+19].transform.position - initialPosition[i+19]), projectVector[i+19]).magnitude;
                    dp[i+19] = Vector3.Project((virtualTargetPosition - leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i+19].transform.position), projectVector[i+19]).magnitude;
                    warpVector[i+19] = (ds[i+19] / (ds[i+19] + dp[i+19])) * (physicalTargetPosition - virtualTargetPosition);
                
                }
                else
                {
                    projectVector[i+19] = (physicalTargetPosition - initialPosition[i+19]).normalized;

                    ds[i+19] = Vector3.Project((leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i+19].transform.position - initialPosition[i+19]), projectVector[i+19]).magnitude;
                    dp[i+19] = Vector3.Project((physicalTargetPosition - leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i+19].transform.position), projectVector[i+19]).magnitude;
                    
                    // ds[i+19] = Vector3.Project((leftHand.transform.position - initialPosition[i+19]), projectVector[i+19]).magnitude;
                    // dp[i+19] = Vector3.Project((physicalTargetPosition - leftHand.transform.position), projectVector[i+19]).magnitude;
                    
                    warpVector[i+19] = (ds[i+19] / (ds[i+19] + dp[i+19])) * (virtualTargetPosition - physicalTargetPosition);
                }
                // }
            }
            else
            {
                warpVector[i+19] = Vector3.zero;
                phalanxData[i+19].startWarp = false;
                initialPosition[i+19] = new Vector3(leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.x, leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.y, leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.z);
                // startWarp = false;
            }

            warpedRightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position = new Vector3(rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.x + warpVector[i].x, rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.y + warpVector[i].y, rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.z + warpVector[i].z);
        	warpedRightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles = new Vector3(rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.x, rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.y, rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.z);

            warpedLeftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position = new Vector3(leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.x + warpVector[i+19].x, leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.y + warpVector[i+19].y, leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position.z + warpVector[i+19].z);
        	warpedLeftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles = new Vector3(leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.x, leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.y, leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.z);
    		
    		CapsulesL_Replica[i].CapsuleCollider.gameObject.transform.position = new Vector3(Capsules[i].CapsuleCollider.gameObject.transform.position.x + warpVector[i+19].x, Capsules[i].CapsuleCollider.gameObject.transform.position.y + warpVector[i+19].y, Capsules[i].CapsuleCollider.gameObject.transform.position.z + warpVector[i+19].z);    		
    		CapsulesR_Replica[i].CapsuleCollider.gameObject.transform.position = new Vector3(CapsulesR[i].CapsuleCollider.gameObject.transform.position.x + warpVector[i].x, CapsulesR[i].CapsuleCollider.gameObject.transform.position.y + warpVector[i].y, CapsulesR[i].CapsuleCollider.gameObject.transform.position.z + warpVector[i].z);

			CapsulesL_Replica[i].CapsuleCollider.gameObject.transform.eulerAngles = new Vector3(Capsules[i].CapsuleCollider.gameObject.transform.eulerAngles.x, Capsules[i].CapsuleCollider.gameObject.transform.eulerAngles.y, Capsules[i].CapsuleCollider.gameObject.transform.eulerAngles.z);
    		CapsulesR_Replica[i].CapsuleCollider.gameObject.transform.eulerAngles = new Vector3(CapsulesR[i].CapsuleCollider.gameObject.transform.eulerAngles.x, CapsulesR[i].CapsuleCollider.gameObject.transform.eulerAngles.y, CapsulesR[i].CapsuleCollider.gameObject.transform.eulerAngles.z);
        
        	CapsulesL_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().center = Capsules[i].CapsuleCollider.center;
        	CapsulesL_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().radius = Capsules[i].CapsuleCollider.radius;
        	CapsulesL_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().height = Capsules[i].CapsuleCollider.height;
        	CapsulesL_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().direction = Capsules[i].CapsuleCollider.direction;
        
        	CapsulesR_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().center = CapsulesR[i].CapsuleCollider.center;
        	CapsulesR_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().radius = CapsulesR[i].CapsuleCollider.radius;
        	CapsulesR_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().height = CapsulesR[i].CapsuleCollider.height;
        	CapsulesR_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().direction = CapsulesR[i].CapsuleCollider.direction;

    	}
    }

}
