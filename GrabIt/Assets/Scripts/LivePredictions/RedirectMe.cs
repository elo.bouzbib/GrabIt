﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedirectMe : MonoBehaviour
{	
	[HideInInspector]
	public GameObject rightHand, leftHand, warpedRightHand, warpedLeftHand;//, warpedSecondRightHand;

	public IList<OVRBoneCapsule> Capsules { get; private set; }
	public IList<OVRBoneCapsule> CapsulesR { get; private set; }

	public IList<OVRBoneCapsule> CapsulesL_Replica { get; private set; }
	public IList<OVRBoneCapsule> CapsulesR_Replica { get; private set; }

	public Vector3 projectVector, warpVector, warpVector_second;
	private Vector3 physicalTargetPosition, virtualTargetPosition;
	public float ds, dp, ds_second, dp_second;

	private GameObject physicalTarget, virtualTarget, startPosition;
	public bool startWarp;

	public ClosestToHand_Live[] phalanxData;
	public GameObject distractOrange;

	public Instantiate_Live gameManager;
	private Vector3 initialPosition;

    public bool reverse;

    public ReadPosAndWrite readPosWrite;


    // Start is called before the first frame update
    void Start()
    {
	    gameManager = GameObject.Find("GameManager").GetComponent<Instantiate_Live>();

		leftHand = GameObject.Find("OVRCustomHandPrefab_L"); // THIS IS HP = Hand Physical
    	rightHand = GameObject.Find("OVRCustomHandPrefab_R");
    	
    	warpedRightHand = GameObject.Find("WarpedRightHand"); // THIS IS HV = Hand Virtual
    	warpedLeftHand = GameObject.Find("WarpedLeftHand");
    	// warpedSecondRightHand = GameObject.Find("WarpedSecondRightHand"); // THIS IS HV = Hand Virtual
        readPosWrite = GameObject.FindObjectOfType<ReadPosAndWrite>();

    	Capsules = leftHand.GetComponent<OVRCustomSkeleton>().Capsules;
		CapsulesR = rightHand.GetComponent<OVRCustomSkeleton>().Capsules;

		CapsulesL_Replica = warpedLeftHand.GetComponent<OVRCustomSkeleton>().Capsules;
		CapsulesR_Replica = warpedRightHand.GetComponent<OVRCustomSkeleton>().Capsules;

        for(int i = 0; i < 19; i++)
        {
            if(!readPosWrite.simulationDebug)
            {
                CapsulesL_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().center = Capsules[i].CapsuleCollider.center;
                CapsulesL_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().radius = Capsules[i].CapsuleCollider.radius;
                CapsulesL_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().height = Capsules[i].CapsuleCollider.height;
                CapsulesL_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().direction = Capsules[i].CapsuleCollider.direction;
            
                CapsulesR_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().center = CapsulesR[i].CapsuleCollider.center;
                CapsulesR_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().radius = CapsulesR[i].CapsuleCollider.radius;
                CapsulesR_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().height = CapsulesR[i].CapsuleCollider.height;
                CapsulesR_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().direction = CapsulesR[i].CapsuleCollider.direction;

            }
            else
            {
                CapsulesL_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().center = GameObject.Find("LeftHandColl").transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().center;
                CapsulesL_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().radius = GameObject.Find("LeftHandColl").transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().radius;
                CapsulesL_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().height = GameObject.Find("LeftHandColl").transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().height;
                CapsulesL_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().direction = GameObject.Find("LeftHandColl").transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().direction;
            
                CapsulesR_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().center = GameObject.Find("RightHandColl").transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().center;
                CapsulesR_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().radius = GameObject.Find("RightHandColl").transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().radius;
                CapsulesR_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().height = GameObject.Find("RightHandColl").transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().height;
                CapsulesR_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().direction = GameObject.Find("RightHandColl").transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().direction;

            }
        }

		warpVector = new Vector3(); // = (Ds/(Ds + Dp)) * (pv - pp) (hand Global position)
		warpVector_second = new Vector3(); // = (Ds/(Ds + Dp)) * (pv - pp) (per CustomBone)

		projectVector = new Vector3();

		physicalTarget = GameObject.Find("PhysicalTotem");
		virtualTarget = GameObject.Find("VirtualTotem"); 
    	distractOrange = GameObject.Find("InitialPositions");

        startPosition = GameObject.Find("StartPosition");
		// VIRTUAL TARGET = read from AlgoHints. Same to start Warp -> as soon as prediction starts,
		// warp can begin.

		physicalTargetPosition = physicalTarget.transform.position;
		virtualTargetPosition = virtualTarget.transform.position; 
		initialPosition = new Vector3();
    	startWarp = false;

    	phalanxData = new ClosestToHand_Live[38];

    	for(int k = 0; k < 38; k++)
    	{
            // phalanxData[k] = GameObject.Find("RightHandColl").transform.GetChild(k).gameObject.GetComponent<ClosestToHand_Live>();
            phalanxData[k] = GameObject.FindObjectsOfType<ClosestToHand_Live>()[k];

    	}

    }

    // Update is called once per frame
    void Update()
    {
    	for(int k = 0; k < 38; k++) // 38
    	{
    		// if(!startWarp && gameManager.state == 1) // In gameManager -> initalize startWarp at new config
            if(gameManager.state == 1)
            {
    			if(phalanxData[k].startWarp)
	    		{
                    distractOrange.GetComponent<MeshRenderer>().material.color = Color.green;
	    			// initialPosition = phalanxData[k].initialWarpPos;
                    initialPosition = startPosition.transform.position;
                    startWarp = true;

	    		}
                else
                {
                    startWarp = false;
                    distractOrange.GetComponent<MeshRenderer>().material.color = Color.red;
                    initialPosition = Vector3.zero;
                }
    		}
    		else
    		{
    			startWarp = false;
    			initialPosition = Vector3.zero;
    		}
    		
    	}

    	// distractOrange.transform.position = new Vector3(initialPosition.x, initialPosition.y, initialPosition.z);

        // projectVector = (physicalTargetPosition - initialPosition).normalized;
        if(reverse)
        {
            projectVector = (virtualTargetPosition - initialPosition).normalized;
        }
        else
        {
            projectVector = (physicalTargetPosition - initialPosition).normalized;
        }

        for(int i = 0; i < leftHand.GetComponent<OVRCustomSkeleton>().CustomBones.Count; i++)
        {
        	if(startWarp)
        	{
                if(reverse)
                {
                    ds = Vector3.Project((rightHand.transform.position - initialPosition), projectVector).magnitude;
                    dp = Vector3.Project((virtualTargetPosition - rightHand.transform.position), projectVector).magnitude;
                

                    // ds_second = Vector3.Project((rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position - initialPosition), projectVector).magnitude;
                    // dp_second = Vector3.Project((virtualTargetPosition - rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position), projectVector).magnitude;
                    
                    // warpVector_second = (ds_second / (ds_second + dp_second)) * (physicalTargetPosition - virtualTargetPosition);
                    warpVector = (ds / (ds + dp)) * (physicalTargetPosition - virtualTargetPosition);                
                }
                else
                {
                    dp = Vector3.Project((physicalTargetPosition - rightHand.transform.position), projectVector).magnitude;
                    ds = Vector3.Project((rightHand.transform.position - initialPosition), projectVector).magnitude;

                    // ds_second = Vector3.Project((rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position - initialPosition), projectVector).magnitude;
                    // dp_second = Vector3.Project((physicalTargetPosition - rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position), projectVector).magnitude;
                
                    // warpVector_second = (ds_second / (ds_second + dp_second)) * (virtualTargetPosition - physicalTargetPosition);
                    warpVector = (ds / (ds + dp)) * (virtualTargetPosition - physicalTargetPosition); 
                }
            
        	}
        	else
        	{
        		warpVector = Vector3.zero;
        		// warpVector_second = Vector3.zero;
        	}

			
            warpedRightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position = rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position + warpVector;
        	warpedRightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles = new Vector3(rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.x, rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.y, rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.z);

        	// warpedSecondRightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position = rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position + warpVector_second;
        	// warpedSecondRightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles = new Vector3(rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.x, rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.y, rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.z);

			warpedLeftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position = leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position;
        	warpedLeftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles = new Vector3(leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.x, leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.y, leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.z);
    		
    		CapsulesL_Replica[i].CapsuleCollider.gameObject.transform.position = new Vector3(Capsules[i].CapsuleCollider.gameObject.transform.position.x, Capsules[i].CapsuleCollider.gameObject.transform.position.y, Capsules[i].CapsuleCollider.gameObject.transform.position.z);
    		CapsulesR_Replica[i].CapsuleCollider.gameObject.transform.position = new Vector3(CapsulesR[i].CapsuleCollider.gameObject.transform.position.x, CapsulesR[i].CapsuleCollider.gameObject.transform.position.y, CapsulesR[i].CapsuleCollider.gameObject.transform.position.z);    		
    		
    		// CapsulesR_Replica[i].CapsuleCollider.gameObject.transform.position = CapsulesR[i].CapsuleCollider.gameObject.transform.position;
// 
			CapsulesL_Replica[i].CapsuleCollider.gameObject.transform.eulerAngles = new Vector3(Capsules[i].CapsuleCollider.gameObject.transform.eulerAngles.x, Capsules[i].CapsuleCollider.gameObject.transform.eulerAngles.y, Capsules[i].CapsuleCollider.gameObject.transform.eulerAngles.z);
    		CapsulesR_Replica[i].CapsuleCollider.gameObject.transform.eulerAngles = new Vector3(CapsulesR[i].CapsuleCollider.gameObject.transform.eulerAngles.x, CapsulesR[i].CapsuleCollider.gameObject.transform.eulerAngles.y, CapsulesR[i].CapsuleCollider.gameObject.transform.eulerAngles.z);
        
    	}
    }

}
