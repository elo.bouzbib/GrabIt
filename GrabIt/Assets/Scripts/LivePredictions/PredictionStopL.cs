﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System.IO;
using System.Globalization;
using UnityEditor;

public class PredictionStopL : MonoBehaviour
{

	public CollisionPlanes_Lefty[] planesL;
	public int predState;
	private float[] distanceToPred, distanceToClosest, closest;
	private float[] distanceToPred1, distanceToPred2, posPhalanges;
	public float timePrediction;
    private static float timeStamp;
    public bool stopPredictions;
    private Predict_Live predictPos;
    // [HideInInspector]
    public Vector3[] finalPred;
    // [HideInInspector]
    public float[] finalDistToPred;
    // [HideInInspector]
    // public string[] name, mesh;
    // [HideInInspector]
    // public float[] scale;


    [Serializable]  
    public struct PhalanxPrediction {
        public string namePhalanx;
        public float posPhalanx;
        public float posPred;
        public float posPred1;
        public float posPred2;
        public float distToPred;
        public float distToPred1;
        public float distToPred2;
        public bool phalanxContact;
        public float closestPoint;
        public float distToClosest;
    }
    [Header("Phalanges")]   
    public PhalanxPrediction[] phalanxPrediction;



    // Start is called before the first frame update
    void Start()
    {
    	phalanxPrediction = new PhalanxPrediction[19];

    	// planesL = new CollisionPlanes_Lefty[3];
    	predictPos = new Predict_Live();
    	predictPos = GameObject.FindObjectOfType<Predict_Live>();

    	// for(int k = 0; k < 3; k++)
    	// {
    	// 	if(GameObject.FindGameObjectsWithTag("CutSectionsLeft")[k].GetComponent<CollisionPlanes_Lefty>().planeID == k)
    	// 	{
    	// 		planesL[k] = GameObject.FindGameObjectsWithTag("CutSectionsLeft")[k].GetComponent<CollisionPlanes_Lefty>();
    	// 	}
    	// }
        predState = 0;
        distanceToPred = new float[19];
		distanceToPred1 = new float[19];
		distanceToPred2 = new float[19];
        closest = new float[19];
        distanceToClosest = new float[19];
        posPhalanges = new float[19];
        finalPred = new Vector3[19];
        finalDistToPred = new float[19];
        // name = new string[19];
        // mesh = new string[19];
        // scale = new float[19];

    }

    // Update is called once per frame
    void Update()
    {
		distanceToPred = new float[19];
		distanceToPred1 = new float[19];
		distanceToPred2 = new float[19];
        distanceToClosest = new float[19];
        posPhalanges = new float[19];
        finalPred = new Vector3[19];
        finalDistToPred = new float[19];

        phalanxPrediction = new PhalanxPrediction[19];

        
        // name = new string[19];
        // mesh = new string[19];
        // scale = new float[19];

        stopPredictions = planesL[0].stopPredictions;
		
    	if(!stopPredictions)
        {
        	predState = 0;
        }

        // for(int m = 0; m < 3; m++)
        // {
        //     for(int k = 0; k < 19; k++)
        //     {
        //         if(planesL[m].name[k] != "")
        //         {
        //             name[k] = planesL[m].name[k];
        //             scale[k] = planesL[m].scale[k];
        //             mesh[k] = planesL[m].mesh[k];
        //         }
        //     }
        // }

        switch(predState)
        {
            case 0:
            	timePrediction = 0;
            	if(GameObject.FindObjectOfType<SimulationHand>() != null)
            	{
                    timeStamp = float.Parse(GameObject.FindObjectOfType<SimulationHand>().timeRead, CultureInfo.InvariantCulture);
            		for(int i = 0; i < 19; i++)
            		{
	            		closest[i] = predictPos.closest[i+19].closest_distance.magnitude;
                        distanceToPred[i] = Vector3.Distance(planesL[0].projPerPhalOnOOI[i], planesL[0].childL[i].transform.position);
                        distanceToPred1[i] = Vector3.Distance(planesL[1].projPerPhalOnOOI[i], planesL[1].childL[i].transform.position);
                        distanceToPred2[i] = Vector3.Distance(planesL[2].projPerPhalOnOOI[i], planesL[2].childL[i].transform.position);

                        distanceToClosest[i] = Mathf.Abs(closest[i] - planesL[0].childL[i].transform.position.magnitude);
	                    posPhalanges[i] = planesL[0].childL[i].transform.position.magnitude;
            		}
            	}
            	else
            	{
            		timeStamp = Time.time;
            		for(int i = 0; i < 19; i++)
            		{
	            		closest[i] = predictPos.closest[i+19].closest_distance.magnitude;
                        distanceToPred[i] = Vector3.Distance(planesL[0].projPerPhalOnOOI[i], planesL[0].childL[i].transform.position);
                		distanceToPred1[i] = Vector3.Distance(planesL[1].projPerPhalOnOOI[i], planesL[1].childL[i].transform.position);
                        distanceToPred2[i] = Vector3.Distance(planesL[2].projPerPhalOnOOI[i], planesL[2].childL[i].transform.position);
						
                        distanceToClosest[i] = Mathf.Abs(closest[i] - planesL[0].childL[i].transform.position.magnitude);
            			posPhalanges[i] = planesL[0].childL[i].transform.position.magnitude;
            			// Debug.Log(posPhalanges[i]);
            		}
            	}
                predState = 1; 
            break;

            case 1:
                for(int i = 0; i < 19; i++)
                {
                    closest[i] = predictPos.closest[i+19].closest_distance.magnitude;
                    distanceToPred[i] = Vector3.Distance(planesL[0].projPerPhalOnOOI[i], planesL[0].childL[i].transform.position);
                	distanceToPred1[i] = Vector3.Distance(planesL[1].projPerPhalOnOOI[i], planesL[1].childL[i].transform.position);
                    distanceToPred2[i] = Vector3.Distance(planesL[2].projPerPhalOnOOI[i], planesL[2].childL[i].transform.position);
        			posPhalanges[i] = planesL[0].childL[i].transform.position.magnitude;

                	if(predictPos.contactPerPhal[i])
                	{
                		timePrediction = timePrediction;
                		// Debug.Log(i + ";" + phalanxPrediction[i].namePhalanx);
                	}
                	else
                	{
                		if(GameObject.FindObjectOfType<SimulationHand>() != null)
                		{
                    		timePrediction = float.Parse(GameObject.FindObjectOfType<SimulationHand>().timeRead, CultureInfo.InvariantCulture) - timeStamp;
                		}
                		else
                		{
                			timePrediction = Time.time - timeStamp;
                		}
                        distanceToClosest[i] = Mathf.Abs(closest[i] - planesL[0].childL[i].transform.position.magnitude);
                	}
                }
                
            break;
        }


		for(int k = 0; k < 19; k++)
		{
			phalanxPrediction[k].namePhalanx = planesL[0].childL[k].gameObject.name;
			phalanxPrediction[k].posPhalanx = posPhalanges[k];
            phalanxPrediction[k].posPred = planesL[0].projPerPhalOnOOI[k].magnitude;
            phalanxPrediction[k].posPred1 = planesL[1].projPerPhalOnOOI[k].magnitude;
            phalanxPrediction[k].posPred2 = planesL[2].projPerPhalOnOOI[k].magnitude;
			phalanxPrediction[k].distToPred = distanceToPred[k];// - planesL[0].childL[k].GetComponent<CapsuleCollider>().radius; // minus PhalanxCapsule Radius
			phalanxPrediction[k].distToPred1 = distanceToPred1[k];// - planesL[0].childL[k].GetComponent<CapsuleCollider>().radius; // minus PhalanxCapsule Radius
			phalanxPrediction[k].distToPred2 = distanceToPred2[k];// - planesL[0].childL[k].GetComponent<CapsuleCollider>().radius; // minus PhalanxCapsule Radius

			phalanxPrediction[k].closestPoint = closest[k];
			phalanxPrediction[k].distToClosest = distanceToClosest[k];

            if((Vector3.Distance(predictPos.closest[k].closest_distance, planesL[0].projPerPhalOnOOI[k]) <= Vector3.Distance(predictPos.closest[k].closest_distance, planesL[2].projPerPhalOnOOI[k])) && (Vector3.Distance(predictPos.closest[k].closest_distance, planesL[0].projPerPhalOnOOI[k]) <= Vector3.Distance(predictPos.closest[k].closest_distance, planesL[1].projPerPhalOnOOI[k])))
            {
                finalPred[k] = planesL[0].projPerPhalOnOOI[k];
                finalDistToPred[k] = distanceToPred[k];

            }
            if((Vector3.Distance(predictPos.closest[k].closest_distance, planesL[1].projPerPhalOnOOI[k]) <= Vector3.Distance(predictPos.closest[k].closest_distance, planesL[2].projPerPhalOnOOI[k])) && (Vector3.Distance(predictPos.closest[k].closest_distance, planesL[1].projPerPhalOnOOI[k]) <= Vector3.Distance(predictPos.closest[k].closest_distance, planesL[0].projPerPhalOnOOI[k])))
            {
                finalPred[k] = planesL[1].projPerPhalOnOOI[k];
                finalDistToPred[k] = distanceToPred1[k];

            }
            if((Vector3.Distance(predictPos.closest[k].closest_distance, planesL[2].projPerPhalOnOOI[k]) <= Vector3.Distance(predictPos.closest[k].closest_distance, planesL[0].projPerPhalOnOOI[k])) && (Vector3.Distance(predictPos.closest[k].closest_distance, planesL[2].projPerPhalOnOOI[k]) <= Vector3.Distance(predictPos.closest[k].closest_distance, planesL[1].projPerPhalOnOOI[k])))
            {
                finalPred[k] = planesL[2].projPerPhalOnOOI[k];
                finalDistToPred[k] = distanceToPred2[k];

            }


            if((finalPred[k].magnitude != 0) && (predictPos.contactPerPhal[k+19]))
            {
                phalanxPrediction[k].phalanxContact = predictPos.contactPerPhal[k+19];
            }
            else
            {
                phalanxPrediction[k].phalanxContact = false;
            }
            // Debug.Log(phalanxPrediction[k].namePhalanx + ";" + finalPred[k]);

            // for(int i = 0; i < OOI.transform.childCount; i++)
            // {
            //     if(OOI.transform.GetChild(i).transform.GetComponent<Collider>().bounds.Contains(finalPred[k]))
            //     {
            //         name[k] = OOI.transform.GetChild(i).name;
            //         scale[k] = (100*Vector3.Scale(OOI.transform.GetChild(i).transform.localScale, OOI.transform.localScale).x * 100*Vector3.Scale(OOI.transform.GetChild(i).transform.localScale, OOI.transform.localScale).y * 100*Vector3.Scale(OOI.transform.GetChild(i).transform.localScale, OOI.transform.localScale).z);
            //         mesh[k] = OOI.transform.GetChild(i).gameObject.GetComponent<MeshFilter>().mesh.name;
            //     }
            //     else
            //     {
            //         name[k] = "";
            //         scale[k] = 0f;
            //         mesh[k] = "";
            //     }

            // }   

		}


        
    }

}
