﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System.IO;
using System.Globalization;
using UnityEditor;

public class PredictAlpha : MonoBehaviour
{

	private float[] distanceToPred, closest;
	private float[] posPhalanges;
    private Predict_Live predictPos;

    [Serializable]  
    public struct PhalanxPrediction {
        public string namePhalanx;
        public float posPhalanx;
        public float posPred;
        public float distToPred;
        public bool phalanxContact;
        public float closestPoint;
    }
    [Header("Phalanges")]   
    public PhalanxPrediction[] phalanxPrediction;



    // Start is called before the first frame update
    void Start()
    {
    	phalanxPrediction = new PhalanxPrediction[38];

    	predictPos = new Predict_Live();
    	predictPos = GameObject.FindObjectOfType<Predict_Live>();

        distanceToPred = new float[38];
        closest = new float[38];
        posPhalanges = new float[38];

    }

    // Update is called once per frame
    void Update()
    {
		distanceToPred = new float[38];
        posPhalanges = new float[38];
		
    	if(GameObject.FindObjectOfType<SimulationHand>() != null)
    	{
    		for(int i = 0; i < 19; i++)
    		{
        		closest[i] = predictPos.closest[i].closest_distance.magnitude;
        		closest[i+19] = predictPos.closest[i+19].closest_distance.magnitude;

                // distanceToPred[i] = Vector3.Distance(predictPos.handChildR[i].transform.position, predictPos.posToPredict[i]);
                // distanceToPred[i+19] = Vector3.Distance(predictPos.handChildL[i].transform.position, predictPos.posToPredict[i+19]);

                posPhalanges[i] = predictPos.handChildR[i].transform.position.magnitude;
                posPhalanges[i+19] = predictPos.handChildL[i].transform.position.magnitude;
            	
            }
    	}
    	
        


		for(int k = 0; k < 38; k++)
		{
			phalanxPrediction[k].namePhalanx = predictPos.transform.GetChild(k).gameObject.name;
            // phalanxPrediction[k].posPred = predictPos.posToPredict[k].magnitude;
            phalanxPrediction[k].posPhalanx = posPhalanges[k];
			// phalanxPrediction[k].distToPred = distanceToPred[k];// - planesR[0].childR[k].GetComponent<CapsuleCollider>().radius; // minus PhalanxCapsule Radius

			phalanxPrediction[k].phalanxContact = predictPos.contactPerPhal[k];
			phalanxPrediction[k].closestPoint = closest[k];
		}	
    }

}

