﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CollisionPlanes_Lefty : MonoBehaviour
{
    private GameObject OOI;
    [HideInInspector]
    public Vector3 closestFromVF, virtualFinger;
	private Vector3 closestDist;
	private GameObject thumb, palm, index;

	public int planeID, nbOfPlanes;
    [HideInInspector]
    public Vector3[] projPerPhal, projPerPhalOnOOI;
    [HideInInspector]
    public GameObject[] childL;

    public bool fromTop, fromSideX, fromSideZ = false;

    [HideInInspector]
    public Vector3 oldPos;

    [HideInInspector]
    public bool stopPredictions;
    public bool varyBetaGamma, varyEpsilon;

    public bool warped;


    void Start()
    {
      	OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");

        warped = GameObject.FindObjectOfType<Predict_Live>().warped;

        if(!warped)
        {
            palm = GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L/OculusHand_L/b_l_wrist/l_palm_center_marker").gameObject;
            index = GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L/OculusHand_L/b_l_wrist/b_l_index1/b_l_index2/b_l_index3/l_index_finger_pad_marker").gameObject;
            thumb = GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L/OculusHand_L/b_l_wrist/b_l_thumb0/b_l_thumb1/b_l_thumb2/b_l_thumb3/l_thumb_finger_pad_marker").gameObject;

        }
    	else
        {
            palm = GameObject.Find("Warped_OculusHand_L/b_l_wrist/l_palm_center_marker").gameObject;
            index = GameObject.Find("Warped_OculusHand_L/b_l_wrist/b_l_index1/b_l_index2/b_l_index3/l_index_finger_pad_marker").gameObject;
            thumb = GameObject.Find("Warped_OculusHand_L/b_l_wrist/b_l_thumb0/b_l_thumb1/b_l_thumb2/b_l_thumb3/l_thumb_finger_pad_marker").gameObject;
           
        }

        childL = new GameObject[19];

        for(int i = 0; i < 19; i++)
        {
            if(!warped)
            {
                childL[i] = GameObject.Find("LeftHandColl").transform.GetChild(i).transform.gameObject;
                childL[i].name = GameObject.Find("LeftHandColl").transform.GetChild(i).name;
            }
            else
            {
                childL[i] = GameObject.Find("WarpedLeftHandColl").transform.GetChild(i).transform.gameObject;
                childL[i].name = GameObject.Find("WarpedLeftHandColl").transform.GetChild(i).name;
            }
        }
        projPerPhal = new Vector3[19];
        projPerPhalOnOOI = new Vector3[19];
        fromTop = false;
        fromSideX = false;
        fromSideZ = false;
    }

    void Update()
    {
        projPerPhal = new Vector3[19];
        projPerPhalOnOOI = new Vector3[19];

      	OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");
      	// int nbKids = OOI.transform.childCount;
      	childL = new GameObject[19];
      	
  	 	for(int i = 0; i < 19; i++)
        {
            if(!warped)
            {
                childL[i] = GameObject.Find("LeftHandColl").transform.GetChild(i).transform.gameObject;
                childL[i].name = GameObject.Find("LeftHandColl").transform.GetChild(i).name;
            }
            else
            {
                childL[i] = GameObject.Find("WarpedLeftHandColl").transform.GetChild(i).transform.gameObject;
                childL[i].name = GameObject.Find("WarpedLeftHandColl").transform.GetChild(i).name;
            }
        }

    	if(!warped)
        {
            palm = GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L/OculusHand_L/b_l_wrist/l_palm_center_marker").gameObject;
            index = GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L/OculusHand_L/b_l_wrist/b_l_index1/b_l_index2/b_l_index3/l_index_finger_pad_marker").gameObject;
            thumb = GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L/OculusHand_L/b_l_wrist/b_l_thumb0/b_l_thumb1/b_l_thumb2/b_l_thumb3/l_thumb_finger_pad_marker").gameObject;

        }
        else
        {
            palm = GameObject.Find("Warped_OculusHand_L/b_l_wrist/l_palm_center_marker").gameObject;
            index = GameObject.Find("Warped_OculusHand_L/b_l_wrist/b_l_index1/b_l_index2/b_l_index3/l_index_finger_pad_marker").gameObject;
            thumb = GameObject.Find("Warped_OculusHand_L/b_l_wrist/b_l_thumb0/b_l_thumb1/b_l_thumb2/b_l_thumb3/l_thumb_finger_pad_marker").gameObject;
           
        }
    	// NEED TO CREATE PLANE, from l to this point. GetContactPoints of VFMagnitude
    	virtualFinger = (index.transform.position - thumb.transform.position);
    	closestFromVF = whosClosest(thumb.transform.position + virtualFinger/2);
   	
    	Vector3 lengthToPalm = (thumb.transform.position + virtualFinger/2) - palm.transform.position;

		// this.transform.position = new Vector3(closestFromVF.x + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.right).x, closestFromVF.y + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.up).y, closestFromVF.z + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.forward).z);
		if(GameObject.FindObjectOfType<SimulationHand>() != null)
        {
            varyBetaGamma = GameObject.FindObjectOfType<SimulationHand>().varyBetaGamma;
            varyEpsilon = GameObject.FindObjectOfType<SimulationHand>().varyEpsilon;
            if(!varyEpsilon)
            {
                if(varyBetaGamma)
                {
                   SimulationHand parameters = GameObject.FindObjectOfType<SimulationHand>();
                    if (Vector3.Distance(whosClosest(palm.transform.position), palm.transform.position) < parameters.beta*lengthToPalm.magnitude)// && onGoingVector.magnitude < parameters.gamma)
                    {
                        stopPredictions = true;
                        this.transform.position = this.transform.position;
                        // this.transform.position = new Vector3(closestFromVF.x - Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.right).x, closestFromVF.y - Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.up).y, closestFromVF.z - Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.forward).z);

                    }
                    else
                    {
                        stopPredictions = false;
                        this.transform.position = new Vector3(closestFromVF.x + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.right).x, closestFromVF.y + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.up).y, closestFromVF.z + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.forward).z);
                    } 
                }
                else
                {
                    if (Vector3.Distance(whosClosest(palm.transform.position), palm.transform.position) <= 1.05f*lengthToPalm.magnitude)// && onGoingVector.magnitude < 0.01f)
                    {
                        stopPredictions = true;
                        this.transform.position = new Vector3(closestFromVF.x - Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.right).x, closestFromVF.y - Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.up).y, closestFromVF.z - Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.forward).z);

                        // this.transform.position = this.transform.position;
                    }
                    else
                    {
                        stopPredictions = false;
                        this.transform.position = new Vector3(closestFromVF.x + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.right).x, closestFromVF.y + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.up).y, closestFromVF.z + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.forward).z);
                    }
                }
            }
            else
            {
                this.transform.position = new Vector3(closestFromVF.x + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.right).x, closestFromVF.y + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.up).y, closestFromVF.z + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.forward).z);
            }
            
        }
        else
        {
            if (Vector3.Distance(whosClosest(palm.transform.position), palm.transform.position) <= 1.05f*lengthToPalm.magnitude)// && onGoingVector.magnitude < 0.01f)
            {
                stopPredictions = true;
                this.transform.position = this.transform.position;
            }
            else
            {
                stopPredictions = false;
                this.transform.position = new Vector3(closestFromVF.x + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.right).x, closestFromVF.y + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.up).y, closestFromVF.z + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.forward).z);
            }

        }
        
        if((Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.up).y) > Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.forward).z)) && (Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.up).y) > Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.right).x)) )
        {
            this.transform.forward = virtualFinger/virtualFinger.magnitude;
            fromTop = true;
            fromSideX = false;
            fromSideZ = false;
        }
        else
        {
            this.transform.forward = virtualFinger/virtualFinger.magnitude;
            this.transform.rotation = this.transform.rotation* Quaternion.FromToRotation(Vector3.right, Vector3.up);
            fromTop = false;
            
            if((Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.up).y) < Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.right).x)) && (Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.forward).z) < Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.right).x)))
            {
                // Debug.Log("from side X");
                fromSideX = true;
                fromSideZ = false;
            }
            else if ((Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.up).y) < Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.forward).z)) && (Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.forward).z) > Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.right).x)))
            {
                // Debug.Log("from side Z");
                fromSideX = false;
                fromSideZ = true;
            }
            else
            {
                // Debug.Log("Kindof in the middle");
                fromSideX = false;
                fromSideZ = false;
            }
        }

    	this.transform.localScale = new Vector3(virtualFinger.magnitude + 0.05f, virtualFinger.magnitude + 0.05f, virtualFinger.magnitude + 0.05f)*virtualFinger.magnitude*2.0f;
        // this.transform.localScale = new Vector3(virtualFinger.magnitude, virtualFinger.magnitude, virtualFinger.magnitude)*virtualFinger.magnitude*2;

        for(int i = 0; i < 19; i++)
        {
            if(!warped)
            {
                childL[i] = GameObject.Find("LeftHandColl").transform.GetChild(i).transform.gameObject;

            }
            else
            {
                childL[i] = GameObject.Find("WarpedLeftHandColl").transform.GetChild(i).transform.gameObject;

            }


            projPerPhal[i] = this.GetComponent<Collider>().ClosestPoint(this.GetComponent<Collider>().ClosestPointOnBounds(childL[i].transform.position));
            // projPerPhalOnOOI[i] = whosClosest(projPerPhal[i]);
            if(this.GetComponent<Collider>().bounds.Contains(whosClosest(projPerPhal[i])))
            {
                projPerPhalOnOOI[i] = whosClosest(projPerPhal[i]);
            }
            else
            {

            }
        }

    }


    public Vector3 whosClosest(Vector3 fromThis)
    {
    	int nbKids = OOI.transform.childCount;
    	Vector3[] closestPoint = new Vector3[nbKids];
    	Vector3[] closest_distance = new Vector3[nbKids];
    	float[] REAL_MIN_DISTANCE = new float[nbKids];

    	for(int i = 0; i < nbKids; i++)
        {
	    	// Vector3[] objVertices = OOI.transform.GetChild(i).gameObject.GetComponent<MeshFilter>().mesh.vertices;
	    	// float REAL_MIN_DISTANCE;
        	closestPoint[i] = OOI.transform.GetChild(i).gameObject.GetComponent<Collider>().ClosestPointOnBounds(fromThis);
        	closest_distance[i] = OOI.transform.GetChild(i).gameObject.GetComponent<Collider>().ClosestPoint(closestPoint[i]);
	        REAL_MIN_DISTANCE[i] = Vector3.Distance(closest_distance[i], fromThis);
	    	// }
            if(nbKids > 1)
            {
                if(i>1)
                {
                    if(REAL_MIN_DISTANCE[i-1] <= REAL_MIN_DISTANCE[i])
                    {
                        closest_distance[i] = closest_distance[i-1];
                        closestDist = closest_distance[i];
                        // Debug.Log(OOI.transform.GetChild(i-1).gameObject.name);
                        REAL_MIN_DISTANCE[i] = REAL_MIN_DISTANCE[i-1];
                    }
                    else
                    {
                        closestDist = closest_distance[i];
                    }
                }
            }
	    	
	    	
	    }
    	return closestDist;

    }


}
