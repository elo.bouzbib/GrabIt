using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Predict_Live : MonoBehaviour
{
	private GameObject handR, handL;
	// public GameObject skeletonR;

    [HideInInspector]
    public Vector3[] oldPos;
    [HideInInspector]
	public bool recordNow;


	public int slidingWindow = 45;
	private int j;

    [HideInInspector]
    public ClosestToHand_Live[] closest;
    private Vector3[] toClosestPoint;
    [HideInInspector]
    public Vector3[] onGoingVector; 
    // [HideInInspector]
    // public float[] onGoingAngle, error, errorThumb, errorIndex, errorMiddle;
    [HideInInspector]
    public GameObject[] handChildR, handChildL;
    [HideInInspector]
    public Vector3[] projection;

    public GameObject OOI;
    
    // public Vector3 virtualFinger;
    // [HideInInspector]
    // public float projVFX, projVFY, projVFZ;
    // [HideInInspector]
    // public GameObject palm, thumb, index;

    public bool[] contactPerPhal;//, inZone_fromProbsPred;

    // public bool[] inZone, contactPerPhal, inZone_fromPlane, inZone_fromPlaneLeft;//, inZone_fromProbsPred;
    // [HideInInspector]
    // public Vector3[] errorPredict;
    // [HideInInspector]
    // public GameObject[] zone;
    // [HideInInspector]
    // public Vector3[] posToPredict;

    // // [HideInInspector]
    // // public AlgoHints algoHint;

    // public bool oneContact, clampGrip;
    // // public string[] touchedBy, probsTouchedBy, touchByPlane;
    // public List<string> gonnaTouch;
    // public int nbKids;
    // public bool varyAlpha;

    [HideInInspector]
    public string[] name, mesh;
    [HideInInspector]
    public float[] scale;

    [HideInInspector]
    public string[] namePred, meshPred;
    [HideInInspector]
    public float[] scalePred;
    [Tooltip("Is the user seeing his hand or the warped one? Intentions on warped hand?")]
    public bool warped;

    void Start()
    {

        if(!warped)
        {
            handR = GameObject.Find("RightHandColl");
            handL = GameObject.Find("LeftHandColl");
        }
        else
        {
            handR = GameObject.Find("WarpedRightHandColl");
            handL = GameObject.Find("WarpedLeftHandColl");
        }

    	handChildR = new GameObject[19];
    	handChildL = new GameObject[19];
        closest = new ClosestToHand_Live[38];
        // toClosestPoint = new Vector3[38];
        onGoingVector = new Vector3[38];
        // error = new float[38];

        for(int i = 0; i < 19; i++)
        {
        	handChildR[i] = handR.transform.GetChild(i).transform.gameObject;
        	handChildL[i] = handL.transform.GetChild(i).transform.gameObject;
            closest[i] = handChildR[i].GetComponent<ClosestToHand_Live>();
            closest[i+19] = handChildL[i].GetComponent<ClosestToHand_Live>();
         }

		oldPos = new Vector3[38];
        // projection = new Vector3[38];
        projection = new Vector3[2];
        toClosestPoint = new Vector3[2];


        for(int i = 0; i < 19; i++)
        {
            oldPos[i] = handChildR[i].transform.position;
            oldPos[i+19] = handChildL[i].transform.position;
        }
		j = 0;

        OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");
        
  //       palm = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/r_palm_center_marker").gameObject;
  //       index = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/b_r_index1/b_r_index2/b_r_index3/r_index_finger_pad_marker").gameObject;
  //       thumb = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/b_r_thumb0/b_r_thumb1/b_r_thumb2/b_r_thumb3/r_thumb_finger_pad_marker").gameObject;
       
  //       virtualFinger = (index.transform.position - thumb.transform.position);
		// projVFX = Vector3.Project(virtualFinger, Vector3.right).x;
  //       projVFY = Vector3.Project(virtualFinger, Vector3.up).y;
  //       projVFZ = Vector3.Project(virtualFinger, Vector3.forward).z;

  //       posToPredict = new Vector3[38];
  //       errorPredict = new Vector3[38];
        contactPerPhal = new bool[38];

		// zone = new GameObject[OOI.transform.childCount];
  // //       inZone = new bool[OOI.transform.childCount];

  //       for(int k = 0; k < OOI.transform.childCount; k++)
  //       {
  //           zone[k] = OOI.transform.GetChild(k).gameObject;
  //   		// inZone_fromPlane = new bool[OOI.transform.childCount];
  //     //       inZone_fromPlaneLeft = new bool[OOI.transform.childCount];

  //     //       inZone[k] = false;
  //     //       inZone_fromPlane[k] = false;
  //     //       inZone_fromPlaneLeft[k] = false;
  //       }
  //       oneContact = false;
  //       clampGrip = false;
  //       gonnaTouch = new List<string>();
  //       nbKids = OOI.transform.childCount;

        namePred = new string[38];
        meshPred = new string[38];
        scalePred = new float[38];
    }

    void Update()
    {
        recordNow = false;
    	StartCoroutine(RecordPos());

        name = new string[38];
        mesh = new string[38];
        scale = new float[38];
        
        namePred = new string[38];
        meshPred = new string[38];
        scalePred = new float[38];

        // oneContact = false;
        // clampGrip = false;

        // for(int i = 0; i < 19; i++)
        // {
        //     this.transform.GetChild(i+19).gameObject.name = "L-" + handL.transform.GetChild(i).transform.gameObject.name;
        //     this.transform.GetChild(i).gameObject.name = "R-" + handR.transform.GetChild(i).transform.gameObject.name;
        // }

        OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");
  //       nbKids = OOI.transform.childCount;
  //       zone = new GameObject[nbKids];
  // //       inZone = new bool[nbKids];
		// // inZone_fromPlane = new bool[nbKids];
  // //       inZone_fromPlaneLeft = new bool[nbKids];

  //       for(int k = 0; k < nbKids; k++)
  //       {
  //           zone[k] = OOI.transform.GetChild(k).gameObject;
  //           // inZone[k] = false;
  //           // inZone_fromPlane[k] = false;
  //           // inZone_fromPlaneLeft[k] = false;
  //       }
                
        for(int i = 0; i < 19; i++)
        {
            // toClosestPoint[i] = -closest[i].closest_distance + handChildR[i].transform.position; // WHITE RAY

            onGoingVector[i] = handChildR[i].transform.position - oldPos[i]; // RED RAY
            // projection[i] = handChildR[i].transform.position + Vector3.Project(-toClosestPoint[i], onGoingVector[i]);

            // toClosestPoint[i+19] = -closest[i+19].closest_distance + handChildL[i].transform.position;
            onGoingVector[i+19] = handChildL[i].transform.position - oldPos[i+19]; 
            // projection[i+19] = handChildL[i].transform.position + Vector3.Project(-toClosestPoint[i+19], onGoingVector[i+19]);
            
            // if(GameObject.FindObjectOfType<SimulationHand>() != null)
            // {
            //     varyAlpha = GameObject.FindObjectOfType<SimulationHand>().varyAlpha;
            //     SimulationHand parameters = GameObject.FindObjectOfType<SimulationHand>();
            //     if(varyAlpha)
            //     {
            //         posToPredict[i] = parameters.alpha*projection[i] + (1-parameters.alpha)*closest[i].closest_distance;
            //         posToPredict[i+19] = parameters.alpha*projection[i+19] + (1-parameters.alpha)*closest[i+19].closest_distance;
            //     }
            //     else
            //     {
            //         posToPredict[i] = 0.3f*projection[i] + (1-0.3f)*closest[i].closest_distance;
            //         posToPredict[i+19] = 0.3f*projection[i+19] + (1-0.3f)*closest[i+19].closest_distance;

            //     }
            // }
            // else
            // {
            //     posToPredict[i] = 0.3f*projection[i] + (1-0.3f)*closest[i].closest_distance;
            //     posToPredict[i+19] = 0.3f*projection[i+19] + (1-0.3f)*closest[i+19].closest_distance;

            // }

            contactPerPhal[i] = closest[i].graspContact;
            // contactPerPhal[i] = closest[i].phalanxContact;
            
            // errorPredict[i] = posToPredict[i] - handChildR[i].transform.position;
            // this.transform.GetChild(i).gameObject.transform.position = posToPredict[i]; 

            contactPerPhal[i+19] = closest[i+19].graspContact;
            // // contactPerPhal[i+19] = closest[i+19].phalanxContact;
            // errorPredict[i+19] = posToPredict[i+19] - handChildL[i].transform.position;
            // this.transform.GetChild(i+19).gameObject.transform.position = posToPredict[i+19];

            name[i] = closest[i].name;
            mesh[i] = closest[i].mesh;
            scale[i] = closest[i].scale;

            name[i+19] = closest[i].name;
            mesh[i+19] = closest[i].mesh;
            scale[i+19] = closest[i].scale;


            namePred[i] = OOI.GetComponent<CollideCutSections_inKids>().name[i];
            meshPred[i] = OOI.GetComponent<CollideCutSections_inKids>().mesh[i];
            scalePred[i] = OOI.GetComponent<CollideCutSections_inKids>().scale[i];
            namePred[i+19] = OOI.GetComponent<CollideCutSections_inKids>().name[i];
            meshPred[i+19] = OOI.GetComponent<CollideCutSections_inKids>().mesh[i];
            scalePred[i+19] = OOI.GetComponent<CollideCutSections_inKids>().scale[i];

            // for(int k = 0; k < nbKids; k++)
            // {
            //     // if((zone[k].GetComponent<Collider>().bounds.Contains(closest[i].closest_distance)) && (closest[i].graspContact))
            //     if((zone[k].GetComponent<Collider>().bounds.Contains(handChildR[i].transform.position)) && (closest[i].graspContact))
            //     {
            //         name[i] = zone[k].name;
            //         scale[i] = (100*Vector3.Scale(zone[k].transform.localScale, OOI.transform.localScale).x * 100*Vector3.Scale(zone[k].transform.localScale, OOI.transform.localScale).y * 100*Vector3.Scale(zone[k].transform.localScale, OOI.transform.localScale).z);
            //         mesh[i] = zone[k].gameObject.GetComponent<MeshFilter>().mesh.name;
            //     }
            //     if((zone[k].GetComponent<Collider>().bounds.Contains(handChildL[i].transform.position)) && (closest[i+19].graspContact))
            //     {
            //         name[i+19] = zone[k].name;
            //         scale[i+19] = (100*Vector3.Scale(zone[k].transform.localScale, OOI.transform.localScale).x * 100*Vector3.Scale(zone[k].transform.localScale, OOI.transform.localScale).y * 100*Vector3.Scale(zone[k].transform.localScale, OOI.transform.localScale).z);
            //         mesh[i+19] = zone[k].gameObject.GetComponent<MeshFilter>().mesh.name;
            //     }
            // }

            // for(int k = 0; k < zone.Length; k++)
            // {
            //     if(zone[k].GetComponent<Collider>().bounds.Contains(posToPredict[i]))
            //     {
            //         // ALL CHILDREN's COLLIDER -> JENGA/COLOR FACE / "UIST21!""
            //         inZone[k] = true;
            //         // zone[k].GetComponent<MeshRenderer>().material.color = Color.green;
            //         // touchedBy[k] = handChildR[i].name;
            //         if(!gonnaTouch.Contains(handChildR[i].name))
            //         {
            //             gonnaTouch.Add(this.transform.GetChild(i).gameObject.name);
            //         }
                    

            //         // SI LA LISTE NE CONTIENT PAS LE NOM, AJOUTER LE NOM, PUIS COMPTER NB Future contact Points
            //     }
            //     else
            //     {
            //         // touchedBy[k] = " ";
            //         gonnaTouch.Remove(this.transform.GetChild(i).gameObject.name);
            //         // zone[k].GetComponent<MeshRenderer>().material.color = Color.white;
            //         // FLUSH THE LIST
            //     }
            //     // LEFT
            //     if(zone[k].GetComponent<Collider>().bounds.Contains(posToPredict[i+19]))
            //     {
            //         // ALL CHILDREN's COLLIDER -> JENGA/COLOR FACE / "UIST21!""
            //         inZone[k] = true;
            //         // zone[k].GetComponent<MeshRenderer>().material.color = Color.green;
            //         // touchedBy[k] = handChildR[i].name;
            //         if(!gonnaTouch.Contains(handChildL[i].name))
            //         {
            //             gonnaTouch.Add(this.transform.GetChild(i+19).gameObject.name);
            //         }
                    

            //         // SI LA LISTE NE CONTIENT PAS LE NOM, AJOUTER LE NOM, PUIS COMPTER NB Future contact Points
            //     }
            //     else
            //     {
            //         // touchedBy[k] = " ";
            //         gonnaTouch.Remove(this.transform.GetChild(i+19).gameObject.name);
            //         // zone[k].GetComponent<MeshRenderer>().material.color = Color.white;
            //         // FLUSH THE LIST
            //     }

            //  //    inZone_fromPlane[k] = OOI.GetComponent<CollideCutSections_inKids>().collidingCutSections[k];
            // 	// if(inZone_fromPlane[k])
            // 	// {
            // 	// 	// zone[k].GetComponent<MeshRenderer>().material.color = Color.cyan;
            // 	// }

            // 	// if((!inZone_fromPlane[k]) && (!zone[k].GetComponent<Collider>().bounds.Contains(posToPredict[i])))
            // 	// {
            //  //        // zone[k].GetComponent<MeshRenderer>().material.color = Color.white;
            // 	// }

            //  //    inZone_fromPlaneLeft[k] = OOI.GetComponent<CollideCutSections_inKids>().collidingCutSectionsLeft[k];
            //  //    if(inZone_fromPlaneLeft[k])
            //  //    {
            //  //        // zone[k].GetComponent<MeshRenderer>().material.color = Color.yellow;
            //  //    }

            //  //    if((!inZone_fromPlaneLeft[k]) && (!zone[k].GetComponent<Collider>().bounds.Contains(posToPredict[i+19])))
            //  //    {
            //  //        // zone[k].GetComponent<MeshRenderer>().material.color = Color.white;
            //  //    }
            // }
        }
    
        

        // toClosestPoint[0] = -closest[0].closest_distance + handChildR[0].transform.position; // WHITE RAY
        // projection[0] = handChildR[0].transform.position + Vector3.Project(-toClosestPoint[0], onGoingVector[0]);

        // toClosestPoint[1] = -closest[19].closest_distance + handChildL[0].transform.position;
        // projection[1] = handChildL[0].transform.position + Vector3.Project(-toClosestPoint[1], onGoingVector[19]);
        
        // Debug.DrawRay(handChildR[0].transform.position, onGoingVector[0], Color.blue);
        // Debug.DrawRay(handChildL[0].transform.position, onGoingVector[19], Color.blue);
        
        j = j+1;
    	if(j%slidingWindow == 0)
    	{
    		recordNow = true;
    	}

        
        // palm = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/r_palm_center_marker").gameObject;
        // index = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/b_r_index1/b_r_index2/b_r_index3/r_index_finger_pad_marker").gameObject;
        // thumb = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/b_r_thumb0/b_r_thumb1/b_r_thumb2/b_r_thumb3/r_thumb_finger_pad_marker").gameObject;
        // virtualFinger = (index.transform.position - thumb.transform.position);

        // if(Mathf.Abs(Vector3.Dot(thumb.transform.right.normalized, palm.transform.right.normalized)) > 0.70f)
        // {
        //     if(Mathf.Abs(Vector3.Dot(thumb.transform.right.normalized, index.transform.right.normalized)) > 0.85f)
        //     {
        //         oneContact = true;
        //     }
        //     else
        //     {
        //         clampGrip = true;
        //     }

        // }

        // projVFX = Vector3.Project(virtualFinger, Vector3.right).x;
        // projVFY = Vector3.Project(virtualFinger, Vector3.up).y;
        // projVFZ = Vector3.Project(virtualFinger, Vector3.forward).z;
    }

    IEnumerator RecordPos()
    {
    	yield return new WaitUntil (() => recordNow == true);
    	for(int i = 0; i < 19; i++)
        {
            oldPos[i] = handChildR[i].transform.position;
            oldPos[i+19] = handChildL[i].transform.position;

        }
    }

}
