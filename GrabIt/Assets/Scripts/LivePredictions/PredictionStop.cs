﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System.IO;
using System.Globalization;
using UnityEditor;

public class PredictionStop : MonoBehaviour
{

	private CollisionPlanes_withKids[] planesR;
	public int predState;
	private float[] distanceToPred, distanceToClosest, closest;
	private float[] distanceToPred1, distanceToPred2, posPhalanges;
	public float timePrediction;
    private static float timeStamp;
    public bool stopPredictions;
    private Predict_Live predictPos;
    // [HideInInspector]
    public Vector3[] finalPred;
    // [HideInInspector]
    public float[] finalDistToPred;

    // [HideInInspector]
    // public string[] name, mesh;
    // [HideInInspector]
    // public float[] scale;

    [Serializable]  
    public struct PhalanxPrediction {
        public string namePhalanx;
        public float posPhalanx;
        public float posPred;
        public float posPred1;
        public float posPred2;
        public float distToPred;
        public float distToPred1;
        public float distToPred2;
        public bool phalanxContact;
        public float closestPoint;
        public float distToClosest;
    }
    [Header("Phalanges")]   
    public PhalanxPrediction[] phalanxPrediction;



    // Start is called before the first frame update
    void Start()
    {
    	phalanxPrediction = new PhalanxPrediction[19];

    	planesR = new CollisionPlanes_withKids[3];
    	predictPos = new Predict_Live();
    	predictPos = GameObject.FindObjectOfType<Predict_Live>();

    	for(int k = 0; k < 3; k++)
    	{
    		if(GameObject.FindGameObjectsWithTag("CutSections")[k].GetComponent<CollisionPlanes_withKids>().planeID == k)
    		{
    			planesR[k] = GameObject.FindGameObjectsWithTag("CutSections")[k].GetComponent<CollisionPlanes_withKids>();
    		}
    	}
        predState = 0;
        distanceToPred = new float[19];
		distanceToPred1 = new float[19];
		distanceToPred2 = new float[19];
        closest = new float[19];
        distanceToClosest = new float[19];
        posPhalanges = new float[19];
        finalPred = new Vector3[19];
        finalDistToPred = new float[19];

        // name = new string[19];
        // mesh = new string[19];
        // scale = new float[19];
    }

    // Update is called once per frame
    void Update()
    {
		distanceToPred = new float[19];
		distanceToPred1 = new float[19];
		distanceToPred2 = new float[19];
        distanceToClosest = new float[19];
        posPhalanges = new float[19];
        finalPred = new Vector3[19];
        finalDistToPred = new float[19];

        // name = new string[19];
        // mesh = new string[19];
        // scale = new float[19];

        stopPredictions = planesR[0].stopPredictions;
		
    	if(!stopPredictions)
        {
        	predState = 0;
        }

        // for(int m = 0; m < 3; m++)
        // {
        //     for(int k = 0; k < 19; k++)
        //     {
        //         if(planesR[m].name[k] != "")
        //         {
        //             name[k] = planesR[m].name[k];
        //             scale[k] = planesR[m].scale[k];
        //             mesh[k] = planesR[m].mesh[k];
        //         }
        //     }
        // }

        switch(predState)
        {
            case 0:
            	timePrediction = 0;
            	if(GameObject.FindObjectOfType<SimulationHand>() != null)
            	{
                    timeStamp = float.Parse(GameObject.FindObjectOfType<SimulationHand>().timeRead, CultureInfo.InvariantCulture);
            		for(int i = 0; i < 19; i++)
            		{
	            		closest[i] = predictPos.closest[i].closest_distance.magnitude;
                        distanceToPred[i] = Vector3.Distance(planesR[0].projPerPhalOnOOI[i], planesR[0].childR[i].transform.position);
                        distanceToPred1[i] = Vector3.Distance(planesR[1].projPerPhalOnOOI[i], planesR[1].childR[i].transform.position);
                        distanceToPred2[i] = Vector3.Distance(planesR[2].projPerPhalOnOOI[i], planesR[2].childR[i].transform.position);

                        distanceToClosest[i] = Mathf.Abs(closest[i] - planesR[0].childR[i].transform.position.magnitude);
	                    posPhalanges[i] = planesR[0].childR[i].transform.position.magnitude;

                    }
            	}
            	else
            	{
            		timeStamp = Time.time;
            		for(int i = 0; i < 19; i++)
            		{
	            		closest[i] = predictPos.closest[i].closest_distance.magnitude;
                        distanceToPred[i] = Vector3.Distance(planesR[0].projPerPhalOnOOI[i], planesR[0].childR[i].transform.position);
                		distanceToPred1[i] = Vector3.Distance(planesR[1].projPerPhalOnOOI[i], planesR[1].childR[i].transform.position);
                        distanceToPred2[i] = Vector3.Distance(planesR[2].projPerPhalOnOOI[i], planesR[2].childR[i].transform.position);
						
                        distanceToClosest[i] = Mathf.Abs(closest[i] - planesR[0].childR[i].transform.position.magnitude);
	                    posPhalanges[i] = planesR[0].childR[i].transform.position.magnitude;

                    }
            	}
                predState = 1; 
            break;

            case 1:
                for(int i = 0; i < 19; i++)
                {
            		closest[i] = predictPos.closest[i].closest_distance.magnitude;
                    distanceToPred[i] = Vector3.Distance(planesR[0].projPerPhalOnOOI[i], planesR[0].childR[i].transform.position);
                	distanceToPred1[i] = Vector3.Distance(planesR[1].projPerPhalOnOOI[i], planesR[1].childR[i].transform.position);
                    distanceToPred2[i] = Vector3.Distance(planesR[2].projPerPhalOnOOI[i], planesR[2].childR[i].transform.position);
                    posPhalanges[i] = planesR[0].childR[i].transform.position.magnitude;

                	if(predictPos.contactPerPhal[i])
                	{
                		timePrediction = timePrediction;
                		// Debug.Log(i + ";" + phalanxPrediction[i].namePhalanx);
                	}
                	else
                	{
                		if(GameObject.FindObjectOfType<SimulationHand>() != null)
                		{
                    		timePrediction = float.Parse(GameObject.FindObjectOfType<SimulationHand>().timeRead, CultureInfo.InvariantCulture) - timeStamp;
                		}
                		else
                		{
                			timePrediction = Time.time - timeStamp;
                		}
                        distanceToClosest[i] = Mathf.Abs(closest[i] - planesR[0].childR[i].transform.position.magnitude);
                	}
                }
                
                // If grasp -> back to 0;
            break;
        }


		for(int k = 0; k < 19; k++)
		{
			phalanxPrediction[k].namePhalanx = planesR[0].childR[k].gameObject.name;
            phalanxPrediction[k].posPred = planesR[0].projPerPhalOnOOI[k].magnitude;
            phalanxPrediction[k].posPred1 = planesR[1].projPerPhalOnOOI[k].magnitude;
            phalanxPrediction[k].posPred2 = planesR[2].projPerPhalOnOOI[k].magnitude;
            phalanxPrediction[k].posPhalanx = posPhalanges[k];
			phalanxPrediction[k].distToPred = distanceToPred[k];// - planesR[0].childR[k].GetComponent<CapsuleCollider>().radius; // minus PhalanxCapsule Radius
			phalanxPrediction[k].distToPred1 = distanceToPred1[k];// - planesR[0].childR[k].GetComponent<CapsuleCollider>().radius; // minus PhalanxCapsule Radius
			phalanxPrediction[k].distToPred2 = distanceToPred2[k];// - planesR[0].childR[k].GetComponent<CapsuleCollider>().radius; // minus PhalanxCapsule Radius

			phalanxPrediction[k].closestPoint = closest[k];
			phalanxPrediction[k].distToClosest = distanceToClosest[k];

            if((Vector3.Distance(predictPos.closest[k].closest_distance, planesR[0].projPerPhalOnOOI[k]) <= Vector3.Distance(predictPos.closest[k].closest_distance, planesR[2].projPerPhalOnOOI[k])) && (Vector3.Distance(predictPos.closest[k].closest_distance, planesR[0].projPerPhalOnOOI[k]) <= Vector3.Distance(predictPos.closest[k].closest_distance, planesR[1].projPerPhalOnOOI[k])))
            {
                finalPred[k] = planesR[0].projPerPhalOnOOI[k];
                finalDistToPred[k] = distanceToPred[k];
            }
            if((Vector3.Distance(predictPos.closest[k].closest_distance, planesR[1].projPerPhalOnOOI[k]) <= Vector3.Distance(predictPos.closest[k].closest_distance, planesR[2].projPerPhalOnOOI[k])) && (Vector3.Distance(predictPos.closest[k].closest_distance, planesR[1].projPerPhalOnOOI[k]) <= Vector3.Distance(predictPos.closest[k].closest_distance, planesR[0].projPerPhalOnOOI[k])))
            {
                finalPred[k] = planesR[1].projPerPhalOnOOI[k];
                finalDistToPred[k] = distanceToPred1[k];
            }
            if((Vector3.Distance(predictPos.closest[k].closest_distance, planesR[2].projPerPhalOnOOI[k]) <= Vector3.Distance(predictPos.closest[k].closest_distance, planesR[0].projPerPhalOnOOI[k])) && (Vector3.Distance(predictPos.closest[k].closest_distance, planesR[2].projPerPhalOnOOI[k]) <= Vector3.Distance(predictPos.closest[k].closest_distance, planesR[1].projPerPhalOnOOI[k])))
            {
                finalPred[k] = planesR[2].projPerPhalOnOOI[k];
                finalDistToPred[k] = distanceToPred2[k];
            }  

            if((finalPred[k].magnitude != 0) && (predictPos.contactPerPhal[k]))
            {
                phalanxPrediction[k].phalanxContact = predictPos.contactPerPhal[k];
            }
            else
            {
                phalanxPrediction[k].phalanxContact = false;
            }

		}

    }

    // void OnDrawGizmos()
    // {
    //     Debug.DrawRay(planesR[0].childR[9].transform.position, finalPred[9] - planesR[0].childR[9].transform.position, Color.white);
    // }

}
