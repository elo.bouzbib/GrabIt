﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class Instantiate_Live : MonoBehaviour
{

	public bool warped;

	[Serializable]	
 	public struct TasksChoice {
	    public string nameTask;
	    public int nbTask;
 	}
 	
 	[Tooltip("Touch, Pull, Push, Move Over, Raise, Push Down, Fit, Roll")]	
 	[Header("Tasks")]
 	public TasksChoice[] declaredTasks;

	[Serializable]	
 	public struct ObjectChoice {
	    public string objectName;
	    public int nbObject;
 	}
 	[Header("Objects")]
 	public ObjectChoice[] declaredObjects;

	[Header("Scales")]
	public Vector3[] scales;

	private string[] tasks;
    [HideInInspector]
	public GameObject[] objectTypes;
	private GameObject[] phantomObject;
	public List<int> configException;

    private Vector3[] posPhantoms;
    private Vector3 posOrigin;

    [Range(0, 127)]
    public int config;
    public Vector3 scaleObj;
    public GameObject objToCatch;
    public string taskToDo;
    private int taskNumber, objId;

    public int nbBloc = 0;
    public int nbBlocMax = 1;
    public int state = -1;

    public Material phantomMaterialRed, phantomMaterialGreen;

    private Transform[] walls;
    private TextMesh[] otherwalls;
    [HideInInspector]
	public GameObject Text3D;

	public bool realGame;
	// private GameObject resetButton, nextButton, startButton, startLeftButton;
	private GameObject startButton;
	private GameObject[] hands;

	public GameObject[] hands_b;

	private Vector3 previousAngles;

	public int nbTouch = 0;
	public bool grasp;
	private bool[] graspR, graspL, graspOne;


	private int correctBug = 0;
	private bool startOver = false;
	private int secondBug = 0;

	[HideInInspector]
	public GameObject handCollR, handCollL;

    // Start is called before the first frame update
    void Start()
    {
    	warped = GameObject.FindObjectOfType<Predict_Live>().warped;
    	// posOrigin = new Vector3(-0.25f, -0.63f, 0.335f);
    	posOrigin = new Vector3(-0.25f, -0.4f, 0.335f);

		// TASKS
		tasks = new string[declaredTasks.Length];
		for(int i = 0; i < declaredTasks.Length; i++)
		{
			tasks[declaredTasks[i].nbTask] = declaredTasks[i].nameTask;
		}
		

		// OBJECTS
		objectTypes = new GameObject[declaredObjects.Length];
		phantomObject = new GameObject[declaredObjects.Length];
		for(int i = 0; i < declaredObjects.Length; i++)
		{
			objectTypes[declaredObjects[i].nbObject] = GameObject.Find("ObjectsOfInterest/" + declaredObjects[i].objectName);
			phantomObject[declaredObjects[i].nbObject] = GameObject.Find("PhantomObjects/" + declaredObjects[i].objectName);
		}
    	for(int i = 0; i < objectTypes.Length; i++)
    	{
	    	objectTypes[i].SetActive(false);
	    	phantomObject[i].SetActive(false);
	    	// 0: cylinder; 1: cube; 2: sphere; 3: capsule
    	}

// REAL
		posPhantoms = new Vector3[tasks.Length];
		for(int i = 0; i < tasks.Length; i++)
		{
			if(declaredTasks[i].nameTask == "Touch")
			{
				posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z); // TOUCH
			}
			if(declaredTasks[i].nameTask == "Push")
			{
				posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z + 0.2f); // PUSH
			}
			if(declaredTasks[i].nameTask == "Pull")
			{
				posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z - 0.2f); // PULL
			}
			if(declaredTasks[i].nameTask == "Move Over")
			{
				posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x + 0.6f, posOrigin.y, posOrigin.z - 0.15f); // MOVEOVER
			}
			if(declaredTasks[i].nameTask == "Raise")
			{
				posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x, posOrigin.y + 0.5f, posOrigin.z); // RAISE
			}
			if(declaredTasks[i].nameTask == "Push Down")
			{
				posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x, posOrigin.y - 0.1f, posOrigin.z); // PUSHDOWN
			}
			if(declaredTasks[i].nameTask == "Fit")
			{
				posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x - 0.6f, posOrigin.y, posOrigin.z + 0.35f); // FIT
			}
			if(declaredTasks[i].nameTask == "Roll")
			{
				posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x + 0.8f, posOrigin.y, posOrigin.z); // MOVE
			}
		}

		configException = new List<int>();
		config = UnityEngine.Random.Range(0, tasks.Length*objectTypes.Length*scales.Length);

    	walls = GameObject.Find("Walls").GetComponentsInChildren<Transform>();
		Text3D = GameObject.Find("Text3D");
		otherwalls = new TextMesh[walls.Length];

    	for (int i = 2; i < otherwalls.Length; i++)
    	{
	    	otherwalls[i] = (TextMesh)Instantiate(Text3D, walls[i].gameObject.transform).GetComponent<TextMesh>();
	    	otherwalls[i].transform.position = walls[i].gameObject.transform.position;
	    	otherwalls[i].transform.eulerAngles = new Vector3(walls[i].gameObject.transform.eulerAngles.x, walls[i].gameObject.transform.eulerAngles.y - 90f, walls[i].gameObject.transform.eulerAngles.z);

	    	otherwalls[i].characterSize = 0.02f;
    	}

    	// resetButton = GameObject.Find("ResetButton");
    	// nextButton = GameObject.Find("NextButton");
    	// hands = new GameObject[]{GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L"), GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R")};
    	
    	startButton = GameObject.Find("StartPosition");
    	// startLeftButton = GameObject.Find("StartLeft");
    	grasp = false;

    	if(!warped)
    	{
	    	hands = new GameObject[]{GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L"), GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R")};
	    	hands_b = new GameObject[]{GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L"), GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R")};

	    	handCollR = GameObject.Find("RightHandColl");
	    	handCollL = GameObject.Find("LeftHandColl");    		
    	}
    	else
    	{
	    	hands = new GameObject[]{GameObject.Find("WarpedLeftHand"), GameObject.Find("WarpedRightHand")};
	    	hands_b = new GameObject[]{GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L"), GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R")};

	    	handCollR = GameObject.Find("WarpedRightHandColl");
    		handCollL = GameObject.Find("WarpedLeftHandColl");
    	}



    }

    // Update is called once per frame
    void Update()
    {
    	hands_b = new GameObject[]{GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L"), GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R")};

    	// Going through i, j, k and assigning tasks/objects/scales to configurations numbers.
		for(int k = 0; k < scales.Length; k++)
		{
			if((config < tasks.Length*objectTypes.Length*(k+1)) && (config >= tasks.Length*objectTypes.Length*k))
			{
				taskToDo = tasks[config%tasks.Length];
				taskNumber = config%tasks.Length;
				for(int j = 0; j < objectTypes.Length; j++)
				{
					if((config < tasks.Length*(objectTypes.Length*k + j+1)) && (config >= tasks.Length*(objectTypes.Length*k + j)))
					{
						objToCatch = objectTypes[j];
						objId = j;
					}
				}
				scaleObj = scales[k];
			}
		}

		Debug.Log("Etat: " + state);

		// FOR ANALYSIS, WaitUntil Grasp and FreezeAll in the meantime;
    	// ATTENTION: GRAVITY OF OOI REMOVED IN COLORCHANGEKids.CS FOR ANALYSIS OF GRASP BEHAVIOUR
		for(int k = 0; k < 19; k++)
		{
			graspR = new bool[19];
			graspL = new bool[19];
			if(realGame)
			{
				graspR[k] = handCollR.transform.GetChild(k).GetComponent<ClosestToHand_Live>().graspContact;
				graspL[k] = handCollL.transform.GetChild(k).GetComponent<ClosestToHand_Live>().graspContact;
				// if(graspR[k] || graspL[k])
				// {
				// 	grasp = true;
				// }
				if(((graspR[k]) && (GameObject.FindObjectOfType<PredictionStop>().finalPred[k].magnitude != 0)) || ((graspL[k]) && (GameObject.FindObjectOfType<PredictionStopL>().finalPred[k].magnitude != 0)))
				{
					grasp = true;
				}
			}
			else
			{
				grasp = true;
			}
		}

		for(int m = 0; m < configException.Count; m++)
		{
			for(int p = 0; p < configException.Count; p++)
			{
				if(p != m)
				{
					if(configException[m] == configException[p])
					{
						configException.Remove(configException[m]);
					}
				}
			}
		}

		// if(handCollR.transform.GetChild(9).GetComponent<ClosestToHand_Live>().phalanxContact)
  //   	{
	 //    	GameObject.Find("DistractOrange").GetComponent<MeshRenderer>().material.color = Color.green;
  //   		// handCollR.transform.GetChild(9).GetComponent<ClosestToHand_Live>().REAL_MIN_DISTANCE[0];
  //   	}

		// for(int k = 0; k < objectTypes[objId].transform.childCount; k++)
		// {
		// 	graspOne = new bool[objectTypes[objId].transform.childCount];
		// 	objectTypes[objId].transform.GetChild(k).GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
		// 	if(realGame)
		// 	{
		// 		graspOne[k] = objectTypes[objId].transform.GetChild(k).GetComponent<ColorChangeLive>().graspContact;	
		// 		if(graspOne[k])
		// 		{
		// 			grasp = true;
		// 		}
		// 	}
		// 	else
		// 	{
		// 		grasp = true;
		// 	}
		// }

		if(Input.GetKeyDown(KeyCode.A))
		{
			realGame = !realGame;
		}
		
		if(correctBug >= 1)
		{
			state = 3;
		}

		// if(grasp)
		// {
		// 	GameObject.Find("Distractor").GetComponent<MeshRenderer>().material.color = Color.green;
		// }
		// else
		// {
		// 	GameObject.Find("Distractor").GetComponent<MeshRenderer>().material.color = Color.red;
		// }

		startOver = false;


		switch(state)
		{
			case -2:

				nbTouch = nbTouch + 1;
				if(configException.Count >= (tasks.Length*objectTypes.Length*scales.Length))
				{
					nbBloc = nbBloc + 1;
					if(nbBloc >= nbBlocMax)
					{
						state = 2;
					}
					else
					{
						startOver = true;
					}
				}
				else
				{
					Debug.Log("NBTOUCH +1");
					state = -1;
				}

				if(startOver)
				{
					configException.Clear();
					configException = new List<int>();
					Debug.Log("NBTOUCH -1");
					nbTouch = -1;
				}
			break;
		
			case -1:
				config = UnityEngine.Random.Range(0, tasks.Length*objectTypes.Length*scales.Length);
				while(configException.Contains(config))
				{
					config = UnityEngine.Random.Range(0, tasks.Length*objectTypes.Length*scales.Length);
				}
				for(int i = 0; i < objectTypes.Length; i++)
				{
					objectTypes[i].SetActive(false);
					phantomObject[i].SetActive(false);
				}

				state = 0;

			break;

			case 0:
				grasp = false;
				configException.Add(config);
				
                objectTypes[objId].SetActive(true);
                objectTypes[objId].transform.position = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z);
                objectTypes[objId].transform.localScale = new Vector3(scaleObj.x, scaleObj.y, scaleObj.z);

                // ATTENTION ANGLES de BASE
                if((objectTypes[objId].name == "S") || (objectTypes[objId].name == "S"))
                {
                	objectTypes[objId].transform.eulerAngles = new Vector3(0f, 0f, -90f);
                }
                else if((objectTypes[objId].name == "T"))
                {
                	objectTypes[objId].transform.eulerAngles = new Vector3(180f, 0f, 0f);
                }
                else if((objectTypes[objId].name == "rabbit"))
                {
                	objectTypes[objId].transform.eulerAngles = new Vector3(-90f, 0f, 160f);
                }
                else
                {
	                objectTypes[objId].transform.eulerAngles = new Vector3(0f, 0f, 0f);
                }

                phantomObject[objId].SetActive(true);
                phantomObject[objId].transform.position = new Vector3(posPhantoms[taskNumber].x, posPhantoms[taskNumber].y, posPhantoms[taskNumber].z);
                phantomObject[objId].transform.localScale = new Vector3(scaleObj.x, scaleObj.y, scaleObj.z);


                previousAngles = new Vector3(objectTypes[objId].transform.eulerAngles.x, objectTypes[objId].transform.eulerAngles.y, objectTypes[objId].transform.eulerAngles.z);
                for(int k = 0; k < objectTypes[objId].transform.childCount; k++)
				{
                    phantomObject[objId].transform.GetChild(k).gameObject.GetComponent<MeshRenderer>().material = phantomMaterialRed;
               		phantomObject[objId].transform.GetChild(k).gameObject.GetComponent<Collider>().enabled = false;

	                objectTypes[objId].transform.GetChild(k).gameObject.GetComponent<Renderer>().enabled = true;
	                phantomObject[objId].transform.GetChild(k).gameObject.GetComponent<Renderer>().enabled = true;

	    			objectTypes[objId].transform.GetChild(k).GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;


    			}

    			previousAngles = new Vector3(objectTypes[objId].transform.eulerAngles.x, objectTypes[objId].transform.eulerAngles.y, objectTypes[objId].transform.eulerAngles.z);
                // StartCoroutine(WaitForFirstGrasp());
                state = 1;
			break;

			case 1:
                GameObject.Find("ObjectsOfInterest").transform.parent = null;
				correctBug = 0;
				// grasp = false;
				StartCoroutine(Consignes());
				
				// if((GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L") != null) && (GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R") != null))
				// {
				// 	// for(int i = 0; i < hands.Length; i++)
				// 	// {
				// 	// 	if((Mathf.Abs(hands[i].transform.position.x - resetButton.transform.position.x) < 0.07f) && (Mathf.Abs(hands[i].transform.position.y - resetButton.transform.position.y) < 0.07f) && (Mathf.Abs(hands[i].transform.position.z - resetButton.transform.position.z) < 0.07f))
				// 	// 	{
				// 	// 		// resetButton.GetComponent<MeshRenderer>().material.color = Color.green;
				// 	// 		objectTypes[objId].transform.position = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z);
				// 	// 		objectTypes[objId].transform.eulerAngles = new Vector3(previousAngles.x, previousAngles.y, previousAngles.z);
				// 	// 	}
				// 	// }

				// 	// for(int i = 0; i < hands.Length; i++)
				// 	// {
				// 	// 	if((Mathf.Abs(hands[i].transform.position.x - nextButton.transform.position.x) < 0.07f) && (Mathf.Abs(hands[i].transform.position.y - nextButton.transform.position.y) < 0.07f) && (Mathf.Abs(hands[i].transform.position.z - nextButton.transform.position.z) < 0.07f))
				// 	// 	{
				// 	// 		secondBug = secondBug + 1;
				// 	// 		if(secondBug == 1)
				// 	// 		{
				// 	// 			StartCoroutine(WaitingTask());
				// 	// 		}
				// 	// 	}
				// 	// }
				// }
				if(Input.GetKeyDown(KeyCode.Space))
				{
					StartCoroutine(WaitForNext());
				}
				
				for(int k = 0; k < objectTypes[objId].transform.childCount; k++)
				{
                    phantomObject[objId].transform.GetChild(k).gameObject.GetComponent<MeshRenderer>().material = phantomMaterialRed;
    			
                    if((Mathf.Abs(objectTypes[objId].transform.position.x - phantomObject[objId].transform.position.x) < 0.07f) && (Mathf.Abs(objectTypes[objId].transform.position.y - phantomObject[objId].transform.position.y) < 0.07f) && (Mathf.Abs(objectTypes[objId].transform.position.z - phantomObject[objId].transform.position.z) < 0.07f))
					{
						phantomObject[objId].transform.GetChild(k).gameObject.GetComponent<MeshRenderer>().material = phantomMaterialGreen;
						
					}
    			}

				
				if((GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L") != null) && (GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R") != null))
				{
					if(grasp) 
					{
						secondBug = secondBug + 1;
					}
					if(secondBug == 1)
					{
						StartCoroutine(WaitingTask());
					}
				}
				
			break;

			case 2:
				StartCoroutine(EndOfTheGame());
			break;

			case 3:
				correctBug = 0;
				secondBug = 0;
				grasp = false;
				Debug.Log("Just waiting for Coroutine to end...");
				Debug.Log("NbTouch: " + nbTouch + "; ConfigCount: " + configException.Count);

				for(int k = 0; k < objectTypes[objId].transform.childCount; k++)
				{
			    	objectTypes[objId].transform.GetChild(k).gameObject.GetComponent<Renderer>().enabled = false;
					phantomObject[objId].transform.GetChild(k).gameObject.GetComponent<Renderer>().enabled = false;
				}
				for (int i = 2; i < otherwalls.Length; i++)
		    	{
			    	otherwalls[i].text = "Press Start Buttons to Get Started Again. ";// + "\n NbTouch: " + configException.Count;
		    	}
				if((GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L") != null) && (GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R") != null))
				{
					for(int i = 0; i < hands.Length; i++)
					{
						// if((Mathf.Abs(hands[1].transform.position.x - startButton.transform.position.x) < 0.07f) && (Mathf.Abs(hands[1].transform.position.y - startButton.transform.position.y) < 0.07f) && (Mathf.Abs(hands[1].transform.position.z - startButton.transform.position.z) < 0.07f))
						// {
						// 	if((Mathf.Abs(hands[0].transform.position.x - startLeftButton.transform.position.x) < 0.07f) && (Mathf.Abs(hands[0].transform.position.y - startLeftButton.transform.position.y) < 0.07f) && (Mathf.Abs(hands[0].transform.position.z - startLeftButton.transform.position.z) < 0.07f))
						// 	{
						// 		for (int j = 2; j < otherwalls.Length; j++)
						//     	{
						// 	    	otherwalls[j].text = "";
						//     	}
						//     	StartCoroutine(CollisionWait());
						// 		state = -2;
						// 	}
							
						// }
						if((Mathf.Abs(hands[i].transform.position.x - startButton.transform.position.x) < 0.07f) && (Mathf.Abs(hands[i].transform.position.y - startButton.transform.position.y) < 0.07f) && (Mathf.Abs(hands[i].transform.position.z - startButton.transform.position.z) < 0.07f))
						{
							for (int j = 2; j < otherwalls.Length; j++)
					    	{
						    	otherwalls[j].text = "";
					    	}
					    	StartCoroutine(CollisionWait());
							state = -2;
						}

						if((Mathf.Abs(hands_b[i].transform.position.x - startButton.transform.position.x) < 0.07f) && (Mathf.Abs(hands_b[i].transform.position.y - startButton.transform.position.y) < 0.07f) && (Mathf.Abs(hands_b[i].transform.position.z - startButton.transform.position.z) < 0.07f))
						{
							for (int j = 2; j < otherwalls.Length; j++)
					    	{
						    	otherwalls[j].text = "";
					    	}
					    	StartCoroutine(CollisionWait());
							state = -2;
						}
					}
				}

				if(Input.GetKeyDown(KeyCode.Space))
				{
					for (int j = 2; j < otherwalls.Length; j++)
			    	{
				    	otherwalls[j].text = "";
			    	}
			    	StartCoroutine(CollisionWait());
					state = -2;
				}
			break;
		}
    }

    IEnumerator Consignes()
    {
    	// yield return new WaitForSeconds(1);
    	for (int i = 2; i < otherwalls.Length; i++)
    	{
	    	otherwalls[i].text = taskToDo + " the WHITE " + objToCatch.gameObject.name + "\n into the RED " + objToCatch.gameObject.name + " until it becomes GREEN. \n Task #" + (configException.Count + tasks.Length*objectTypes.Length*scales.Length*nbBloc) + "/" + (tasks.Length*objectTypes.Length*scales.Length*nbBlocMax);
    	}
    	// yield return new WaitForSeconds(5);
    	yield return new WaitUntil (() => state == 3);
    }

    IEnumerator CollisionWait()
    {
    	for (int i = 2; i < otherwalls.Length; i++)
    	{
	    	otherwalls[i].text = "";
    	}
        // yield return new WaitForSeconds(1);
        yield return new WaitUntil (() => state == 1);
    }

    IEnumerator WaitForFirstGrasp()
    { // FOR ANALYSIS, WaitUntil Grasp and FreezeAll in the meantime;
    	// ATTENTION: GRAVITY OF OOI REMOVED IN COLORCHANGEKids.CS FOR ANALYSIS OF GRASP BEHAVIOUR
		// objectTypes[objId].GetComponentInChildren<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
		// yield return new WaitUntil(() => (objectTypes[objId].GetComponentInChildren<ColorChangeKids>().graspContact == true));
		for(int k = 0; k < objectTypes[objId].transform.childCount; k++)
		{
			//0:Hold, 1:Push: 2:Pull: 3:MoveOver; 4:Raise; 5:PsuhDown; 6:Fit; 7:Roll 
			if(declaredTasks[objId].nameTask == "Touch")//if(taskNumber == 0) // All
			{
				objectTypes[objId].transform.GetChild(k).gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
			}
			if(declaredTasks[objId].nameTask == "Push" || declaredTasks[objId].nameTask == "Pull")//if(taskNumber == 1 || taskNumber == 2)//Push/Pull -> FreeZ
			{
				objectTypes[objId].transform.GetChild(k).gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY & RigidbodyConstraints.FreezePositionX & RigidbodyConstraints.FreezeRotationX & RigidbodyConstraints.FreezeRotationY & RigidbodyConstraints.FreezeRotationZ;
			}
			if(declaredTasks[objId].nameTask == "Fit" || declaredTasks[objId].nameTask == "Move Over")//if(taskNumber == 3 || taskNumber == 6) // None
			{
				objectTypes[objId].transform.GetChild(k).gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;

			}
			if(declaredTasks[objId].nameTask == "Raise" || declaredTasks[objId].nameTask == "PushDown")//if(taskNumber == 4 || taskNumber == 5)//Raise/PushDown -> FreeY
			{
				objectTypes[objId].transform.GetChild(k).gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ & RigidbodyConstraints.FreezePositionX & RigidbodyConstraints.FreezeRotationX & RigidbodyConstraints.FreezeRotationY & RigidbodyConstraints.FreezeRotationZ;
			}
			if(declaredTasks[objId].nameTask == "Roll")//if(taskNumber == 7) // FreeX | RotZ | RotY
			{
				objectTypes[objId].transform.GetChild(k).gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ & RigidbodyConstraints.FreezePositionY & RigidbodyConstraints.FreezeRotationX;// | RigidbodyConstraints.FreezeRotationY;
			}
		}
		yield return new WaitUntil(() => (grasp == true)); //(objectTypes[objId].GetComponentInChildren<ColorChangeKids>().graspContact == true));
	}
    	

    IEnumerator WaitForNext()
    {
    	// MAYBE ADD NEED TO GO START FOR NEXT STEP
    	Debug.Log("Coroutine");
    	yield return new WaitForSeconds(0.5f); // 1sec for tech eval
    	correctBug = correctBug + 1; // This instead of having state = 3 in coroutine.
    	for(int k = 0; k < objectTypes[objId].transform.childCount; k++)
		{
	    	objectTypes[objId].transform.GetChild(k).gameObject.GetComponent<Renderer>().enabled = false;
			phantomObject[objId].transform.GetChild(k).gameObject.GetComponent<Renderer>().enabled = false;
			graspR[k] = false;
			graspL[k] = false;
		}
        objectTypes[objId].transform.position = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z);
    }

    IEnumerator WaitingTask()
    {
    	// CAN INCREASE HERE AND HAVE PEOPLE TOUCH WITHIN 5SECS INSTEAD!
    	yield return new WaitForSeconds(0.5f); // 2 for collect user tech eval
    	grasp = false;
		StartCoroutine(WaitForNext());
    }

    IEnumerator EndOfTheGame()
    {
    	for(int i = 0; i < objectTypes.Length; i++)
    	{
	    	objectTypes[i].SetActive(false);
	    	phantomObject[i].SetActive(false);
    	}
    	for (int i = 2; i < otherwalls.Length; i++)
    	{
	    	otherwalls[i].text = "The Game is Over. Thank you!";
    	}

    	Debug.Log("Fin du Game!");
    	yield return new WaitForSeconds(2);
    	if(realGame)
    	{
	    	Application.Quit();
    	}
    	else
    	{
	    	Debug.Break();
    	}
    }
}

