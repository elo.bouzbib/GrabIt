using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ClosestToHand_Live : MonoBehaviour
{
  public bool phalanxContact;
  public GameObject OOI;
  public bool graspContact;

  [HideInInspector]
  public Vector3[] closestDist, closestPoint;
  public Vector3 closest_distance;

  public float[] REAL_MIN_DISTANCE;
  private Vector3 worldVertex;
  public int nbKids;

  // [HideInInspector]
  public bool startWarp;
  [HideInInspector]
  public Vector3 initialWarpPos;

  [HideInInspector]
  public string name, mesh;
  [HideInInspector]
  public float scale;

  public string shareMe,shareMeTooX, shareMeTooY, shareMeTooZ;


  // private Collision collisionInfo;

    void Start()
    {
      OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");
      nbKids = OOI.transform.childCount;
      graspContact = false;
      initialWarpPos = new Vector3();
      scale = new float();
      name = "";
      mesh = "";
      // collisionInfo = new Collision();
    }

    void Update()
    {
        graspContact = false;
        scale = new float();
        name = "";
        mesh = "";

        OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");

        
        nbKids = OOI.transform.childCount;
        REAL_MIN_DISTANCE = new float[nbKids];
        closestPoint = new Vector3[nbKids];
        closestDist = new Vector3[nbKids];

        for(int i = 0; i < nbKids; i++)
        {
            closestPoint[i] = OOI.transform.GetChild(i).gameObject.GetComponent<Collider>().ClosestPointOnBounds(this.transform.position);
            closestDist[i] = OOI.transform.GetChild(i).gameObject.GetComponent<Collider>().ClosestPoint(closestPoint[i]);
            REAL_MIN_DISTANCE[i] = Vector3.Distance(closestDist[i], this.transform.position);// - this.GetComponent<CapsuleCollider>().radius;

            if(nbKids <= 1)
            {
                closest_distance = closestDist[i];
                if((REAL_MIN_DISTANCE[i] < 0.02f) && phalanxContact)// && (gameManager.state == 1))
                {
                  graspContact = true;
                  if(GameObject.FindObjectOfType<SimulationHand>() != null)
                  {
                    name = OOI.transform.GetChild(i).gameObject.name;
                    mesh = OOI.transform.GetChild(i).gameObject.GetComponent<MeshFilter>().mesh.name;
                    scale = (100*Vector3.Scale(OOI.transform.GetChild(i).gameObject.transform.localScale, OOI.transform.localScale).x * 100*Vector3.Scale(OOI.transform.GetChild(i).gameObject.transform.localScale, OOI.transform.localScale).y * 100*Vector3.Scale(OOI.transform.GetChild(i).gameObject.transform.localScale, OOI.transform.localScale).z);
                  }
                }
                
            }
            else
            {

              if(i>1)
              {
                  if(REAL_MIN_DISTANCE[i-1] <= REAL_MIN_DISTANCE[i])
                  {
                      closestDist[i] = closestDist[i-1];
                      closest_distance = closestDist[i];
                      REAL_MIN_DISTANCE[i] = REAL_MIN_DISTANCE[i-1];
                  }
                  else
                  {
                      closest_distance = closestDist[i];
                      REAL_MIN_DISTANCE[i-1] = REAL_MIN_DISTANCE[i];
                  }

                  if((REAL_MIN_DISTANCE[i] < 0.02f) && phalanxContact)
                  {
                    graspContact = true;

                    // Set New parent? Movements need to be constraint
                    if(GameObject.FindObjectOfType<Instantiate_Live>() != null)
                    {
                    	if(this.transform.parent.name == "RightHandColl")
	                    {
		                    OOI.transform.parent.transform.SetParent(GameObject.Find("R-VF").transform);
	                    }
	                    if(this.transform.parent.name == "LeftHandColl")
	                    {
		                    OOI.transform.parent.transform.SetParent(GameObject.Find("L-VF").transform);
	                    }
                      if(this.transform.parent.name == "WarpedRightHandColl")
                      {
                        OOI.transform.parent.transform.SetParent(GameObject.Find("R-VF").transform);
                      }
                      if(this.transform.parent.name == "WarpedLeftHandColl")
                      {
                        OOI.transform.parent.transform.SetParent(GameObject.Find("L-VF").transform);
                      }
                    }
                    if(GameObject.FindObjectOfType<SimulationHand>() != null)
                    {
                      name = OOI.transform.GetChild(i).gameObject.name;
                      mesh = OOI.transform.GetChild(i).gameObject.GetComponent<MeshFilter>().mesh.name;
                      scale = (100*Vector3.Scale(OOI.transform.GetChild(i).gameObject.transform.localScale, OOI.transform.localScale).x * 100*Vector3.Scale(OOI.transform.GetChild(i).gameObject.transform.localScale, OOI.transform.localScale).y * 100*Vector3.Scale(OOI.transform.GetChild(i).gameObject.transform.localScale, OOI.transform.localScale).z);
                    }
                    // OOI.transform.parent.transform.SetParent(this.transform.parent.transform);
                    // StartCoroutine(MoveOOI());
                  }
                    
                }
                shareMeTooX = closest_distance.x.ToString("F4");
                shareMeTooY = closest_distance.y.ToString("F4");
                shareMeTooZ = closest_distance.z.ToString("F4");

                shareMe = REAL_MIN_DISTANCE[i].ToString("F4"); 

            }
                
        }
        // if(GameObject.FindGameObjectWithTag("WarpZone") != null)
        // {
        //     if(GameObject.FindGameObjectWithTag("WarpZone").transform.gameObject.GetComponent<Collider>().bounds.Contains(this.transform.position))
        //     {
        //       startWarp = true;
        //     }
        //     else
        //     {
        //       startWarp = false;
        //     }
        // }          
    }

    void OnCollisionStay(Collision collisionInfo) // IF CONTACT FROM VH ON OOI / PH ON TOTEM
    {
      if(GameObject.FindObjectOfType<Instantiate_Live>() != null)
      {
        if(collisionInfo != null)
        {
          if(collisionInfo.collider.gameObject.transform.parent.tag == "ObjectOfInterest")
          {
            this.phalanxContact = true;
          }
        }
      }
      if(GameObject.FindObjectOfType<SimulationHand>() != null)
      {
        if(collisionInfo != null)
        {
          if(collisionInfo.collider.gameObject.transform.parent.tag == "ObjectOfInterest")
          {
            this.phalanxContact = true;
          }
        }
      }       
    }

    void OnCollisionExit(Collision collisionInfo)
    {
      if(GameObject.FindObjectOfType<Instantiate_Live>() != null)
      {
        if(collisionInfo != null)
        {
          if(collisionInfo.collider.gameObject.transform.parent.tag == "ObjectOfInterest")
          {
            this.phalanxContact = false;
          }

        }
      }

      if(GameObject.FindObjectOfType<SimulationHand>() != null)
      {
        if(collisionInfo != null)
        {
          if(collisionInfo.collider.gameObject.transform.parent.tag == "ObjectOfInterest")
          {
            this.phalanxContact = false;
          }
        }
      }
    }

}





