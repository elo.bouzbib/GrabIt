﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PlanesForWarpedHands : MonoBehaviour
{
    private GameObject OOI;
    [HideInInspector]
    public Vector3 closestFromVF, virtualFinger;
	private Vector3 closestDist;
	private GameObject thumb, palm, index;

	public int planeID, nbOfPlanes;

    [HideInInspector]
    public Vector3[] projPerPhal, projPerPhalOnOOI;
    [HideInInspector]
    public GameObject[] childR;

    [HideInInspector]
    public bool stopPredictions;


    void Start()
    {
      	OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");

        childR = new GameObject[19];

        for(int i = 0; i < 19; i++)
        {
            childR[i] = GameObject.Find("WarpedRightHandColl").transform.GetChild(i).transform.gameObject;
            childR[i].name = GameObject.Find("WarpedRightHandColl").transform.GetChild(i).name;
        }
        projPerPhal = new Vector3[19];
        projPerPhalOnOOI = new Vector3[19];

    }

    void Update()
    {

      	OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");
      	// int nbKids = OOI.transform.childCount;
        childR = new GameObject[19];

        for(int i = 0; i < 19; i++)
        {
            childR[i] = GameObject.Find("WarpedRightHandColl").transform.GetChild(i).transform.gameObject;
            childR[i].name = GameObject.Find("WarpedRightHandColl").transform.GetChild(i).name;
        }

    	palm = GameObject.Find("Warped_OculusHand_R/b_r_wrist/r_palm_center_marker").gameObject;
        index = GameObject.Find("Warped_OculusHand_R/b_r_wrist/b_r_index1/b_r_index2/b_r_index3/r_index_finger_pad_marker").gameObject;
        thumb = GameObject.Find("Warped_OculusHand_R/b_r_wrist/b_r_thumb0/b_r_thumb1/b_r_thumb2/b_r_thumb3/r_thumb_finger_pad_marker").gameObject;
       
    	virtualFinger = (index.transform.position - thumb.transform.position);
    	closestFromVF = whosClosest(thumb.transform.position + virtualFinger/2);
   	
    	Vector3 lengthToPalm = (thumb.transform.position + virtualFinger/2) - palm.transform.position;
    	
        
        if (Vector3.Distance(whosClosest(palm.transform.position), palm.transform.position) <= 1.05f*lengthToPalm.magnitude ) //2.5f*lengthToPalm.magnitude && onGoingVector.magnitude < 0.01f)
        {
            stopPredictions = true;
            this.transform.position = this.transform.position;
        }
        else
        {
            stopPredictions = false;
            this.transform.position = new Vector3(closestFromVF.x + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.right).x, closestFromVF.y + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.up).y, closestFromVF.z + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.forward).z);
        }
		
        if((Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.up).y) > Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.forward).z)) && (Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.up).y) > Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.right).x)) )
        {
            this.transform.forward = virtualFinger/virtualFinger.magnitude;
            Debug.Log("FromTop");
        }
        else
        {

            this.transform.forward = virtualFinger/virtualFinger.magnitude;
            this.transform.rotation = this.transform.rotation* Quaternion.FromToRotation(Vector3.right, Vector3.up);
            
            if((Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.up).y) < Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.right).x)) && (Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.forward).z) < Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.right).x)))
            {
                Debug.Log("from side X");

            }
            else if ((Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.up).y) < Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.forward).z)) && (Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.forward).z) > Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.right).x)))
            {
                Debug.Log("from side Z");

            }
            else
            {
                // Debug.Log("Kindof in the middle");

            }
        }

    	this.transform.localScale = new Vector3(virtualFinger.magnitude + 0.05f, virtualFinger.magnitude + 0.05f, virtualFinger.magnitude + 0.05f)*virtualFinger.magnitude*2.0f;


        for(int i = 0; i < 19; i++)
        {
            childR[i] = GameObject.Find("WarpedRightHandColl").transform.GetChild(i).transform.gameObject;
            projPerPhal[i] = this.GetComponent<Collider>().ClosestPoint(this.GetComponent<Collider>().ClosestPointOnBounds(childR[i].transform.position));
            // projPerPhalOnOOI[i] = whosClosest(projPerPhal[i]);
            if(this.GetComponent<Collider>().bounds.Contains(whosClosest(projPerPhal[i])))
            {
                projPerPhalOnOOI[i] = whosClosest(projPerPhal[i]);
            }
            else
            {
                //projPerPhalOnOOI[i] = new Vector3(0,0,0);
            }
        }        

    }


    public Vector3 whosClosest(Vector3 fromThis)
    {
    	int nbKids = OOI.transform.childCount;
    	Vector3[] closestPoint = new Vector3[nbKids];
    	Vector3[] closest_distance = new Vector3[nbKids];
    	float[] REAL_MIN_DISTANCE = new float[nbKids];

    	for(int i = 0; i < nbKids; i++)
        {
	    	// Vector3[] objVertices = OOI.transform.GetChild(i).gameObject.GetComponent<MeshFilter>().mesh.vertices;
	    	// float REAL_MIN_DISTANCE;
        	closestPoint[i] = OOI.transform.GetChild(i).gameObject.GetComponent<Collider>().ClosestPointOnBounds(fromThis);
        	closest_distance[i] = OOI.transform.GetChild(i).gameObject.GetComponent<Collider>().ClosestPoint(closestPoint[i]);
	        REAL_MIN_DISTANCE[i] = Vector3.Distance(closest_distance[i], fromThis);
	    	// }
            if(nbKids > 1)
            {
                if(i>1)
                {
                    if(REAL_MIN_DISTANCE[i-1] <= REAL_MIN_DISTANCE[i])
                    {
                        closest_distance[i] = closest_distance[i-1];
                        closestDist = closest_distance[i];
                        // Debug.Log(OOI.transform.GetChild(i-1).gameObject.name);
                        REAL_MIN_DISTANCE[i] = REAL_MIN_DISTANCE[i-1];
                    }
                    else
                    {
                        closestDist = closest_distance[i];
                    }
                }
            }
	    	
	    	
	    }
    	return closestDist;

    }

}


