﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedirectMe_Sim_Index : MonoBehaviour
{	
	[HideInInspector]
	public GameObject rightHand, leftHand, warpedRightHand, warpedLeftHand;

	public IList<OVRBoneCapsule> Capsules { get; private set; }
	public IList<OVRBoneCapsule> CapsulesR { get; private set; }

	public IList<OVRBoneCapsule> CapsulesL_Replica { get; private set; }
	public IList<OVRBoneCapsule> CapsulesR_Replica { get; private set; }

	// public Vector3 projectVector, warpVector;
 //    public Vector3 projectVectorL, warpVectorL;

	private Vector3[] projectVector, warpVector;
    private Vector3[] projectVectorL, warpVectorL;

	private Vector3[] physicalTargetPosition, virtualTargetPosition;
    private Vector3[] physicalTargetPositionL, virtualTargetPositionL;

	// private Vector3 physicalTargetPosition, virtualTargetPosition;
 //    private Vector3 physicalTargetPositionL, virtualTargetPositionL;

	public float[] ds, dp;//, ds_second, dp_second;
    public float[] dsL, dpL;//, ds_second, dp_second;

    public GameObject physicalTarget;
    public string[] nameTargetR, nameTargetL;
	private GameObject virtualTarget, startPosition, startPositionLeft;
	public bool startWarp;
    public bool startWarpL;

	public ClosestToHand_Live[] phalanxData;
	private Vector3 initialPosition, initialPositionL;

    public bool reverse;
    public GameObject warpZone;

    public Predict_Live predict;
    public PredictionStop predictStop;
    public PredictionStopL predictStopL;
    private GameObject OOI;

    public GameObject[] targetToTouch;
    public GameObject[] warpedPhal, realPhal;
    // Start is called before the first frame update
    void Start()
    {
		leftHand = GameObject.Find("OVRCustomHandPrefab_L"); // THIS IS HP = Hand Physical
    	rightHand = GameObject.Find("OVRCustomHandPrefab_R");
    	
    	warpedRightHand = GameObject.Find("WarpedRightHand"); // THIS IS HV = Hand Virtual
    	warpedLeftHand = GameObject.Find("WarpedLeftHand");

    	Capsules = leftHand.GetComponent<OVRCustomSkeleton>().Capsules;
		CapsulesR = rightHand.GetComponent<OVRCustomSkeleton>().Capsules;

		CapsulesL_Replica = warpedLeftHand.GetComponent<OVRCustomSkeleton>().Capsules;
		CapsulesR_Replica = warpedRightHand.GetComponent<OVRCustomSkeleton>().Capsules;

		warpVector = new Vector3[19]; // = (Ds/(Ds + Dp)) * (pv - pp) (hand Global position)
        warpVectorL = new Vector3[19];


		physicalTargetPosition = new Vector3[19]; 
		physicalTargetPositionL = new Vector3[19];

		virtualTargetPosition = new Vector3[19]; 
		virtualTargetPositionL = new Vector3[19];

		projectVector = new Vector3[19];
		projectVectorL = new Vector3[19];

		// physicalTarget = GameObject.FindGameObjectWithTag("PhysicalTotem");
        physicalTarget = GameObject.Find("S-PhysicalTotem");
		virtualTarget = GameObject.Find("VirtualTotem"); 

        startPosition = GameObject.Find("StartPosition");
        startPositionLeft = GameObject.Find("StartLeft");

		// VIRTUAL TARGET = read from AlgoHints. Same to start Warp -> as soon as prediction starts,
		// warp can begin.

		// physicalTargetPosition = physicalTarget.transform.position;
		// virtualTargetPosition = virtualTarget.transform.position; 
		initialPosition = new Vector3();
        initialPositionL = new Vector3();
    	startWarp = false;
        startWarpL = false;

    	phalanxData = new ClosestToHand_Live[38];
        warpedPhal = new GameObject[19];
        realPhal = new GameObject[19];
    	for(int k = 0; k < 19; k++)
        {
            warpedPhal[k] = GameObject.Find("WarpedRightHandColl").transform.GetChild(k).transform.gameObject;
            realPhal[k] = GameObject.Find("RightHandColl").transform.GetChild(k).transform.gameObject;

            if(reverse)
            {
                phalanxData[k] = GameObject.Find("RightHandColl").transform.GetChild(k).gameObject.GetComponent<ClosestToHand_Live>();
                phalanxData[k+19] = GameObject.Find("LeftHandColl").transform.GetChild(k).gameObject.GetComponent<ClosestToHand_Live>();
 
            }
            else
            {

                phalanxData[k] = GameObject.Find("WarpedRightHandColl").transform.GetChild(k).gameObject.GetComponent<ClosestToHand_Live>();
                phalanxData[k+19] = GameObject.Find("WarpedLeftHandColl").transform.GetChild(k).gameObject.GetComponent<ClosestToHand_Live>();

            }

        }

        warpZone = GameObject.FindGameObjectWithTag("WarpZone");
        predict = GameObject.FindObjectOfType<Predict_Live>();

        predictStop = GameObject.FindObjectOfType<PredictionStop>();
        predictStopL = GameObject.FindObjectOfType<PredictionStopL>();

        ds = new float[19];
        dsL = new float[19];
        dp = new float[19];
        dpL = new float[19];

        nameTargetR = new string[19];
        nameTargetL = new string[19];
        targetToTouch = new GameObject[38];

    }

    // Update is called once per frame
    void Update()
    {
        startPosition = GameObject.Find("StartPosition");
        startPositionLeft = GameObject.Find("StartLeft");
        warpZone = GameObject.FindGameObjectWithTag("WarpZone");

        OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");
        
        // TEST REDIRECTION
        // physicalTargetPosition = physicalTarget.transform.position;
        // virtualTargetPosition = OOI.transform.position; // Should be prediction

        // virtualTargetPosition = OOI.transform.position; // Should be prediction // Chosen from Gaze/Traj?
        // virtualTargetPositionL = OOI.transform.position; // Should be prediction

        phalanxData = new ClosestToHand_Live[38];

        for(int k = 0; k < 19; k++)
        {
            if(reverse)
            {
                phalanxData[k] = GameObject.Find("RightHandColl").transform.GetChild(k).gameObject.GetComponent<ClosestToHand_Live>();
                phalanxData[k+19] = GameObject.Find("LeftHandColl").transform.GetChild(k).gameObject.GetComponent<ClosestToHand_Live>();
 
            }
            else
            {

                phalanxData[k] = GameObject.Find("WarpedRightHandColl").transform.GetChild(k).gameObject.GetComponent<ClosestToHand_Live>();
                phalanxData[k+19] = GameObject.Find("WarpedLeftHandColl").transform.GetChild(k).gameObject.GetComponent<ClosestToHand_Live>();
                physicalTargetPosition[k] = GameObject.Find("WarpProjection ("+k.ToString()+")").transform.position;
                physicalTargetPositionL[k] = GameObject.Find("WarpProjection ("+(k+19).ToString()+")").transform.position;

            }

        }

        if(OOI.name.Contains("-XS"))
        {
            physicalTarget = GameObject.Find("XS-PhysicalTotem");
        }
        if(OOI.name.Contains("-M"))
        {
            physicalTarget = GameObject.Find("M-PhysicalTotem");
        }
        if(OOI.name.Contains("-L"))
        {
            physicalTarget = GameObject.Find("L-PhysicalTotem");
        }
        if(OOI.name.Contains("-S"))
        {
            physicalTarget = GameObject.Find("S-PhysicalTotem");                
        }
        for(int k = 0; k < 19; k++)
        {
        	if(reverse)
            {
                phalanxData[k] = GameObject.Find("RightHandColl").transform.GetChild(k).gameObject.GetComponent<ClosestToHand_Live>();
                phalanxData[k+19] = GameObject.Find("LeftHandColl").transform.GetChild(k).gameObject.GetComponent<ClosestToHand_Live>();
                
                if(predict.namePred[k] != null)
                {
                    if(predict.namePred[k].Contains("Cylinder"))
                    {
                        targetToTouch[k] = physicalTarget.transform.GetChild(1).gameObject;

                    }
                    if(predict.namePred[k].Contains("Cube"))
                    {
                        targetToTouch[k] = physicalTarget.transform.GetChild(0).gameObject;
                        // physicalTarget.transform.GetChild(0).gameObject.transform.GetChild(k).gameObject.transform.localPosition = predict.predObj[k].localPosition;
                    }
                    // physicalTargetPosition[k] = GameObject.Find("WarpProjection ("+k.ToString()+")").transform.position;
                    
                }

                physicalTargetPosition[k] = GameObject.Find("WarpProjection ("+k.ToString()+")").transform.position;

                // if(GameObject.Find("WarpProjection ("+k.ToString()+")").transform.parent == targetToTouch[k])
                // {
                //     physicalTargetPosition[k] = GameObject.Find("WarpProjection ("+k.ToString()+")").transform.position;
                // }
                // else
                // {
                //     physicalTargetPosition[k] = physicalTarget.transform.GetChild(0).gameObject.transform.position;
                // }

                if(predict.namePred[k+19] != null)
                {
                    if(predict.namePred[k+19].Contains("Cylinder"))
                    {
                        targetToTouch[k+19] = physicalTarget.transform.GetChild(1).gameObject;
                    }
                    if(predict.namePred[k+19].Contains("Cube"))
                    {
                        targetToTouch[k+19] = physicalTarget.transform.GetChild(0).gameObject;
                    }

                    // physicalTargetPositionL[k] = GameObject.Find("WarpProjection ("+(k+19).ToString()+")").transform.position;
                }

                if(GameObject.Find("WarpProjection ("+(k+19).ToString()+")").transform.parent == targetToTouch[k+19])
                {
                    physicalTargetPositionL[k] = GameObject.Find("WarpProjection ("+(k+19).ToString()+")").transform.position;
                }
                else
                {
                    physicalTargetPositionL[k] = physicalTarget.transform.GetChild(0).gameObject.transform.position;
                }
               

            }
            else
            {

                phalanxData[k] = GameObject.Find("WarpedRightHandColl").transform.GetChild(k).gameObject.GetComponent<ClosestToHand_Live>();
                phalanxData[k+19] = GameObject.Find("WarpedLeftHandColl").transform.GetChild(k).gameObject.GetComponent<ClosestToHand_Live>();

                if(predict.namePred[k] != null)
                {
                    if(predict.namePred[k].Contains("Cylinder"))
                    {
                        targetToTouch[k] = physicalTarget.transform.GetChild(1).gameObject;

                    }
                    if(predict.namePred[k].Contains("Cube"))
                    {
                        targetToTouch[k] = physicalTarget.transform.GetChild(0).gameObject;
                        // GameObject.Find("WarpedRightHandColl").transform.parent = OOI.transform.GetChild(0).gameObject.transform;
                        // GameObject.Find("RightHandColl").transform.parent = targetToTouch[k].transform;

                        // physicalTarget.transform.GetChild(0).gameObject.transform.GetChild(k).gameObject.transform.localPosition = predict.predObj[k].localPosition;
                    }

                }
                else
                {
                    GameObject.Find("WarpedRightHandColl").transform.parent = null;
                    GameObject.Find("RightHandColl").transform.parent = null;
                
                }

                physicalTargetPosition[k] = GameObject.Find("WarpProjection ("+k.ToString()+")").transform.position;

                // if(GameObject.Find("WarpProjection ("+k.ToString()+")").transform.parent == targetToTouch[k])
                // {
                //     physicalTargetPosition[k] = GameObject.Find("WarpProjection ("+k.ToString()+")").transform.position;
                // }
                // else
                // {
                //     physicalTargetPosition[k] = physicalTarget.transform.GetChild(0).gameObject.transform.position;
                // }

                if(predict.namePred[k+19] != null)
                {
                    if(predict.namePred[k+19].Contains("Cylinder"))
                    {
                        targetToTouch[k+19] = physicalTarget.transform.GetChild(1).gameObject;
                    }
                    if(predict.namePred[k+19].Contains("Cube"))
                    {
                        targetToTouch[k+19] = physicalTarget.transform.GetChild(0).gameObject;
                    }
                    // physicalTargetPositionL[k] = GameObject.Find("WarpProjection ("+(k+19).ToString()+")").transform.position;

                }

                if(GameObject.Find("WarpProjection ("+(k+19).ToString()+")").transform.parent == targetToTouch[k+19])
                {
                    physicalTargetPositionL[k] = GameObject.Find("WarpProjection ("+(k+19).ToString()+")").transform.position;
                }
                else
                {
                    physicalTargetPositionL[k] = physicalTarget.transform.GetChild(0).gameObject.transform.position;
                }
            
                targetToTouch[k] = physicalTarget.transform.GetChild(0).gameObject;
                targetToTouch[k+19] = physicalTarget.transform.GetChild(0).gameObject;

   
            }

            nameTargetR[k] = predict.namePred[k];
            nameTargetL[k] = predict.namePred[k+19];

            virtualTargetPosition[k] = predictStop.finalPred[k];
        	virtualTargetPositionL[k] = predictStopL.finalPred[k];

        }

        for(int k = 0; k < 19; k++) // 38
        {
            if(!warpZone.GetComponent<Collider>().bounds.Contains(phalanxData[k].gameObject.transform.position) )
            {
                initialPosition = Vector3.zero;
                startWarp = false;
            }
            else
            {
                initialPosition = startPosition.transform.position;
                startWarp = true;
            }
            if(!warpZone.GetComponent<Collider>().bounds.Contains(phalanxData[k+19].gameObject.transform.position) )
            {
                initialPositionL = Vector3.zero;
                startWarpL = false;
            }
            else
            {
                initialPositionL = startPositionLeft.transform.position;
                startWarpL = true;
            }

        }


        for(int i = 0; i < 19; i++)
        {
			if(reverse)
	        {
	            projectVector[i] = (virtualTargetPosition[i] - initialPosition).normalized;
                // projectVector[i] = (OOI.transform.position - initialPosition).normalized;
	            projectVectorL[i] = (virtualTargetPosition[i] - initialPositionL).normalized;
	        }
	        else
	        {
	            projectVector[i] = (physicalTargetPosition[i] - initialPosition).normalized;
	            projectVectorL[i] = (physicalTargetPosition[i] - initialPositionL).normalized;



                // TESSSSSTTTTTTT
                // projectVector[i] = (GameObject.Find("WarpedRightHandColl").transform.GetChild(i).transform.position - initialPosition).normalized;
                // projectVector[i] = (physicalTarget.transform.GetChild(0).gameObject.transform.position - initialPosition).normalized;
                // projectVector[i] = (physicalTarget.transform.GetChild(0).gameObject.transform.position - initialPosition).normalized;

	        }


        	if(startWarp)
        	{
                if(reverse)
                {
                    ds[i] = Vector3.Project((rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position - initialPosition), projectVector[i]).magnitude;
                    dp[i] = Vector3.Project((virtualTargetPosition[i] - rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position), projectVector[i]).magnitude;
                
                    warpVector[i] = (ds[i] / (ds[i] + dp[i])) * (physicalTargetPosition[i] - virtualTargetPosition[i]);                
                    // ds[i] = Vector3.Project((rightHand.transform.position - initialPosition), projectVector[i]).magnitude;
                    // dp[i] = Vector3.Project((OOI.transform.position - rightHand.transform.position), projectVector[i]).magnitude;
                
                    // warpVector[i] = (ds[i] / (ds[i] + dp[i])) * (physicalTargetPosition[i] - virtualTargetPosition[i]);                
                    // warpVector[i] = (ds[i] / (ds[i] + dp[i])) * (physicalTargetPosition[i] - virtualTargetPosition[i]);                
                
                }
                else
                {
                    dp[i] = Vector3.Project((physicalTargetPosition[i] - rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position), projectVector[i]).magnitude;
                    ds[i] = Vector3.Project((rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position - initialPosition), projectVector[i]).magnitude;
                    
                    // TESSSSSTTTTTTT    
                    // dp[i] = Vector3.Project((physicalTarget.transform.GetChild(0).gameObject.transform.position - rightHand.transform.position), projectVector[i]).magnitude;
                    dp[i] = Vector3.Project((physicalTarget.transform.GetChild(0).gameObject.transform.position - rightHand.transform.position), projectVector[i]).magnitude;
                    ds[i] = Vector3.Project((rightHand.transform.position - initialPosition), projectVector[i]).magnitude;
                    
                    // warpVector[i] = Vector3.zero;
                	warpVector[i] = (ds[i] / (ds[i] + dp[i])) * (virtualTargetPosition[i] - physicalTargetPosition[i]); 
                    // warpVector[i] = (ds[i] / (ds[i] + dp[i])) * (OOI.transform.GetChild(0).transform.position - physicalTarget.transform.GetChild(0).gameObject.transform.position); 
                    // warpVector[i] = (ds[i] / (ds[i] + dp[i])) * (GameObject.Find("Plane").GetComponent<CollisionPlanes_withKids>().closestFromVF - physicalTarget.transform.GetChild(0).gameObject.transform.position); 
                    

                    // warpVector[i] = GameObject.Find("WarpedRightHandColl").transform.GetChild(i).transform.position;

                }
            
        	}
        	else
        	{
        		warpVector[i] = Vector3.zero;
                GameObject.Find("WarpedRightHandColl").transform.parent = null;
                GameObject.Find("RightHandColl").transform.parent = null;
        	}

            if(startWarpL)
            {
                if(reverse)
                {
                    dsL[i] = Vector3.Project((leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position - initialPositionL), projectVectorL[i]).magnitude;
                    dpL[i] = Vector3.Project((virtualTargetPositionL[i] - leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position), projectVectorL[i]).magnitude;
                
                    warpVectorL[i] = (dsL[i] / (dsL[i] + dpL[i])) * (physicalTargetPositionL[i] - virtualTargetPositionL[i]);                
                }
                else
                {
                    dpL[i] = Vector3.Project((physicalTargetPositionL[i] - leftHand.transform.position), projectVectorL[i]).magnitude;
                    dsL[i] = Vector3.Project((leftHand.transform.position - initialPositionL), projectVectorL[i]).magnitude;

                    warpVectorL[i] = (dsL[i] / (dsL[i] + dpL[i])) * (virtualTargetPositionL[i] - physicalTargetPositionL[i]); 
                }
            
            }
            else
            {
                warpVectorL[i] = Vector3.zero;
            }

            // GameObject.Find("Warped_OculusHand_R/b_r_wrist").transform.position = GameObject.Find("WarpedRightHandColl").transform.GetChild(0).transform.position;
            GameObject.Find("Warped_OculusHand_R/b_r_wrist").transform.position = Vector3.Lerp(GameObject.Find("Warped_OculusHand_R/b_r_wrist").transform.position, GameObject.Find("WarpedRightHandColl").transform.GetChild(0).transform.position, 0.2f);

			// MAKES HANDS CHILDREN AND TAKE LOCALPOSITIONS AS WELL
            // if(startWarp)
            // {            
            //     GameObject.Find("Warped_OculusHand_R/b_r_wrist").transform.position = GameObject.Find("WarpedRightHandColl").transform.GetChild(0).transform.position;
            //     // warpedRightHand.GetComponent<OVRCustomSkeleton>().CustomBones[0].transform.position = GameObject.Find("WarpedRightHandColl").transform.GetChild(0).transform.position;//rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position + warpVector[i];
                
            //     warpedRightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.localPosition = rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.localPosition;
            //     warpedRightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.localEulerAngles = rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.localEulerAngles;
                
            //     // Vector3 ratioM = new Vector3(OOI.transform.GetChild(0).transform.localScale.x/targetToTouch[0].transform.localScale.x, OOI.transform.GetChild(0).transform.localScale.y/targetToTouch[0].transform.localScale.y, OOI.transform.GetChild(0).transform.localScale.z/targetToTouch[0].transform.localScale.z);
            //     // warpedRightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.localScale = Vector3.Scale(ratioM, rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.localScale);
            
            // }
            // else
            // {
            //     warpedRightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position = rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position;
            // }
           
            warpedRightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.localPosition = rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.localPosition;
            warpedRightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.localEulerAngles = rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.localEulerAngles;

            if(!startWarp)
            {
                GameObject.Find("WarpedRightHandColl").transform.parent = null;
                GameObject.Find("RightHandColl").transform.parent = null;
                GameObject.Find("WarpedRightHandColl").transform.position = GameObject.Find("RightHandColl").transform.position;
            }
            // GameObject.Find("WarpedRightHandColl").transform.parent = null;
            // GameObject.Find("RightHandColl").transform.parent = null;
            
            // warpedRightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position = rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position + warpVector[i];
            // warpedRightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles = new Vector3(rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.x, rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.y, rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.z);
            

            // REAL ONE HERE
            // warpedRightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position = rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position + warpVector[i];

            // GameObject.Find("WarpedSecond").GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position = GameObject.Find("WarpedRightHandColl").transform.GetChild(i).transform.position;
            // GameObject.Find("WarpedSecond").GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles = new Vector3(GameObject.Find("WarpedRightHandColl").transform.GetChild(i).transform.eulerAngles.x, GameObject.Find("WarpedRightHandColl").transform.GetChild(i).transform.eulerAngles.y, GameObject.Find("WarpedRightHandColl").transform.GetChild(i).transform.eulerAngles.z);



			warpedLeftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position = leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position + warpVectorL[i];
            warpedLeftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles = new Vector3(leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.x, leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.y, leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.z);
    		
            if(GameObject.FindObjectOfType<Instantiate_Live>() != null)
            {
                if(GameObject.FindObjectOfType<Instantiate_Live>())
                {
                    CapsulesL_Replica[i].CapsuleCollider.gameObject.transform.position = new Vector3(Capsules[i].CapsuleCollider.gameObject.transform.position.x + warpVectorL[i].x, Capsules[i].CapsuleCollider.gameObject.transform.position.y + warpVectorL[i].y, Capsules[i].CapsuleCollider.gameObject.transform.position.z + warpVectorL[i].z);
                    CapsulesR_Replica[i].CapsuleCollider.gameObject.transform.position = new Vector3(CapsulesR[i].CapsuleCollider.gameObject.transform.position.x + warpVector[i].x, CapsulesR[i].CapsuleCollider.gameObject.transform.position.y + warpVector[i].y, CapsulesR[i].CapsuleCollider.gameObject.transform.position.z + warpVector[i].z);
                    CapsulesL_Replica[i].CapsuleCollider.gameObject.transform.eulerAngles = new Vector3(Capsules[i].CapsuleCollider.gameObject.transform.eulerAngles.x, Capsules[i].CapsuleCollider.gameObject.transform.eulerAngles.y, Capsules[i].CapsuleCollider.gameObject.transform.eulerAngles.z);
                    CapsulesR_Replica[i].CapsuleCollider.gameObject.transform.eulerAngles = new Vector3(CapsulesR[i].CapsuleCollider.gameObject.transform.eulerAngles.x, CapsulesR[i].CapsuleCollider.gameObject.transform.eulerAngles.y, CapsulesR[i].CapsuleCollider.gameObject.transform.eulerAngles.z);
                }
            }    
    	}
    }



  //   void OnDrawGizmos()
  //   {
		// for(int k = 0; k < 19; k++)
  //       {
  //           // Vector3 ratioM = new Vector3(targetToTouch[0].transform.localScale.x/OOI.transform.GetChild(0).transform.localScale.x, targetToTouch[0].transform.localScale.y/OOI.transform.GetChild(0).transform.localScale.y, targetToTouch[0].transform.localScale.z/OOI.transform.GetChild(0).transform.localScale.z);
  //       	Gizmos.color = Color.blue;
  //       	Gizmos.DrawRay(physicalTargetPosition[k], rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[k].transform.position - physicalTargetPosition[k]);
  //       	// Debug.Log("RH:" + Vector3.Distance(physicalTargetPosition[k],rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[k].transform.position));
  //        //    Debug.Log("VH+Ratio:" + Vector3.Distance(virtualTargetPosition[k], GameObject.Find("WarpedRightHandColl").transform.GetChild(k).transform.position));

  //        //    Debug.Log("Ratio+RH:" + Vector3.Scale(physicalTargetPosition[k] - rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[k].transform.position, ratioM).magnitude);
  //        //    Debug.Log("VH:" + Vector3.Scale(virtualTargetPosition[k]-GameObject.Find("WarpedRightHandColl").transform.GetChild(k).transform.position, ratioM).magnitude);

  //           Gizmos.color = Color.magenta;
  //       	// Gizmos.DrawSphere(physicalTargetPositionL[k], 0.01f);
  //           Gizmos.DrawRay(virtualTargetPosition[k], warpedRightHand.GetComponent<OVRCustomSkeleton>().CustomBones[k].transform.position - virtualTargetPosition[k]);

  //           Gizmos.color = Color.green;
  //           // Gizmos.DrawSphere(physicalTargetPositionL[k], 0.01f);
  //           Gizmos.DrawRay(virtualTargetPosition[k], GameObject.Find("WarpedRightHandColl").transform.GetChild(k).transform.position - virtualTargetPosition[k]);


  //       }

  //   }

}


