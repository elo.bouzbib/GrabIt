﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedirectMe_New : MonoBehaviour
{   
    [HideInInspector]
    public GameObject rightHand, leftHand, warpedRightHand, warpedLeftHand;

    public IList<OVRBoneCapsule> Capsules { get; private set; }
    public IList<OVRBoneCapsule> CapsulesR { get; private set; }

    public IList<OVRBoneCapsule> CapsulesL_Replica { get; private set; }
    public IList<OVRBoneCapsule> CapsulesR_Replica { get; private set; }

    public Vector3 projectVector, warpVector;
    public Vector3 projectVectorL, warpVectorL;

    private Vector3 physicalTargetPosition, virtualTargetPosition;
    public float ds, dp;//, ds_second, dp_second;
    public float dsL, dpL;//, ds_second, dp_second;


    private GameObject physicalTarget, virtualTarget, startPosition, startPositionLeft;
    public bool startWarp;
    private bool startWarpL;

    public ClosestToHand_Live[] phalanxData;
    // public GameObject distractOrange;

    public Instantiate_Live gameManager;
    private Vector3 initialPosition, initialPositionL;

    public bool reverse;
    public GameObject warpZone;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindObjectOfType<Instantiate_Live>();

        leftHand = GameObject.Find("OVRCustomHandPrefab_L"); // THIS IS HP = Hand Physical
        rightHand = GameObject.Find("OVRCustomHandPrefab_R");
        
        warpedRightHand = GameObject.Find("WarpedRightHand"); // THIS IS HV = Hand Virtual
        warpedLeftHand = GameObject.Find("WarpedLeftHand");

        Capsules = leftHand.GetComponent<OVRCustomSkeleton>().Capsules;
        CapsulesR = rightHand.GetComponent<OVRCustomSkeleton>().Capsules;

        CapsulesL_Replica = warpedLeftHand.GetComponent<OVRCustomSkeleton>().Capsules;
        CapsulesR_Replica = warpedRightHand.GetComponent<OVRCustomSkeleton>().Capsules;

        for(int i = 0; i < 19; i++)
        {
            CapsulesL_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().center = GameObject.Find("LeftHandColl").transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().center;
            CapsulesL_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().radius = GameObject.Find("LeftHandColl").transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().radius;
            CapsulesL_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().height = GameObject.Find("LeftHandColl").transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().height;
            CapsulesL_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().direction = GameObject.Find("LeftHandColl").transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().direction;
        
            CapsulesR_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().center = GameObject.Find("RightHandColl").transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().center;
            CapsulesR_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().radius = GameObject.Find("RightHandColl").transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().radius;
            CapsulesR_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().height = GameObject.Find("RightHandColl").transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().height;
            CapsulesR_Replica[i].CapsuleCollider.gameObject.GetComponent<CapsuleCollider>().direction = GameObject.Find("RightHandColl").transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().direction;

        }

        warpVector = new Vector3(); // = (Ds/(Ds + Dp)) * (pv - pp) (hand Global position)
        warpVectorL = new Vector3();

        projectVector = new Vector3();

        physicalTarget = GameObject.Find("PhysicalTotem");
        virtualTarget = GameObject.Find("VirtualTotem"); 
        // distractOrange = GameObject.Find("InitialPositions");

        startPosition = GameObject.Find("StartPosition");
        startPositionLeft = GameObject.Find("StartLeft");

        // VIRTUAL TARGET = read from AlgoHints. Same to start Warp -> as soon as prediction starts,
        // warp can begin.

        physicalTargetPosition = physicalTarget.transform.position;
        virtualTargetPosition = virtualTarget.transform.position; 
        initialPosition = new Vector3();
        initialPositionL = new Vector3();
        startWarp = false;
        startWarpL = false;

        phalanxData = new ClosestToHand_Live[38];

        for(int k = 0; k < 19; k++)
        {
            // phalanxData[k] = GameObject.FindObjectsOfType<ClosestToHand_Live>()[k];
            phalanxData[k] = GameObject.Find("RightHandColl").transform.GetChild(k).gameObject.GetComponent<ClosestToHand_Live>();
            phalanxData[k+19] = GameObject.Find("LeftHandColl").transform.GetChild(k).gameObject.GetComponent<ClosestToHand_Live>();

        }
        warpZone = GameObject.FindGameObjectWithTag("WarpZone");

    }

    // Update is called once per frame
    void Update()
    {
        physicalTarget = GameObject.Find("PhysicalTotem");
        virtualTarget = GameObject.Find("VirtualTotem"); 
        startPosition = GameObject.Find("StartPosition");
        startPositionLeft = GameObject.Find("StartLeft");
        warpZone = GameObject.FindGameObjectWithTag("WarpZone");

        physicalTargetPosition = physicalTarget.transform.position;
        virtualTargetPosition = virtualTarget.transform.position; 

        phalanxData = new ClosestToHand_Live[38];

        for(int k = 0; k < 19; k++)
        {
            phalanxData[k] = GameObject.Find("RightHandColl").transform.GetChild(k).gameObject.GetComponent<ClosestToHand_Live>();
            phalanxData[k+19] = GameObject.Find("LeftHandColl").transform.GetChild(k).gameObject.GetComponent<ClosestToHand_Live>();            
            
        }

        for(int k = 0; k < 19; k++) // 38
        {
            if(!warpZone.GetComponent<Collider>().bounds.Contains(phalanxData[k].gameObject.transform.position) )
            {
                initialPosition = Vector3.zero;
                startWarp = false;

            }
            else
            {
                initialPosition = startPosition.transform.position;
                startWarp = true;

            }
            if(!warpZone.GetComponent<Collider>().bounds.Contains(phalanxData[k+19].gameObject.transform.position) )
            {
                initialPositionL = Vector3.zero;
                startWarpL = false;

            }
            else
            {
                initialPositionL = startPositionLeft.transform.position;
                startWarpL = true;

            }

        }
        
        if(reverse)
        {
            projectVector = (virtualTargetPosition - initialPosition).normalized;
            projectVectorL = (virtualTargetPosition - initialPositionL).normalized;
        }
        else
        {
            projectVector = (physicalTargetPosition - initialPosition).normalized;
            projectVectorL = (physicalTargetPosition - initialPositionL).normalized;
        }

        for(int i = 0; i < 19; i++)
        {
            if(startWarp)
            {
                if(reverse)
                {
                    ds = Vector3.Project((rightHand.transform.position - initialPosition), projectVector).magnitude;
                    dp = Vector3.Project((virtualTargetPosition - rightHand.transform.position), projectVector).magnitude;
                
                    warpVector = (ds / (ds + dp)) * (physicalTargetPosition - virtualTargetPosition);                
                }
                else
                {
                    dp = Vector3.Project((physicalTargetPosition - rightHand.transform.position), projectVector).magnitude;
                    ds = Vector3.Project((rightHand.transform.position - initialPosition), projectVector).magnitude;

                    warpVector = (ds / (ds + dp)) * (virtualTargetPosition - physicalTargetPosition); 
                }
            
            }
            else
            {
                warpVector = Vector3.zero;
            }

            if(startWarpL)
            {
                if(reverse)
                {
                    dsL = Vector3.Project((leftHand.transform.position - initialPosition), projectVectorL).magnitude;
                    dpL = Vector3.Project((virtualTargetPosition - leftHand.transform.position), projectVectorL).magnitude;
                
                    warpVectorL = (dsL / (dsL + dpL)) * (physicalTargetPosition - virtualTargetPosition);                
                }
                else
                {
                    dpL = Vector3.Project((physicalTargetPosition - leftHand.transform.position), projectVectorL).magnitude;
                    dsL = Vector3.Project((leftHand.transform.position - initialPositionL), projectVectorL).magnitude;

                    warpVectorL = (dsL / (dsL + dpL)) * (virtualTargetPosition - physicalTargetPosition); 
                }
            
            }
            else
            {
                warpVectorL = Vector3.zero;
            }

            
            warpedRightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position = rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position + warpVector;
            warpedRightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles = new Vector3(rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.x, rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.y, rightHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.z);

            warpedLeftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position = leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position + warpVectorL;
            warpedLeftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles = new Vector3(leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.x, leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.y, leftHand.GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles.z);
            
            if(GameObject.FindObjectOfType<ReadPosAndWrite>() != null)
            {
                if(!GameObject.FindObjectOfType<ReadPosAndWrite>().simulationDebug)
                {
                    CapsulesL_Replica[i].CapsuleCollider.gameObject.transform.position = new Vector3(Capsules[i].CapsuleCollider.gameObject.transform.position.x + warpVectorL.x, Capsules[i].CapsuleCollider.gameObject.transform.position.y + warpVectorL.y, Capsules[i].CapsuleCollider.gameObject.transform.position.z + warpVectorL.z);
                    CapsulesR_Replica[i].CapsuleCollider.gameObject.transform.position = new Vector3(CapsulesR[i].CapsuleCollider.gameObject.transform.position.x + warpVector.x, CapsulesR[i].CapsuleCollider.gameObject.transform.position.y + warpVector.y, CapsulesR[i].CapsuleCollider.gameObject.transform.position.z + warpVector.z);         
                    CapsulesL_Replica[i].CapsuleCollider.gameObject.transform.eulerAngles = new Vector3(Capsules[i].CapsuleCollider.gameObject.transform.eulerAngles.x, Capsules[i].CapsuleCollider.gameObject.transform.eulerAngles.y, Capsules[i].CapsuleCollider.gameObject.transform.eulerAngles.z);
                    CapsulesR_Replica[i].CapsuleCollider.gameObject.transform.eulerAngles = new Vector3(CapsulesR[i].CapsuleCollider.gameObject.transform.eulerAngles.x, CapsulesR[i].CapsuleCollider.gameObject.transform.eulerAngles.y, CapsulesR[i].CapsuleCollider.gameObject.transform.eulerAngles.z);
                }
            }
            else
            {
                CapsulesL_Replica[i].CapsuleCollider.gameObject.transform.position = new Vector3(GameObject.Find("LeftHandColl").transform.GetChild(i).transform.position.x + warpVectorL.x, GameObject.Find("LeftHandColl").transform.GetChild(i).transform.position.y + warpVectorL.y, GameObject.Find("LeftHandColl").transform.GetChild(i).transform.position.z + warpVectorL.z);
                CapsulesR_Replica[i].CapsuleCollider.gameObject.transform.position = new Vector3(GameObject.Find("RightHandColl").transform.GetChild(i).transform.position.x + warpVector.x, GameObject.Find("RightHandColl").transform.GetChild(i).transform.position.y + warpVector.y, GameObject.Find("RightHandColl").transform.GetChild(i).transform.position.z + warpVector.z);         
                CapsulesL_Replica[i].CapsuleCollider.gameObject.transform.eulerAngles = new Vector3(GameObject.Find("LeftHandColl").transform.GetChild(i).transform.eulerAngles.x, GameObject.Find("LeftHandColl").transform.GetChild(i).transform.eulerAngles.y, GameObject.Find("LeftHandColl").transform.GetChild(i).transform.eulerAngles.z);
                CapsulesR_Replica[i].CapsuleCollider.gameObject.transform.eulerAngles = new Vector3(GameObject.Find("RightHandColl").transform.GetChild(i).transform.eulerAngles.x, GameObject.Find("RightHandColl").transform.GetChild(i).transform.eulerAngles.y, GameObject.Find("RightHandColl").transform.GetChild(i).transform.eulerAngles.z);
                
            }
        }
    }

}

