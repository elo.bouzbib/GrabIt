﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System.IO;
using System.Globalization;


public class WritePredictAlpha : MonoBehaviour
{
	private static float time0;
	private string path, fileName;
	private StreamWriter writer;
	private int state = 0;

	private GameObject handCollRight, handCollLeft;
	public PredictAlpha predictAlpha;
	public SimulationObjOfInterest_withKids objectInstance;
	public SimulationHand fromCollection;

    public string addWord;



    void Start()
    {
		fromCollection = FindObjectOfType<SimulationHand>();
    	objectInstance = FindObjectOfType<SimulationObjOfInterest_withKids>();
    	predictAlpha = GameObject.FindObjectOfType<PredictAlpha>();
		fileName = fromCollection.FileName;

		
        time0 = Time.time;

		handCollRight = GameObject.Find("RightHandColl");
		handCollLeft = GameObject.Find("LeftHandColl");
		path = "Assets/Resources/DataSimulation/AlgoHints/A-" + fromCollection.alpha + "-" + addWord + fileName + ".csv";

    }

    // Update is called once per frame
    void Update()
    {
		path = "Assets/Resources/DataSimulation/AlgoHints/A-" + fromCollection.alpha + "-" + addWord + fileName + ".csv";
        writer = new StreamWriter(path, true);

        if(fromCollection.startAgain)
        {
        	state = 0;
        }

    	switch(state)
		{
			case 0:

				writer.Write("Time;Alpha;Config;NbBloc;Grasp");

				for(int i = 0; i < 19; i++)
				{
					writer.Write(";BoneID;BoneName;BonePos;PosPred;DistToPred;BoneRadius;BoneContact;ClosestPoint");
					writer.Write(";BoneIDL;BoneNameL;BonePosL;PosPredL;DistToPredL;BoneRadiusL;BoneContactL;ClosestPointL");
				}
				writer.Close();

				state = 1;
			break;

// WRITE OBJECTS OF INTEREST / REMOVE RB/COLLIDER AND ADD ROT/POS?
			case 1:
				writer.WriteLine();		
				if(fromCollection.frame > 3)
				{
					writer.Write(fromCollection.timeRead + ";" + fromCollection.alpha + ";" + objectInstance.config + ";" + objectInstance.nbBloc + ";" + objectInstance.grasp);
					
					for(int i = 0; i < 19 ; i++)
			    	{
						writer.Write(";" + predictAlpha.phalanxPrediction[i].namePhalanx + ";" + handCollRight.transform.GetChild(i).gameObject.transform.position.magnitude.ToString() + ";" + predictAlpha.phalanxPrediction[i].posPred.ToString() + ";" + predictAlpha.phalanxPrediction[i].distToPred.ToString() + ";" + handCollRight.transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().radius.ToString() + ";" + predictAlpha.phalanxPrediction[i].phalanxContact + ";" + predictAlpha.phalanxPrediction[i].closestPoint.ToString());
						writer.Write(";" + predictAlpha.phalanxPrediction[i+19].namePhalanx + ";" + handCollLeft.transform.GetChild(i).gameObject.transform.position.magnitude.ToString() + ";" + predictAlpha.phalanxPrediction[i+19].posPred.ToString() + ";" + predictAlpha.phalanxPrediction[i+19].distToPred.ToString() + ";" + handCollLeft.transform.GetChild(i).gameObject.GetComponent<CapsuleCollider>().radius.ToString() + ";" + predictAlpha.phalanxPrediction[i+19].phalanxContact + ";" + predictAlpha.phalanxPrediction[i+19].closestPoint.ToString());
					}
				}

		    	writer.Close();
			break;
		}

	}


}
