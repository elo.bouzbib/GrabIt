﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VF_Position : MonoBehaviour
{
	public GameObject index, thumb, palm;
	private Vector3 virtualFinger;

  public bool warped;
    // Start is called before the first frame update
    void Start()
    {

      warped = GameObject.FindObjectOfType<Predict_Live>().warped;

      if(!warped)
      {
        if(this.name == "R-VF")
        {
          palm = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/r_palm_center_marker").gameObject;
          index = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/b_r_index1/b_r_index2/b_r_index3/r_index_finger_pad_marker").gameObject;
          thumb = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/b_r_thumb0/b_r_thumb1/b_r_thumb2/b_r_thumb3/r_thumb_finger_pad_marker").gameObject;
          virtualFinger = (index.transform.position - thumb.transform.position);    
        }
        if(this.name == "L-VF")
        {
          palm = GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L/OculusHand_L/b_l_wrist/l_palm_center_marker").gameObject;
          index = GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L/OculusHand_L/b_l_wrist/b_l_index1/b_l_index2/b_l_index3/l_index_finger_pad_marker").gameObject;
          thumb = GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L/OculusHand_L/b_l_wrist/b_l_thumb0/b_l_thumb1/b_l_thumb2/b_l_thumb3/l_thumb_finger_pad_marker").gameObject;
          virtualFinger = (index.transform.position - thumb.transform.position);    
        }
      }
      else
      {
        if(this.name == "R-VF")
        {
          palm = GameObject.Find("Warped_OculusHand_R/b_r_wrist/r_palm_center_marker").gameObject;
          index = GameObject.Find("Warped_OculusHand_R/b_r_wrist/b_r_index1/b_r_index2/b_r_index3/r_index_finger_pad_marker").gameObject;
          thumb = GameObject.Find("Warped_OculusHand_R/b_r_wrist/b_r_thumb0/b_r_thumb1/b_r_thumb2/b_r_thumb3/r_thumb_finger_pad_marker").gameObject;
          virtualFinger = (index.transform.position - thumb.transform.position);    
        }
        if(this.name == "L-VF")
        {
          palm = GameObject.Find("Warped_OculusHand_L/b_l_wrist/l_palm_center_marker").gameObject;
          index = GameObject.Find("Warped_OculusHand_L/b_l_wrist/b_l_index1/b_l_index2/b_l_index3/l_index_finger_pad_marker").gameObject;
          thumb = GameObject.Find("Warped_OculusHand_L/b_l_wrist/b_l_thumb0/b_l_thumb1/b_l_thumb2/b_l_thumb3/l_thumb_finger_pad_marker").gameObject;
          virtualFinger = (index.transform.position - thumb.transform.position);    
        }
      }
    	

    	this.transform.position = (thumb.transform.position + virtualFinger/2);// - palm.transform.position;
		
    }

    // Update is called once per frame
    void Update()
    {
    	if(!warped)
      {
        if(this.name == "R-VF")
        {
          palm = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/r_palm_center_marker").gameObject;
          index = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/b_r_index1/b_r_index2/b_r_index3/r_index_finger_pad_marker").gameObject;
          thumb = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRCustomHandPrefab_R/OculusHand_R/b_r_wrist/b_r_thumb0/b_r_thumb1/b_r_thumb2/b_r_thumb3/r_thumb_finger_pad_marker").gameObject;
          virtualFinger = (index.transform.position - thumb.transform.position);    
        }
        if(this.name == "L-VF")
        {
          palm = GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L/OculusHand_L/b_l_wrist/l_palm_center_marker").gameObject;
          index = GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L/OculusHand_L/b_l_wrist/b_l_index1/b_l_index2/b_l_index3/l_index_finger_pad_marker").gameObject;
          thumb = GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRCustomHandPrefab_L/OculusHand_L/b_l_wrist/b_l_thumb0/b_l_thumb1/b_l_thumb2/b_l_thumb3/l_thumb_finger_pad_marker").gameObject;
          virtualFinger = (index.transform.position - thumb.transform.position);    
        }
      }
      else
      {
        if(this.name == "R-VF")
        {
          palm = GameObject.Find("Warped_OculusHand_R/b_r_wrist/r_palm_center_marker").gameObject;
          index = GameObject.Find("Warped_OculusHand_R/b_r_wrist/b_r_index1/b_r_index2/b_r_index3/r_index_finger_pad_marker").gameObject;
          thumb = GameObject.Find("Warped_OculusHand_R/b_r_wrist/b_r_thumb0/b_r_thumb1/b_r_thumb2/b_r_thumb3/r_thumb_finger_pad_marker").gameObject;
          virtualFinger = (index.transform.position - thumb.transform.position);    
        }
        if(this.name == "L-VF")
        {
          palm = GameObject.Find("Warped_OculusHand_L/b_l_wrist/l_palm_center_marker").gameObject;
          index = GameObject.Find("Warped_OculusHand_L/b_l_wrist/b_l_index1/b_l_index2/b_l_index3/l_index_finger_pad_marker").gameObject;
          thumb = GameObject.Find("Warped_OculusHand_L/b_l_wrist/b_l_thumb0/b_l_thumb1/b_l_thumb2/b_l_thumb3/l_thumb_finger_pad_marker").gameObject;
          virtualFinger = (index.transform.position - thumb.transform.position);    
        }
      }

    	this.transform.position = thumb.transform.position + virtualFinger/2;//) - palm.transform.position;
		
    }
}
