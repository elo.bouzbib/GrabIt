using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System.IO;
using System.Globalization;

public class SimulationHand : MonoBehaviour
{
	private static float time0;
	private string path, pathCollProperty, pathCollNames;
	public string FileName;
    private Transform posUser;

    private GameObject handLeft, handRight;

    private string[] dataSplit, dataCollProp, dataCollPos, dataNames;
    private string[] splitSentence, collProperties, collPosOrient;
    public int frame = 1;

    private float frame_max;

    public int etat = 0;
    private int boneId;

    private StreamReader sr, srCollProp, srCollPos, srNames;
    
    public IList<OVRBoneCapsule> Capsules { get; private set; }
    public IList<OVRBoneCapsule> CapsulesR { get; private set; }

    private GameObject leftHandColl, rightHandColl;

    // public float alpha, incremAlpha, maxAlpha, beta, incremBeta, maxBeta, gamma, incremGamma, maxGamma, epsilon, incremEpsilon, maxEpsilon;
    public float alpha, maxAlpha, beta, gamma;
    public float epsilon;


    public bool startAgain;
    public string timeRead;

    public bool varyAlpha, varyEpsilon, varyBetaGamma;
    public float[] list_alpha, list_gamma, list_beta;

    public bool readBetaList, readGammaList;
    public float[] listOtherBeta, listOtherGamma, listOtherAlpha;
    public int readList = 0;

	
	void Start()
	{
		posUser = GameObject.Find("Main Camera").transform;
		handLeft = GameObject.Find("OVRCustomHandPrefab_L");
		handRight = GameObject.Find("OVRCustomHandPrefab_R");
        leftHandColl = GameObject.Find("LeftHandColl");
        rightHandColl = GameObject.Find("RightHandColl");

		string path = "Assets/Resources/DataCollection/" + FileName + ".csv";
        // string pathCollProperty = "Assets/Resources/DataCollection/" + FileName + "-Coll.csv";
        string pathCollProperty = "Assets/Resources/DataCollection/propsColliders.csv";

        string pathPosOrientColl = "Assets/Resources/DataCollection/" + FileName + "pos-orient.csv";
        string pathCollNames = "Assets/Resources/DataCollection/namesColliders.csv";

		StreamReader sr = new StreamReader(path, true);
		if (sr.Peek() > -1) 
        {
            string line = sr.ReadToEnd();     
            dataSplit = line.Split('\n');
        }

        StreamReader srCollProp = new StreamReader(pathCollProperty, true);
        if (srCollProp.Peek() > -1) 
        {
            string line = srCollProp.ReadToEnd();     
            dataCollProp = line.Split('\n');
        }
        collProperties = dataCollProp[1].Split(';');

        StreamReader srCollPos = new StreamReader(pathPosOrientColl, true);
        if (srCollPos.Peek() > -1) 
        {
            string line = srCollPos.ReadToEnd();     
            dataCollPos = line.Split('\n');
        }
        collPosOrient = dataCollPos[2].Split(';');

        frame_max = (dataSplit.Length - 1);

        StreamReader srNames = new StreamReader(pathCollNames, true);
        if (srNames.Peek() > -1) 
        {
            string line = srNames.ReadToEnd();     
            dataNames = line.Split('\n');
        }

        // list_alpha = new float[5];
        // list_beta = new float[5];
        // list_gamma = new float[5];

        // listOtherBeta = new float[5];
        // listOtherAlpha = new float[5];
        // listOtherGamma = new float[5];

        // list_alpha = [0f, 0.25f, 0.5f, 0.75f, 1f];
        // list_gamma = [0f, 0.001f, 0.001f, 0.005f, 0.005f];
        // list_beta = [0f, 1.5f, 3f, 1.5f, 3f];

        // listOtherBeta = [1f, 1.5f, 2f, 2.5f, 3f];
        // listOtherGamma = [0.005f, 0.01f, 0.05f, 0.1f, 0.5f];
        // listOtherAlpha = [0.05f, 0.1f, 0.15f, 0.2f, 0.3f];
        if(!readGammaList && !readBetaList)
        {
            alpha = list_alpha[readList];
            beta = list_beta[readList];
            gamma = list_gamma[readList];
        }

        if(!readGammaList && !readBetaList && !varyAlpha && !varyBetaGamma)
        {
            // read from inspector
            beta = 1.05f;
        }

        if(readGammaList)
        {
            beta = 10;

            gamma = listOtherGamma[readList];
            alpha = listOtherAlpha[readList];
        }
        if(readBetaList)
        {
            gamma = listOtherGamma[readList];
            beta = listOtherBeta[readList];
            GameObject.FindObjectOfType<WritePredictAlpha>().gameObject.GetComponent<WritePredictAlpha>().enabled = false;
        }
        // if(readBetaList)
        // {
        //     gamma = 10;
        //     beta = listOtherBeta[readList];
        //     GameObject.FindObjectOfType<WritePredictAlpha>().gameObject.GetComponent<WritePredictAlpha>().enabled = false;
        // }

        maxAlpha = 0;
        
	}

	void FixedUpdate()
	{
        Time.fixedDeltaTime = 0.05f;

		if (Input.GetKeyDown(KeyCode.Q))
        {
            Application.Quit();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        startAgain = false;

        switch(etat)
        {
            case 0:                
                Debug.Log("Frame: " + frame + " / " + frame_max);

			  	frame = frame + 1;

            	if(frame >= frame_max)
                {
                    Debug.Log("FRAMEMAX");
                    etat = 1;
                }
               	splitSentence = dataSplit[frame].Split(';');
                // collProperties = dataCollProp[frame].Split(';');
                collPosOrient = dataCollPos[frame].Split(';');
            	timeRead = splitSentence[0];
                posUser.position = new Vector3(float.Parse((splitSentence[1]), CultureInfo.InvariantCulture)/1000, float.Parse((splitSentence[2]), CultureInfo.InvariantCulture)/1000, float.Parse((splitSentence[3]), CultureInfo.InvariantCulture)/1000);
                posUser.eulerAngles = new Vector3(float.Parse((splitSentence[4]), CultureInfo.InvariantCulture)/1000, float.Parse((splitSentence[5]), CultureInfo.InvariantCulture)/1000, float.Parse((splitSentence[6]), CultureInfo.InvariantCulture)/1000);

                for(int i = 7; i < (handLeft.GetComponent<OVRCustomSkeleton>().CustomBones.Count)*13; i = i+13)
                {
                    boneId = int.Parse(splitSentence[i]);
                    handLeft.GetComponent<OVRCustomSkeleton>().CustomBones[boneId].transform.position = new Vector3(float.Parse((splitSentence[i+1]), CultureInfo.InvariantCulture), float.Parse((splitSentence[i+2]), CultureInfo.InvariantCulture), float.Parse((splitSentence[i+3]), CultureInfo.InvariantCulture));
                    handLeft.GetComponent<OVRCustomSkeleton>().CustomBones[boneId].transform.eulerAngles = new Vector3(float.Parse((splitSentence[i+4]), CultureInfo.InvariantCulture), float.Parse((splitSentence[i+5]), CultureInfo.InvariantCulture), float.Parse((splitSentence[i+6]), CultureInfo.InvariantCulture));
                    
                    handRight.GetComponent<OVRCustomSkeleton>().CustomBones[boneId].transform.position = new Vector3(float.Parse((splitSentence[i+7]), CultureInfo.InvariantCulture), float.Parse((splitSentence[i+8]), CultureInfo.InvariantCulture), float.Parse((splitSentence[i+9]), CultureInfo.InvariantCulture));
                    handRight.GetComponent<OVRCustomSkeleton>().CustomBones[boneId].transform.eulerAngles = new Vector3(float.Parse((splitSentence[i+10]), CultureInfo.InvariantCulture), float.Parse((splitSentence[i+11]), CultureInfo.InvariantCulture), float.Parse((splitSentence[i+12]), CultureInfo.InvariantCulture));    
                }

            break;

            case 1:
                
                

                if(varyAlpha)
                {
                    if(alpha >= maxAlpha)
                    {
                        Debug.Break();
                        Debug.Log("STOP");
                        sr.Close();
                        srCollPos.Close();
                        srCollProp.Close();
                        Debug.Break();
                    }
                    else
                    {
                        readList = readList +1;
                        Debug.Log("Change Alpha");
                        if(readGammaList)
                        {
                            beta = 10;
                            gamma = listOtherGamma[readList];
                            alpha = listOtherAlpha[readList];
                        }
                        if(readBetaList)
                        {
                            // gamma = 10;
                            gamma = listOtherGamma[readList];
                            beta = listOtherBeta[readList];
                            GameObject.FindObjectOfType<WritePredictAlpha>().gameObject.GetComponent<WritePredictAlpha>().enabled = false;
                        }
                        if(!readGammaList && !readBetaList)
                        {
                            alpha = list_alpha[readList];
                            beta = list_beta[readList];
                            gamma = list_gamma[readList];  
                        }
                        
                        frame = 0;
                        startAgain = true;
                        etat = 0;
                    }
                }
                // if(varyAlpha)
                // {
                //     if(alpha >= maxAlpha)
                //     {
                //         Debug.Break();
                //         Debug.Log("STOP");
                //         sr.Close();
                //         srCollPos.Close();
                //         srCollProp.Close();
                //         Debug.Break();
                //     }
                //     else
                //     {
                //         Debug.Log("Change Alpha");
                //         alpha = alpha + incremAlpha;
                //         frame = 0;
                //         startAgain = true;
                //         etat = 0;
                //     }
                // }
                
                // if(varyEpsilon)
                // {
                //     if(epsilon >= maxEpsilon)
                //     {
                //         Debug.Break();
                //         Debug.Log("STOP");
                //         sr.Close();
                //         srCollPos.Close();
                //         srCollProp.Close();
                //         Debug.Break();
                //     }
                //     else
                //     {
                //         Debug.Log("Change Epsilon");
                //         epsilon = epsilon + incremEpsilon;
                //         frame = 0;
                //         startAgain = true;
                //         etat = 0;
                //     }
                // }

                // if(varyBetaGamma)
                // {
                //     if(beta >= maxBeta)
                //     {
                //     //     if(gamma >= maxGamma)
                //     //     {
                //             Debug.Break();
                //             Debug.Log("STOP");
                //             sr.Close();
                //             srCollPos.Close();
                //             srCollProp.Close();
                //             Debug.Break();
                //         // }
                //         // else
                //         // {
                //         //     Debug.Log("Change Gamma");
                //         //     gamma = gamma + incremGamma;
                //         //     frame = 0;
                //         //     startAgain = true;
                //         //     etat = 0;
                //         // }    
                //     }
                //     else
                //     {
                //         Debug.Log("Change Beta");
                //         beta = beta + incremBeta;
                //         frame = 0;
                //         startAgain = true;
                //         etat = 0;                      
                //     }

                //     if(gamma >= maxGamma)
                //     {
                //         Debug.Break();
                //         Debug.Log("STOP");
                //         sr.Close();
                //         srCollPos.Close();
                //         srCollProp.Close();
                //         Debug.Break();
                //     }
                //     else
                //     {
                //         Debug.Log("Change Gamma");
                //         gamma = gamma + incremGamma;
                //         frame = 0;
                //         startAgain = true;
                //         etat = 0;
                //     }    
                // }

                if(!varyBetaGamma && !varyAlpha && !varyEpsilon)
                {
                    Debug.Break();
                    Debug.Log("STOP");
                    sr.Close();
                    srCollPos.Close();
                    srCollProp.Close();
                    Debug.Break();
                }


                // if(epsilon > maxEpsilon)
                // {
                //     if(alpha > maxAlpha)
                //     {
                //         if(beta > maxBeta)
                //         {
                //             if(gamma > maxGamma)
                //             {
                //                 Debug.Break();
                //                 Debug.Log("STOP");
                //                 sr.Close();
                //                 srCollPos.Close();
                //                 srCollProp.Close();
                //                 Debug.Break();
                //             }
                //             else
                //             {
                //                 Debug.Log("Change Gamma");
                //                 gamma = gamma + incremGamma;
                //                 frame = 0;
                //                 startAgain = true;
                //                 etat = 0;
                //             }    
                //         }
                //         else
                //         {
                //             Debug.Log("Change Beta");
                //             beta = beta + incremBeta;
                //             frame = 0;
                //             startAgain = true;
                //             etat = 0;                      
                //         }
                //     }
                //     else
                //     {
                //         Debug.Log("Change Alpha");
                //         alpha = alpha + incremAlpha;
                //         frame = 0;
                //         startAgain = true;
                //         etat = 0;
                //     }
                // }
                // else
                // {
                //     Debug.Log("Change Epsilon");
                //     epsilon = epsilon + incremEpsilon;
                //     frame = 0;
                //     startAgain = true;
                //     etat = 0;
                // }

            break;
        }

	}
}
