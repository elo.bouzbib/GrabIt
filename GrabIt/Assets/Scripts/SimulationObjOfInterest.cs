﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System.IO;
using System.Globalization;

public class SimulationObjOfInterest : MonoBehaviour
{
	private static float time0;
	private string path, FileName;
	public SimulationHand fromHand;

    // public ObjectOfInterest[] OOI;

    private string[] dataSplit;
    private string[] splitSentence;
    private int frame = 1;

    private float frame_max;

    private int etat;
    private int boneId;

    private StreamReader sr;

    [Serializable]  
    public struct TasksChoice {
        public string nameTask;
        public int nbTask;
    }
    [Header("Tasks")]   
    public TasksChoice[] declaredTasks;

    [Serializable]  
    public struct ObjectChoice {
        public string objectName;
        public int nbObject;
    }
    [Header("Objects")]
    public ObjectChoice[] declaredObjects;

    [Header("Scales")]
    public Vector3[] scales;


    private string[] tasks;
    public GameObject[] objectTypes;
    private GameObject[] phantomObject;

    public List<int> configException;

    // private Transform[] posPhantoms;
    private Vector3[] posPhantoms;
    private Vector3 posOrigin;

    [Range(0, 127)]
    public int config;
    public Vector3 scaleObj;
    public GameObject objToCatch;
    public string taskToDo;
    private int taskNumber, objId;

    public int nbBloc = 0;
    public int nbBlocMax = 1;
    public int state = 0;
    public int nbBlocFromData;

    public Material phantomMaterialRed, phantomMaterialGreen;

    private Transform[] walls;
    private TextMesh[] otherwalls;
    public GameObject Text3D;

    public GameObject resetButton, nextButton;
    private GameObject[] hands;

    private Vector3 previousAngles;

    public int nbTouch = 0;	

    public bool changeConfig = false;
    public bool grasp = false;


    void Start()
    {
    	fromHand = FindObjectOfType<SimulationHand>();
    	FileName = fromHand.FileName;

		string path = "Assets/Resources/DataCollection/" + FileName + "-OOI.csv";       
        StreamReader sr = new StreamReader(path, true);
		if (sr.Peek() > -1) 
        {
            string line = sr.ReadToEnd();     
            dataSplit = line.Split('\n');
        }
        frame_max = (dataSplit.Length - 1);


    	posOrigin = new Vector3(-0.25f, -0.63f, 0.335f);

        // TASKS
        tasks = new string[declaredTasks.Length];
        for(int i = 0; i < declaredTasks.Length; i++)
        {
            tasks[declaredTasks[i].nbTask] = declaredTasks[i].nameTask;
        }
        

        // OBJECTS
        objectTypes = new GameObject[declaredObjects.Length];
        phantomObject = new GameObject[declaredObjects.Length];
        for(int i = 0; i < declaredObjects.Length; i++)
        {
            objectTypes[declaredObjects[i].nbObject] = GameObject.Find("ObjectsOfInterest/" + declaredObjects[i].objectName);
            phantomObject[declaredObjects[i].nbObject] = GameObject.Find("PhantomObjects/" + declaredObjects[i].objectName);
        }
// PILOT
        // tasks = new string[]{"Push"};//, "Push", "Pull", "Move Over", "Raise", "Push Down", "Fit", "Move"};
        // tasks = new string[]{"Hold", "Push", "Pull"};
        //0:Hold, 1:Push: 2:Pull: 3:MoveOver; 4:Raise; 5:PushDown; 6:Fit; 7:Move
// REAL
        // objectTypes = new GameObject[]{GameObject.Find("ObjectsOfInterest/Cylinder"), GameObject.Find("ObjectsOfInterest/Cube"), GameObject.Find("ObjectsOfInterest/Sphere")};//, GameObject.Find("ObjectsOfInterest/Capsule")};//, GameObject.Find("ObjectsOfInterest/Cylinder90")}; // j
// PILOT
        // objectTypes = new GameObject[]{GameObject.Find("ObjectsOfInterest/Sphere"), GameObject.Find("ObjectsOfInterest/Cube"), GameObject.Find("ObjectsOfInterest/Cylinder")};//, GameObject.Find("ObjectsOfInterest/Capsule")};//, GameObject.Find("ObjectsOfInterest/Cylinder90")}; // j

        //0:Cylinder, 1:Cube, 2:Sphere, 3:Capsule //, 4:LyingCylinder
//REAL      
        // phantomObject = new GameObject[]{GameObject.Find("PhantomObjects/Cylinder"), GameObject.Find("PhantomObjects/Cube"), GameObject.Find("PhantomObjects/Sphere")};//, GameObject.Find("PhantomObjects/Capsule")};//, GameObject.Find("ObjectsOfInterest/Cylinder90")};
// PILOT
        // phantomObject = new GameObject[]{GameObject.Find("PhantomObjects/Sphere"), GameObject.Find("PhantomObjects/Cube"), GameObject.Find("PhantomObjects/Cylinder")};

        for(int i = 0; i < objectTypes.Length; i++)
        {
            objectTypes[i].SetActive(false);
            phantomObject[i].SetActive(false);
            // 0: cylinder; 1: cube; 2: sphere; 3: capsule
        }

        // scales = new Vector3[3];
        // scales[0] = new Vector3(0.03f, 0.03f, 0.03f);
        // scales[1] = new Vector3(0.05f, 0.05f, 0.05f);
        // scales[2] = new Vector3(0.1f, 0.1f, 0.1f);


// REAL
        // posPhantoms = new Vector3[tasks.Length];
     // posPhantoms[0] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z); // TOUCH
     // posPhantoms[1] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z + 0.2f); // PUSH
     // posPhantoms[2] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z - 0.2f); // PULL
  //    posPhantoms[3] = new Vector3(posOrigin.x + 0.6f, posOrigin.y, posOrigin.z - 0.15f);
        // posPhantoms[4] = new Vector3(posOrigin.x, posOrigin.y + 0.5f, posOrigin.z);
        // posPhantoms[5] = new Vector3(posOrigin.x, posOrigin.y - 0.1f, posOrigin.z);
        // posPhantoms[6] = new Vector3(posOrigin.x - 0.6f, posOrigin.y, posOrigin.z + 0.35f);
        // posPhantoms[7] = new Vector3(posOrigin.x + 0.8f, posOrigin.y, posOrigin.z);

// PILOT
        // posPhantoms = new Vector3[tasks.Length];
        // // posPhantoms[0] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z + 0.2f);
        // posPhantoms[0] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z); // TOUCH
        // posPhantoms[1] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z + 0.2f); // PUSH
        // posPhantoms[2] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z - 0.2f); // PULL        

        posPhantoms = new Vector3[tasks.Length];
        for(int i = 0; i < tasks.Length; i++)
        {
            if(declaredTasks[i].nameTask == "Touch")
            {
                posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z); // TOUCH
            }
            if(declaredTasks[i].nameTask == "Push")
            {
                posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z + 0.2f); // PUSH
            }
            if(declaredTasks[i].nameTask == "Pull")
            {
                posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x, posOrigin.y, posOrigin.z - 0.2f); // PULL
            }
            if(declaredTasks[i].nameTask == "Move Over")
            {
                posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x + 0.6f, posOrigin.y, posOrigin.z - 0.15f); // MOVEOVER
            }
            if(declaredTasks[i].nameTask == "Raise")
            {
                posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x, posOrigin.y + 0.5f, posOrigin.z); // RAISE
            }
            if(declaredTasks[i].nameTask == "Push Down")
            {
                posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x, posOrigin.y - 0.1f, posOrigin.z); // PUSHDOWN
            }
            if(declaredTasks[i].nameTask == "Fit")
            {
                posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x - 0.6f, posOrigin.y, posOrigin.z + 0.35f); // FIT
            }
            if(declaredTasks[i].nameTask == "Roll")
            {
                posPhantoms[declaredTasks[i].nbTask] = new Vector3(posOrigin.x + 0.8f, posOrigin.y, posOrigin.z); // MOVE
            }
        }



        configException = new List<int>();

        walls = GameObject.Find("Walls").GetComponentsInChildren<Transform>();
        Text3D = GameObject.Find("Text3D");
        otherwalls = new TextMesh[walls.Length];

        for (int i = 2; i < otherwalls.Length; i++)
        {
            otherwalls[i] = (TextMesh)Instantiate(Text3D, walls[i].gameObject.transform).GetComponent<TextMesh>();
            otherwalls[i].transform.position = walls[i].gameObject.transform.position;
            otherwalls[i].transform.eulerAngles = new Vector3(walls[i].gameObject.transform.eulerAngles.x, walls[i].gameObject.transform.eulerAngles.y - 90f, walls[i].gameObject.transform.eulerAngles.z);

            otherwalls[i].characterSize = 0.02f;
        }

        resetButton = GameObject.Find("ResetButton");
        nextButton = GameObject.Find("NextButton");
        hands = new GameObject[]{GameObject.Find("OVRCustomHandPrefab_L"), GameObject.Find("OVRCustomHandPrefab_R")};
        changeConfig = false;
        grasp = false;
    }

    // Update is called once per frame
    void Update()
    {
    	etat = fromHand.etat;
        if(fromHand.startAgain)
        {
            frame = 0;
        }

        changeConfig = false;
        switch(etat)
        {
            case 0:
               	frame = frame + 1;
                
               	splitSentence = dataSplit[frame].Split(';');
                config = int.Parse(splitSentence[1]);
                for(int i = 2; i < (objectTypes.Length*7); i = i+7)
                {
                    int OOI_id = int.Parse(splitSentence[i]);
                    objectTypes[OOI_id].transform.position = new Vector3(float.Parse((splitSentence[i+1]), CultureInfo.InvariantCulture), float.Parse((splitSentence[i+2]), CultureInfo.InvariantCulture), float.Parse((splitSentence[i+3]), CultureInfo.InvariantCulture));
                    objectTypes[OOI_id].transform.eulerAngles = new Vector3(float.Parse((splitSentence[i+4]), CultureInfo.InvariantCulture), float.Parse((splitSentence[i+5]), CultureInfo.InvariantCulture), float.Parse((splitSentence[i+6]), CultureInfo.InvariantCulture));
                }
                nbBlocFromData = int.Parse(splitSentence[objectTypes.Length*7 + 2]);
                // NEW
                grasp = bool.Parse(splitSentence[objectTypes.Length*7 + 3]);
                // OR grasp = OOI ColorChangeSimulation graspContact
                // Going through i, j, k and assigning tasks/objects/scales to configurations numbers.
                for(int k = 0; k < scales.Length; k++)
                {
                    if((config < tasks.Length*objectTypes.Length*(k+1)) && (config >= tasks.Length*objectTypes.Length*k))
                    {
                        taskToDo = tasks[config%tasks.Length];
                        taskNumber = config%tasks.Length;
                        for(int j = 0; j < objectTypes.Length; j++)
                        {
                            if((config < tasks.Length*(objectTypes.Length*k + j+1)) && (config >= tasks.Length*(objectTypes.Length*k + j)))
                            {
                                objToCatch = objectTypes[j];
                                objId = j;
                            }
                        }
                        scaleObj = scales[k];
                    }
                }

                if(config != int.Parse(dataSplit[frame-1].Split(';')[1]))
                {
                    changeConfig = true;
                    nbTouch = nbTouch + 1;  
                    if(nbTouch >= (tasks.Length*objectTypes.Length*scales.Length))
                    {
                        nbBloc = nbBloc + 1;
                        configException.Clear();
                        configException = new List<int>();
                        nbTouch = 0;

                    }  
                }

                if(configException.Contains(config))
                {
                    // DO NOTHING
                }
                else
                {
                    configException.Add(config);
                }
                

                for(int i = 0; i < objectTypes.Length; i++)
                {
                    if(i != objId)
                    {
                        objectTypes[i].SetActive(false);
                        phantomObject[i].SetActive(false);
                    }
                }
                
                objectTypes[objId].SetActive(true);
                // objectTypes[objId].GetComponent<Rigidbody>().isKinematic = true;
                // objectTypes[objId].GetComponent<ObjectOfInterest>().collision = false;
                // objectTypes[objId].GetComponent<ObjectOfInterest>().toBePicked = true;
                objectTypes[objId].transform.localScale = new Vector3(scaleObj.x, scaleObj.y, scaleObj.z);

                phantomObject[objId].SetActive(true);
                phantomObject[objId].transform.position = new Vector3(posPhantoms[taskNumber].x, posPhantoms[taskNumber].y, posPhantoms[taskNumber].z);
                phantomObject[objId].transform.localScale = new Vector3(scaleObj.x, scaleObj.y, scaleObj.z);

                phantomObject[objId].GetComponent<MeshRenderer>().material = phantomMaterialRed;
                phantomObject[objId].GetComponent<Collider>().enabled = false;
                previousAngles = new Vector3(objectTypes[objId].transform.eulerAngles.x, objectTypes[objId].transform.eulerAngles.y, objectTypes[objId].transform.eulerAngles.z);
                StartCoroutine(Consignes());

                if((Mathf.Abs(objectTypes[objId].transform.position.x - phantomObject[objId].transform.position.x) < 0.07f) && (Mathf.Abs(objectTypes[objId].transform.position.y - phantomObject[objId].transform.position.y) < 0.07f) && (Mathf.Abs(objectTypes[objId].transform.position.z - phantomObject[objId].transform.position.z) < 0.07f))
                {
                    phantomObject[objId].GetComponent<MeshRenderer>().material = phantomMaterialGreen;
                    // nbTouch = configException.Count;
                    if(grasp)//if(objectTypes[objId].GetComponent<ColorChangeSimulation>().graspContact)// && (Mathf.Abs(objectTypes[objId].transform.position.x - phantomObject[objId].transform.position.x) < 0.05f) && (Mathf.Abs(objectTypes[objId].transform.position.y - phantomObject[objId].transform.position.y) < 0.05f) && (Mathf.Abs(objectTypes[objId].transform.position.z - phantomObject[objId].transform.position.z) < 0.05f))
                    {
                        // objectTypes[objId].GetComponent<ObjectOfInterest>().collision = true;
                        StartCoroutine(CollisionWait());
                    }
                }

            break;

            case 1:
            	sr.Close();
			break;
        }
    }

    IEnumerator Consignes()
    {
        yield return new WaitForSeconds(1);
        for (int i = 2; i < otherwalls.Length; i++)
        {
            otherwalls[i].text = taskToDo + " the WHITE " + objToCatch.gameObject.name + "\n into the RED " + objToCatch.gameObject.name + " until it becomes GREEN. \n Task #" + (configException.Count + tasks.Length*objectTypes.Length*scales.Length*nbBloc) + "/" + (tasks.Length*objectTypes.Length*scales.Length*nbBlocMax);
        }
        yield return new WaitForSeconds(5);
    }

    IEnumerator CollisionWait()
    {
        for (int i = 2; i < otherwalls.Length; i++)
        {
            otherwalls[i].text = "";
        }
        objectTypes[objId].GetComponent<Renderer>().enabled = false;
        phantomObject[objId].GetComponent<Renderer>().enabled = false;      
        yield return new WaitForSeconds(1);
        objectTypes[objId].GetComponent<Renderer>().enabled = true;
        phantomObject[objId].GetComponent<Renderer>().enabled = true;
        // objectTypes[objId].GetComponent<ObjectOfInterest>().collision = false;
    }

}
