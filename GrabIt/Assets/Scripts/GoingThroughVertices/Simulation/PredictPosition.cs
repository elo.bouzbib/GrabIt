using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PredictPosition : MonoBehaviour
{
	private GameObject handR, handL;
	// public GameObject skeletonR;

    [HideInInspector]
    public Vector3[] oldPos;
    [HideInInspector]
	public bool recordNow;


	public int slidingWindow = 45;
	private int j;

    [HideInInspector]
    public ClosestToHand[] closest;
    private Vector3[] toClosestPoint;
    [HideInInspector]
    public Vector3[] onGoingVector; 
    [HideInInspector]
    public float[] onGoingAngle, error, errorThumb, errorIndex, errorMiddle;
    [HideInInspector]
    public GameObject[] handChildR, handChildL;
    [HideInInspector]
    public Vector3[] projection;

    private GameObject OOI;
    
    public Vector3 virtualFinger;
    [HideInInspector]
    public float projVFX, projVFY, projVFZ;

    private GameObject palm, thumb, index;
    [HideInInspector]
    public SimulationHand readFromData;

    public bool[] inZone, contactPerPhal, inZone_fromPlane;//, inZone_fromProbsPred;
    [HideInInspector]
    public Vector3[] errorPredict;
    [HideInInspector]
    public GameObject[] zone;
    [HideInInspector]
    public Vector3[] posToPredict;

    [HideInInspector]
    public AlgoHints algoHint;

    public bool oneContact, clampGrip;
    // public string[] touchedBy, probsTouchedBy, touchByPlane;
    public List<string> gonnaTouch;

    void Start()
    {
    	handR = GameObject.Find("RightHandColl");
    	handL = GameObject.Find("LeftHandColl");
    	// childR = new GameObject[19];
    	handChildR = new GameObject[19];
    	// childL = new GameObject[19];
    	handChildL = new GameObject[19];
    	// skeletonR = GameObject.Find("OVRCustomHandPrefab_R");
        closest = new ClosestToHand[19];
        toClosestPoint = new Vector3[19];
        onGoingVector = new Vector3[19];
        onGoingAngle = new float[19];
        error = new float[19];

        readFromData = FindObjectOfType<SimulationHand>();

        for(int i = 0; i < 19; i++)
        {
        	handChildR[i] = handR.transform.GetChild(i).transform.gameObject;
        	handChildL[i] = handL.transform.GetChild(i).transform.gameObject;
            closest[i] = handChildR[i].GetComponent<ClosestToHand>();
        }

		oldPos = new Vector3[19];
        projection = new Vector3[19];

        for(int i = 0; i < 19; i++)
        {
            oldPos[i] = handChildR[i].transform.position;
        }
		j = 0;

        OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");

        virtualFinger = handChildR[9].transform.position - handChildR[6].transform.position;
        projVFX = Vector3.Project(virtualFinger, Vector3.right).x;
        projVFY = Vector3.Project(virtualFinger, Vector3.up).y;
        projVFZ = Vector3.Project(virtualFinger, Vector3.forward).z;

        palm = GameObject.Find("r_palm_center_marker");
        index = GameObject.Find("r_index_finger_pad_marker");
        thumb = GameObject.Find("r_thumb_finger_pad_marker");
        
        posToPredict = new Vector3[19];
        zone = new GameObject[6];
        inZone = new bool[6];
        inZone_fromPlane = new bool[6];
        // inZone_fromProbsPred = new bool[6];

        errorPredict = new Vector3[19];
        contactPerPhal = new bool[19];

        algoHint = FindObjectOfType<AlgoHints>();

        for(int k = 0; k < zone.Length; k++)
        {
            zone[k] = OOI.transform.GetChild(k).gameObject;
            inZone[k] = false;
            // inZone_fromPlane[k] = false;
            // inZone_fromProbsPred[k] = false;
        }
        oneContact = false;
        clampGrip = false;

        // touchedBy = new string[6];
        // probsTouchedBy = new string[6];
        // touchByPlane = new string[6];
        gonnaTouch = new List<string>();
    }

    void Update()
    {
        recordNow = false;
    	StartCoroutine(RecordPos());

        oneContact = false;
        clampGrip = false;

        OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");

        for(int k = 0; k < zone.Length; k++)
        {
            zone[k] = OOI.transform.GetChild(k).gameObject;
            inZone[k] = false;
            inZone_fromPlane[k] = false;
            // inZone_fromProbsPred[k] = false;
        }

// SQUARES EVERYWHERE? IF CYLINDER, IF SPHERE, IF CUBE DIFFERENT?
        zone[0].transform.position = new Vector3(OOI.transform.position.x + OOI.transform.localScale.x/2, OOI.transform.position.y, OOI.transform.position.z);
        zone[0].transform.localScale = new Vector3(0.2f, 1f, 1f);
        
        zone[1].transform.position = new Vector3(OOI.transform.position.x - OOI.transform.localScale.x/2, OOI.transform.position.y, OOI.transform.position.z);
        zone[1].transform.localScale = new Vector3(0.2f, 1f, 1f);
        
        if(OOI.gameObject.name == "Cylinder")
        {
            zone[2].transform.position = new Vector3(OOI.transform.position.x, OOI.transform.position.y + OOI.transform.localScale.y, OOI.transform.position.z);
            zone[2].transform.localScale = new Vector3(1f, 0.2f, 1f);

        }
        else
        {
            zone[2].transform.position = new Vector3(OOI.transform.position.x, OOI.transform.position.y + OOI.transform.localScale.y/2, OOI.transform.position.z);
            zone[2].transform.localScale = new Vector3(1f, 0.05f, 1f);

        }
        zone[3].transform.position = new Vector3(OOI.transform.position.x, OOI.transform.position.y - OOI.transform.localScale.y/2, OOI.transform.position.z);
        
        zone[3].transform.localScale = new Vector3(1f, 0.05f, 1f);

        zone[4].transform.position = new Vector3(OOI.transform.position.x, OOI.transform.position.y, OOI.transform.position.z + OOI.transform.localScale.z/2);
        zone[5].transform.position = new Vector3(OOI.transform.position.x, OOI.transform.position.y, OOI.transform.position.z - OOI.transform.localScale.z/2);
    
        zone[4].transform.localScale = new Vector3(1f, 1f, 0.2f);
        zone[5].transform.localScale = new Vector3(1f, 1f, 0.2f);



        for(int i = 0; i < 19; i++)
        {
            toClosestPoint[i] = -closest[i].closest_distance + handChildR[i].transform.position; // WHITE RAY

            onGoingVector[i] = handChildR[i].transform.position - oldPos[i]; // RED RAY
            onGoingAngle[i] = Vector3.Angle(-toClosestPoint[i], onGoingVector[i]);

            // PROJECTION WHITE ONTO RED: white.magnitude * red.magnitude * cos(angle) * direction(red)
            projection[i] = handChildR[i].transform.position + Vector3.Project(-toClosestPoint[i], onGoingVector[i]);
            error[i] = Vector3.Distance(closest[i].closest_distance, projection[i]);
        
            posToPredict[i] = readFromData.alpha*projection[i] + (1-readFromData.alpha)*closest[i].closest_distance;
            contactPerPhal[i] = closest[i].phalanxContact;

            errorPredict[i] = posToPredict[i] - handChildR[i].transform.position;
            for(int k = 0; k < zone.Length; k++)
            {
                if(zone[k].GetComponent<Collider>().bounds.Contains(posToPredict[i]))
                {
                    // ALL CHILDREN's COLLIDER -> JENGA/COLOR FACE / "UIST21!""
                    inZone[k] = true;
                    zone[k].GetComponent<MeshRenderer>().material.color = Color.green;
                    // touchedBy[k] = handChildR[i].name;
                    if(!gonnaTouch.Contains(handChildR[i].name))
                    {
                        gonnaTouch.Add(handChildR[i].name);
                    }
                    

                    // SI LA LISTE NE CONTIENT PAS LE NOM, AJOUTER LE NOM, PUIS COMPTER NB Future contact Points
                }
                else
                {
                    // touchedBy[k] = " ";
                    gonnaTouch.Remove(handChildR[i].name);
                    zone[k].GetComponent<MeshRenderer>().material.color = Color.white;
                    // FLUSH THE LIST
                }

                inZone_fromPlane[k] = zone[k].GetComponent<CollideCutSections>().collidingCutSections;
                // touchByPlane[k] = zone[k].GetComponent<CollideCutSections>().name;
                // if((zone[k].GetComponent<Collider>().bounds.Contains(algoHint.probsP[i])) || (zone[k].GetComponent<Collider>().bounds.Contains(algoHint.probsQ[i])) || (zone[k].GetComponent<Collider>().bounds.Contains(algoHint.probsR[i])))
                // {
                //     inZone_fromProbsPred[k] = true;
                //     probsTouchedBy[k] = handChildR[i].name;
                // }
                // else
                // {
                //     probsTouchedBy[k] = " ";
                // }

            }
        }
        
        j = j+1;
    	if(j%slidingWindow == 0)
    	{
    		recordNow = true;
    	}
    	
        palm = GameObject.Find("r_palm_center_marker");
        index = GameObject.Find("r_index_finger_pad_marker");
        thumb = GameObject.Find("r_thumb_finger_pad_marker");
        virtualFinger = (index.transform.position - thumb.transform.position);

        if(Mathf.Abs(Vector3.Dot(thumb.transform.right.normalized, palm.transform.right.normalized)) > 0.70f)
        {
            if(Mathf.Abs(Vector3.Dot(thumb.transform.right.normalized, index.transform.right.normalized)) > 0.85f)
            {
                oneContact = true;
            }
            else
            {
                clampGrip = true;
            }

        }


        projVFX = Vector3.Project(virtualFinger, Vector3.right).x;
        projVFY = Vector3.Project(virtualFinger, Vector3.up).y;
        projVFZ = Vector3.Project(virtualFinger, Vector3.forward).z;

        // Debug.DrawRay(thumb.transform.position, virtualFinger, Color.blue);
    }


    // public void OnDrawGizmos()
    // {
    // 	// for(int i = 0; i < 4; i++)
    // 	// {
    // 	// 	Debug.DrawRay(closest[i].closest_distance, toClosestPoint[i], Color.white);
    //  //        Debug.DrawRay(handChildR[i].transform.position, onGoingVector[i]/onGoingVector[i].magnitude, Color.red);

    //  //        Gizmos.color = Color.green;
    //  //        // Gizmos.DrawSphere(handChildR[i].transform.position + Vector3.Project(-toClosestPoint[i], onGoingVector[i]), 0.01f);
    //  //        Gizmos.DrawSphere(projection[i], 0.005f);
    //  //        // Gizmos.DrawLine(closest[i].closest_distance, handChildR[i].transform.position + Vector3.Project(-toClosestPoint[i], onGoingVector[i]));
    //  //        Gizmos.DrawLine(closest[i].closest_distance, projection[i]);

    //  //        Gizmos.DrawWireSphere(closest[i].closest_distance, 0.005f);
    //  //        // // ALL THREE DEBUGS WORK THE SAME
    //  //        // Debug.DrawRay(closest[i].closest_distance, projection[i] - closest[i].closest_distance, Color.green);
    //  //        // Debug.Log("Angle Greenw: " + Vector3.Angle(onGoingVector[i], handChildR[i].transform.position + Vector3.Project(-toClosestPoint[i], onGoingVector[i]) - closest[i].closest_distance));
    //  //        // Debug.Log("Angle Green: " + Vector3.Angle(onGoingVector[i], projection[i] - closest[i].closest_distance));
    //  //    }
    //     GameObject thumb = GameObject.Find("r_thumb_finger_pad_marker");

    //     // Debug.DrawRay(handChildR[6].transform.position, (handChildR[9].transform.position - handChildR[6].transform.position), Color.white);

    //     Debug.DrawRay(thumb.transform.position, virtualFinger, Color.blue);

    //     Vector3 projY = Vector3.Project((handChildR[9].transform.position - handChildR[6].transform.position), Vector3.up);
    //     Vector3 projX = Vector3.Project((handChildR[9].transform.position - handChildR[6].transform.position), Vector3.right);
    //     Vector3 projZ = Vector3.Project((handChildR[9].transform.position - handChildR[6].transform.position), Vector3.forward);

    //     // Debug.DrawRay(handChildR[6].transform.position, projX, Color.red);
    //     // Debug.DrawRay(handChildR[6].transform.position, projY, Color.green);
    //     // Debug.DrawRay(handChildR[6].transform.position, projZ, Color.blue);

    //     // Debug.DrawRay(handChildR[6].transform.position + virtualFinger/2, handChildR[0].transform.position - handChildR[6].transform.position - virtualFinger/2, Color.cyan);
        

    //     // for(int i = 6; i < 7; i++)
    //     // {
    //     //     Debug.DrawRay(thumbTarget, toThumbTarget[i], Color.white);
    //     //     Debug.DrawRay(handChildR[i].transform.position, onGoingVector[i]/onGoingVector[i].magnitude, Color.red);

    //     //     // Debug.DrawRay(palm.transform.position, onGoingVector[0]/onGoingVector[0].magnitude, Color.magenta);

    //     //     // Vector3 projThumb = new Vector3(Vector3.Project(onGoingVector[0], Vector3.right).x, Vector3.Project(onGoingVector[0], Vector3.up).y, Vector3.Project(onGoingVector[0], Vector3.forward).z);

    //     //     // Gizmos.color = Color.red;
    //     //     // Gizmos.DrawLine(palm.transform.position, projpalmX);
    //     //     // Debug.DrawRay(palm.transform.position, projpalmX - palm.transform.position, Color.red);


    //     //     // Gizmos.color = Color.green;
    //     //     // Gizmos.DrawLine(palm.transform.position, projpalmY);

    //     //     // Gizmos.color = Color.blue;
    //     //     // Gizmos.DrawLine(palm.transform.position, projpalmZ);


    //     //     Gizmos.color = Color.green;
    //     //     Gizmos.DrawSphere(projectionThumb[i], 0.005f);
    //     //     Gizmos.DrawLine(thumbTarget, projectionThumb[i]);

    //     //     // Gizmos.color = Color.blue;
    //     //     // Gizmos.DrawLine(sphere1Target, handChildR[i].transform.position + Vector3.Project(-(-sphere1Target + handChildR[i].transform.position), onGoingVector[i]));
            
    //     //     // Gizmos.color = Color.blue;
    //     //     // Gizmos.DrawLine(sphere2Target, handChildR[i].transform.position + Vector3.Project(-(-sphere2Target + handChildR[i].transform.position), onGoingVector[i]));
            
    //     //     // Gizmos.color = Color.blue;
    //     //     // Gizmos.DrawLine(sphere3Target, handChildR[i].transform.position + Vector3.Project(-(-sphere3Target + handChildR[i].transform.position), onGoingVector[i]));
            
    //     //     // Gizmos.color = Color.blue;
    //     //     // Gizmos.DrawLine(sphere4Target, handChildR[i].transform.position + Vector3.Project(-(-sphere4Target + handChildR[i].transform.position), onGoingVector[i]));
            
    //     // }
    //     // Debug.DrawRay(virtualFinger/2 + thumb.transform.position, onGoingVector[0]/onGoingVector[0].magnitude, Color.red);

    //     Debug.DrawRay(handChildR[6].transform.position, handChildR[6].transform.right/10, Color.black);
    //     Debug.DrawRay(handChildR[9].transform.position, handChildR[9].transform.right/10, Color.magenta);
    //     // Debug.Log("Angle: " + Vector3.Angle(handChildR[6].transform.right, handChildR[9].transform.right) + " ; Cos: " + Mathf.Cos(Vector3.Angle(handChildR[6].transform.right, handChildR[9].transform.right)));

    //     // Debug.Log("Angle0: " + Vector3.Angle(handChildR[6].transform.right, handChildR[0].transform.right) + " ; Cos: " + Mathf.Cos(Vector3.Angle(handChildR[6].transform.right, handChildR[9].transform.right)));

    //     // for(int i = 7; i < 8; i++)
    //     // {
    //     //     Debug.DrawRay(indexTarget, toIndexTarget[i], Color.white);
    //     //     Debug.DrawRay(handChildR[i].transform.position, onGoingVector[i]/onGoingVector[i].magnitude, Color.red);

    //     //     Gizmos.color = Color.green;
    //     //     Gizmos.DrawSphere(projectionIndex[i], 0.005f);
    //     //     Gizmos.DrawLine(indexTarget, projectionIndex[i]);
    //     // }
    //     // for(int i = 18; i < 19; i++)
    //     // {
    //     //     Debug.DrawRay(indexTarget, toIndexTarget[i], Color.white);
    //     //     Debug.DrawRay(handChildR[i].transform.position, onGoingVector[i]/onGoingVector[i].magnitude, Color.red);

    //     //     Gizmos.color = Color.green;
    //     //     Gizmos.DrawSphere(projectionIndex[i], 0.005f);
    //     //     Gizmos.DrawLine(indexTarget, projectionIndex[i]);
    //     // }

    // }


    IEnumerator RecordPos()
    {
    	yield return new WaitUntil (() => recordNow == true);
    	for(int i = 0; i < 19; i++)
        {
            oldPos[i] = handChildR[i].transform.position;

        }

    }

}
