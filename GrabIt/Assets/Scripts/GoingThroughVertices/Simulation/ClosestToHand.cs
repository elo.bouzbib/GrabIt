using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ClosestToHand : MonoBehaviour
{
	private Mesh objMesh;
	private Vector3[] objVertices; 
	private Vector3 closestPointPerVertex;
	// public Color[] colors;

	public bool phalanxContact;
	public GameObject OOI;

	public Vector3 closest_distance, closestPoint;

	public float REAL_MIN_DISTANCE;

	// private Shader shader;
  private Vector3 worldVertex;


    void Start()
    {
  		// shader = Shader.Find("Legacy Shaders/Particles/VertexLit Blended");
      OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");
  		objVertices = OOI.GetComponent<MeshFilter>().mesh.vertices;
  		objMesh = OOI.GetComponent<MeshFilter>().mesh;

    }

    // Update is called once per frame
    void Update()
    {
      OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");
      objVertices = OOI.GetComponent<MeshFilter>().mesh.vertices;
      objMesh = OOI.GetComponent<MeshFilter>().mesh;
      // OOI.GetComponent<MeshRenderer>().material.shader = shader;
      // colors = new Color[objVertices.Length];
    	
    	for(int k = 0; k < objVertices.Length; k++)
      {
        worldVertex = transform.TransformPoint(objVertices[k]);
      	// closestPoint = OOI.GetComponent<Collider>().ClosestPointOnBounds(this.transform.position);
        closestPoint = this.GetComponent<Collider>().ClosestPointOnBounds(worldVertex);

      	closest_distance = OOI.GetComponent<Collider>().ClosestPoint(closestPoint);
        REAL_MIN_DISTANCE = Vector3.Distance(closest_distance, this.transform.position);
	    }


      

    }

    // THIS DEBUGS THE DISTANCE AND LOCATION OF THE CLOSEST POINT FROM ONE PHALANX ON THE OBJECTS' COLLIDER (SURFACE POINT)
  //   public void OnDrawGizmos()
  //   {
  // //       GUIStyle style = new GUIStyle();
  // //       style.normal.textColor = Color.red; 
		// // // Handles.Label(closest_distance, "D: " + REAL_MIN_DISTANCE.ToString("F3"), style);
		//   // Gizmos.DrawWireSphere(closest_distance, 0.005f);
  //     for(int k = 0; k < objVertices.Length; k++)
  //       {
  //         worldVertex = OOI.transform.TransformPoint(objVertices[k]);
  //         Gizmos.DrawWireSphere(worldVertex, 0.005f);
  //         Gizmos.color = Color.red;
  //         Gizmos.DrawSphere(closest_distance, 0.005f);
  //       }
		
  //   }

    // IEnumerator RecordPos()
    // {
    // 	yield return new WaitUntil (() => recordNow == true);    	
    //     oldPos = this.transform.position;

    // }

    void OnCollisionStay(Collision collisionInfo)
    {
      if(collisionInfo.collider.gameObject.tag == "ObjectOfInterest")
      {
        // Debug.Log(this.gameObject.name + " PhalanxContact");
        this.phalanxContact = true;
      }
    }

    void OnCollisionExit(Collision collisionInfo)
    {
      if(collisionInfo.collider.gameObject.tag == "ObjectOfInterest")
      {
        // Debug.Log(this.gameObject.name + " PhalanxContact");
        this.phalanxContact = false;
      }
    }
}
