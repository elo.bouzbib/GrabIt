﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CollisionPlanes : MonoBehaviour
{
    private GameObject OOI;
    [HideInInspector]
    public Vector3 closestFromVF, virtualFinger;
	private Vector3 closestDist;
	private GameObject thumb, palm, index;

	public int planeID, nbOfPlanes;
    [HideInInspector]
	public AlgoHints algoHints;
    [HideInInspector]
    public Vector3[] projPerPhal, projPerPhalOnOOI;
    [HideInInspector]
    public GameObject[] childR;

    public bool fromTop, fromSideX, fromSideZ = false;

    void Start()
    {
      	OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");

    	palm = GameObject.Find("r_palm_center_marker");
    	index = GameObject.Find("r_index_finger_pad_marker");
    	thumb = GameObject.Find("r_thumb_finger_pad_marker");
    	algoHints = GameObject.FindObjectOfType<AlgoHints>();
        childR = new GameObject[19];

        for(int i = 0; i < 19; i++)
        {
            childR[i] = GameObject.Find("PredictedHandPositions").transform.GetChild(i).transform.gameObject;
        }
        projPerPhal = new Vector3[19];
        projPerPhalOnOOI = new Vector3[19];
        fromTop = false;
        fromSideX = false;
        fromSideZ = false;
    }

    void Update()
    {
      	OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");

    	palm = GameObject.Find("r_palm_center_marker");
    	index = GameObject.Find("r_index_finger_pad_marker");
    	thumb = GameObject.Find("r_thumb_finger_pad_marker");
    	
    	// NEED TO CREATE PLANE, from l to this point. GetContactPoints of VFMagnitude
    	virtualFinger = (index.transform.position - thumb.transform.position);
    	closestFromVF = whosClosest(thumb.transform.position + virtualFinger/2);
   	
    	Vector3 lengthToPalm = (thumb.transform.position + virtualFinger/2) - palm.transform.position;
    	
		this.transform.position = new Vector3(closestFromVF.x + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.right).x, closestFromVF.y + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.up).y, closestFromVF.z + Vector3.Project(planeID*lengthToPalm/(nbOfPlanes - 1), Vector3.forward).z);
		
		if((Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.up).y) > Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.forward).z)) && (Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.up).y) > Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.right).x)) )
		{
			this.transform.forward = virtualFinger/virtualFinger.magnitude;
            fromTop = true;
            fromSideX = false;
            fromSideZ = false;
		}
		else
		{
			this.transform.forward = virtualFinger/virtualFinger.magnitude;
    		this.transform.rotation = this.transform.rotation* Quaternion.FromToRotation(Vector3.right, Vector3.up);
    		fromTop = false;
            
            if((Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.up).y) < Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.right).x)) && (Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.forward).z) < Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.right).x)))
    		{
				// Debug.Log("from side X");
                fromSideX = true;
                fromSideZ = false;
    		}
    		else if ((Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.up).y) < Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.forward).z)) && (Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.forward).z) > Mathf.Abs(Vector3.Project(palm.transform.up, Vector3.right).x)))
    		{
    			// Debug.Log("from side Z");
                fromSideX = false;
                fromSideZ = true;
    		}
    		else
    		{
    			// Debug.Log("Kindof in the middle");
                fromSideX = false;
                fromSideZ = false;
    		}
		}

    	this.transform.localScale = new Vector3(virtualFinger.magnitude + 0.05f, virtualFinger.magnitude + 0.05f, virtualFinger.magnitude + 0.05f)*virtualFinger.magnitude*1.2f;

        for(int i = 0; i < 19; i++)
        {
            projPerPhal[i] = this.GetComponent<Collider>().ClosestPoint(this.GetComponent<Collider>().ClosestPointOnBounds(childR[i].transform.position));
            projPerPhalOnOOI[i] = whosClosest(projPerPhal[i]);
        }

        

    }


    public Vector3 whosClosest(Vector3 fromThis)
    {
    	// Vector3[] objVertices = OOI.GetComponent<MeshFilter>().mesh.vertices;
    	float REAL_MIN_DISTANCE;

    	// for(int k = 0; k < objVertices.Length; k++)
     //    {
    	Vector3 closestPoint = OOI.GetComponent<Collider>().ClosestPointOnBounds(fromThis);
    	closestDist = OOI.GetComponent<Collider>().ClosestPoint(closestPoint);
        REAL_MIN_DISTANCE = Vector3.Distance(closestDist, fromThis);
    	// }
    	return closestDist;

    }

    void OnCollisionEnter(Collision collisionInfo)
    {
        if(this.gameObject.name == "Plane")
        {
            if((collisionInfo.collider.gameObject.name == "Zone4") || (collisionInfo.collider.gameObject.name == "Zone5") || (collisionInfo.collider.gameObject.name == "Zone3")  || (collisionInfo.collider.gameObject.name == "Zone2")  || (collisionInfo.collider.gameObject.name == "Zone1")  || (collisionInfo.collider.gameObject.name == "Zone0"))
            {
                collisionInfo.collider.gameObject.GetComponent<CollideCutSections>().collidingCutSections = true;
            }
        }
    }

    void OnCollisionExit(Collision collisionInfo)
    {
        if((collisionInfo.collider.gameObject.name == "Zone4") || (collisionInfo.collider.gameObject.name == "Zone5") || (collisionInfo.collider.gameObject.name == "Zone3")  || (collisionInfo.collider.gameObject.name == "Zone2")  || (collisionInfo.collider.gameObject.name == "Zone1")  || (collisionInfo.collider.gameObject.name == "Zone0"))
        {
            collisionInfo.collider.gameObject.GetComponent<CollideCutSections>().collidingCutSections = false;
        }
    }
}
