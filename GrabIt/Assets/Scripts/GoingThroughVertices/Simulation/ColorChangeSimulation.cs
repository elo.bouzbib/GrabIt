﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class ColorChangeSimulation : MonoBehaviour
{
	public GameObject rightHand, leftHand;
	private float distanceL, distanceR;

	private float[] distancePerCapsL, distancePerCapsR;
	private Vector3[] closestPointPerVertexHandLeft, closestPointPerVertexHandRight;

	public float distance;
	private Mesh thisMesh;
	private Vector3[] thisVertices, closestPointPerVertexRight, closestPointPerVertexLeft;
	private Color[] colors;
	private Vector3 worldVertex;

	public bool graspContact;

	private Collider handCollidersRight, handCollidersLeft;
	private Shader shader;

	public IList<OVRBoneCapsule> Capsules { get; private set; }
	public IList<OVRBoneCapsule> CapsulesR { get; private set; }

	public ClosestToHand[] closestToHand;

	public int numberVertex;
	// public SimulationHand readHand;

	// private float[] weight_dist, weight_vel, weight_orient;
	// public float[] weight;

	// private float timer, timeStamp;

    void Start()
    {
		shader = Shader.Find("Legacy Shaders/Particles/VertexLit Blended");
		this.GetComponent<MeshRenderer>().material.shader = shader;
		thisMesh = this.GetComponent<MeshFilter>().mesh;
		thisVertices = this.GetComponent<MeshFilter>().mesh.vertices;	
		colors = new Color[thisVertices.Length];
		closestPointPerVertexRight = new Vector3[thisVertices.Length];	
		closestPointPerVertexLeft = new Vector3[thisVertices.Length];
		graspContact = false;

		rightHand = GameObject.Find("RightHandColl");
		leftHand = GameObject.Find("LeftHandColl");
		Capsules = GameObject.Find("OVRCustomHandPrefab_L").GetComponent<OVRCustomSkeleton>().Capsules;
		CapsulesR = GameObject.Find("OVRCustomHandPrefab_R").GetComponent<OVRCustomSkeleton>().Capsules;

		closestToHand = FindObjectsOfType<ClosestToHand>();
		numberVertex = 0;
    }

    void Update()
    {

    	graspContact = false;
    	this.transform.SetParent(GameObject.Find("ObjectsOfInterest").transform);
    	this.GetComponent<Rigidbody>().isKinematic = false;
		this.GetComponent<Rigidbody>().useGravity = true;

    	for(int j = 0; j < leftHand.GetComponentsInChildren<Collider>().Length; j++)
    	{
    		handCollidersRight = rightHand.GetComponentsInChildren<Collider>()[j];
    		handCollidersLeft = leftHand.GetComponentsInChildren<Collider>()[j];
    	}
    	numberVertex = thisVertices.Length;
        for(int k = 0; k < thisVertices.Length; k++)
        {
        	worldVertex = transform.TransformPoint(thisVertices[k]);
			closestPointPerVertexRight[k] = handCollidersRight.ClosestPoint(worldVertex);
			closestPointPerVertexLeft[k] = handCollidersLeft.ClosestPoint(worldVertex);
			
			distanceL = Vector3.Distance(closestPointPerVertexLeft[k], worldVertex);
			distanceR = Vector3.Distance(closestPointPerVertexRight[k], worldVertex);

			if(distanceL < distanceR)
			{
				distance = distanceL;
			}
			else
			{
				distance = distanceR;
			}

			if(distance < 0.02f)
			{
				graspContact = true;
			}
			// for(int i = 0; i < closestToHand.Length; i++)
			// {
			// 	distance = 10f*Vector3.Distance(closestToHand[i].closest_distance, closestPointPerVertexRight[k]);
			// 	// distance = closestToHand[i].REAL_MIN_DISTANCE;
			// 	// Debug.Log(distance + "; "+ i + " ; "+ k);
			// 	if(distance >= 1f)
			// 	{
			// 		timer = 0f;
			// 		weight_dist[k] = 0f;
			// 		// colors[k] = Color.Lerp(Color.white, Color.yellow, (1-distance/10));
			// 	}
			// 	if(distance < 1f)
			// 	{
			// 		timeStamp = ((Time.time - Time.fixedTime) / Time.fixedDeltaTime);
			// 		// colors[k] = Color.Lerp(Color.yellow, Color.red, (1-distance/10));
			// 		timer += timeStamp;
					
			// 	}
			// 	weight_dist[k] = Mathf.Exp(timer*(1-distance));
			// }

			// weight[k] = (readHand.alpha * weight_dist[k] + readHand.beta * weight_vel[k] + readHand.gamma * weight_orient[k])/(readHand.alpha + readHand.beta + readHand.gamma);
			// colors[k] = Color.Lerp(Color.white, Color.red, weight[k]);

			// Debug.Log(distance);
			// if(distanceL < distanceR)
			// {
			// 	distance = distanceL;
			// }
			// else
			// {
			// 	distance = distanceR;
			// }

			// HERE DISTANCE DEPENDS ON DISTANCE FROM CLOSESTPOINT TO MESHTRIANGLE
			// if(distance >= 0.10f)
			// {
			// 	colors[k] = Color.Lerp(Color.white, Color.yellow, (1-distance));
			// }
			// if(distance < 0.10f)
			// {
			// 	colors[k] = Color.Lerp(Color.yellow, Color.red, (1-distance));
			// }
			for(int i = 0; i < closestToHand.Length; i++)
			{
				if(closestToHand[i].REAL_MIN_DISTANCE < 0.05f)
				{
					// graspContact = true;
				}

				// if(closestToHand[i].REAL_MIN_DISTANCE >= 0.15f)
				// {
				// 	colors[k] = Color.Lerp(Color.white, Color.yellow, (1-closestToHand[i].REAL_MIN_DISTANCE));
				// }

				// if(closestToHand[i].REAL_MIN_DISTANCE < 0.15f)
				// {
				// 	colors[k] = Color.Lerp(Color.yellow, Color.red, (1-closestToHand[i].REAL_MIN_DISTANCE));
				// }

			}
		// thisMesh.colors = colors;
		}
			// Debug.Log(worldVertex + "; D: " + distance);
	}
    

   // THIS DEBUGS DISTANCES ON EACH VERTEX OF THE GAMEOBJECT
  //  public void OnDrawGizmos()
  //   {
  //       // GUIStyle style = new GUIStyle();
  //       // style.normal.textColor = Color.black; 

  //       for(int k = 0; k < thisVertices.Length; k++)
  //       {

		// // for(int k = 150; k < 200; k++)
  // //       {
  //       	worldVertex = transform.TransformPoint(thisVertices[k]);
	 //        closestPointPerVertexRight[k] = handCollidersRight.ClosestPoint(worldVertex);
		// 	// closestPointPerVertexLeft[k] = handCollidersLeft.ClosestPoint(worldVertex);
		// 	// distanceL = Vector3.Distance(closestPointPerVertexLeft[k], worldVertex);
		// 	distanceR = Vector3.Distance(closestPointPerVertexRight[k], worldVertex);
		// 	Gizmos.DrawWireSphere(worldVertex, 0.005f);

		// 	// Debug.DrawRay(closestPointPerVertexRight[k], (worldVertex - closestPointPerVertexRight[k]), Color.white);
		// 	// Debug.DrawRay(closestPointPerVertexLeft[k], (worldVertex - closestPointPerVertexLeft[k]), Color.red);

		// 	// if(distanceL < distanceR)
		// 	// {
		// 	// 	distance = distanceL;
		// 	// 	Vector3 closestPointPerVertex = new Vector3(closestPointPerVertexLeft[k].x, closestPointPerVertexLeft[k].y, closestPointPerVertexLeft[k].z);
  //  //      		Handles.Label(worldVertex, "Distance: " + distance.ToString("F2"), style);
		// 	// }
		// 	// else
		// 	// {
		// 	// 	distance = distanceR;
		// 	// 	Vector3 closestPointPerVertex = new Vector3(closestPointPerVertexRight[k].x, closestPointPerVertexRight[k].y, closestPointPerVertexRight[k].z);
	 //  //       	Handles.Label(worldVertex, "Distance: " + distance.ToString("F2"), style);
		// 	// }
  //   	}
  //   }
}
