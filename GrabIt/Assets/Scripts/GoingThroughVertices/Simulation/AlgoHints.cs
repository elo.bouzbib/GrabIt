using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class AlgoHints : MonoBehaviour
{
  	private GameObject[] childR, childL, handChildL, handChildR; 
  	private GameObject handR, handL;
    [HideInInspector]
    public ClosestToHand[] closest;
    private Vector3[] onGoingVector; 
    [HideInInspector]
    public PredictPosition predictPos;

    private GameObject OOI;
    [HideInInspector]
    public SimulationHand readFromData;
    [HideInInspector]
    public Vector3[] projPhalOnPlanes, errorPerPhal;
    [HideInInspector]
    public Vector3 virtualFinger;
    private Vector3 closestDist;
    private GameObject thumb, palm, index;
    public Vector3 probsA, probsB, debugProbsA, debugProbsB;
    [HideInInspector]
    public Vector3[] probsP, probsQ, probsR;
    [HideInInspector]
    public int nbOfPlanes;
    [HideInInspector]
    public GameObject[] plane;

    void Start()
    {

    	predictPos = FindObjectOfType<PredictPosition>();
    	readFromData = FindObjectOfType<SimulationHand>();
      handR = GameObject.Find("RightHandColl");
    	handL = GameObject.Find("LeftHandColl");
    	childR = new GameObject[19];
    	handChildR = new GameObject[19];
    	childL = new GameObject[19];
    	handChildL = new GameObject[19];
      closest = new ClosestToHand[19];
      onGoingVector = new Vector3[19];

      // planes = GameObject.Find("Planes");
      plane = new GameObject[3];
      nbOfPlanes = GameObject.FindGameObjectWithTag("CutSections").GetComponent<CollisionPlanes>().nbOfPlanes;

      for(int i = 0; i < GameObject.FindGameObjectsWithTag("CutSections").Length; i++)
      {
          plane[i] = GameObject.FindGameObjectsWithTag("CutSections")[i];

      }

      probsB = new Vector3();
      probsA = new Vector3();

      probsP = new Vector3[19];
      probsQ = new Vector3[19];
      probsR = new Vector3[19];
      errorPerPhal = new Vector3[19*nbOfPlanes];

  	// debugProbsB = new Vector3[nbOfPlanes];
  	// debugProbsA = new Vector3[nbOfPlanes];

      debugProbsB = new Vector3();
      debugProbsA = new Vector3();
      // projPhalOnPlanes = new Vector3[19*nbOfPlanes];

      for(int i = 0; i < 19; i++)
      {
      	childR[i] = this.transform.GetChild(i).transform.gameObject;
      	handChildR[i] = handR.transform.GetChild(i).transform.gameObject;
      	childL[i] = this.transform.GetChild(i+19).transform.gameObject;
      	handChildL[i] = handL.transform.GetChild(i).transform.gameObject;
        closest[i] = handChildR[i].GetComponent<ClosestToHand>();
      }

    	OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");

    	palm = GameObject.Find("r_palm_center_marker");
    	index = GameObject.Find("r_index_finger_pad_marker");
    	thumb = GameObject.Find("r_thumb_finger_pad_marker");
    }

    void FixedUpdate()
    {
        // SETUP 20 FPS - 0.05 // 
      Time.fixedDeltaTime = 0.08f;

      OOI = GameObject.FindGameObjectWithTag("ObjectOfInterest");

    	palm = GameObject.Find("r_palm_center_marker");
    	index = GameObject.Find("r_index_finger_pad_marker");
    	thumb = GameObject.Find("r_thumb_finger_pad_marker");
    	// closestFromVF = whosClosest(handChildR[6].transform.position + (handChildR[9].transform.position - handChildR[6].transform.position)/2);

    	// NEED TO CREATE PLANE, from l to this point. GetContactPoints of VFMagnitude
    	// virtualFinger = (handChildR[9].transform.position - handChildR[6].transform.position);

    	virtualFinger = (index.transform.position - thumb.transform.position);
    	// closestFromVF = whosClosest(thumb.transform.position + virtualFinger/2);

    	// Vector3 lengthToPalm = (handChildR[6].transform.position + virtualFinger/2) - handChildR[0].transform.position;
    	
    	Vector3 lengthToPalm = (thumb.transform.position + virtualFinger/2) - palm.transform.position;
    	
        for(int i = 0; i < 19; i++)
        {
            childR[i].transform.position = GameObject.Find("OVRCustomHandPrefab_R").GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position;
            childR[i].transform.eulerAngles = GameObject.Find("OVRCustomHandPrefab_R").GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles;
            
            childL[i].transform.position = GameObject.Find("OVRCustomHandPrefab_L").GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.position;            
            childL[i].transform.eulerAngles = GameObject.Find("OVRCustomHandPrefab_L").GetComponent<OVRCustomSkeleton>().CustomBones[i].transform.eulerAngles;            
        }

        for(int k = 0; k < 19; k++)
        {
            for(int m = 0; m < 19; m++)
            {
                if(m != k)
                {
                    if( (Mathf.Abs(Vector3.Distance(plane[2].GetComponent<CollisionPlanes>().projPerPhal[m], plane[2].GetComponent<CollisionPlanes>().projPerPhalOnOOI[m])) < 0.05f) && (Mathf.Abs(Vector3.Distance(plane[2].GetComponent<CollisionPlanes>().projPerPhal[k], plane[2].GetComponent<CollisionPlanes>().projPerPhalOnOOI[k])) < 0.05f ) )
                    {
                        if( (Mathf.Abs( Vector3.Distance(plane[2].GetComponent<CollisionPlanes>().projPerPhalOnOOI[m], plane[2].GetComponent<CollisionPlanes>().projPerPhalOnOOI[k]) - (plane[2].GetComponent<CollisionPlanes>().projPerPhal[m] - plane[2].GetComponent<CollisionPlanes>().projPerPhal[k]).magnitude) < 0.5f*(plane[2].GetComponent<CollisionPlanes>().projPerPhal[m] - plane[2].GetComponent<CollisionPlanes>().projPerPhal[k]).magnitude ))
                        {
                            probsP[k] = plane[2].GetComponent<CollisionPlanes>().projPerPhalOnOOI[k];
                            errorPerPhal[k] = probsP[k] - childR[k].transform.position;
                        }
                        else
                        {
                            // probsP[k] = Vector3.zero;
                            // errorPerPhal[k] = Vector3.zero;
                        }

                    }
                    if( (Mathf.Abs(Vector3.Distance(plane[1].GetComponent<CollisionPlanes>().projPerPhal[m], plane[1].GetComponent<CollisionPlanes>().projPerPhalOnOOI[m])) < 0.05f) && (Mathf.Abs(Vector3.Distance(plane[1].GetComponent<CollisionPlanes>().projPerPhal[k], plane[1].GetComponent<CollisionPlanes>().projPerPhalOnOOI[k])) < 0.05f ) )
                    {
                        if( (Mathf.Abs( Vector3.Distance(plane[1].GetComponent<CollisionPlanes>().projPerPhalOnOOI[m], plane[1].GetComponent<CollisionPlanes>().projPerPhalOnOOI[k]) - (plane[1].GetComponent<CollisionPlanes>().projPerPhal[m] - plane[1].GetComponent<CollisionPlanes>().projPerPhal[k]).magnitude) < 0.5f*(plane[1].GetComponent<CollisionPlanes>().projPerPhal[m] - plane[1].GetComponent<CollisionPlanes>().projPerPhal[k]).magnitude ))
                        {
                            probsQ[k] = plane[1].GetComponent<CollisionPlanes>().projPerPhalOnOOI[k];
                            errorPerPhal[k*2] = probsP[k] - childR[k].transform.position;
                        }
                        else
                        {
                            // probsP[k] = Vector3.zero;
                            // errorPerPhal[k] = Vector3.zero;
                        }

                    }
                    if( (Mathf.Abs(Vector3.Distance(plane[0].GetComponent<CollisionPlanes>().projPerPhal[m], plane[0].GetComponent<CollisionPlanes>().projPerPhalOnOOI[m])) < 0.05f) && (Mathf.Abs(Vector3.Distance(plane[0].GetComponent<CollisionPlanes>().projPerPhal[k], plane[0].GetComponent<CollisionPlanes>().projPerPhalOnOOI[k])) < 0.05f ) )
                    {
                        if( (Mathf.Abs( Vector3.Distance(plane[0].GetComponent<CollisionPlanes>().projPerPhalOnOOI[m], plane[0].GetComponent<CollisionPlanes>().projPerPhalOnOOI[k]) - (plane[0].GetComponent<CollisionPlanes>().projPerPhal[m] - plane[0].GetComponent<CollisionPlanes>().projPerPhal[k]).magnitude) < 0.5f*(plane[0].GetComponent<CollisionPlanes>().projPerPhal[m] - plane[0].GetComponent<CollisionPlanes>().projPerPhal[k]).magnitude ))
                        {
                            probsR[k] = plane[0].GetComponent<CollisionPlanes>().projPerPhalOnOOI[k];
                            errorPerPhal[k*3] = probsP[k] - childR[k].transform.position;
                        }
                        else
                        {
                            // probsP[k] = Vector3.zero;
                            // errorPerPhal[k] = Vector3.zero;
                        }

                    }
                }
            }
        }

        if( (Mathf.Abs(Vector3.Distance(plane[2].GetComponent<CollisionPlanes>().projPerPhal[5], plane[2].GetComponent<CollisionPlanes>().projPerPhalOnOOI[5])) < 0.05f) && (Mathf.Abs(Vector3.Distance(plane[2].GetComponent<CollisionPlanes>().projPerPhal[8], plane[2].GetComponent<CollisionPlanes>().projPerPhalOnOOI[8])) < 0.05f) )
        {
            debugProbsA = plane[2].GetComponent<CollisionPlanes>().projPerPhal[5];
            debugProbsB = plane[2].GetComponent<CollisionPlanes>().projPerPhal[8];
            if((Mathf.Abs(Vector3.Distance(plane[2].GetComponent<CollisionPlanes>().projPerPhalOnOOI[5], plane[2].GetComponent<CollisionPlanes>().projPerPhalOnOOI[8]) - virtualFinger.magnitude) < 0.5f*virtualFinger.magnitude) )
            {
                probsA = plane[2].GetComponent<CollisionPlanes>().projPerPhalOnOOI[5];
                probsB = plane[2].GetComponent<CollisionPlanes>().projPerPhalOnOOI[8];
            }
            else
            {
                // probsA = Vector3.zero;
                // probsB = Vector3.zero;
            }
        }
        else
        {
            // debugProbsA = Vector3.zero;
            // debugProbsB = Vector3.zero;
        }
    }
// 		for(int i = 0; i < nbOfPlanes; i++)
// 		{
//             projPhalOnPlanes[(i+1)*k] = planes.transform.GetChild(i).gameObject.GetComponent<Collider>().ClosestPoint(planes.transform.GetChild(i).gameObject.GetComponent<Collider>().ClosestPointOnBounds(childR[k].transform.position));

//         	for(int p = i*19; p < (i+1)*19; p++)
//         	{
//         		for(int q = i*19; q < (i+1)*19; q++)
//         		{
//         			if(q != p)
//         			{
//         				// OTHER FINGERS NOW
// 						if( (Mathf.Abs(Vector3.Distance(projPhalOnPlanes[p], whosClosest(projPhalOnPlanes[p]))) < 0.05f) && (Mathf.Abs(Vector3.Distance(projPhalOnPlanes[q], whosClosest(projPhalOnPlanes[q]))) < 0.05f) )
// 						{
// 							// debugProbsMe.Add(projPhalOnPlanes[p]);
// 							// debugProbsMe2.Add(projPhalOnPlanes[q]);
// 							// r = r +1;
// 							if( (Mathf.Abs( Vector3.Distance(whosClosest(projPhalOnPlanes[p]), whosClosest(projPhalOnPlanes[q])) - (projPhalOnPlanes[p] - projPhalOnPlanes[q]).magnitude) < 0.5f*(projPhalOnPlanes[p] - projPhalOnPlanes[q]).magnitude ))
// 							{
// 								// probsMe.Add(whosClosest(projPhalOnPlanes[p]));
// 								// probsMe2.Add(whosClosest(projPhalOnPlanes[q]));
// 								probsP[p] = whosClosest(projPhalOnPlanes[p]);
//                                 probsQ[q] = whosClosest(projPhalOnPlanes[q]);
//                                 // l = l+1;
//                                 errorPerPhal[p] = probsP[p] - childR[p/(i+1)].transform.position;
//                                 errorPerPhal[q] = probsQ[q] - childR[q/(i+1)].transform.position;
// 								// Debug.Log(p + " ; " + GameObject.Find("OVRCustomHandPrefab_R").GetComponent<OVRCustomSkeleton>().CustomBones[p/(i+1)].name + " ; " + q + " ; " + GameObject.Find("OVRCustomHandPrefab_R").GetComponent<OVRCustomSkeleton>().CustomBones[q/(i+1)].name);					
// 								// Debug.DrawRay(projPhalOnPlanes[p], projPhalOnPlanes[q] - projPhalOnPlanes[p], Color.red);
							
// 							}
//         				}
        			
// 					}	
        			
//         		}
//         	}

// // // WORKING / SAMEDI
			
			// if( (Mathf.Abs(Vector3.Distance(projPhalOnPlanes[5], whosClosest(projPhalOnPlanes[5]))) < 0.05f) && (Mathf.Abs(Vector3.Distance(projPhalOnPlanes[8], whosClosest(projPhalOnPlanes[8]))) < 0.05f) )
			// {
			// 	debugProbsA[i] = projPhalOnPlanes[5];
			// 	debugProbsB[i] = projPhalOnPlanes[8];
			// 	if((Mathf.Abs(Vector3.Distance(whosClosest(projPhalOnPlanes[5]), whosClosest(projPhalOnPlanes[8])) - virtualFinger.magnitude) < 0.5f*virtualFinger.magnitude) )
			// 	{
			// 		probsA[i] = whosClosest(projPhalOnPlanes[5]);
			// 		probsB[i] = whosClosest(projPhalOnPlanes[8]);
			// 	}
			// }
// 		}

    


   //  public void OnDrawGizmos()
   //  {

   //  	GUIStyle style = new GUIStyle();
   //      // style.n\ormal.textColor = Color.red; 
   //      // style.fontSize = 1;
   //  	Gizmos.color = Color.green;
   //  	for(int i = 0; i < 19; i++)
   //  	{
	  //   	// Gizmos.DrawSphere(posToPredict[i], 0.01f);

	  //   	errorPredict[i] = posToPredict[i] - handChildR[i].transform.position;
	  //   	if(errorPredict[i].magnitude < 0.02f)
	  //   	{
	  //   		style.normal.textColor = Color.green; 
	  //   	}
	  //   	else
	  //   	{
	  //   		style.normal.textColor = Color.red;
	  //   	}
			// // Handles.Label(closest[i].closest_distance, "D: " + (errorPredict[i].magnitude * 1000).ToString("F1"), style);

   //  	}
   //  	Gizmos.color = Color.cyan;
   //  	Gizmos.DrawSphere(closestFromVF, 0.015f);

   //  	// Debug.Log(Quaternion.LookRotation(virtualFinger.normalized)* Quaternion.FromToRotation(Vector3.forward, Vector3.right));
   //  	// Debug.DrawRay(thumb.transform.position + virtualFinger/2, new Vector3((Quaternion.LookRotation(predictPos.onGoingVector[0].normalized)* Quaternion.FromToRotation(Vector3.right, Vector3.up)).eulerAngles.x, -(Quaternion.LookRotation(predictPos.onGoingVector[0].normalized)* Quaternion.FromToRotation(Vector3.right, Vector3.up)).eulerAngles.y , (Quaternion.LookRotation(predictPos.onGoingVector[0].normalized)* Quaternion.FromToRotation(Vector3.right, Vector3.up)).eulerAngles.z), Color.blue);

   //  	// Debug.DrawRay(thumb.transform.position + virtualFinger/2, new Vector3((Quaternion.LookRotation(virtualFinger.normalized)* Quaternion.FromToRotation(Vector3.forward, Vector3.right)).eulerAngles.x, -(Quaternion.LookRotation(virtualFinger.normalized)* Quaternion.FromToRotation(Vector3.forward, Vector3.right)).eulerAngles.y , (Quaternion.LookRotation(virtualFinger.normalized)* Quaternion.FromToRotation(Vector3.forward, Vector3.right)).eulerAngles.z), Color.red);

   //  	// Debug.DrawRay(thumb.transform.position + virtualFinger/2, new Vector3(90, -(Quaternion.LookRotation(virtualFinger.normalized)* Quaternion.FromToRotation(Vector3.forward, Vector3.right)).eulerAngles.y , (Quaternion.LookRotation(virtualFinger.normalized)* Quaternion.FromToRotation(Vector3.forward, Vector3.right)).eulerAngles.z), Color.green);

   //  	// Debug.DrawRay(closestFromVF, new Vector3((Quaternion.LookRotation(predictPos.onGoingVector[0].normalized)* Quaternion.FromToRotation(Vector3.right, Vector3.up) * Quaternion.FromToRotation(Vector3.forward, Vector3.right)).eulerAngles.x, -(Quaternion.LookRotation(predictPos.onGoingVector[0].normalized)* Quaternion.FromToRotation(Vector3.right, Vector3.up) * Quaternion.FromToRotation(Vector3.forward, Vector3.right)).eulerAngles.y , (Quaternion.LookRotation(predictPos.onGoingVector[0].normalized)* Quaternion.FromToRotation(Vector3.right, Vector3.up) * Quaternion.FromToRotation(Vector3.forward, Vector3.right)).eulerAngles.z), Color.cyan);
   //  	// OFFSET PLAN SELON VECTEUR BLEU
   //  	// Debug.DrawRay(virtualFinger/2 + thumb.transform.position, new Vector3((Quaternion.LookRotation(predictPos.onGoingVector[0].normalized)* Quaternion.FromToRotation(Vector3.forward, Vector3.right)).eulerAngles.x, -(Quaternion.LookRotation(predictPos.onGoingVector[0].normalized)* Quaternion.FromToRotation(Vector3.forward, Vector3.right)).eulerAngles.y , (Quaternion.LookRotation(predictPos.onGoingVector[0].normalized)* Quaternion.FromToRotation(Vector3.forward, Vector3.right)).eulerAngles.z), Color.white);
   //  	// Debug.DrawRay(virtualFinger/2 + thumb.transform.position, new Vector3((Quaternion.LookRotation(predictPos.onGoingVector[0].normalized)).eulerAngles.x, -(Quaternion.LookRotation(predictPos.onGoingVector[0].normalized)).eulerAngles.y , (Quaternion.LookRotation(predictPos.onGoingVector[0].normalized)).eulerAngles.z), Color.red);
   //  	// Debug.DrawRay(virtualFinger/2 + thumb.transform.position, new Vector3((Quaternion.LookRotation(predictPos.onGoingVector[0].normalized)* Quaternion.FromToRotation(Vector3.up, Vector3.forward)).eulerAngles.x, -(Quaternion.LookRotation(predictPos.onGoingVector[0].normalized)* Quaternion.FromToRotation(Vector3.up, Vector3.forward)).eulerAngles.y , (Quaternion.LookRotation(predictPos.onGoingVector[0].normalized)* Quaternion.FromToRotation(Vector3.up, Vector3.forward)).eulerAngles.z), Color.green);

   //  	// Debug.DrawRay(planes.transform.GetChild(0).gameObject.transform.position, planes.transform.GetChild(0).gameObject.transform.right, Color.red);
   //  	// Debug.DrawRay(planes.transform.GetChild(0).gameObject.transform.position, planes.transform.GetChild(0).gameObject.transform.forward, Color.blue);
   //  	// Debug.DrawRay(planes.transform.GetChild(0).gameObject.transform.position, planes.transform.GetChild(0).gameObject.transform.up, Color.green);

   //  }

  //   public void OnDrawGizmos()
  //   {

  //   	Gizmos.DrawSphere(closestFromVF, 0.004f);
  //   	for(int k = 0; k < nbOfPlanes; k++)
  //   	// for(int k = 0; k < 19; k++)
  //   	{
  //   		Gizmos.color = Color.red;
	 //    	Gizmos.DrawSphere(probsA[k], 0.005f);
	 //    	Gizmos.color = Color.green;
	 //    	Gizmos.DrawSphere(probsB[k], 0.005f); 
  //   	// 	Gizmos.color = Color.white;
  //   	// 	Gizmos.DrawSphere(projPhalOnPlanes[k], 0.005f);

  //   	// 	Gizmos.color = Color.blue;
  //   	// 	Gizmos.DrawSphere(whosClosest(projPhalOnPlanes[k]), 0.002f);

  //   	// 	Gizmos.color = Color.yellow;
  //   	// 	Gizmos.DrawSphere(whosClosest(projPhalOnPlanes[5]), 0.002f);
  //   	// 	Gizmos.DrawSphere(whosClosest(projPhalOnPlanes[8]), 0.002f);

	 //   	}
	 //   	// Gizmos.color = Color.red;
  //   	// Gizmos.DrawSphere(probsA[0], 0.005f);
  //   	// Gizmos.color = Color.green;
  //   	// Gizmos.DrawSphere(probsB[0], 0.005f); 

		// Gizmos.color = Color.magenta;
  //   	Gizmos.DrawSphere(debugProbsA[0], 0.003f);
  //   	Gizmos.color = Color.cyan;
  //   	Gizmos.DrawSphere(debugProbsB[0], 0.003f); 
  //   	Gizmos.color = Color.magenta;
  //   	Gizmos.DrawSphere(debugProbsA[1], 0.003f);
  //   	Gizmos.color = Color.cyan;
  //   	Gizmos.DrawSphere(debugProbsB[1], 0.003f); 



		// Gizmos.color = Color.red;
  //   	Gizmos.DrawSphere(probsA[0], 0.005f);
  //   	Gizmos.DrawSphere(probsA[1], 0.005f);

  //   	Gizmos.color = Color.green;
  //   	Gizmos.DrawSphere(probsB[0], 0.005f); 
  //   	Gizmos.DrawSphere(probsB[1], 0.005f); 

  //   	if(l < 10)
  //   	{
  //   		for(int e = 0; e < l; e++)
	 //    	{
	 //    		Gizmos.DrawWireSphere(probsMe[e], 0.003f); 
	 //    	}
  //   	}
  //   	else
  //   	{
  //   		for(int e = (l - 10); e < l; e++)
	 //    	{
	 //    		Gizmos.DrawWireSphere(probsMe[e], 0.003f); 
	 //    	}
  //   	}
    	
  //   }

    // public Vector3 whosClosest(Vector3 fromThis)
    // {
    // 	Vector3[] objVertices = OOI.GetComponent<MeshFilter>().mesh.vertices;
    // 	float REAL_MIN_DISTANCE;

    // 	for(int k = 0; k < objVertices.Length; k++)
    //     {
    //     	Vector3 closestPoint = OOI.GetComponent<Collider>().ClosestPointOnBounds(fromThis);
    //     	closestDist = OOI.GetComponent<Collider>().ClosestPoint(closestPoint);
	   //      REAL_MIN_DISTANCE = Vector3.Distance(closestDist, fromThis);
    // 	}
    // 	return closestDist;

    // }
}
