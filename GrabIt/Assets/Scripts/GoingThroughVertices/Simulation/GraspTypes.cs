﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraspTypes : MonoBehaviour
{
	public GameObject[] leftHand, rightHand;
	public bool powerGrasp, precisionGrasp, intermediateGrasp, bimanual;
	public TextMesh graspTypeText, bimanualText;
	public int nbContactPoints;
	public SimulationObjOfInterest simulation;

    // Start is called before the first frame update
    void Start()
    {
        // Retrieve all hand colliders and their distance to the object.
    	leftHand = new GameObject[19];
    	rightHand = new GameObject[19];
    	for(int i = 0; i < (GameObject.Find("LeftHandColl").GetComponentsInChildren<Transform>().Length - 1); i++)
    	{
    		leftHand[i] = GameObject.Find("LeftHandColl").GetComponentsInChildren<Transform>()[i+1].gameObject;
    		rightHand[i] = GameObject.Find("RightHandColl").GetComponentsInChildren<Transform>()[i+1].gameObject;
    	}
    	graspTypeText = GameObject.Find("GraspType").GetComponent<TextMesh>();
    	graspTypeText.characterSize = 0.02f;
    	bimanualText = GameObject.Find("Bimanual").GetComponent<TextMesh>();
    	bimanualText.characterSize = 0.02f;
    	nbContactPoints = 0;
    	simulation = FindObjectOfType<SimulationObjOfInterest>();
    }

    // Update is called once per frame
    void Update()
    {
    	bimanual = false;
    	powerGrasp = false;
    	precisionGrasp = false;
    	intermediateGrasp = false;
    	graspTypeText.text = "";
    	bimanualText.text = "";

    	for(int i = 0; i < leftHand.Length; i++)
    	{
    		for(int j = 0; j < rightHand.Length; j++)
    		{
    			if((leftHand[i].GetComponent<ClosestToHand>().phalanxContact) && (rightHand[j].GetComponent<ClosestToHand>().phalanxContact))
    			{
    				bimanual = true;
    				bimanualText.text = "Bimanual";
    			}
    		}
    	}
    	for(int i = 0; i < 4; i++)
    	{
    		if((leftHand[i].GetComponent<ClosestToHand>().phalanxContact) || (rightHand[i].GetComponent<ClosestToHand>().phalanxContact))
    		{
    			powerGrasp = true;
    			// Debug.Log("POWER Grasp");
    			graspTypeText.text = "Power";
    			// PALM OR PAD

    				// THEN THUMB AB/ADDUCTED
    		}
    	}
    	for(int i = 4; i < leftHand.Length; i++)
    	{
    		for(int j = 0; j < 4; j++)
    		{
    			if(((leftHand[i].GetComponent<ClosestToHand>().phalanxContact) || (rightHand[i].GetComponent<ClosestToHand>().phalanxContact)) && ((leftHand[j].GetComponent<ClosestToHand>().phalanxContact) || (!rightHand[j].GetComponent<ClosestToHand>().phalanxContact)))
	    		{
	    			precisionGrasp = true;
	    			// Debug.Log("PRECISION Grasp");
	    			graspTypeText.text = "Precision";
	    			// PAD OR SIDE
	    			nbContactPoints = nbContactPoints + 1;

	    				// THEN THUMB AB/ADDUCTED
	    		}
	    		// Debug.Log("Task: " + simulation.taskToDo + " ; NbContactPoints: " + nbContactPoints);
	    		// Debug.Log("NbContactPoints: " + nbContactPoints);

    			// RECORD THIS AND ANALYZE AFTERWARDS FOR PROBS!
    			// TRACK DIRECTION OF THUMB / DIRECTION OF "VIRTUALFINGERS"
    		}
    		nbContactPoints = 0;
    	}
    	

    }
}
