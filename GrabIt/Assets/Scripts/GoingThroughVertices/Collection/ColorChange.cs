using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class ColorChange : MonoBehaviour
{
	public GameObject rightHand, leftHand;
	private float distanceL, distanceR;

	private float[] distancePerCapsL, distancePerCapsR;
	private Vector3[] closestPointPerVertexHandLeft, closestPointPerVertexHandRight;

	public float distance;
	private Mesh thisMesh;
	private Vector3[] thisVertices, closestPointPerVertexRight, closestPointPerVertexLeft;
	private Color[] colors;
	private Vector3 worldVertex;

	public bool graspContact, collision;

	private Collider handCollidersRight, handCollidersLeft;
	private Shader shader;

	public IList<OVRBoneCapsule> Capsules { get; private set; }
	public IList<OVRBoneCapsule> CapsulesR { get; private set; }

	public InstantiateGameObjects gameManager;

    void Start()
    {
    	leftHand = GameObject.Find("OVRCustomHandPrefab_L");
    	rightHand = GameObject.Find("OVRCustomHandPrefab_R");
    	gameManager = GameObject.Find("GameManager").GetComponent<InstantiateGameObjects>();
		shader = Shader.Find("Legacy Shaders/Particles/VertexLit Blended");
		this.GetComponent<MeshRenderer>().material.shader = shader;
		thisMesh = this.GetComponent<MeshFilter>().mesh;
		thisVertices = this.GetComponent<MeshFilter>().mesh.vertices;	
		colors = new Color[thisVertices.Length];
		closestPointPerVertexRight = new Vector3[thisVertices.Length];	
		closestPointPerVertexLeft = new Vector3[thisVertices.Length];
		graspContact = false;


		Capsules = GameObject.Find("OVRCustomHandPrefab_L").GetComponent<OVRCustomSkeleton>().Capsules;
		CapsulesR = GameObject.Find("OVRCustomHandPrefab_R").GetComponent<OVRCustomSkeleton>().Capsules;
    }

    void Update()
    {
    	collision = false;
    	graspContact = false;
    	this.transform.SetParent(GameObject.Find("ObjectsOfInterest").transform);
    	this.GetComponent<Rigidbody>().isKinematic = false;
		this.GetComponent<Rigidbody>().useGravity = false;
		// this.GetComponent<Rigidbody>().useGravity = true;

		
		for(int j = 0; j < leftHand.GetComponentsInChildren<Collider>().Length; j++)
    	{
    		handCollidersRight = rightHand.GetComponentsInChildren<Collider>()[j];
    		handCollidersLeft = leftHand.GetComponentsInChildren<Collider>()[j];
    	}
		if((GameObject.Find("OVRCustomHandPrefab_L") != null) && (GameObject.Find("OVRCustomHandPrefab_R") != null))
		{
			for(int k = 0; k < thisVertices.Length; k++)
        	{
	        	worldVertex = transform.TransformPoint(thisVertices[k]);
		  
		        closestPointPerVertexRight[k] = handCollidersRight.ClosestPoint(worldVertex);
				closestPointPerVertexLeft[k] = handCollidersLeft.ClosestPoint(worldVertex);
				distanceL = Vector3.Distance(closestPointPerVertexLeft[k], worldVertex);
				distanceR = Vector3.Distance(closestPointPerVertexRight[k], worldVertex);

				if(distanceL < distanceR)
				{
					distance = distanceL;

				}
				else
				{
					distance = distanceR;
				}

				if(distance >= 0.15f)
				{
					colors[k] = Color.Lerp(Color.white, Color.yellow, (1-distance));
				}
				else
				{
					colors[k] = Color.Lerp(Color.yellow, Color.red, (1-distance));
				}
				
				
					

				if((distance < 0.02f) && (gameManager.state == 1)) // 0.01
				{
					graspContact = true;
					
					// IMPORTANT: PUT THAT BACK IN FOR OTHER INTERACTIONS!!!!
					this.GetComponent<Rigidbody>().useGravity = false;

					if(distance == distanceR)
					{
						this.transform.SetParent(rightHand.transform);
					}
					else
					{
						this.transform.SetParent(leftHand.transform);
					}
				}

			}
		}
        
		thisMesh.colors = colors;
    }

// SHOW GIZMOOOOOOOS -> vertex between user's hands and OOI
  //   public void OnDrawGizmos()
  //   {

  //   	for(int j = 0; j < rightHand.GetComponentsInChildren<Collider>().Length; j++)
  //   	{
		// 	handCollidersRight = rightHand.GetComponentsInChildren<Collider>()[j];
		// 	handCollidersLeft = leftHand.GetComponentsInChildren<Collider>()[j];
  //   	}

  //       for(int i = 0; i < thisVertices.Length; i++)
  //       {
  //       	worldVertex = transform.TransformPoint(thisVertices[i]);
	 //        closestPointPerVertexRight[i] = handCollidersRight.ClosestPoint(worldVertex);
		// 	closestPointPerVertexLeft[i] = handCollidersLeft.ClosestPoint(worldVertex);

  //       	// Gizmos.DrawSphere(worldVertex, 0.1f);
	 //        // Gizmos.DrawWireSphere(closestPointPerVertex[i], 0.05f);
		// 	Debug.DrawRay(closestPointPerVertexRight[i], (worldVertex - closestPointPerVertexRight[i]), Color.blue);
		// 	Debug.DrawRay(closestPointPerVertexLeft[i], (worldVertex - closestPointPerVertexLeft[i]), Color.red);

		// }
  //   }

   //  public void OnDrawGizmos()
   //  {
   //      GUIStyle style = new GUIStyle();
   //      style.normal.textColor = Color.black; 

   //      for(int k = 0; k < thisVertices.Length; k++)
   //      {

   //      	worldVertex = transform.TransformPoint(thisVertices[k]);
	  //       closestPointPerVertexRight[k] = handCollidersRight.ClosestPoint(worldVertex);
			// closestPointPerVertexLeft[k] = handCollidersLeft.ClosestPoint(worldVertex);
			// distanceL = Vector3.Distance(closestPointPerVertexLeft[k], worldVertex);
			// distanceR = Vector3.Distance(closestPointPerVertexRight[k], worldVertex);

			// // Debug.DrawRay(closestPointPerVertexRight[k], (worldVertex - closestPointPerVertexRight[k]), Color.white);
			// // Debug.DrawRay(closestPointPerVertexLeft[k], (worldVertex - closestPointPerVertexLeft[k]), Color.red);

			// if(distanceL < distanceR)
			// {
			// 	distance = distanceL;
			// 	Vector3 closestPointPerVertex = new Vector3(closestPointPerVertexLeft[k].x, closestPointPerVertexLeft[k].y, closestPointPerVertexLeft[k].z);
   //      		Handles.Label(worldVertex, "Distance: " + distance.ToString("F2"), style);
			// }
			// else
			// {
			// 	distance = distanceR;
			// 	Vector3 closestPointPerVertex = new Vector3(closestPointPerVertexRight[k].x, closestPointPerVertexRight[k].y, closestPointPerVertexRight[k].z);
	  //       	Handles.Label(worldVertex, "Distance: " + distance.ToString("F2"), style);
			// }
   //  	}
   //  }
}
