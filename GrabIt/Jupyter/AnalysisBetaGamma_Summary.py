import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import shutil
import sys

date = input("Enter User Name or Date:")
gamma = input("Enter Gamma:")
beta = input("Enter Beta:")
#word = input("Enter Word:")
word = ''
#addWord = input("Enter AddWord:")
addword = ''
write = input("Write Summary? (Y/N)")


analysisA = None
analysisA = pd.read_csv('./Analysis/'+ 'G-' + str(gamma) + 'B-' + str(beta) + word +'-'+ date + '-FullAnalysis-' + addword + '.csv', sep = ";", decimal = '.')

boneNames = []
bones = pd.Series()
bones = pd.read_csv('./BoneNames.csv')
bones = bones.transpose()
bones = bones.drop(bones.index[0])
for i in range(0, 19):
    boneNames.append('R-' + bones.iloc[0, i])
for i in range(0, 19):
    boneNames.append('L-' + bones.iloc[0, i])
    

analyseMe = pd.DataFrame(columns = ['Config'])

def progress(count, total, suffix=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', suffix))
    sys.stdout.flush()  # As suggested by Rom Ruben

    
for i in range(0, 135):
    analyseMe.loc[i] = pd.Series({'Config' : i})
    for j in range(0, 19):
        analyseMe['Contact.'+str(j)] = analysisA['Contact.'+str(j)]
        analyseMe['Contact.'+str(j+19)] = analysisA['ContactL.'+str(j)]
        
        for k in range(50, 0, -5):
            name = 'TimeTol0.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['TimeInTol0.'+str(j)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['TimeInTol0.'+str(j)+'.'+str(k)] = 0
                
            name = 'TimeTol0L.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['TimeInTol0.'+str(j+19)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['TimeInTol0.'+str(j+19)+'.'+str(k)] = 0
            
            name = 'TimeTol1.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['TimeInTol1.'+str(j)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['TimeInTol1.'+str(j)+'.'+str(k)] = 0
            
            name = 'TimeTol1L.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['TimeInTol1.'+str(j+19)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['TimeInTol1.'+str(j+19)+'.'+str(k)] = 0
                
            name = 'TimeTol2.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['TimeInTol2.'+str(j)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['TimeInTol2.'+str(j)+'.'+str(k)] = 0
            
            name = 'TimeTol2L.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['TimeInTol2.'+str(j+19)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['TimeInTol2.'+str(j+19)+'.'+str(k)] = 0


            name = 'DistToClos0.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['DistToClos0.'+str(j)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['DistToClos0.'+str(j)+'.'+str(k)] = 0
            
            name = 'DistToClos0L.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['DistToClos0.'+str(j+19)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['DistToClos0.'+str(j+19)+'.'+str(k)] = 0 

            name = 'DistToClos1.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['DistToClos1.'+str(j)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['DistToClos1.'+str(j)+'.'+str(k)] = 0
            
            name = 'DistToClos1L.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['DistToClos1.'+str(j+19)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['DistToClos1.'+str(j+19)+'.'+str(k)] = 0 

            name = 'DistToClos2.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['DistToClos2.'+str(j)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['DistToClos2.'+str(j)+'.'+str(k)] = 0
            
            name = 'DistToClos2L.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['DistToClos2.'+str(j+19)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['DistToClos2.'+str(j+19)+'.'+str(k)] = 0 

analyseMeAllPlanes = pd.DataFrame(columns = ['Config'])
    
# for i in range(0, 135):
#     analyseMeAllPlanes.loc[i] = pd.Series({'Config' : i})
#     for j in range(0, 38):
#         analyseMeAllPlanes['Contact.'+str(j)] = analyseMe['Contact.'+str(j)]
        
#         for k in range(50, 0, -5):
#             name = 'TimeTol.'+str(j)+'.'+str(k)
# # compare with Dist To Closest
#             analyseMeAllPlanes[name] = analyseMe['TimeInTol0.'+str(j)+'.'+str(k)] + analyseMe['TimeInTol1.'+str(j)+'.'+str(k)] + analyseMe['TimeInTol2.'+str(j)+'.'+str(k)]

for i in range(0, 135):
    analyseMeAllPlanes.loc[i] = pd.Series({'Config' : i})
    for j in range(0, 38):
        analyseMeAllPlanes['Contact.'+str(j)] = analyseMe['Contact.'+str(j)]
        
        for k in range(50, 0, -5):
            name = 'TimeTol.'+str(j)+'.'+str(k)
# compare with Dist To Closest

            if(((np.isnan(analyseMe['DistToClos0.'+str(j)+'.'+str(k)].iloc[i]) != True) & (analyseMe['DistToClos0.'+str(j)+'.'+str(k)].iloc[i] != 0)) | ((np.isnan(analyseMe['DistToClos1.'+str(j)+'.'+str(k)].iloc[i]) != True) & (analyseMe['DistToClos1.'+str(j)+'.'+str(k)].iloc[i] != 0)) | ((np.isnan(analyseMe['DistToClos2.'+str(j)+'.'+str(k)].iloc[i]) != True) & (analyseMe['DistToClos2.'+str(j)+'.'+str(k)].iloc[i] != 0))) :
                val0 = analyseMe['DistToClos0.'+str(j)+'.'+str(k)].iloc[i]
                val1 = analyseMe['DistToClos1.'+str(j)+'.'+str(k)].iloc[i]
                val2 = analyseMe['DistToClos2.'+str(j)+'.'+str(k)].iloc[i]
                if((val0 <= val1) & (val0 <= val2)):
                    analyseMeAllPlanes[name] = analyseMe['TimeInTol0.'+str(j)+'.'+str(k)]
                if((val1 <= val2) & (val1 <= val0)):
                    analyseMeAllPlanes[name] = analyseMe['TimeInTol1.'+str(j)+'.'+str(k)]
                if((val2 <= val1) & (val2 <= val0)):
                    analyseMeAllPlanes[name] = analyseMe['TimeInTol2.'+str(j)+'.'+str(k)]
            else:
                analyseMeAllPlanes[name] = analyseMe['TimeInTol0.'+str(j)+'.'+str(k)] + analyseMe['TimeInTol1.'+str(j)+'.'+str(k)] + analyseMe['TimeInTol2.'+str(j)+'.'+str(k)]


meanTimeInTol = pd.DataFrame()

for i in range(0, 135):
    progress(i, 5*135)
    #meanTimeInTol.loc[i] = pd.Series({'Config' : i})
    for j in range(0, 38):
        for k in range(50, 0, -5):
            name = 'TimeTol.'+str(j)+'.'+str(k)
            nbPlanes = 0
            meanTime = 0
            for m in range(0, 3):
                #print(m, j, analyseMe['TimeInTol'+str(m)+'.'+str(j)+'.'+str(k)].loc[i], np.isnan(analyseMe['TimeInTol'+str(m)+'.'+str(j)+'.'+str(k)].loc[i]))
                if(np.isnan(analyseMeAllPlanes['TimeTol.'+str(j)+'.'+str(k)].loc[i]) == False):
                    nbPlanes = nbPlanes + 1
                    meanTime = meanTime + analyseMeAllPlanes['TimeTol.'+str(j)+'.'+str(k)].iloc[i]
                    
            if(nbPlanes != 0):
                meanTimeInTol.loc[i, name] = meanTime/nbPlanes
            else:
                meanTimeInTol.loc[i, name] = 0
                #pass

meanTimePerTol = pd.DataFrame()
meanTimeHand = pd.DataFrame()

nbPlanes = 0
meanTimePhal = 0
meanTimeAllPhal = 0
nbPhal = 0
meanTimeMorePhal = 0

for i in range(0, 135):
    progress(i + 135, 5*135)
    for k in range(50, 0, -5):
        name = 'TimeTol.'+str(k)
        nbPhal = 0
        meanTimePhal = 0
        for j in range(0, 38):
            if(meanTimeInTol['TimeTol.'+str(j)+'.'+str(k)].iloc[i] != 0):
                meanTimePhal = meanTimePhal + meanTimeInTol['TimeTol.'+str(j)+'.'+str(k)].iloc[i]
                nbPhal = nbPhal + 1

                meanTimePerTol.loc[k, 'Phalanx.'+str(j)] = meanTimeInTol['TimeTol.'+str(j)+'.'+str(k)].dropna().mean()   

        if(nbPhal != 0):
            meanTimeHand.loc[i, name] = meanTimePhal/nbPhal

# for j in range(0, 38):
#     for k in range(50, 0, -5):
#         meanTimePerTol.loc[k, 'Phalanx.'+str(j)] = meanTimeInTol['TimeTol.'+str(j)+'.'+str(k)].dropna().mean()
#         #meanTimeInTol[meanTimeInTol['TimeTol.'+str(j)+'.'+str(k)] != 0]['TimeTol.'+str(j)+'.'+str(k)].dropna().mean()         
            
meanTimePerTol
# write = input("Write Mean Times Per Tolerance? (Y/N)")

if(write == str('Y')):
    meanTimePerTol.to_csv('./Analysis/MeanTime/'+ date + '/Planes/Planes-'+date+word+addword+'-G-'+str(gamma)+'-B-'+str(beta)+'.csv', sep = ';')

plt.figure(figsize=(20,10))
for j in range(6, 19, 3):
    plt.plot(meanTimePerTol.index, meanTimePerTol['Phalanx.'+str(j)], label = boneNames[j])
    
    plt.title('Mean Time Prediction Known (s), per Phalanx, as a Function of Tolerance (mm)')
    plt.legend()
    
if(write == str('Y')):
    plt.savefig('./Analysis/MeanTime/'+ date + '/Planes/Planes-FingerTips-'+date+word+addword+'-G-'+str(gamma)+'-B-'+str(beta)+'.pdf')


plt.figure(figsize=(20,10))
for j in range(0, 19):
    plt.plot(meanTimePerTol.index, meanTimePerTol['Phalanx.'+str(j)], label = boneNames[j])
    
    plt.title('Mean Time Prediction Known (s), per Phalanx, as a Function of Tolerance (mm)')
    plt.legend()
    
if(write == str('Y')):
    plt.savefig('./Analysis/MeanTime/'+ date + '/Planes/Planes-AllFingers-'+date+word+addword+'-G-'+str(gamma)+'-B-'+str(beta)+'.pdf')

successRatesPlanes = pd.DataFrame()
successPerTolAllPlanes = pd.DataFrame()
for j in range(0, 38):
    for k in range(50, 0, -5):
        condAll = (np.isnan(analyseMeAllPlanes['TimeTol.'+str(j)+'.'+str(k)]) == False)  & (analyseMeAllPlanes['TimeTol.'+str(j)+'.'+str(k)] != 0)    
        successPerTolAllPlanes.loc[k, 'Phalanx.'+str(j)] = analyseMeAllPlanes[condAll]['TimeTol.'+str(j)+'.'+str(k)].dropna().count() / analyseMe[analyseMe['Contact.'+str(j)] == 'True']['Contact.'+str(j)].count() * 100

successPerTolAllPlanes

# write = input("Write Success Rates per Tolerance? (Y/N)")
if(write == str('Y')):
    successPerTolAllPlanes.to_csv('./Analysis/SuccessRates/'+ date + '/Planes/Planes-'+date+word+addword+'-G-'+str(gamma)+'-B-'+str(beta)+'.csv', sep = ';')

plt.figure(figsize=(20,10))
for j in range(6, 19, 3):
    plt.plot(successPerTolAllPlanes.index, successPerTolAllPlanes['Phalanx.'+str(j)], label = boneNames[j])
    
    plt.title('Success Rate per Phalanx as a Function of Tolerance (mm)')
    plt.legend()
    
if(write == str('Y')):
    plt.savefig('./Analysis/SuccessRates/'+ date + '/Planes/Planes-FingerTips-'+date+word+addword+'-G-'+str(gamma)+'-B-'+str(beta)+'.pdf')

plt.figure(figsize=(20,10))
for j in range(0, 19):
    plt.plot(successPerTolAllPlanes.index, successPerTolAllPlanes['Phalanx.'+str(j)], label = boneNames[j])
    
    plt.title('Success Rate per Phalanx as a Function of Tolerance (mm)')
    plt.legend()
if(write == str('Y')):
    plt.savefig('./Analysis/SuccessRates/'+ date + '/Planes/Planes-AllFingers-'+date+word+addword+'-G-'+str(gamma)+'-B-'+str(beta)+'.pdf')

plt.figure(figsize=(20,10))
for j in range(25, 38, 3):
    plt.plot(successPerTolAllPlanes.index, successPerTolAllPlanes['Phalanx.'+str(j)], label = boneNames[j])
    
    plt.title('Success Rate per Phalanx as a Function of Tolerance (mm)')
    plt.legend()
    
if(write == str('Y')):
    plt.savefig('./Analysis/SuccessRates/'+ date + '/Planes/Planes-FingerTipsL-'+date+word+addword+'-G-'+str(gamma)+'-B-'+str(beta)+'.pdf')

# Regroup By Object types, Gestures, Scales

successRatesPerConfig = pd.DataFrame()
for i in range(0, 135):
    progress(i + 2*135, 5*135)
    for k in range(50, 0, -5):
        contact = 0
        touch = 0
        for j in range(0, 38):
            if(analyseMe['Contact.'+str(j)].iloc[i] == 'True'):
                contact = contact + 1
                if(np.isnan(analyseMeAllPlanes['TimeTol.'+str(j)+'.'+str(k)].iloc[i]) == False):
                    touch = touch + 1
                    #print(i, j, contact, k, touch)
                successRatesPerConfig.loc[i, 'Tolerance.'+str(k)] = touch / contact * 100
                
                
tasks = ['Touch', 'Raise', 'Pull', 'Push', 'Push Down']
objects = ['obj0', 'obj1', 'obj2', 'obj3', 'obj4', 'obj5', 'obj6', 'Cube', 'Cylinder']
scales = ['small', 'medium', 'large']
config = 0
for i in range(0, 135):
    config = i
    progress(i + 3*135, 5*135)
    for k in range(0, len(scales)):
        if((config < len(tasks)*len(objects)*(k+1)) & (config >= len(tasks)*len(objects)*(k))):
            taskToDo = tasks[config%len(tasks)]
            for j in range(0, len(objects)):
                if((config < len(tasks)*(len(objects)*k + j+1)) & (config >= len(tasks)*(len(objects)*k + j))):
                    objToCatch = objects[j]
            objScale = scales[k]
            
    analyseMe.loc[i, 'Task'] = taskToDo
    analyseMe.loc[i, 'Obj'] = objToCatch
    analyseMe.loc[i, 'Scale'] = objScale
    
    meanTimeInTol.loc[i, 'Task'] = taskToDo
    meanTimeInTol.loc[i, 'Obj'] = objToCatch
    meanTimeInTol.loc[i, 'Scale'] = objScale
    
    analyseMeAllPlanes.loc[i, 'Task'] = taskToDo
    analyseMeAllPlanes.loc[i, 'Obj'] = objToCatch
    analyseMeAllPlanes.loc[i, 'Scale'] = objScale
    
    successRatesPerConfig.loc[i, 'Task'] = taskToDo
    successRatesPerConfig.loc[i, 'Obj'] = objToCatch
    successRatesPerConfig.loc[i, 'Scale'] = objScale

# meanTimeHand = pd.DataFrame()

# nbPlanes = 0
# meanTimePhal = 0
# meanTimeAllPhal = 0
# nbPhal = 0
# meanTimeMorePhal = 0


# for i in range(0, 135):
#     for k in range(50, 0, -5):
#         name = 'TimeTol.'+str(k)
#         nbPhal = 0
#         meanTimePhal = 0
#         for j in range(0, 38):
#             if(meanTimeInTol['TimeTol.'+str(j)+'.'+str(k)].iloc[i] != 0):
#                 meanTimePhal = meanTimePhal + meanTimeInTol['TimeTol.'+str(j)+'.'+str(k)].iloc[i]
#                 nbPhal = nbPhal + 1

#         if(nbPhal != 0):
#             meanTimeHand.loc[i, name] = meanTimePhal/nbPhal
        

meanTimePerTask = pd.DataFrame()
meanTimePerObject = pd.DataFrame()
meanTimePerScale = pd.DataFrame()

for i in range(0, len(tasks)):
    condTask = meanTimeInTol['Task'] == tasks[i]
    for j in range(0, 38):
        for k in range(50, 0, -5):
            meanTimePerTask.loc[k, tasks[i]] = meanTimeHand[condTask]['TimeTol.'+str(k)].dropna().mean()
            
for i in range(0, len(objects)):
    condObjects = meanTimeInTol['Obj'] == objects[i]
    for j in range(0, 38):
        for k in range(50, 0, -5):
            meanTimePerObject.loc[k, objects[i]] = meanTimeHand[condObjects]['TimeTol.'+str(k)].dropna().mean()   

for i in range(0, len(scales)):
    condScales = meanTimeInTol['Scale'] == scales[i]
    for j in range(0, 38):
        for k in range(50, 0, -5):
            meanTimePerScale.loc[k, scales[i]] = meanTimeHand[condScales]['TimeTol.'+str(k)].dropna().mean()

# write = input("Write Mean Times Per Obj, Task, Scale? (Y/N)")

if(write == str('Y')):
    meanTimePerObject.to_csv('./Analysis/MeanTime/'+ date + '/Planes/Planes-perObject-'+date+word+addword+'-G-'+str(gamma)+'-B-'+str(beta)+'.csv', sep = ';')
if(write == str('Y')):
    meanTimePerTask.to_csv('./Analysis/MeanTime/'+ date + '/Planes/Planes-perTask-'+date+word+addword+'-G-'+str(gamma)+'-B-'+str(beta)+'.csv', sep = ';')
if(write == str('Y')):
    meanTimePerScale.to_csv('./Analysis/MeanTime/'+ date + '/Planes/Planes-perScale-'+date+word+addword+'-G-'+str(gamma)+'-B-'+str(beta)+'.csv', sep = ';')

successRatesPerObj = pd.DataFrame()
successRatesPerTask = pd.DataFrame()
successRatesPerScale = pd.DataFrame()

for i in range(0, len(tasks)):
    condTask = successRatesPerConfig['Task'] == tasks[i]
    progress(i + 4*135, 5*135)
    for k in range(50, 0, -5):
        successRatesPerTask.loc[k, tasks[i]] = successRatesPerConfig[condTask]['Tolerance.'+str(k)].dropna().mean()

for i in range(0, len(objects)):
    condObj = successRatesPerConfig['Obj'] == objects[i]
    for k in range(50, 0, -5):
        successRatesPerObj.loc[k, objects[i]] = successRatesPerConfig[condObj]['Tolerance.'+str(k)].dropna().mean()
        
for i in range(0, len(scales)):
    condScales = successRatesPerConfig['Scale'] == scales[i]
    for k in range(50, 0, -5):
        successRatesPerScale.loc[k, scales[i]] = successRatesPerConfig[condScales]['Tolerance.'+str(k)].dropna().mean()

if(write == str('Y')):
    successRatesPerObj.to_csv('./Analysis/SuccessRates/'+ date + '/Planes/Planes-perObject-'+date+word+addword+'-G-'+str(gamma)+'-B-'+str(beta)+'.csv', sep = ';')
if(write == str('Y')):
    successRatesPerTask.to_csv('./Analysis/SuccessRates/'+ date + '/Planes/Planes-perTask-'+date+word+addword+'-G-'+str(gamma)+'-B-'+str(beta)+'.csv', sep = ';')
if(write == str('Y')):
    successRatesPerScale.to_csv('./Analysis/SuccessRates/'+ date + '/Planes/Planes-perScale-'+date+word+addword+'-G-'+str(gamma)+'-B-'+str(beta)+'.csv', sep = ';')

if(write == str('Y')):
    shutil.move('./Analysis/'+ 'G-' + str(gamma) + 'B-' + str(beta) + word +'-'+ date + '-FullAnalysis-' + addword + '.csv', './Analysis/Analysed/'+ date + '/Planes/' + 'G-' + str(gamma) + 'B-' + str(beta) + word +'-'+ date + '-FullAnalysis-' + addword + '.csv')
if(write == str('Y')):
    shutil.move('./Analysis/'+ 'G-' + str(gamma) + 'B-' + str(beta) + word +'-'+ date + '-DescribeDf-' + addword + '.csv', './Analysis/Analysed/'+ date + '/Planes/' + 'G-' + str(gamma) + 'B-' + str(beta) + word +'-'+ date + '-DescribeDf-' + addword + '.csv')

progress(100, 100)
print('\n')
