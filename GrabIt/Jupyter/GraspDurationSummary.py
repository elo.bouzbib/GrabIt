import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import shutil
import sys

date = input("Enter User Name or Date:")

list_beta = [1.05]
list_gamma = [0]

word = 'AllTimes-3sB'
addword = ''
write = input("Write Summary? (Y/N)")

read = 0
for read in range(0, len(list_beta)):
    gamma = list_gamma[read]
    beta = list_beta[read]
    print(gamma, beta)
    analysisA = None
    analysisA = pd.read_csv('./Analysis/WriteHere/NoParam-G-' + str(gamma) + 'B-' + str(beta) +'-'+ date + '-FullAnalysis-' + word +'.csv', sep = ";", decimal = '.', low_memory = False)

    boneNames = []
    bones = pd.Series()
    bones = pd.read_csv('./BoneNames.csv')
    bones = bones.transpose()
    bones = bones.drop(bones.index[0])
    for i in range(0, 19):
        boneNames.append('R-' + bones.iloc[0, i])
    for i in range(0, 19):
        boneNames.append('L-' + bones.iloc[0, i])
        

    analyseMe = pd.DataFrame(columns = ['Config'])
    def progress(count, total, suffix=''):
        bar_len = 60
        filled_len = int(round(bar_len * count / float(total)))

        percents = round(100.0 * count / float(total), 1)
        bar = '=' * filled_len + '-' * (bar_len - filled_len)

        sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', suffix))
        sys.stdout.flush()  # As suggested by Rom Ruben

    progress(0, 135)    
    for i in range(0, 135):
        analyseMe.loc[i] = pd.Series({'Config' : i})
        for j in range(0, 19):
            analyseMe['Contact.'+str(j)] = analysisA['Contact.'+str(j)]
            analyseMe['Contact.'+str(j+19)] = analysisA['ContactL.'+str(j)]

            name = 'GraspDuration.'+str(j)
            if(name in analysisA.columns):
                analyseMe['GraspDuration.'+str(j)] = analysisA['GraspDuration.'+str(j)]
            else:
                analyseMe['GraspDuration.'+str(j)] = 0

            name = 'GraspDuration.'+str(j+19)
            if(name in analysisA.columns):
                analyseMe['GraspDuration.'+str(j+19)] = analysisA['GraspDuration.'+str(j+19)]
            else:
                analyseMe['GraspDuration.'+str(j+19)] = 0

            k = 0
            name = 'MaxDistanceInTolTime0.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['MaxDistanceInTolTime.'+str(j)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['MaxDistanceInTolTime.'+str(j)+'.'+str(k)] = 0
                
            name = 'MaxDistanceInTolTimeL0.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['MaxDistanceInTolTime.'+str(j+19)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['MaxDistanceInTolTime.'+str(j+19)+'.'+str(k)] = 0

            name = 'DistToPred.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['DistToPred.'+str(j)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['DistToPred.'+str(j)+'.'+str(k)] = 0

            name = 'DistToPredL.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['DistToPred.'+str(j+19)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['DistToPred.'+str(j+19)+'.'+str(k)] = 0
                
            name = 'MaxDistanceInTolTimeL0.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['MaxDistanceInTolTime.'+str(j+19)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['MaxDistanceInTolTime.'+str(j+19)+'.'+str(k)] = 0

            name = 'DistBoneInTolTime0.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['DistBoneInTolTime.'+str(j)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['DistBoneInTolTime.'+str(j)+'.'+str(k)] = 0
                
            name = 'DistBoneInTolTimeL0.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['DistBoneInTolTime.'+str(j+19)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['DistBoneInTolTime.'+str(j+19)+'.'+str(k)] = 0

            name = 'Speed.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['Speed.'+str(j)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['Speed.'+str(j)+'.'+str(k)] = 0
            name = 'SpeedL.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['Speed.'+str(j+19)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['Speed.'+str(j+19)+'.'+str(k)] = 0

            # for k in range(300, 2050, 50):
            for k in range(100, 3050, 10):
                name = 'MaxDistanceInTolTime0.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['MaxDistanceInTolTime.'+str(j)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['MaxDistanceInTolTime.'+str(j)+'.'+str(k)] = 0
                    
                name = 'MaxDistanceInTolTimeL0.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['MaxDistanceInTolTime.'+str(j+19)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['MaxDistanceInTolTime.'+str(j+19)+'.'+str(k)] = 0

                name = 'DistBoneInTolTime0.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['DistBoneInTolTime.'+str(j)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['DistBoneInTolTime.'+str(j)+'.'+str(k)] = 0
                    
                name = 'DistBoneInTolTimeL0.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['DistBoneInTolTime.'+str(j+19)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['DistBoneInTolTime.'+str(j+19)+'.'+str(k)] = 0

                name = 'DistToPred.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['DistToPred.'+str(j)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['DistToPred.'+str(j)+'.'+str(k)] = 0

                name = 'DistToPredL.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['DistToPred.'+str(j+19)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['DistToPred.'+str(j+19)+'.'+str(k)] = 0

                name = 'Speed.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['Speed.'+str(j)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['Speed.'+str(j)+'.'+str(k)] = 0
                name = 'SpeedL.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['Speed.'+str(j+19)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['Speed.'+str(j+19)+'.'+str(k)] = 0

    meanDistInTol = pd.DataFrame()

    for i in range(0, 135):
        progress(i, 4*135)
        for j in range(0, 38):
            k = 0
            if((analyseMe['Contact.'+str(j)].loc[i] == 'True') | (analyseMe['Contact.'+str(j)].loc[i] == '1.0')):
                nameBoneDist = 'DistBoneInTolTime.'+str(j)+'.'+str(k)
                meanDistInTol.loc[i, nameBoneDist] = analyseMe[nameBoneDist].loc[i]
                nameBonePred = 'DistToPred.'+str(j)+'.'+str(k)
                meanDistInTol.loc[i, nameBonePred] = analyseMe[nameBonePred].loc[i]

                meanDistInTol.loc[i, 'Speed.'+str(j)+'.'+str(k)] = analyseMe['Speed.'+str(j)+'.'+str(k)].loc[i]

                if(analyseMe['GraspDuration.'+str(j)].loc[i] > 0):
                    meanDistInTol.loc[i, 'GraspDuration.'+str(j)] = analyseMe['GraspDuration.'+str(j)].loc[i]
                else:
                    meanDistInTol.loc[i, 'GraspDuration.'+str(j)] = 0

                name = 'DistanceInTolTime.'+str(j)+'.'+str(k)
                if(analyseMe['MaxDistanceInTolTime.'+str(j)+'.'+str(k)].loc[i] > 0):
                    meanDistInTol.loc[i, name] = analyseMe['MaxDistanceInTolTime.'+str(j)+'.'+str(k)].loc[i]
                else:
                    meanDistInTol.loc[i, name] = 0     

            else:
                pass
            
            # for k in range(300, 2050, 50):
            for k in range(100, 3050, 10):

                if((analyseMe['Contact.'+str(j)].loc[i] == 'True') | (analyseMe['Contact.'+str(j)].loc[i] == '1.0')):
                    nameBoneDist = 'DistBoneInTolTime.'+str(j)+'.'+str(k)
                    meanDistInTol.loc[i, nameBoneDist] = analyseMe[nameBoneDist].loc[i]
                    nameBonePred = 'DistToPred.'+str(j)+'.'+str(k)
                    meanDistInTol.loc[i, nameBonePred] = analyseMe[nameBonePred].loc[i]

                    meanDistInTol.loc[i, 'Speed.'+str(j)+'.'+str(k)] = analyseMe['Speed.'+str(j)+'.'+str(k)].loc[i]

                    name = 'DistanceInTolTime.'+str(j)+'.'+str(k)
                    if(analyseMe['MaxDistanceInTolTime.'+str(j)+'.'+str(k)].loc[i] > 0):
                        meanDistInTol.loc[i, name] = analyseMe['MaxDistanceInTolTime.'+str(j)+'.'+str(k)].loc[i]
                    else:
                        meanDistInTol.loc[i, name] = 0         
                    
                else:
                    pass

    meanDistPerTol = pd.DataFrame()
    meanDistHand = pd.DataFrame()

    nbPlanes = 0
    meanDistPhal = 0
    meanDistAllPhal = 0
    nbPhal = 0
    meanDistMorePhal = 0

    for i in range(0, 135):
        progress(i + 135, 4*135)

        k = 0
        name = 'DistanceInTolTime.'+str(k)
        nameBoneDist = 'DistBoneInTolTime.'+str(k)
        nameBonePred = 'DistToPred.'+str(k)
        nameSpeed = 'Speed.'+str(k)
        nameDuration = 'GraspDuration'
        nbPhalDist = 0
        nbBone = 0
        nbPred = 0    
        meanDistPhal = 0
        meanBoneDistPhal = 0
        meanDistPred = 0
        meanSpeed = 0
        nbSpeed = 0
        meanDuration = 0
        nbDuration = 0
        for j in range(0, 38):
            if((analyseMe['Contact.'+str(j)].loc[i] == 'True') | (analyseMe['Contact.'+str(j)].loc[i] == '1.0')):
                if(np.isnan(meanDistInTol['DistanceInTolTime.'+str(j)+'.'+str(k)].loc[i]) == False):
                    meanDistPhal = meanDistPhal + meanDistInTol['DistanceInTolTime.'+str(j)+'.'+str(k)].loc[i]
                    nbPhalDist = nbPhalDist + 1
                else:
                    pass
                if(np.isnan(meanDistInTol['DistBoneInTolTime.'+str(j)+'.'+str(k)].loc[i]) == False):
                    meanBoneDistPhal = meanBoneDistPhal + meanDistInTol['DistBoneInTolTime.'+str(j)+'.'+str(k)].loc[i]
                    nbBone = nbBone + 1
                else:
                    pass
                if(np.isnan(meanDistInTol['DistToPred.'+str(j)+'.'+str(k)].loc[i]) == False):
                    meanDistPred = meanDistPred + meanDistInTol['DistToPred.'+str(j)+'.'+str(k)].loc[i]
                    nbPred = nbPred + 1
                else:
                    pass

                if(np.isnan(meanDistInTol['Speed.'+str(j)+'.'+str(k)].loc[i]) == False):
                    meanSpeed = meanSpeed + meanDistInTol['Speed.'+str(j)+'.'+str(k)].loc[i]
                    nbSpeed = nbSpeed + 1
                else:
                    pass

# GraspDuration
                if(np.isnan(meanDistInTol['GraspDuration.'+str(j)].loc[i]) == False):
                    meanDuration = meanDuration + meanDistInTol['GraspDuration.'+str(j)].loc[i]
                    nbDuration = nbDuration + 1
                else:
                    pass


                if(len(meanDistInTol['DistanceInTolTime.'+str(j)+'.'+str(k)].dropna()) != 0):
                    meanDistPerTol.loc[k, 'Phalanx.'+str(j)] = meanDistInTol['DistanceInTolTime.'+str(j)+'.'+str(k)].dropna().mean()   
                    meanDistPerTol.loc[k, 'BoneDist.'+str(j)] = meanDistInTol['DistBoneInTolTime.'+str(j)+'.'+str(k)].dropna().mean()
                    meanDistPerTol.loc[k, 'DistToPred.'+str(j)] = meanDistInTol['DistToPred.'+str(j)+'.'+str(k)].dropna().mean()
                    meanDistPerTol.loc[k, 'Speed.'+str(j)] = meanDistInTol['Speed.'+str(j)+'.'+str(k)].dropna().mean()
                    meanDistPerTol.loc[k, 'GraspDuration.'+str(j)] = meanDistInTol['GraspDuration.'+str(j)].dropna().mean()
                else:
                    pass
            else:
                pass

        if(nbPhalDist != 0):
            meanDistHand.loc[i, name] = meanDistPhal/nbPhalDist
        else:
            pass
        if(nbBone != 0):
            meanDistHand.loc[i, nameBoneDist] = meanBoneDistPhal/nbBone
        else:
            pass
        if(nbPred != 0):
            meanDistHand.loc[i, nameBonePred] = meanDistPred/nbPred
        else:
            pass
        if(nbSpeed != 0):
            meanDistHand.loc[i, nameSpeed] = meanSpeed/nbSpeed
        else:
            pass
        if(nbDuration != 0):
            meanDistHand.loc[i, nameDuration] = meanDuration/nbDuration
        else:
            pass

        for k in range(100, 3050, 10):        
            name = 'DistanceInTolTime.'+str(k)
            nameBoneDist = 'DistBoneInTolTime.'+str(k)
            nameBonePred = 'DistToPred.'+str(k)
            nameSpeed = 'Speed.'+str(k)
            nbPhalDist = 0
            nbBone = 0
            nbPred = 0
            meanDistPhal = 0
            meanBoneDistPhal = 0
            meanDistPred = 0
            meanSpeed = 0
            nbSpeed = 0
            for j in range(0, 38):
                if((analyseMe['Contact.'+str(j)].loc[i] == 'True') | (analyseMe['Contact.'+str(j)].loc[i] == '1.0')):
                    if(np.isnan(meanDistInTol['DistanceInTolTime.'+str(j)+'.'+str(k)].loc[i]) == False):
                        meanDistPhal = meanDistPhal + meanDistInTol['DistanceInTolTime.'+str(j)+'.'+str(k)].loc[i]
                        nbPhalDist = nbPhalDist + 1
                    else:
                        pass
                    if(np.isnan(meanDistInTol['DistBoneInTolTime.'+str(j)+'.'+str(k)].loc[i]) == False):
                        meanBoneDistPhal = meanBoneDistPhal + meanDistInTol['DistBoneInTolTime.'+str(j)+'.'+str(k)].loc[i]
                        nbBone = nbBone + 1
                    else:
                        pass
                    if(np.isnan(meanDistInTol['DistToPred.'+str(j)+'.'+str(k)].loc[i]) == False):
                        meanDistPred = meanDistPred + meanDistInTol['DistToPred.'+str(j)+'.'+str(k)].loc[i]
                        nbPred = nbPred + 1
                    else:
                        pass

                    if(np.isnan(meanDistInTol['Speed.'+str(j)+'.'+str(k)].loc[i]) == False):
                        meanSpeed = meanSpeed + meanDistInTol['Speed.'+str(j)+'.'+str(k)].loc[i]
                        nbSpeed = nbSpeed + 1
                    else:
                        pass

                    if(len(meanDistInTol['DistanceInTolTime.'+str(j)+'.'+str(k)].dropna()) != 0):
                        meanDistPerTol.loc[k, 'Phalanx.'+str(j)] = meanDistInTol['DistanceInTolTime.'+str(j)+'.'+str(k)].dropna().mean()   
                        meanDistPerTol.loc[k, 'BoneDist.'+str(j)] = meanDistInTol['DistBoneInTolTime.'+str(j)+'.'+str(k)].dropna().mean()
                        meanDistPerTol.loc[k, 'DistToPred.'+str(j)] = meanDistInTol['DistToPred.'+str(j)+'.'+str(k)].dropna().mean()
                        meanDistPerTol.loc[k, 'Speed.'+str(j)] = meanDistInTol['Speed.'+str(j)+'.'+str(k)].dropna().mean()
                    else:
                        pass
                else:
                    pass

            if(nbPhalDist != 0):
                meanDistHand.loc[i, name] = meanDistPhal/nbPhalDist
            else:
                pass
            if(nbBone != 0):
                meanDistHand.loc[i, nameBoneDist] = meanBoneDistPhal/nbBone
            else:
                pass
            if(nbPred != 0):
                meanDistHand.loc[i, nameBonePred] = meanDistPred/nbPred
            else:
                pass
            if(nbSpeed != 0):
                meanDistHand.loc[i, nameSpeed] = meanSpeed/nbSpeed
            else:
                pass

    if(write == str('Y')):
        meanDistPerTol.to_csv('./Analysis/TimeTolerance/'+ date + '/NoParam/NoParam-G-'+str(gamma)+'B-'+str(beta)+'-'+date+word+addword+'.csv', sep = ';')

    # plt.figure(figsize=(20,10))
    # for j in range(6, 19, 3):
    #     plt.plot(meanDistPerTol.index/1000, meanDistPerTol['Phalanx.'+str(j)], label = boneNames[j])
        
    #     plt.title('Tolerance (mm) function of Time Prediction Known (s), per Phalanx')
    #     plt.legend()
        
    # if(write == str('Y')):
    #     plt.savefig('./Analysis/TimeTolerance/'+ date + '/NoParam/NoParam-G-'+str(gamma)+'B-'+str(beta)+'-R-FingerTips-'+date+word+addword+'.pdf')

    # for j in range(25, 38, 3):
    #     plt.plot(meanDistPerTol.index/1000, meanDistPerTol['Phalanx.'+str(j)], label = boneNames[j])
        
    #     plt.title('Tolerance (mm) function of Time Prediction Known (s), per Phalanx')
    #     plt.legend()
        
    # if(write == str('Y')):
    #     plt.savefig('./Analysis/TimeTolerance/'+ date + '/NoParam/NoParam-G-'+str(gamma)+'B-'+str(beta)+'-All-FingerTips-'+date+word+addword+'.pdf')

    # plt.figure(figsize=(20,10))
    # for j in range(6, 19, 3):
    #     plt.plot(meanDistPerTol.index/1000, meanDistPerTol['Phalanx.'+str(j)], label = 'Difference Predict/ContactPos.' + boneNames[j])
    #     plt.plot(meanDistPerTol.index/1000, meanDistPerTol['BoneDist.'+str(j)], label = 'Difference Pos/ContactPos.' + boneNames[j])
        
    #     plt.title('Distance between Prediction and Bones Position to Bone Final Contact Position, function of Time Prediction Known (s), per Phalanx')
    #     plt.legend()
        
    # if(write == str('Y')):
    #     plt.savefig('./Analysis/TimeTolerance/'+ date + '/NoParam/NoParam-G-'+str(gamma)+'B-'+str(beta)+'-R-BoneAndPredict-FingerTips-'+date+word+addword+'.pdf')

    # plt.figure(figsize=(20,10))
    # for j in range(25, 38, 3):
    #     plt.plot(meanDistPerTol.index/1000, meanDistPerTol['Phalanx.'+str(j)], label = 'Difference Predict/ContactPos.' + boneNames[j])
    #     plt.plot(meanDistPerTol.index/1000, meanDistPerTol['BoneDist.'+str(j)], label = 'Difference Pos/ContactPos.' + boneNames[j])
        
    #     plt.title('Distance between Prediction and Bones Position to Bone Final Contact Position, function of Time Prediction Known (s), per Phalanx')
    #     plt.legend()
        
    # if(write == str('Y')):
    #     plt.savefig('./Analysis/TimeTolerance/'+ date + '/NoParam/NoParam-G-'+str(gamma)+'B-'+str(beta)+'-L-BoneAndPredict-FingerTips-'+date+word+addword+'.pdf')


    # plt.figure(figsize=(20,10))
    # for j in range(25, 38, 3):
    #     plt.plot(meanDistPerTol.index/1000, meanDistPerTol['Phalanx.'+str(j)], label = boneNames[j])
        
    #     plt.title('Tolerance (mm) function of Time Prediction Known (s), per Phalanx')
    #     plt.legend()
        
    # if(write == str('Y')):
    #     plt.savefig('./Analysis/TimeTolerance/'+ date + '/NoParam/NoParam-G-'+str(gamma)+'B-'+str(beta)+'-L-FingerTips-'+date+word+addword+'.pdf')


    # plt.figure(figsize=(20,10))
    # for j in range(0, 19):
    #     plt.plot(meanDistPerTol.index/1000, meanDistPerTol['Phalanx.'+str(j)], label = boneNames[j])
        
    #     plt.title('Tolerance (mm) function of Time Prediction Known (s), per Phalanx')
    #     plt.legend()
        
    # if(write == str('Y')):
    #     plt.savefig('./Analysis/TimeTolerance/'+ date + '/NoParam/NoParam-G-'+str(gamma)+'B-'+str(beta)+'-R-AllFingers-'+date+word+addword+'.pdf')
                    
    # plt.figure(figsize=(20,10))
    # for j in range(0, 19):
    #     plt.plot(meanDistPerTol.index/1000, meanDistPerTol['Phalanx.'+str(j+19)], label = boneNames[j+19])
        
    #     plt.title('Tolerance (mm) function of Time Prediction Known (s), per Phalanx')
    #     plt.legend()
        
    if(write == str('Y')):
        plt.savefig('./Analysis/TimeTolerance/'+ date + '/NoParam/NoParam-G-'+str(gamma)+'B-'+str(beta)+'-L-AllFingers-'+date+word+addword+'.pdf')
         
    tasks = ['Touch', 'Raise', 'Pull', 'Push', 'Push Down']
    objects = ['obj0', 'obj1', 'obj2', 'obj3', 'obj4', 'obj5', 'obj6', 'Cube', 'Cylinder']
    scales = ['small', 'medium', 'large']

    config = 0
    for i in range(0, 135):
        progress(i + 3*135, 5*135)
        config = i
        for k in range(0, len(scales)):
            if((config < len(tasks)*len(objects)*(k+1)) & (config >= len(tasks)*len(objects)*(k))):
                taskToDo = tasks[config%len(tasks)]
                for j in range(0, len(objects)):
                    if((config < len(tasks)*(len(objects)*k + j+1)) & (config >= len(tasks)*(len(objects)*k + j))):
                        objToCatch = objects[j]
                objScale = scales[k]
                
        analyseMe.loc[i, 'Task'] = taskToDo
        analyseMe.loc[i, 'Obj'] = objToCatch
        analyseMe.loc[i, 'Scale'] = objScale
        
        meanDistInTol.loc[i, 'Task'] = taskToDo
        meanDistInTol.loc[i, 'Obj'] = objToCatch
        meanDistInTol.loc[i, 'Scale'] = objScale

        meanDistHand.loc[i, 'Task'] = taskToDo
        meanDistHand.loc[i, 'Obj'] = objToCatch
        meanDistHand.loc[i, 'Scale'] = objScale
        

    meanDistPerTask = pd.DataFrame()
    meanDistPerObject = pd.DataFrame()
    meanDistPerScale = pd.DataFrame()

    for i in range(0, len(tasks)):
        condTask = meanDistHand['Task'] == tasks[i]
        for j in range(0, 38):
            meanDistPerTask.loc['0', tasks[i]] = meanDistHand[condTask]['DistanceInTolTime.0'].dropna().mean()
            # for k in range(300, 2050, 50):
            for k in range(100, 3050, 10):
                meanDistPerTask.loc[k, tasks[i]] = meanDistHand[condTask]['DistanceInTolTime.'+str(k)].dropna().mean()
                
    for i in range(0, len(objects)):
        condObjects = meanDistHand['Obj'] == objects[i]
        for j in range(0, 38):
            meanDistPerObject.loc['0', objects[i]] = meanDistHand[condObjects]['DistanceInTolTime.0'].dropna().mean()   
            # for k in range(300, 2050, 50):
            for k in range(100, 3050, 10):            
                meanDistPerObject.loc[k, objects[i]] = meanDistHand[condObjects]['DistanceInTolTime.'+str(k)].dropna().mean()   

    for i in range(0, len(scales)):
        condScales = meanDistHand['Scale'] == scales[i]
        for j in range(0, 38):
            meanDistPerScale.loc['0', scales[i]] = meanDistHand[condScales]['DistanceInTolTime.0'].dropna().mean()
            # for k in range(300, 2050, 50):
            for k in range(100, 3050, 10):
                meanDistPerScale.loc[k, scales[i]] = meanDistHand[condScales]['DistanceInTolTime.'+str(k)].dropna().mean()     

    if(write == str('Y')):
        meanDistPerObject.to_csv('./Analysis/TimeTolerance/'+ date + '/NoParam/NoParam-G-'+str(gamma)+'B-'+str(beta)+'-perObject-'+date+word+addword+'.csv', sep = ';')
    if(write == str('Y')):
        meanDistPerTask.to_csv('./Analysis/TimeTolerance/'+ date + '/NoParam/NoParam-G-'+str(gamma)+'B-'+str(beta)+'-perTask-'+date+word+addword+'.csv', sep = ';')
    if(write == str('Y')):
        meanDistPerScale.to_csv('./Analysis/TimeTolerance/'+ date + '/NoParam/NoParam-G-'+str(gamma)+'B-'+str(beta)+'-perScale-'+date+word+addword+'.csv', sep = ';')
    if(write == str('Y')):
        meanDistHand.to_csv('./Analysis/TimeTolerance/'+ date + '/NoParam/NoParam-G-'+str(gamma)+'B-'+str(beta)+'-AllConfigTypes-'+date+word+addword+'.csv', sep = ';')


    boneDistPerTask = pd.DataFrame()
    boneDistPerObject = pd.DataFrame()
    boneDistPerScale = pd.DataFrame()

    for i in range(0, len(tasks)):
        condTask = meanDistHand['Task'] == tasks[i]
        for j in range(0, 38):
            boneDistPerTask.loc['0', tasks[i]] = meanDistHand[condTask]['DistBoneInTolTime.0'].dropna().mean()
            # for k in range(300, 2050, 50):
            for k in range(100, 3050, 10):
                boneDistPerTask.loc[k, tasks[i]] = meanDistHand[condTask]['DistBoneInTolTime.'+str(k)].dropna().mean()
                
    for i in range(0, len(objects)):
        condObjects = meanDistHand['Obj'] == objects[i]
        for j in range(0, 38):
            boneDistPerObject.loc['0', objects[i]] = meanDistHand[condObjects]['DistBoneInTolTime.0'].dropna().mean()   
            # for k in range(300, 2050, 50):
            for k in range(100, 3050, 10):
                boneDistPerObject.loc[k, objects[i]] = meanDistHand[condObjects]['DistBoneInTolTime.'+str(k)].dropna().mean()   

    for i in range(0, len(scales)):
        condScales = meanDistHand['Scale'] == scales[i]
        for j in range(0, 38):
            boneDistPerScale.loc['0', scales[i]] = meanDistHand[condScales]['DistBoneInTolTime.0'].dropna().mean()
            # for k in range(300, 2050, 50):
            for k in range(100, 3050, 10):            
                boneDistPerScale.loc[k, scales[i]] = meanDistHand[condScales]['DistBoneInTolTime.'+str(k)].dropna().mean()

    if(write == str('Y')):
        boneDistPerObject.to_csv('./Analysis/TimeTolerance/'+ date + '/NoParam/NoParam-G-'+str(gamma)+'B-'+str(beta)+'-perObject-BoneDist-'+date+word+addword+'.csv', sep = ';')
    if(write == str('Y')):
        boneDistPerTask.to_csv('./Analysis/TimeTolerance/'+ date + '/NoParam/NoParam-G-'+str(gamma)+'B-'+str(beta)+'-perTask-BoneDist-'+date+word+addword+'.csv', sep = ';')
    if(write == str('Y')):
        boneDistPerScale.to_csv('./Analysis/TimeTolerance/'+ date + '/NoParam/NoParam-G-'+str(gamma)+'B-'+str(beta)+'-perScale-BoneDist-'+date+word+addword+'.csv', sep = ';')

    progress(100, 100)
    print('\n')

