import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import shutil
import sys

date = input("Enter User Name or Date:")
#whichList = input("First Sim (1), Second (2), or Third (3), (4)?")

word = 'AllTimes-5s'
addword = ''
projectionData = None

list_beta = [1.05]
list_gamma = [0]


def progress(count, total, suffix=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', suffix))
    sys.stdout.flush()  # As suggested by Rom Ruben

# projectionData = pd.read_csv('./Analysis/WriteHere//'+ date + '/G-' + str(gamma) + '-B-' + str(beta) + '-' + word + date + '.csv', sep = ";", decimal = '.')#, low_memory = False)
read = 0
for read in range(0, len(list_beta)):
    gamma = list_gamma[read]
    beta = list_beta[read]
    # print(gamma, beta)
    # projectionData = pd.read_csv('../Assets/Resources/DataSimulation/AlgoHints/NoParam-G-' + str(gamma) + '-B-' + str(beta) + '-' + date + '.csv', sep = ";", decimal = '.')#, low_memory = False)
    projectionData = pd.read_csv('./Analysis/FinalWrite/NoParam-G-' + str(gamma) + '-B-' + str(beta) + '-' + date + '.csv', sep = ";", decimal = '.')#, low_memory = False)

    projectionData = projectionData.dropna()

    projectionData['BoneName.0'] = projectionData['BoneName']
    projectionData['BoneNameL.0'] = projectionData['BoneNameL']

    projectionData['BonePos.0'] = projectionData['BonePos']
    projectionData['BonePosL.0'] = projectionData['BonePosL']

    projectionData['PosPred.0'] = projectionData['PosPred']
    
    projectionData['PosPredL.0'] = projectionData['PosPredL']
    
    projectionData['DistToPred.0'] = projectionData['DistToPred']
    
    projectionData['DistToPredL.0'] = projectionData['DistToPredL']
    
    projectionData['BoneRadius.0'] = projectionData['BoneRadius']
    projectionData['BoneContact.0'] = projectionData['BoneContact']

    projectionData['BoneRadiusL.0'] = projectionData['BoneRadiusL']
    projectionData['BoneContactL.0'] = projectionData['BoneContactL']

    projectionData['ClosestPoint.0'] = projectionData['ClosestPoint']
    projectionData['ClosestPointL.0'] = projectionData['ClosestPointL']

    projectionData['DistToClosest.0'] = projectionData['DistToClosest']
    projectionData['DistToClosestL.0'] = projectionData['DistToClosestL']

    for i in range(0, 135):
        cond1 = projectionData['Config'] == i
        projectionData = projectionData.drop(projectionData[projectionData['Config'] == i].index[0:2])

    for j in range(0,19):
        projectionData.loc[projectionData['BoneContact.'+str(j)] == True, 'Touching.'+str(j)] = projectionData['BonePos.'+str(j)]
        projectionData.loc[projectionData['BoneContactL.'+str(j)] == True, 'TouchingL.'+str(j)] = projectionData['BonePosL.'+str(j)]
        
    predictionsData = None
    predictionsData = pd.DataFrame(columns = ['Beta', 'Gamma'])


    for i in range(0,135):
        indexContact = 0
        indexPrediction = 0
        indexPrediction1 = 0
        indexPrediction2 = 0

        indexStart = []

        indexCond = 0
        
        indexContactL = 0
        indexPredictionL = 0
        indexPrediction1L = 0
        indexPrediction2L = 0

        progress(i, 135)

        if(len(projectionData[projectionData['Config'] == i]) != 0):
            cond1 = (projectionData['Config'] == i)
            predictionsData.loc[i] = pd.Series({'Gamma' : projectionData[cond1]['Gamma'].iloc[0], 'Beta' : projectionData[cond1]['Beta'].iloc[0]})

            for j in range(0, 19):                                                
                
                predictionsData.loc[i, 'Contact.'+str(j)] = len(projectionData[cond1]['Touching.'+str(j)].dropna()) > 3
                predictionsData.loc[i, 'ContactL.'+str(j)] = len(projectionData[cond1]['TouchingL.'+str(j)].dropna()) > 3

                if(len(projectionData[cond1]['Touching.'+str(j)].dropna()) != 0):
                    indexContact = projectionData[cond1]['Touching.'+str(j)].dropna().index[0]
                    #print(j, projectionData[cond1]['Touching.'+str(j)].dropna().index[0])

                    if(np.isnan(projectionData['Touching.'+str(j)][indexContact]) == False):
                        # print(j, abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred.'+str(j)][indexContact]))
                        indexCond = projectionData[cond1]['Time'].index
                        
                        nameBoneDist = 'DistBoneInTolTime0.'+str(j)+'.0'
                        predictionsData.loc[i, nameBoneDist] = 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['BonePos.'+str(j)][indexContact]))

                        #print(i, j, predictionsData[nameBoneDist].iloc[i], projectionData['DistToPred.'+str(j)][indexContact], abs(predictionsData[nameBoneDist].iloc[i] - 1000*projectionData['DistToPred.'+str(j)][indexContact]))

                        nameDist = 'MaxDistanceInTolTime0.'+str(j)+'.0'
                        predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred.'+str(j)][indexContact]) - projectionData['BoneRadius.'+str(j)][indexContact])

                        nameDistPred = 'DistToPred.'+str(j)+'.0'
                        predictionsData.loc[i, nameDistPred] = 1000*projectionData['DistToPred.'+str(j)][indexContact]

                        # for k in range(300, 2050, 50):
                        for k in range(100, 5050, 10):

                            tolerance = k/1000
                            indexStart = []
                            indexPrediction = 0
                            indexPrediction1 = 0
                            indexPrediction2 = 0
                            indexTamere = 0

                            for p in range(0, len(indexCond)):
                                if((projectionData['Time'][indexContact] - projectionData['Time'][indexCond[p]]) > 0):
                                    if(projectionData['Time'][indexContact] - projectionData['Time'][indexCond[p]] <= tolerance):
                                        if((projectionData['Time'][indexContact] - projectionData['Time'][indexCond[p]]) >= tolerance - 0.05):

                                            indexStart.append(indexCond[p])
                                            nameSpeed = 'Speed.'+str(j)+'.0'
                                            if((projectionData['Time'][indexContact] - projectionData['Time'][indexCond[0]]) != 0):
                                                predictionsData.loc[i, nameSpeed] = 100*(projectionData['BonePos.'+str(j)][indexContact] - projectionData['BonePos.'+str(j)][indexCond[0]]) /(projectionData['Time'][indexContact] - projectionData['Time'][indexCond[0]])   
                                            else:
                                                pass
                                            if((projectionData['Time'][indexCond[p]] - projectionData['Time'][indexCond[p-1]]) != 0):
                                                nameSpeed = 'Speed.'+str(j)+'.'+str(k)
                                                predictionsData.loc[i, nameSpeed] = 100*(projectionData['BonePos.'+str(j)][indexCond[p]] - projectionData['BonePos.'+str(j)][indexCond[p-1]]) /(projectionData['Time'][indexCond[p]] - projectionData['Time'][indexCond[p-1]])   
                                            else:
                                                pass

                            indexTamere = indexStart

                            if(len(indexTamere) != 0):
                                indexPrediction = indexTamere[0]
                                # for q in range(1, len(indexTamere)):
                                #     if( abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred.'+str(j)][indexPrediction] ) < abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred.'+str(j)][indexTamere[q]]) ):
                                #         indexPrediction = indexTamere[q]
                                #     else:
                                #         pass
                                for q in range(1, len(indexTamere)):
                                    if( abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred.'+str(j)][indexPrediction] ) < abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred.'+str(j)][indexTamere[q]]) ):
                                        indexPrediction = indexTamere[q]
                                    else:
                                        indexPrediction = indexTamere[q]

                                nameDist = 'MaxDistanceInTolTime0.'+str(j)+'.'+str(k)
                                nameClos = 'DistToClos0.'+str(j)+'.'+str(k)
                                nameBoneDist = 'DistBoneInTolTime0.'+str(j)+'.'+str(k)

                                predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred.'+str(j)][indexPrediction]) - projectionData['BoneRadius.'+str(j)][indexPrediction])
                                predictionsData.loc[i, nameClos] = 1000*(abs(projectionData['ClosestPoint.'+str(j)][indexPrediction] - projectionData['PosPred.'+str(j)][indexPrediction]) - projectionData['BoneRadius.'+str(j)][indexPrediction])
                                predictionsData.loc[i, nameBoneDist] = 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['BonePos.'+str(j)][indexPrediction]))

                                nameDistPred = 'DistToPred.'+str(j)+'.'+str(k)
                                predictionsData.loc[i, nameDistPred] = 1000*projectionData['DistToPred.'+str(j)][indexPrediction]

                                #print(k, j, indexPrediction, abs(projectionData['PosPred.'+str(j)][indexPrediction]- projectionData['BonePos.'+str(j)][indexContact]), 1000*projectionData['DistToPred.'+str(j)][indexPrediction])
                            else:
                                pass
                    else:
                        pass
                else:
                    pass

                if(len(projectionData[cond1]['TouchingL.'+str(j)].dropna()) != 0):
                    indexContactL = projectionData[cond1]['TouchingL.'+str(j)].dropna().index[0]

                    if(np.isnan(projectionData['TouchingL.'+str(j)][indexContactL]) == False):
                        
                        indexCond = projectionData[cond1]['Time'].index

                        nameBoneDist = 'DistBoneInTolTimeL0.'+str(j)+'.0'
                        predictionsData.loc[i, nameBoneDist] = 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['BonePosL.'+str(j)][indexContactL]))

                        nameDist = 'MaxDistanceInTolTimeL0.'+str(j)+'.0'
                        predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL.'+str(j)][indexContactL]) - projectionData['BoneRadius.'+str(j)][indexContactL])
                        
                        nameDistPred = 'DistToPredL.'+str(j)+'.0'
                        predictionsData.loc[i, nameDistPred] = 1000*projectionData['DistToPredL.'+str(j)][indexContactL]
                        
                        # for k in range(300, 2050, 50):
                        for k in range(100, 5050, 10):
                            tolerance = k/1000

                            indexStart = []
                            indexPredictionL = 0
                            indexPrediction1L = 0
                            indexPrediction2L = 0
                            indexTamere = 0
                        
                            for p in range(0, len(indexCond)):
                                if((projectionData['Time'][indexContactL] - projectionData['Time'][indexCond[p]]) > 0):
                                    if(projectionData['Time'][indexContactL] - projectionData['Time'][indexCond[p]] <= tolerance):
                                        if((projectionData['Time'][indexContactL] - projectionData['Time'][indexCond[p]]) >= tolerance - 0.05):

                                            indexStart.append(indexCond[p])
                                            nameSpeed = 'SpeedL.'+str(j)+'.0'
                                            if((projectionData['Time'][indexContactL] - projectionData['Time'][indexCond[0]]) != 0):
                                                predictionsData.loc[i, nameSpeed] = 100*(projectionData['BonePosL.'+str(j)][indexContactL] - projectionData['BonePos.'+str(j)][indexCond[0]]) /(projectionData['Time'][indexContactL] - projectionData['Time'][indexCond[0]])   
                                            else:
                                                pass

                                            if((projectionData['Time'][indexCond[p]] - projectionData['Time'][indexCond[p-1]]) != 0):
                                                nameSpeed = 'SpeedL.'+str(j)+'.'+str(k)
                                                predictionsData.loc[i, nameSpeed] = 100*(projectionData['BonePosL.'+str(j)][indexCond[p]] - projectionData['BonePos.'+str(j)][indexCond[p-1]]) /(projectionData['Time'][indexCond[p]] - projectionData['Time'][indexCond[p-1]]) 
                                            else:
                                                pass

                            indexTamere = indexStart
                            if(len(indexTamere) != 0):    
                                indexPredictionL = indexTamere[0]
                                # for q in range(1, len(indexTamere)):
                                #     if( abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL.'+str(j)][indexPredictionL] ) < abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL.'+str(j)][indexTamere[q]]) ):
                                #         indexPredictionL = indexTamere[q]
                                #     else:
                                #         pass

                                for q in range(1, len(indexTamere)):
                                    if( abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL.'+str(j)][indexPredictionL] ) < abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL.'+str(j)][indexTamere[q]]) ):
                                        indexPredictionL = indexTamere[q]
                                    else:
                                        indexPredictionL = indexTamere[q]

                                nameDist = 'MaxDistanceInTolTimeL0.'+str(j)+'.'+str(k)
                                nameClos = 'DistToClosL0.'+str(j)+'.'+str(k)
                                nameBoneDist = 'DistBoneInTolTimeL0.'+str(j)+'.'+str(k)

                                predictionsData.loc[i, nameBoneDist] = 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['BonePosL.'+str(j)][indexPredictionL]))
                                predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL.'+str(j)][indexPredictionL]) - projectionData['BoneRadiusL.'+str(j)][indexPredictionL])
                                predictionsData.loc[i, nameClos] = 1000*(abs(projectionData['ClosestPointL.'+str(j)][indexPredictionL] - projectionData['PosPredL.'+str(j)][indexPredictionL]) - projectionData['BoneRadiusL.'+str(j)][indexPredictionL])

                                nameDistPred = 'DistToPredL.'+str(j)+'.'+str(k)
                                predictionsData.loc[i, nameDistPred] = 1000*projectionData['DistToPredL.'+str(j)][indexPredictionL]
                                
                            else:
                                pass  
                    else:
                        pass
                else:
                    pass

    predictionsData.to_csv('./Analysis/WriteHere/NoParam-'+addword+'G-'+ str(gamma) + 'B-' + str(beta) + '-' + date + '-FullAnalysis-' + word + '.csv', sep = ';')
    predictionsData.describe().to_csv('./Analysis/WriteHere/NoParam-'+addword+'G-'+ str(gamma) + 'B-' + str(beta) + '-' + date + '-DescribeDf-' + word + '.csv', sep = ';')
    predictionsData

    progress(100, 100)
    print('\n')


read = 0
for read in range(0, len(list_beta)):
    gamma = list_gamma[read]
    beta = list_beta[read]
    # shutil.move('../Assets/Resources/DataSimulation/AlgoHints/NoParam-G-' + str(gamma) + '-B-' + str(beta) + '-' + word + date + '.csv', './Analysis/FinalWrite/NoParam-G-' + str(gamma) + '-B-' + str(beta) + '-' + word + date + '.csv')

