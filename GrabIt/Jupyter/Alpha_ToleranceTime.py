import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import shutil
import sys

date = input("Enter User Name or Date:")
# alpha = input("Enter Alpha:")
whichList = input("First Sim (1) or Second (2)?")
word = ''
addword = ''
projectionData = None

if(int(whichList) == 1):
    list_alpha = [0, 0.25, 0.5, 0.75, 1]
if(int(whichList) == 2):
    list_alpha = [0.05, 0.1, 0.15, 0.2, 0.3]
# projectionData = pd.read_csv('./Analysis/SimulationData/' + date + '/A-' + str(alpha) + '-' + word + date + '.csv', sep = ";", decimal = '.')

read = 0
for read in range(0, len(list_alpha)):
    alpha = list_alpha[read]
    print(alpha)
    projectionData = pd.read_csv('../Assets/Resources/DataSimulation/AlgoHints/A-' + str(alpha) + '-' + word + date + '.csv', sep = ";", decimal = '.')


    projectionData = projectionData.dropna()

    projectionData['BoneName.0'] = projectionData['BoneName']
    projectionData['BoneNameL.0'] = projectionData['BoneNameL']

    projectionData['BonePos.0'] = projectionData['BonePos']
    projectionData['BonePosL.0'] = projectionData['BonePosL']

    projectionData['PosPred.0'] = projectionData['PosPred']

    projectionData['PosPredL.0'] = projectionData['PosPredL']

    projectionData['DistToPred.0'] = projectionData['DistToPred']

    projectionData['DistToPred0L.0'] = projectionData['DistToPredL']

    projectionData['BoneRadius.0'] = projectionData['BoneRadius']
    projectionData['BoneContact.0'] = projectionData['BoneContact']

    projectionData['BoneRadiusL.0'] = projectionData['BoneRadiusL']
    projectionData['BoneContactL.0'] = projectionData['BoneContactL']

    projectionData['ClosestPoint.0'] = projectionData['ClosestPoint']
    projectionData['ClosestPointL.0'] = projectionData['ClosestPointL']


    for i in range(0, 135):
        cond1 = projectionData['Config'] == i
        projectionData = projectionData.drop(projectionData[projectionData['Config'] == i].index[0:2])

    for j in range(0,19):
        projectionData.loc[projectionData['BoneContact.'+str(j)] == True, 'Touching.'+str(j)] = projectionData['BonePos.'+str(j)]
        projectionData.loc[projectionData['BoneContactL.'+str(j)] == True, 'TouchingL.'+str(j)] = projectionData['BonePosL.'+str(j)]

        
    predictionsData = None
    predictionsData = pd.DataFrame(columns = ['Alpha'])

    def progress(count, total, suffix=''):
        bar_len = 60
        filled_len = int(round(bar_len * count / float(total)))

        percents = round(100.0 * count / float(total), 1)
        bar = '=' * filled_len + '-' * (bar_len - filled_len)

        sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', suffix))
        sys.stdout.flush()  # As suggested by Rom Ruben


    for i in range(0,135):
        indexContact = 0
        indexPrediction = 0
        indexStart = []
        indexTamere = 0

        indexCond = 0

        indexContactL = 0
        indexPredictionL = 0

        progress(i, 135)

        if(len(projectionData[projectionData['Config'] == i]) != 0):
            cond1 = (projectionData['Config'] == i)
            predictionsData.loc[i] = pd.Series({'Alpha' : projectionData[cond1]['Alpha'].iloc[0]})

            for j in range(0, 19):                                                
                predictionsData.loc[i, 'Contact.'+str(j)] = len(projectionData[cond1]['Touching.'+str(j)].dropna()) != 0
                predictionsData.loc[i, 'ContactL.'+str(j)] = len(projectionData[cond1]['TouchingL.'+str(j)].dropna()) != 0

                if(len(projectionData[cond1]['Touching.'+str(j)].dropna()) != 0):
                    indexContact = projectionData[cond1]['Touching.'+str(j)].dropna().index[0]
                    # print('Phalanx ', str(j), 'Time ', "{:.3f}".format(projectionData['Time'][indexContact]))


                    if(np.isnan(projectionData['Touching.'+str(j)][indexContact]) == False):
                        
                        indexCond = projectionData[cond1]['Time'].index
                        
                        nameDist = 'MaxDistanceInTolTime0.'+str(j)+'.0'
                        predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred.'+str(j)][indexContact]) - projectionData['BoneRadius.'+str(j)][indexContact])
                        nameClos = 'DistToClosO.'+str(j)+'.0'
                        predictionsData.loc[i, nameClos] = 1000*(abs(projectionData['ClosestPoint.'+str(j)][indexContact] - projectionData['PosPred.'+str(j)][indexContact]) - projectionData['BoneRadius.'+str(j)][indexContact])
                        nameBoneDist = 'DistBoneInTolTime0.'+str(j)+'.0'
                        predictionsData.loc[i, nameBoneDist] = 0

                        for k in range(300, 2050, 50):
                            tolerance = k/1000
                            indexStart = []
                            indexPrediction = 0
                            indexTamere = 0

                            for p in range(0, len(indexCond)):
                                if((projectionData['Time'][indexContact] - projectionData['Time'][indexCond[p]]) > 0):
                                    if(projectionData['Time'][indexContact] - projectionData['Time'][indexCond[p]] <= tolerance):
                                        indexStart.append(indexCond[p])

                            indexTamere = indexStart
                            if(len(indexTamere) != 0):
                                indexPrediction = indexTamere[0]
                                for q in range(1, len(indexTamere)):
                                    if( abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred.'+str(j)][indexPrediction] ) < abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred.'+str(j)][indexTamere[q]]) ):
                                        indexPrediction = indexTamere[q]
                                    else:
                                        pass

                                nameDist = 'MaxDistanceInTolTime0.'+str(j)+'.'+str(k)
                                nameClos = 'DistToClosO.'+str(j)+'.'+str(k)
                                nameBoneDist = 'DistBoneInTolTime0.'+str(j)+'.'+str(k)

                                predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred.'+str(j)][indexPrediction]) - projectionData['BoneRadius.'+str(j)][indexPrediction])
                                predictionsData.loc[i, nameBoneDist] = 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['BonePos.'+str(j)][indexPrediction]))
                                predictionsData.loc[i, nameClos] = 1000*(abs(projectionData['ClosestPoint.'+str(j)][indexPrediction] - projectionData['PosPred.'+str(j)][indexPrediction]) - projectionData['BoneRadius.'+str(j)][indexPrediction])
                            else:
                                pass
                            
                    else:
                        pass
                else:
                    pass                                

                if(len(projectionData[cond1]['TouchingL.'+str(j)].dropna()) != 0):
                    indexContactL = projectionData[cond1]['TouchingL.'+str(j)].dropna().index[0]

                    if(np.isnan(projectionData['TouchingL.'+str(j)][indexContactL]) == False):
                        
                        nameDist = 'MaxDistanceInTolTimeL0.'+str(j)+'.0'
                        predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL.'+str(j)][indexContactL]) - projectionData['BoneRadiusL.'+str(j)][indexContactL])
                        nameClos = 'DistToClosLO.'+str(j)+'.0'
                        predictionsData.loc[i, nameClos] = 1000*(abs(projectionData['ClosestPointL.'+str(j)][indexContactL] - projectionData['PosPredL.'+str(j)][indexContactL]) - projectionData['BoneRadiusL.'+str(j)][indexContactL])
                        nameBoneDist = 'DistBoneInTolTimeL0.'+str(j)+'.0'
                        predictionsData.loc[i, nameBoneDist] = 0


                        indexCond = projectionData[cond1]['Time'].index
                        
                        for k in range(300, 2050, 50):
                            tolerance = k/1000
                            indexStart = []
                            indexPredictionL = 0
                            indexTamere = 0

                            for p in range(0, len(indexCond)):
                                if((projectionData['Time'][indexContactL] - projectionData['Time'][indexCond[p]]) > 0):
                                    if(projectionData['Time'][indexContactL] - projectionData['Time'][indexCond[p]] <= tolerance):
                                        indexStart.append(indexCond[p])
                            
                            indexTamere = indexStart
                            if(len(indexTamere) != 0):
                                indexPredictionL = indexTamere[0]
                                for q in range(1, len(indexTamere)):
                                    if( abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL.'+str(j)][indexPredictionL] ) < abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL.'+str(j)][indexTamere[q]]) ):
                                        indexPredictionL = indexTamere[q]
                                    else:
                                        pass

                                nameDist = 'MaxDistanceInTolTimeL0.'+str(j)+'.'+str(k)
                                nameClos = 'DistToClosLO.'+str(j)+'.'+str(k)
                                nameBoneDist = 'DistBoneInTolTimeL0.'+str(j)+'.'+str(k)

                                predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL.'+str(j)][indexPredictionL]) - projectionData['BoneRadiusL.'+str(j)][indexPredictionL])
                                predictionsData.loc[i, nameBoneDist] = 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['BonePosL.'+str(j)][indexPredictionL]))
                                predictionsData.loc[i, nameClos] = 1000*(abs(projectionData['ClosestPointL.'+str(j)][indexPredictionL] - projectionData['PosPredL.'+str(j)][indexPredictionL]) - projectionData['BoneRadiusL.'+str(j)][indexPredictionL])
                            else:
                                pass
                    else:
                        pass
                else:
                    pass


    predictionsData.to_csv('./Analysis/TimeTolerance/A-'+ str(alpha) + '-' + date + '-FullAnalysis-' + word + '.csv', sep = ';')
    predictionsData.describe().to_csv('./Analysis/TimeTolerance/A-'+ str(alpha) + '-' + date + '-DescribeDf-' + word + '.csv', sep = ';')
    predictionsData

    # shutil.move('./Analysis/SimulationData/' + date + '/A-' + str(alpha) + '-' + word + date + '.csv', './Analysis/SimulationData/'+ date + '/TimeTolerance/A-' + str(alpha) + '-' + word + date + '.csv')

    progress(100, 100)
    print('\n')

read = 0
for read in range(0, len(list_alpha)):
    alpha = list_alpha[read]
    shutil.move('../Assets/Resources/DataSimulation/AlgoHints/A-' + str(alpha) + '-' + word + date + '.csv', './Analysis/SimulationData/'+ date + '/TimeTolerance/A-' + str(alpha) + '-' + word + date + '.csv')

