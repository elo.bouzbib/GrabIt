import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# date = 'User1'
# word = 'graspContact'
# addword = 'test'
date = input("Enter User Name or Date:")
gamma = input("Enter Gamma:")
beta = input("Enter Beta:")
#word = input("Enter Word:")
word = ''
#addWord = input("Enter AddWord:")
addword = ''
projectionData = None

# stopPredict = input("Stop Predictions in Name? (Y/N)")
# if(stopPredict == str('Y')):
#     projectionData = pd.read_csv('../Assets/Resources/DataSimulation/AlgoHints/G-' + str(gamma) + '-B-' + str(beta) + '-' + word + date + '-stopPredictions.csv', sep = ";", decimal = '.')
# else:
projectionData = pd.read_csv('../Assets/Resources/DataSimulation/AlgoHints/G-' + str(gamma) + '-B-' + str(beta) + '-' + word + date + '.csv', sep = ";", decimal = '.')

projectionData = projectionData.dropna()


projectionData['BoneName.0'] = projectionData['BoneName']
projectionData['BoneNameL.0'] = projectionData['BoneNameL']

projectionData['BonePos.0'] = projectionData['BonePos']
projectionData['BonePosL.0'] = projectionData['BonePosL']

projectionData['PosPred0.0'] = projectionData['PosPred0']
projectionData['PosPred1.0'] = projectionData['PosPred1']
projectionData['PosPred2.0'] = projectionData['PosPred2']

projectionData['PosPredL0.0'] = projectionData['PosPredL0']
projectionData['PosPredL1.0'] = projectionData['PosPredL1']
projectionData['PosPredL2.0'] = projectionData['PosPredL2']

projectionData['DistToPred0.0'] = projectionData['DistToPred0']
projectionData['DistToPred1.0'] = projectionData['DistToPred1']
projectionData['DistToPred2.0'] = projectionData['DistToPred2']

projectionData['DistToPred0L.0'] = projectionData['DistToPred0L']
projectionData['DistToPred1L.0'] = projectionData['DistToPred1L']
projectionData['DistToPred2L.0'] = projectionData['DistToPred2L']

projectionData['BoneRadius.0'] = projectionData['BoneRadius']
projectionData['BoneContact.0'] = projectionData['BoneContact']

projectionData['BoneRadiusL.0'] = projectionData['BoneRadiusL']
projectionData['BoneContactL.0'] = projectionData['BoneContactL']

projectionData['ClosestPoint.0'] = projectionData['ClosestPoint']
projectionData['ClosestPointL.0'] = projectionData['ClosestPointL']

projectionData['DistToClosest.0'] = projectionData['DistToClosest']
projectionData['DistToClosestL.0'] = projectionData['DistToClosestL']

for i in range(0, 135):
    cond1 = projectionData['Config'] == i
    projectionData = projectionData.drop(projectionData[projectionData['Config'] == i].index[0:2])

for j in range(0,19):
    projectionData.loc[projectionData['BoneContact.'+str(j)] == True, 'Touching.'+str(j)] = projectionData['BonePos.'+str(j)]
    projectionData.loc[projectionData['BoneContactL.'+str(j)] == True, 'TouchingL.'+str(j)] = projectionData['BonePosL.'+str(j)]


list_columns = []
list_columns.append('Beta')
list_columns.append('Gamma')
for j in range(0, 19):    
    list_columns.append('Contact.'+str(j))
    for k in range(50, 0, -5):
        list_columns.append('DiffInTol0.'+str(j)+'.'+str(k))
        list_columns.append('TimeInTol0.'+str(j)+'.'+str(k))
        list_columns.append('DiffInTol1.'+str(j)+'.'+str(k))
        list_columns.append('TimeInTol1.'+str(j)+'.'+str(k))
        list_columns.append('DiffInTol2.'+str(j)+'.'+str(k))
        list_columns.append('TimeInTol2.'+str(j)+'.'+str(k))
for j in range(0, 19):    
    list_columns.append('ContactL.'+str(j))
    for k in range(50, 0, -5):
        list_columns.append('DiffInTol0L.'+str(j)+'.'+str(k))
        list_columns.append('TimeInTol0L.'+str(j)+'.'+str(k))
        list_columns.append('DiffInTol1L.'+str(j)+'.'+str(k))
        list_columns.append('TimeInTol1L.'+str(j)+'.'+str(k))
        list_columns.append('DiffInTol2L.'+str(j)+'.'+str(k))
        list_columns.append('TimeInTol2L.'+str(j)+'.'+str(k))
    
predictionsData = None
# predictionsData = pd.DataFrame(columns = list_columns)
predictionsData = pd.DataFrame(columns = ['Beta', 'Gamma'])

condGamma = (projectionData['Gamma'] == 0.05)
condBeta = (projectionData['Beta'] == 2.5)

for i in range(0,135):
    indexContact = 0
    indexPrediction = 0
    indexPrediction1 = 0
    indexPrediction2 = 0
    indexStart = 0
    indexStart1 = 0
    indexStart2 = 0
    indexTamere = 0
    indexTamere1 = 0
    indexTamere2 = 0

    indexCond = 0
    
    indexContactL = 0
    indexPredictionL = 0
    indexPrediction1L = 0
    indexPrediction2L = 0
    indexStartL = 0
    indexStart1L = 0
    indexStart2L = 0
    indexTamereL = 0
    indexTamere1L = 0
    indexTamere2L = 0

    if(len(projectionData[projectionData['Config'] == i]) != 0):
        cond1 = (projectionData['Config'] == i)
        predictionsData.loc[i] = pd.Series({'Gamma' : projectionData[cond1]['Gamma'].iloc[0], 'Beta' : projectionData[cond1]['Beta'].iloc[0]})

        for j in range(0, 19):                                                
            # predictionsData['Contact.'+str(j)].loc[i] = len(projectionData[cond1]['Touching.'+str(j)].dropna()) != 0
            # predictionsData['ContactL.'+str(j)].loc[i] = len(projectionData[cond1]['TouchingL.'+str(j)].dropna()) != 0
            
            predictionsData.loc[i, 'Contact.'+str(j)] = len(projectionData[cond1]['Touching.'+str(j)].dropna()) != 0
            predictionsData.loc[i, 'ContactL.'+str(j)] = len(projectionData[cond1]['TouchingL.'+str(j)].dropna()) != 0

            if(len(projectionData[cond1]['Touching.'+str(j)].dropna()) != 0):
                indexContact = projectionData[cond1]['Touching.'+str(j)].dropna().index[0]
                print('Phalanx ', str(j), 'Time ', "{:.3f}".format(projectionData['Time'][indexContact]))
                
                if(np.isnan(projectionData['Touching.'+str(j)][indexContact]) == False):
                    print('PosAtContact:' , "{:.3f}".format(projectionData['Touching.'+str(j)][indexContact]),
                          'PosPRED0-AtContact:' , "{:.3f}".format(projectionData['PosPred0.'+str(j)][indexContact]), 
                          'PosPRED1-AtContact:' , "{:.3f}".format(projectionData['PosPred1.'+str(j)][indexContact]),
                          'PosPRED2-AtContact:' , "{:.3f}".format(projectionData['PosPred2.'+str(j)][indexContact]))

                    # print('Diff? (mm)',
                    #       'Plan0: ', "{:.3f}".format(1000*(abs(projectionData['PosPred0.'+str(j)][indexContact] - projectionData['BonePos.'+str(j)][indexContact]) - projectionData['BoneRadius.'+str(j)][indexContact])),
                    #       'Plan1: ', "{:.3f}".format(1000*(abs(projectionData['PosPred1.'+str(j)][indexContact] - projectionData['BonePos.'+str(j)][indexContact]) - projectionData['BoneRadius.'+str(j)][indexContact])), 
                    #       'Plan2: ', "{:.3f}".format(1000*(abs(projectionData['PosPred2.'+str(j)][indexContact] - projectionData['BonePos.'+str(j)][indexContact]) - projectionData['BoneRadius.'+str(j)][indexContact])),
                    #      # 'Plan0Clos: ', "{:.3f}".format(1000*(abs(projectionData['PosPred0.'+str(j)][indexContact] - projectionData['ClosestPoint.'+str(j)][indexContact]) - projectionData['BoneRadius.'+str(j)][indexContact])),
                    #      # 'Plan1Clos: ', "{:.3f}".format(1000*(abs(projectionData['PosPred1.'+str(j)][indexContact] - projectionData['ClosestPoint.'+str(j)][indexContact]) - projectionData['BoneRadius.'+str(j)][indexContact])),
                    #      # 'Plan2Clos: ', "{:.3f}".format(1000*(abs(projectionData['PosPred2.'+str(j)][indexContact] - projectionData['ClosestPoint.'+str(j)][indexContact]) - projectionData['BoneRadius.'+str(j)][indexContact]))
                    #      )
                    
                    plt.figure(figsize=(20,10))

                    indexCond = projectionData[cond1]['Time'].index

                    for k in range(50, 0, -5):
                        tolerance = k
                        projectionData.loc[1000*(abs(projectionData['PosPred0.'+str(j)][indexCond] - projectionData['BonePos.'+str(j)][indexContact]) - projectionData['BoneRadius.'+str(j)]) <= tolerance, 'InGoodTol0.'+str(j)+'.'+str(k)] = True
                        projectionData.loc[1000*(abs(projectionData['PosPred1.'+str(j)][indexCond] - projectionData['BonePos.'+str(j)][indexContact]) - projectionData['BoneRadius.'+str(j)]) <= tolerance, 'InGoodTol1.'+str(j)+'.'+str(k)] = True
                        projectionData.loc[1000*(abs(projectionData['PosPred2.'+str(j)][indexCond] - projectionData['BonePos.'+str(j)][indexContact]) - projectionData['BoneRadius.'+str(j)]) <= tolerance, 'InGoodTol2.'+str(j)+'.'+str(k)] = True

                        #print(1000*(abs(projectionData['PosPred2.'+str(j)][indexCond] - projectionData['BonePos.'+str(j)][indexContact]) - projectionData['BoneRadius.'+str(j)][indexCond]),
                        #      1000*(abs(projectionData['PosPred2.'+str(j)][indexCond] - projectionData['BonePos.'+str(j)][indexContact]) - projectionData['BoneRadius.'+str(j)][indexCond]) <= tolerance, projectionData['InGoodTol2.'+str(j)+'.'+str(k)][indexCond])

                        
                        #print(projectionData[cond1]['InGoodTol0.'+str(j)+'.'+str(k)].dropna().index)
                        if(len(projectionData['InGoodTol0.'+str(j)+'.'+str(k)][indexCond].dropna()) != 0):
                            indexTamere = projectionData['InGoodTol0.'+str(j)+'.'+str(k)][indexCond].dropna().index
                            if(len(indexTamere) != 0):
                                if(indexContact in indexTamere):
                                    for q in range(0, len(indexTamere)):
                                        if(indexTamere[q] == indexContact):
    #                                        print('Success!')
                                            indexStart = indexTamere[q]
                                            for p in range(0, len(indexTamere)):
                                                if(indexTamere[q-p] == indexContact-p):
                                                    indexPrediction = indexTamere[q-p]

                                    if( (projectionData['Time'][indexContact] - projectionData['Time'][indexPrediction]) > 0 ):
                                        print('Phalanx ', str(j), 'Tolerance ', k, 'IndexContact ', indexContact, 'IndexPrediction', indexPrediction , 'TimeContact', "{:.2f}".format(projectionData['Time'][indexContact]), 'TimeInTolerance', "{:.2f}".format(projectionData['Time'][indexPrediction]))
                                        print('TimeKnown! ', "{:.2f}".format(projectionData['Time'][indexContact] - projectionData['Time'][indexPrediction]),
                                              'PosDifference0 ', "{:.3f}".format(1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred0.'+str(j)][indexPrediction]) - projectionData['BoneRadius.'+str(j)][indexPrediction])),
                                             'PosClosestPred0 ', "{:.3f}".format(1000*(abs(projectionData['ClosestPoint.'+str(j)][indexPrediction] - projectionData['PosPred0.'+str(j)][indexPrediction]) - projectionData['BoneRadius.'+str(j)][indexPrediction])))
                                        plt.vlines(projectionData['Time'][indexPrediction], 0, k, linestyles = 'dashed', colors = 'red', label = 'Prediction0, Tol.'+str(k))
                                        
                                        nameDist = 'DistInTol0.'+str(j)+'.'+str(k)
                                        nameTime = 'TimeTol0.'+str(j)+'.'+str(k)
                                        nameClos = 'DistToClosO.'+str(j)+'.'+str(k)
                                        
                                        # predictionsData.loc[i] = pd.Series({ nameDist : 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred0.'+str(j)][indexPrediction]) - projectionData['BoneRadius.'+str(j)][indexPrediction])})
                                        # predictionsData.loc[i] = pd.Series({ nameTime : projectionData['Time'][indexContact] - projectionData['Time'][indexPrediction]})
                                        predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred0.'+str(j)][indexPrediction]) - projectionData['BoneRadius.'+str(j)][indexPrediction])
                                        predictionsData.loc[i, nameTime] = projectionData['Time'][indexContact] - projectionData['Time'][indexPrediction]
                                        predictionsData.loc[i, nameClos] = 1000*(abs(projectionData['ClosestPoint.'+str(j)][indexPrediction] - projectionData['PosPred0.'+str(j)][indexPrediction]) - projectionData['BoneRadius.'+str(j)][indexPrediction])


                                    else:
                                        print('Config' + str(i), 'Phalanx. ' + str(j), 'NoPredictions in Tolerance ' + str(k), ' with Plane 0')
                                else:
                                    print('Config' + str(i), 'Phalanx. ' + str(j), 'NoPredictions in Tolerance ' + str(k), ' with Plane 0')
                            else:
                                print('Config' + str(i), 'Phalanx. ' + str(j), 'NoPredictions in Tolerance ' + str(k), ' with Plane 0')
                        
                        if(len(projectionData['InGoodTol1.'+str(j)+'.'+str(k)][indexCond].dropna()) != 0):
                            indexTamere1 = projectionData['InGoodTol1.'+str(j)+'.'+str(k)][indexCond].dropna().index
                        #if(len(projectionData[cond1]['InGoodTol1.'+str(j)+'.'+str(k)].dropna()) != 0):
                        #    indexTamere1 = projectionData[cond1]['InGoodTol1.'+str(j)+'.'+str(k)].dropna().index

                            if(len(indexTamere1) != 0):
                                if(indexContact in indexTamere1):
                                    for q in range(0, len(indexTamere1)):
                                        if(indexTamere1[q] == indexContact):
    #                                        print('Success1!')
                                            indexStart1 = indexTamere1[q]
                                            for p in range(0, len(indexTamere1)):
                                                if(indexTamere1[q-p] == indexContact-p):
                                                    indexPrediction1 = indexTamere1[q-p]

                                    if( (projectionData['Time'][indexContact] - projectionData['Time'][indexPrediction1]) > 0 ):
                                        print('Phalanx ', str(j), 'Tolerance ', k, 'IndexContact ', indexContact, 'IndexPrediction1', indexPrediction1 , 'TimeContact1', "{:.2f}".format(projectionData['Time'][indexContact]), 'TimeInTolerance1', "{:.2f}".format(projectionData['Time'][indexPrediction1]))
                                        print('TimeKnown! ', "{:.2f}".format(projectionData['Time'][indexContact] - projectionData['Time'][indexPrediction1]),
                                              'PosDifference1 ', "{:.3f}".format(1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred1.'+str(j)][indexPrediction1]) - projectionData['BoneRadius.'+str(j)][indexPrediction1])),
                                             'PosClosestPred1 ', "{:.3f}".format(1000*(abs(projectionData['ClosestPoint.'+str(j)][indexPrediction1] - projectionData['PosPred1.'+str(j)][indexPrediction1]) - projectionData['BoneRadius.'+str(j)][indexPrediction1])))
                                        plt.vlines(projectionData['Time'][indexPrediction1], 0, (k+1), linestyles = 'dashed', colors = 'magenta', label = 'Prediction1, Tol.'+str(k))
                                        # #predictionsData['DistInTol1.'+str(j)+'.'+str(k)].loc[i] = 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred1.'+str(j)][indexPrediction1]) - projectionData['BoneRadius.'+str(j)][indexPrediction1])
                                        # #predictionsData['TimeInTol1.'+str(j)+'.'+str(k)].loc[i] = projectionData['Time'][indexContact] - projectionData['Time'][indexPrediction1]
                                    
                                        nameDist = 'DistInTol1.'+str(j)+'.'+str(k)
                                        nameTime = 'TimeTol1.'+str(j)+'.'+str(k)
                                        nameClos = 'DistToClos1.'+str(j)+'.'+str(k)
                                        
                                        predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred1.'+str(j)][indexPrediction1]) - projectionData['BoneRadius.'+str(j)][indexPrediction1])
                                        predictionsData.loc[i, nameTime] = projectionData['Time'][indexContact] - projectionData['Time'][indexPrediction1]
                                        predictionsData.loc[i, nameClos] = 1000*(abs(projectionData['ClosestPoint.'+str(j)][indexPrediction1] - projectionData['PosPred1.'+str(j)][indexPrediction1]) - projectionData['BoneRadius.'+str(j)][indexPrediction1])

                                        # predictionsData.loc[i] = pd.Series({ nameDist : 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred1.'+str(j)][indexPrediction1]) - projectionData['BoneRadius.'+str(j)][indexPrediction1])})
                                        # predictionsData.loc[i] = pd.Series({ nameTime : projectionData['Time'][indexContact] - projectionData['Time'][indexPrediction1]})
                                        
                                    else:
                                        print('Config' + str(i), 'Phalanx. ' + str(j), 'NoPredictions in Tolerance ' + str(k), ' with Plane 1')
                                else:
                                    print('Config' + str(i), 'Phalanx. ' + str(j), 'NoPredictions in Tolerance ' + str(k), ' with Plane 1')
                            else:
                                print('Config' + str(i), 'Phalanx. ' + str(j), 'NoPredictions in Tolerance ' + str(k), ' with Plane 1')
                        
                        
                        if(len(projectionData['InGoodTol2.'+str(j)+'.'+str(k)][indexCond].dropna()) != 0):
                            indexTamere2 = projectionData['InGoodTol2.'+str(j)+'.'+str(k)][indexCond].dropna().index

                            if(len(indexTamere2) != 0):
                                if(indexContact in indexTamere2):
                                    for q in range(0, len(indexTamere2)):
                                        if(indexTamere2[q] == indexContact):
    #                                        print('Success2!')
                                            indexStart2 = indexTamere2[q]
                                            for p in range(0, len(indexTamere2)):
                                                if(indexTamere2[q-p] == indexContact-p):
                                                    indexPrediction2 = indexTamere2[q-p]

                                    if( (projectionData['Time'][indexContact] - projectionData['Time'][indexPrediction2]) > 0 ):
                                        print('Phalanx ', str(j), 'Tolerance ', k, 'IndexContact ', indexContact, 'IndexPrediction2', indexPrediction2 , 'TimeContact2', "{:.2f}".format(projectionData['Time'][indexContact]), 'TimeInTolerance2', "{:.2f}".format(projectionData['Time'][indexPrediction2]))#, 'LengthConditions ', len(projectionData['Time'][indexTamere]))
                                        print('TimeKnown! ', "{:.2f}".format(projectionData['Time'][indexContact] - projectionData['Time'][indexPrediction2]),
                                              'PosDifference2 ', "{:.3f}".format(1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred2.'+str(j)][indexPrediction2]) - projectionData['BoneRadius.'+str(j)][indexPrediction2])),
                                             'PosClosestPred2 ', "{:.3f}".format(1000*(abs(projectionData['ClosestPoint.'+str(j)][indexPrediction2] - projectionData['PosPred2.'+str(j)][indexPrediction2]) - projectionData['BoneRadius.'+str(j)][indexPrediction2])))
                                        plt.vlines(projectionData['Time'][indexPrediction2], 0, (k+2), linestyles = 'dashed', colors = 'blue', label = 'Prediction2, Tol.'+str(k))
                                        #predictionsData['DistInTol2.'+str(j)+'.'+str(k)].loc[i] = 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred2.'+str(j)][indexPrediction2]) - projectionData['BoneRadius.'+str(j)][indexPrediction2])
                                        #predictionsData['TimeInTol2.'+str(j)+'.'+str(k)].loc[i] = projectionData['Time'][indexContact] - projectionData['Time'][indexPrediction2]
                                        
                                        nameDist = 'DistInTol2.'+str(j)+'.'+str(k)
                                        nameTime = 'TimeTol2.'+str(j)+'.'+str(k)
                                        nameClos = 'DistToClos2.'+str(j)+'.'+str(k)

                                        predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred2.'+str(j)][indexPrediction2]) - projectionData['BoneRadius.'+str(j)][indexPrediction2])
                                        predictionsData.loc[i, nameTime] = projectionData['Time'][indexContact] - projectionData['Time'][indexPrediction2]
                                        predictionsData.loc[i, nameClos] = 1000*(abs(projectionData['ClosestPoint.'+str(j)][indexPrediction2] - projectionData['PosPred2.'+str(j)][indexPrediction2]) - projectionData['BoneRadius.'+str(j)][indexPrediction2])

                                    else:
                                        print('Config' + str(i), 'Phalanx. ' + str(j), 'NoPredictions in Tolerance ' + str(k), ' with Plane 2')
                                else:
                                    print('Config' + str(i), 'Phalanx. ' + str(j), 'NoPredictions in Tolerance ' + str(k), ' with Plane 2')
                            else:
                                print('Config' + str(i), 'Phalanx. ' + str(j), 'NoPredictions in Tolerance ' + str(k), ' with Plane 2')

                        plt.scatter(projectionData['Time'][indexCond], k*projectionData['InGoodTol0.'+str(j)+'.'+str(k)][indexCond], label = 'InGoodTol0.'+str(j)+'.'+str(k))
                        plt.scatter(projectionData['Time'][indexCond], (k+1)*projectionData['InGoodTol1.'+str(j)+'.'+str(k)][indexCond], label = 'InGoodTol1.'+str(j)+'.'+str(k))
                        plt.scatter(projectionData['Time'][indexCond], (k+2)*projectionData['InGoodTol2.'+str(j)+'.'+str(k)][indexCond], label = 'InGoodTol2.'+str(j)+'.'+str(k))
                    
                    plt.vlines(projectionData['Time'][indexContact], 0, 53, linestyles = 'dashed', colors = 'cyan', label = 'Contact!')
                    plt.scatter(projectionData['Time'][indexCond], projectionData['BoneContact.'+str(j)][indexCond], label = 'BoneContact')

                    plt.title('Config' + str(i) + '; Phalanx ' + str(j) + 'R-' + projectionData['BoneName.'+str(j)].iloc[0])
                    plt.legend()

                
                else:
                    print('Config', str(i), ' Fail!')
                
        for j in range(0, 19):
            if(len(projectionData[cond1]['TouchingL.'+str(j)].dropna()) != 0):
                indexContactL = projectionData[cond1]['TouchingL.'+str(j)].dropna().index[0]
                #print(projectionData['Time'][indexContact])
                print('PhalanxL ', str(j), 'Time ', "{:.3f}".format(projectionData['Time'][indexContactL]))

                if(np.isnan(projectionData['TouchingL.'+str(j)][indexContactL]) == False):
                    print('PosAtContactL:' , "{:.3f}".format(projectionData['TouchingL.'+str(j)][indexContactL]),
                          'PosPRED0-AtContactL:' , "{:.3f}".format(projectionData['PosPredL0.'+str(j)][indexContactL]), 
                          'PosPRED1-AtContactL:' , "{:.3f}".format(projectionData['PosPredL1.'+str(j)][indexContactL]),
                          'PosPRED2-AtContactL:' , "{:.3f}".format(projectionData['PosPredL2.'+str(j)][indexContactL]))

                    print('Diff? (mm)',
                          'Plan0L: ', "{:.3f}".format(1000*(abs(projectionData['PosPredL0.'+str(j)][indexContactL] - projectionData['BonePosL.'+str(j)][indexContactL]) - projectionData['BoneRadiusL.'+str(j)][indexContactL])),
                          'Plan1L: ', "{:.3f}".format(1000*(abs(projectionData['PosPredL1.'+str(j)][indexContactL] - projectionData['BonePosL.'+str(j)][indexContactL]) - projectionData['BoneRadiusL.'+str(j)][indexContactL])), 
                          'Plan2L: ', "{:.3f}".format(1000*(abs(projectionData['PosPredL2.'+str(j)][indexContactL] - projectionData['BonePosL.'+str(j)][indexContactL]) - projectionData['BoneRadiusL.'+str(j)][indexContactL])),
                         'Plan0LClos: ', "{:.3f}".format(1000*(abs(projectionData['PosPredL0.'+str(j)][indexContactL] - projectionData['ClosestPointL.'+str(j)][indexContactL]) - projectionData['BoneRadiusL.'+str(j)][indexContactL])),
                         'Plan1LClos: ', "{:.3f}".format(1000*(abs(projectionData['PosPredL1.'+str(j)][indexContactL] - projectionData['ClosestPointL.'+str(j)][indexContactL]) - projectionData['BoneRadiusL.'+str(j)][indexContactL])),
                         'Plan2LClos: ', "{:.3f}".format(1000*(abs(projectionData['PosPredL2.'+str(j)][indexContactL] - projectionData['ClosestPointL.'+str(j)][indexContactL]) - projectionData['BoneRadiusL.'+str(j)][indexContactL])))

                    plt.figure(figsize=(20,10))

                    indexCond = projectionData[cond1]['Time'].index

                    for k in range(50, 0, -5):
                        tolerance = k
                        projectionData.loc[1000*(abs(projectionData['PosPredL0.'+str(j)][indexCond] - projectionData['BonePosL.'+str(j)][indexContactL]) - projectionData['BoneRadiusL.'+str(j)]) <= tolerance, 'InGoodTolL0.'+str(j)+'.'+str(k)] = True
                        projectionData.loc[1000*(abs(projectionData['PosPredL1.'+str(j)][indexCond] - projectionData['BonePosL.'+str(j)][indexContactL]) - projectionData['BoneRadiusL.'+str(j)]) <= tolerance, 'InGoodTolL1.'+str(j)+'.'+str(k)] = True
                        projectionData.loc[1000*(abs(projectionData['PosPredL2.'+str(j)][indexCond] - projectionData['BonePosL.'+str(j)][indexContactL]) - projectionData['BoneRadiusL.'+str(j)]) <= tolerance, 'InGoodTolL2.'+str(j)+'.'+str(k)] = True

                        #print(1000*(abs(projectionData['PosPred2.'+str(j)][indexCond] - projectionData['BonePos.'+str(j)][indexContact]) - projectionData['BoneRadius.'+str(j)][indexCond]),
                        #      1000*(abs(projectionData['PosPred2.'+str(j)][indexCond] - projectionData['BonePos.'+str(j)][indexContact]) - projectionData['BoneRadius.'+str(j)][indexCond]) <= tolerance, projectionData['InGoodTol2.'+str(j)+'.'+str(k)][indexCond])


                        #print(projectionData[cond1]['InGoodTol0.'+str(j)+'.'+str(k)].dropna().index)
                        if(len(projectionData['InGoodTolL0.'+str(j)+'.'+str(k)][indexCond].dropna()) != 0):
                            indexTamereL = projectionData['InGoodTolL0.'+str(j)+'.'+str(k)][indexCond].dropna().index
                            if(len(indexTamereL) != 0):
                                if(indexContactL in indexTamereL):
                                    for q in range(0, len(indexTamereL)): #Need to modify -> if indexContact in indexTamere
                                        if(indexTamereL[q] == indexContactL):
    #                                        print('Success!')
                                            indexStartL = indexTamereL[q]
                                            for p in range(0, len(indexTamereL)):
                                                if(indexTamereL[q-p] == indexContactL-p):
                                                    indexPredictionL = indexTamereL[q-p]

                                    if( (projectionData['Time'][indexContactL] - projectionData['Time'][indexPredictionL]) > 0 ):
                                        print('PhalanxL ', str(j), 'Tolerance ', k, 'IndexContactL ', indexContactL, 'IndexPrediction', indexPredictionL, 'TimeContact', "{:.2f}".format(projectionData['Time'][indexContactL]), 'TimeInTolerance', "{:.2f}".format(projectionData['Time'][indexPredictionL]))
                                        print('TimeKnownL! ', "{:.2f}".format(projectionData['Time'][indexContactL] - projectionData['Time'][indexPredictionL]),
                                              'PosDifference0L ', "{:.3f}".format(1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL0.'+str(j)][indexPredictionL]) - projectionData['BoneRadiusL.'+str(j)][indexPredictionL])),
                                             'PosClosestPred0L ', "{:.3f}".format(1000*(abs(projectionData['ClosestPointL.'+str(j)][indexPredictionL] - projectionData['PosPredL0.'+str(j)][indexPredictionL]) - projectionData['BoneRadiusL.'+str(j)][indexPredictionL])))
                                        plt.vlines(projectionData['Time'][indexPredictionL], 0, k, linestyles = 'dashed', colors = 'red', label = 'PredictionL0, Tol.'+str(k))
                                        
                                        nameDist = 'DistInTol0L.'+str(j)+'.'+str(k)
                                        nameTime = 'TimeTol0L.'+str(j)+'.'+str(k)
                                        nameClos = 'DistToClos0L.'+str(j)+'.'+str(k)

                                        predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL0.'+str(j)][indexPredictionL]) - projectionData['BoneRadiusL.'+str(j)][indexPredictionL])
                                        predictionsData.loc[i, nameTime] = projectionData['Time'][indexContactL] - projectionData['Time'][indexPredictionL]
                                        predictionsData.loc[i, nameClos] = 1000*(abs(projectionData['ClosestPointL.'+str(j)][indexPredictionL] - projectionData['PosPredL0.'+str(j)][indexPredictionL]) - projectionData['BoneRadiusL.'+str(j)][indexPredictionL])

                                        # predictionsData.loc[i] = pd.Series({ nameDist : 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL0.'+str(j)][indexPredictionL]) - projectionData['BoneRadiusL.'+str(j)][indexPredictionL])})
                                        # predictionsData.loc[i] = pd.Series({ nameTime : projectionData['Time'][indexContactL] - projectionData['Time'][indexPredictionL]})
                                    else:
                                        print('Config' + str(i), 'PhalanxL. ' + str(j), 'NoPredictionsL in Tolerance ' + str(k), ' with PlaneL 0')
                                else:
                                    print('Config' + str(i), 'PhalanxL. ' + str(j), 'NoPredictionsL in Tolerance ' + str(k), ' with PlaneL 0')
                            else:
                                print('Config' + str(i), 'PhalanxL. ' + str(j), 'NoPredictions in Tolerance ' + str(k), ' with PlaneL 0')


                        if(len(projectionData['InGoodTolL1.'+str(j)+'.'+str(k)][indexCond].dropna()) != 0):
                            indexTamere1L = projectionData['InGoodTolL1.'+str(j)+'.'+str(k)][indexCond].dropna().index
                        #if(len(projectionData[cond1]['InGoodTol1.'+str(j)+'.'+str(k)].dropna()) != 0):
                        #    indexTamere1 = projectionData[cond1]['InGoodTol1.'+str(j)+'.'+str(k)].dropna().index

                            if(len(indexTamere1L) != 0):
                                if(indexContactL in indexTamere1L):
                                    for q in range(0, len(indexTamere1L)):
                                        if(indexTamere1L[q] == indexContactL):
    #                                        print('Success1!')
                                            indexStart1L = indexTamere1L[q]
                                            for p in range(0, len(indexTamere1L)):
                                                if(indexTamere1L[q-p] == indexContactL-p):
                                                    indexPrediction1L = indexTamere1L[q-p]

                                    if( (projectionData['Time'][indexContactL] - projectionData['Time'][indexPrediction1L]) > 0 ):
                                        print('PhalanxL ', str(j), 'Tolerance ', k, 'IndexContactL ', indexContactL, 'IndexPredictionL1', indexPrediction1L , 'TimeContact1L', "{:.2f}".format(projectionData['Time'][indexContactL]), 'TimeInTolerance1L', "{:.2f}".format(projectionData['Time'][indexPrediction1L]))
                                        print('TimeKnownL! ', "{:.2f}".format(projectionData['Time'][indexContactL] - projectionData['Time'][indexPrediction1L]),
                                              'PosDifference1L ', "{:.3f}".format(1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL1.'+str(j)][indexPrediction1L]) - projectionData['BoneRadiusL.'+str(j)][indexPrediction1L])),
                                             'PosClosestPred1L ', "{:.3f}".format(1000*(abs(projectionData['ClosestPointL.'+str(j)][indexPrediction1L] - projectionData['PosPredL1.'+str(j)][indexPrediction1L]) - projectionData['BoneRadiusL.'+str(j)][indexPrediction1L])))
                                        plt.vlines(projectionData['Time'][indexPrediction1L], 0, (k+1), linestyles = 'dashed', colors = 'magenta', label = 'Prediction1L, Tol.'+str(k))
                                        nameDist = 'DistInTol1L.'+str(j)+'.'+str(k)
                                        nameTime = 'TimeTol1L.'+str(j)+'.'+str(k)
                                        nameClos = 'DistToClos1L.'+str(j)+'.'+str(k)
                                        # predictionsData.loc[i] = pd.Series({ nameDist : 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL1.'+str(j)][indexPrediction1L]) - projectionData['BoneRadiusL.'+str(j)][indexPrediction1L])})
                                        # predictionsData.loc[i] = pd.Series({ nameTime : projectionData['Time'][indexContactL] - projectionData['Time'][indexPrediction1L]})
                                        
                                        predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL1.'+str(j)][indexPrediction1L]) - projectionData['BoneRadiusL.'+str(j)][indexPrediction1L])
                                        predictionsData.loc[i, nameTime] = projectionData['Time'][indexContactL] - projectionData['Time'][indexPrediction1L]
                                        predictionsData.loc[i, nameClos] = 1000*(abs(projectionData['ClosestPointL.'+str(j)][indexPrediction1L] - projectionData['PosPredL1.'+str(j)][indexPrediction1L]) - projectionData['BoneRadiusL.'+str(j)][indexPrediction1L])

                                    else:
                                        print('Config' + str(i), 'PhalanxL. ' + str(j), 'NoPredictionsL in Tolerance ' + str(k), ' with PlaneL 1')
                                else:
                                    print('Config' + str(i), 'PhalanxL. ' + str(j), 'NoPredictionsL in Tolerance ' + str(k), ' with PlaneL 1')
                            else:
                                print('Config' + str(i), 'PhalanxL. ' + str(j), 'NoPredictions in Tolerance ' + str(k), ' with PlaneL 1')


                        if(len(projectionData['InGoodTolL2.'+str(j)+'.'+str(k)][indexCond].dropna()) != 0):
                            indexTamere2L = projectionData['InGoodTolL2.'+str(j)+'.'+str(k)][indexCond].dropna().index

                            if(len(indexTamere2L) != 0):
                                if(indexContactL in indexTamere2L):
                                    for q in range(0, len(indexTamere2L)):
                                        if(indexTamere2L[q] == indexContactL):
    #                                        print('Success2!')
                                            indexStart2L = indexTamere2L[q]
                                            for p in range(0, len(indexTamere2L)):
                                                if(indexTamere2L[q-p] == indexContactL-p):
                                                    indexPrediction2L = indexTamere2L[q-p]

                                    if( (projectionData['Time'][indexContactL] - projectionData['Time'][indexPrediction2L]) > 0 ):
                                        print('PhalanxL', str(j), 'Tolerance', k, 'IndexContactL', indexContactL, 'IndexPrediction2L', indexPrediction2L , 'TimeContact2', "{:.2f}".format(projectionData['Time'][indexContactL]), 'TimeInTolerance2L', "{:.2f}".format(projectionData['Time'][indexPrediction2L]))#, 'LengthConditions ', len(projectionData['Time'][indexTamere]))
                                        print('TimeKnownL! ', "{:.2f}".format(projectionData['Time'][indexContactL] - projectionData['Time'][indexPrediction2L]),
                                              'PosDifferenceL2 ', "{:.3f}".format(1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL2.'+str(j)][indexPrediction2L]) - projectionData['BoneRadiusL.'+str(j)][indexPrediction2L])),
                                             'PosClosestPredL2 ', "{:.3f}".format(1000*(abs(projectionData['ClosestPointL.'+str(j)][indexPrediction2L] - projectionData['PosPredL2.'+str(j)][indexPrediction2L]) - projectionData['BoneRadiusL.'+str(j)][indexPrediction2L])))
                                        plt.vlines(projectionData['Time'][indexPrediction2L], 0, (k+2), linestyles = 'dashed', colors = 'blue', label = 'Prediction2, Tol.'+str(k))
                                        #predictionsData['DistInTol2L.'+str(j)+'.'+str(k)].loc[i] = 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL2.'+str(j)][indexPrediction2L]) - projectionData['BoneRadiusL.'+str(j)][indexPrediction2L])
                                        #predictionsData['TimeInTol2L.'+str(j)+'.'+str(k)].loc[i] = projectionData['Time'][indexContactL] - projectionData['Time'][indexPrediction2L]
                                    
                                        nameDist = 'DistInTol2L.'+str(j)+'.'+str(k)
                                        nameTime = 'TimeTol2L.'+str(j)+'.'+str(k)
                                        nameClos = 'DistToClos2L.'+str(j)+'.'+str(k)

                                        predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL2.'+str(j)][indexPrediction2L]) - projectionData['BoneRadiusL.'+str(j)][indexPrediction2L])
                                        predictionsData.loc[i, nameTime] = projectionData['Time'][indexContactL] - projectionData['Time'][indexPrediction2L]
                                        predictionsData.loc[i, nameClos] = 1000*(abs(projectionData['ClosestPointL.'+str(j)][indexPrediction2L] - projectionData['PosPredL2.'+str(j)][indexPrediction2L]) - projectionData['BoneRadiusL.'+str(j)][indexPrediction2L])

                                        # predictionsData.loc[i] = pd.Series({ nameDist : 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL2.'+str(j)][indexPrediction2L]) - projectionData['BoneRadiusL.'+str(j)][indexPrediction2L])})
                                        # predictionsData.loc[i] = pd.Series({ nameTime : projectionData['Time'][indexContactL] - projectionData['Time'][indexPrediction2L]})
                                        
                                    else:
                                        print('Config' + str(i), 'PhalanxL. ' + str(j), 'NoPredictionsL in Tolerance ' + str(k), ' with PlaneL 2')
                                else:
                                    print('Config' + str(i), 'PhalanxL. ' + str(j), 'NoPredictionsL in Tolerance ' + str(k), ' with PlaneL 2')
                            else:
                                print('Config' + str(i), 'PhalanxL. ' + str(j), 'NoPredictions in Tolerance ' + str(k), ' with PlaneL 2')

                        plt.scatter(projectionData['Time'][indexCond], k*projectionData['InGoodTolL0.'+str(j)+'.'+str(k)][indexCond], label = 'InGoodTolL0.'+str(j)+'.'+str(k))
                        plt.scatter(projectionData['Time'][indexCond], (k+1)*projectionData['InGoodTolL1.'+str(j)+'.'+str(k)][indexCond], label = 'InGoodTolL1.'+str(j)+'.'+str(k))
                        plt.scatter(projectionData['Time'][indexCond], (k+2)*projectionData['InGoodTolL2.'+str(j)+'.'+str(k)][indexCond], label = 'InGoodTolL2.'+str(j)+'.'+str(k))

                    plt.vlines(projectionData['Time'][indexContactL], 0, 53, linestyles = 'dashed', colors = 'cyan', label = 'ContactL!')
                    plt.scatter(projectionData['Time'][indexCond], projectionData['BoneContactL.'+str(j)][indexCond], label = 'BoneContact')

                    plt.title('Config' + str(i) + '; Phalanx ' + str(j) + 'L-' + projectionData['BoneNameL.'+str(j)].iloc[0])
                    plt.legend()


                else:
                    print('Config', str(i), ' Fail-L!')

predictionsData.to_csv('./Analysis/'+addword+'G-'+ str(projectionData['Gamma'].iloc[0]) + 'B-' + str(projectionData['Beta'].iloc[0]) + '-' + date + '-FullAnalysis-' + word + '.csv', sep = ';')
predictionsData.describe().to_csv('./Analysis/'+addword+'G-'+ str(projectionData['Gamma'].iloc[0]) + 'B-' + str(projectionData['Beta'].iloc[0]) + '-' + date + '-DescribeDf-' + word + '.csv', sep = ';')
predictionsData