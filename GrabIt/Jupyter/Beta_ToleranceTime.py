import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import shutil
import sys

date = input("Enter User Name or Date:")
# gamma = input("Enter Gamma:")
# beta = input("Enter Beta:")
whichList = input("First Sim (1), Second (2), or Third (3), (4)?")

word = ''
addword = ''
projectionData = None

if(int(whichList) == 1):
    list_beta = [0, 1.5, 3, 1.5, 3]
    list_gamma = [0, 0.01, 0.01, 0.005, 0.005]
if(int(whichList) == 2):
    list_beta = [10, 10, 10, 10, 10]
    list_gamma = [0.005, 0.01, 0.05, 0.1, 0.5]
if(int(whichList) == 3):
    list_gamma = [10, 10, 10, 10, 10]
    list_beta = [1, 1.5, 2, 2.5, 3]
if(int(whichList) == 4):
    list_gamma = [0.1, 0.1]
    list_beta = [2.5, 3]
if(int(whichList) == 5):
    list_gamma = [0.5, 0.5]
    list_beta = [2.5, 3]

def progress(count, total, suffix=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', suffix))
    sys.stdout.flush()  # As suggested by Rom Ruben

# projectionData = pd.read_csv('./Analysis/SimulationData/'+ date + '/G-' + str(gamma) + '-B-' + str(beta) + '-' + word + date + '.csv', sep = ";", decimal = '.')#, low_memory = False)
read = 0
for read in range(0, len(list_beta)):
    gamma = list_gamma[read]
    beta = list_beta[read]
    print(gamma, beta)
    projectionData = pd.read_csv('../Assets/Resources/DataSimulation/AlgoHints/G-' + str(gamma) + '-B-' + str(beta) + '-' + word + date + '.csv', sep = ";", decimal = '.')#, low_memory = False)

    projectionData = projectionData.dropna()

    projectionData['BoneName.0'] = projectionData['BoneName']
    projectionData['BoneNameL.0'] = projectionData['BoneNameL']

    projectionData['BonePos.0'] = projectionData['BonePos']
    projectionData['BonePosL.0'] = projectionData['BonePosL']

    projectionData['PosPred0.0'] = projectionData['PosPred0']
    projectionData['PosPred1.0'] = projectionData['PosPred1']
    projectionData['PosPred2.0'] = projectionData['PosPred2']

    projectionData['PosPredL0.0'] = projectionData['PosPredL0']
    projectionData['PosPredL1.0'] = projectionData['PosPredL1']
    projectionData['PosPredL2.0'] = projectionData['PosPredL2']

    projectionData['DistToPred0.0'] = projectionData['DistToPred0']
    projectionData['DistToPred1.0'] = projectionData['DistToPred1']
    projectionData['DistToPred2.0'] = projectionData['DistToPred2']

    projectionData['DistToPred0L.0'] = projectionData['DistToPred0L']
    projectionData['DistToPred1L.0'] = projectionData['DistToPred1L']
    projectionData['DistToPred2L.0'] = projectionData['DistToPred2L']

    projectionData['BoneRadius.0'] = projectionData['BoneRadius']
    projectionData['BoneContact.0'] = projectionData['BoneContact']

    projectionData['BoneRadiusL.0'] = projectionData['BoneRadiusL']
    projectionData['BoneContactL.0'] = projectionData['BoneContactL']

    projectionData['ClosestPoint.0'] = projectionData['ClosestPoint']
    projectionData['ClosestPointL.0'] = projectionData['ClosestPointL']

    projectionData['DistToClosest.0'] = projectionData['DistToClosest']
    projectionData['DistToClosestL.0'] = projectionData['DistToClosestL']

    for i in range(0, 135):
        cond1 = projectionData['Config'] == i
        projectionData = projectionData.drop(projectionData[projectionData['Config'] == i].index[0:2])

    for j in range(0,19):
        projectionData.loc[projectionData['BoneContact.'+str(j)] == True, 'Touching.'+str(j)] = projectionData['BonePos.'+str(j)]
        projectionData.loc[projectionData['BoneContactL.'+str(j)] == True, 'TouchingL.'+str(j)] = projectionData['BonePosL.'+str(j)]
        
    predictionsData = None
    predictionsData = pd.DataFrame(columns = ['Beta', 'Gamma'])


    for i in range(0,135):
        indexContact = 0
        indexPrediction = 0
        indexPrediction1 = 0
        indexPrediction2 = 0

        indexStart = []

        indexCond = 0
        
        indexContactL = 0
        indexPredictionL = 0
        indexPrediction1L = 0
        indexPrediction2L = 0

        progress(i, 135)

        if(len(projectionData[projectionData['Config'] == i]) != 0):
            cond1 = (projectionData['Config'] == i)
            predictionsData.loc[i] = pd.Series({'Gamma' : projectionData[cond1]['Gamma'].iloc[0], 'Beta' : projectionData[cond1]['Beta'].iloc[0]})

            for j in range(0, 19):                                                
                
                predictionsData.loc[i, 'Contact.'+str(j)] = len(projectionData[cond1]['Touching.'+str(j)].dropna()) != 0
                predictionsData.loc[i, 'ContactL.'+str(j)] = len(projectionData[cond1]['TouchingL.'+str(j)].dropna()) != 0

                if(len(projectionData[cond1]['Touching.'+str(j)].dropna()) != 0):
                    indexContact = projectionData[cond1]['Touching.'+str(j)].dropna().index[0]

                    if(np.isnan(projectionData['Touching.'+str(j)][indexContact]) == False):
                        
                        indexCond = projectionData[cond1]['Time'].index
                        
                        nameBoneDist = 'DistBoneInTolTime0.'+str(j)+'.0'
                        predictionsData.loc[i, nameBoneDist] = 0

                        nameDist = 'MaxDistanceInTolTime0.'+str(j)+'.0'
                        predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred0.'+str(j)][indexContact]) - projectionData['BoneRadius.'+str(j)][indexContact])

                        nameDist = 'MaxDistanceInTolTime1.'+str(j)+'.0'
                        predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred1.'+str(j)][indexContact]) - projectionData['BoneRadius.'+str(j)][indexContact])

                        nameDist = 'MaxDistanceInTolTime2.'+str(j)+'.0'
                        predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred2.'+str(j)][indexContact]) - projectionData['BoneRadius.'+str(j)][indexContact])

                        for k in range(300, 2050, 50):
                            tolerance = k/1000
                            indexStart = []
                            indexPrediction = 0
                            indexPrediction1 = 0
                            indexPrediction2 = 0
                            indexTamere = 0

                            for p in range(0, len(indexCond)):
                                if((projectionData['Time'][indexContact] - projectionData['Time'][indexCond[p]]) > 0):
                                    if(projectionData['Time'][indexContact] - projectionData['Time'][indexCond[p]] <= tolerance):
                                        indexStart.append(indexCond[p])
                            
                            indexTamere = indexStart
                            if(len(indexTamere) != 0):
                                indexPrediction = indexTamere[0]
                                for q in range(1, len(indexTamere)):
                                    if( abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred0.'+str(j)][indexPrediction] ) < abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred0.'+str(j)][indexTamere[q]]) ):
                                        indexPrediction = indexTamere[q]
                                    else:
                                        pass

                                nameDist = 'MaxDistanceInTolTime0.'+str(j)+'.'+str(k)
                                nameClos = 'DistToClos0.'+str(j)+'.'+str(k)
                                nameBoneDist = 'DistBoneInTolTime0.'+str(j)+'.'+str(k)

                                predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred0.'+str(j)][indexPrediction]) - projectionData['BoneRadius.'+str(j)][indexPrediction])
                                predictionsData.loc[i, nameClos] = 1000*(abs(projectionData['ClosestPoint.'+str(j)][indexPrediction] - projectionData['PosPred0.'+str(j)][indexPrediction]) - projectionData['BoneRadius.'+str(j)][indexPrediction])
                                predictionsData.loc[i, nameBoneDist] = 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['BonePos.'+str(j)][indexPrediction]))


                                indexPrediction1 = indexTamere[0]
                                for q in range(1, len(indexTamere)):
                                    if( abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred1.'+str(j)][indexPrediction1] ) < abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred1.'+str(j)][indexTamere[q]]) ):
                                        indexPrediction1 = indexTamere[q]
                                    else:
                                        pass

                                nameDist = 'MaxDistanceInTolTime1.'+str(j)+'.'+str(k)
                                nameClos = 'DistToClos1.'+str(j)+'.'+str(k)
                                
                                predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred1.'+str(j)][indexPrediction1]) - projectionData['BoneRadius.'+str(j)][indexPrediction1])
                                predictionsData.loc[i, nameClos] = 1000*(abs(projectionData['ClosestPoint.'+str(j)][indexPrediction1] - projectionData['PosPred1.'+str(j)][indexPrediction1]) - projectionData['BoneRadius.'+str(j)][indexPrediction1])

                                indexPrediction2 = indexTamere[0]
                                for q in range(1, len(indexTamere)):
                                    if( abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred2.'+str(j)][indexPrediction2] ) < abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred2.'+str(j)][indexTamere[q]]) ):
                                        indexPrediction2 = indexTamere[q]
                                    else:
                                        pass

                                nameDist = 'MaxDistanceInTolTime2.'+str(j)+'.'+str(k)
                                nameClos = 'DistToClos2.'+str(j)+'.'+str(k)
                                
                                predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred2.'+str(j)][indexPrediction2]) - projectionData['BoneRadius.'+str(j)][indexPrediction2])
                                predictionsData.loc[i, nameClos] = 1000*(abs(projectionData['ClosestPoint.'+str(j)][indexPrediction2] - projectionData['PosPred2.'+str(j)][indexPrediction2]) - projectionData['BoneRadius.'+str(j)][indexPrediction2])
                            else:
                                pass
                            
                    else:
                        pass
                else:
                    pass                                

                if(len(projectionData[cond1]['TouchingL.'+str(j)].dropna()) != 0):
                    indexContactL = projectionData[cond1]['TouchingL.'+str(j)].dropna().index[0]

                    if(np.isnan(projectionData['TouchingL.'+str(j)][indexContactL]) == False):
                        
                        indexCond = projectionData[cond1]['Time'].index

                        nameBoneDist = 'DistBoneInTolTimeL0.'+str(j)+'.0'
                        predictionsData.loc[i, nameBoneDist] = 0

                        nameDist = 'MaxDistanceInTolTimeL0.'+str(j)+'.0'
                        predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL0.'+str(j)][indexContactL]) - projectionData['BoneRadius.'+str(j)][indexContactL])

                        nameDist = 'MaxDistanceInTolTimeL1.'+str(j)+'.0'
                        predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL1.'+str(j)][indexContactL]) - projectionData['BoneRadius.'+str(j)][indexContactL])

                        nameDist = 'MaxDistanceInTolTimeL2.'+str(j)+'.0'
                        predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL2.'+str(j)][indexContactL]) - projectionData['BoneRadius.'+str(j)][indexContactL])
                        
                        for k in range(300, 2050, 50):
                            tolerance = k/1000

                            indexStart = []
                            indexPredictionL = 0
                            indexPrediction1L = 0
                            indexPrediction2L = 0
                            indexTamere = 0
                        
                            for p in range(0, len(indexCond)):
                                if((projectionData['Time'][indexContactL] - projectionData['Time'][indexCond[p]]) > 0):
                                    if(projectionData['Time'][indexContactL] - projectionData['Time'][indexCond[p]] <= tolerance):
                                        indexStart.append(indexCond[p])
                            
                            indexTamere = indexStart
                            if(len(indexTamere) != 0):    
                                indexPredictionL = indexTamere[0]
                                for q in range(1, len(indexTamere)):
                                    if( abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL0.'+str(j)][indexPredictionL] ) < abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL0.'+str(j)][indexTamere[q]]) ):
                                        indexPredictionL = indexTamere[q]
                                    else:
                                        pass

                                nameDist = 'MaxDistanceInTolTimeL0.'+str(j)+'.'+str(k)
                                nameClos = 'DistToClosL0.'+str(j)+'.'+str(k)
                                nameBoneDist = 'DistBoneInTolTimeL0.'+str(j)+'.'+str(k)

                                predictionsData.loc[i, nameBoneDist] = 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['BonePosL.'+str(j)][indexPredictionL]))
                                predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL0.'+str(j)][indexPredictionL]) - projectionData['BoneRadiusL.'+str(j)][indexPredictionL])
                                predictionsData.loc[i, nameClos] = 1000*(abs(projectionData['ClosestPointL.'+str(j)][indexPredictionL] - projectionData['PosPredL0.'+str(j)][indexPredictionL]) - projectionData['BoneRadiusL.'+str(j)][indexPredictionL])


                                indexPrediction1L = indexTamere[0]
                                for q in range(1, len(indexTamere)):
                                    if( abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL1.'+str(j)][indexPrediction1L] ) < abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL1.'+str(j)][indexTamere[q]]) ):
                                        indexPrediction1L = indexTamere[q]
                                    else:
                                        pass

                                nameDist = 'MaxDistanceInTolTimeL1.'+str(j)+'.'+str(k)
                                nameClos = 'DistToClosL1.'+str(j)+'.'+str(k)

                                predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL1.'+str(j)][indexPrediction1L]) - projectionData['BoneRadiusL.'+str(j)][indexPrediction1L])
                                predictionsData.loc[i, nameClos] = 1000*(abs(projectionData['ClosestPointL.'+str(j)][indexPrediction1L] - projectionData['PosPredL1.'+str(j)][indexPrediction1L]) - projectionData['BoneRadiusL.'+str(j)][indexPrediction1L])
                                
                                indexPrediction2L = indexTamere[0]
                                for q in range(1, len(indexTamere)):
                                    if( abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL2.'+str(j)][indexPrediction2L] ) < abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL2.'+str(j)][indexTamere[q]]) ):
                                        indexPrediction2L = indexTamere[q]
                                    else:
                                        pass

                                nameDist = 'MaxDistanceInTolTimeL2.'+str(j)+'.'+str(k)
                                nameClos = 'DistToClosL2.'+str(j)+'.'+str(k)

                                predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL2.'+str(j)][indexPrediction2L]) - projectionData['BoneRadiusL.'+str(j)][indexPrediction2L])
                                predictionsData.loc[i, nameClos] = 1000*(abs(projectionData['ClosestPointL.'+str(j)][indexPrediction2L] - projectionData['PosPredL2.'+str(j)][indexPrediction2L]) - projectionData['BoneRadiusL.'+str(j)][indexPrediction2L])
                            else:
                                pass  
                    else:
                        pass
                else:
                    pass

    predictionsData.to_csv('./Analysis/TimeTolerance/'+addword+'G-'+ str(gamma) + 'B-' + str(beta) + '-' + date + '-FullAnalysis-' + word + '.csv', sep = ';')
    predictionsData.describe().to_csv('./Analysis/TimeTolerance/'+addword+'G-'+ str(gamma) + 'B-' + str(beta) + '-' + date + '-DescribeDf-' + word + '.csv', sep = ';')
    predictionsData

    # shutil.move('./Analysis/SimulationData/'+ date + '/G-' + str(gamma) + '-B-' + str(beta) + '-' + word + date + '.csv', './Analysis/SimulationData/'+ date + '/TimeTolerance/G-' + str(gamma) + '-B-' + str(beta) + '-' + word + date + '.csv')

    #shutil.move('../Assets/Resources/DataSimulation/AlgoHints/G-' + str(gamma) + '-B-' + str(beta) + '-' + word + date + '.csv', './Analysis/SimulationData/'+ date + '/G-' + str(gamma) + '-B-' + str(beta) + '-' + word + date + '.csv')

    progress(100, 100)
    print('\n')


read = 0
for read in range(0, len(list_beta)):
    gamma = list_gamma[read]
    beta = list_beta[read]
    shutil.move('../Assets/Resources/DataSimulation/AlgoHints/G-' + str(gamma) + '-B-' + str(beta) + '-' + word + date + '.csv', './Analysis/SimulationData/'+ date + '/TimeTolerance/G-' + str(gamma) + '-B-' + str(beta) + '-' + word + date + '.csv')

