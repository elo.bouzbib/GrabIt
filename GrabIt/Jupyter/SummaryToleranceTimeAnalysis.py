import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import shutil
import sys

date = input("Enter User Name or Date:")

list_beta = [1.05]
list_gamma = [0]

word = ''
addword = 'noZero'
write = input("Write Summary? (Y/N)")

read = 0
for read in range(0, len(list_beta)):
    gamma = list_gamma[read]
    beta = list_beta[read]
    print(gamma, beta)
    analysisA = None
    analysisA = pd.read_csv('./Analysis/TimeTolerance/G-' + str(gamma) + 'B-' + str(beta) + word +'-'+ date + '-FullAnalysis-' + addword + '.csv', sep = ";", decimal = '.', low_memory = False)

    boneNames = []
    bones = pd.Series()
    bones = pd.read_csv('./BoneNames.csv')
    bones = bones.transpose()
    bones = bones.drop(bones.index[0])
    for i in range(0, 19):
        boneNames.append('R-' + bones.iloc[0, i])
    for i in range(0, 19):
        boneNames.append('L-' + bones.iloc[0, i])
        

    analyseMe = pd.DataFrame(columns = ['Config'])
    def progress(count, total, suffix=''):
        bar_len = 60
        filled_len = int(round(bar_len * count / float(total)))

        percents = round(100.0 * count / float(total), 1)
        bar = '=' * filled_len + '-' * (bar_len - filled_len)

        sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', suffix))
        sys.stdout.flush()  # As suggested by Rom Ruben

    progress(0, 5*135)
        
    for i in range(0, 135):
        progress(i, 5*135)

        analyseMe.loc[i] = pd.Series({'Config' : i})
        for j in range(0, 19):
            analyseMe['Contact.'+str(j)] = analysisA['Contact.'+str(j)]
            analyseMe['Contact.'+str(j+19)] = analysisA['ContactL.'+str(j)]
            k = 0
            name = 'MaxDistanceInTolTime0.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['MaxDistanceInTolTime0.'+str(j)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['MaxDistanceInTolTime0.'+str(j)+'.'+str(k)] = 0
                
            name = 'MaxDistanceInTolTimeL0.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['MaxDistanceInTolTime0.'+str(j+19)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['MaxDistanceInTolTime0.'+str(j+19)+'.'+str(k)] = 0

            name = 'MaxDistanceInTolTime1.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['MaxDistanceInTolTime1.'+str(j)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['MaxDistanceInTolTime1.'+str(j)+'.'+str(k)] = 0
                
            name = 'MaxDistanceInTolTimeL1.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['MaxDistanceInTolTime1.'+str(j+19)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['MaxDistanceInTolTime1.'+str(j+19)+'.'+str(k)] = 0

            name = 'MaxDistanceInTolTime2.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['MaxDistanceInTolTime2.'+str(j)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['MaxDistanceInTolTime2.'+str(j)+'.'+str(k)] = 0
                
            name = 'MaxDistanceInTolTimeL2.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['MaxDistanceInTolTime2.'+str(j+19)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['MaxDistanceInTolTime2.'+str(j+19)+'.'+str(k)] = 0

            name = 'DistBoneInTolTime0.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['DistBoneInTolTime.'+str(j)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['DistBoneInTolTime.'+str(j)+'.'+str(k)] = 0
                
            name = 'DistBoneInTolTimeL0.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['DistBoneInTolTime.'+str(j+19)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['DistBoneInTolTime.'+str(j+19)+'.'+str(k)] = 0


            name = 'DistToClos0.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['DistToClos0.'+str(j)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['DistToClos0.'+str(j)+'.'+str(k)] = 0
                
            name = 'DistToClos1.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['DistToClos1.'+str(j)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['DistToClos1.'+str(j)+'.'+str(k)] = 0

            name = 'DistToClos2.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['DistToClos2.'+str(j)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['DistToClos2.'+str(j)+'.'+str(k)] = 0


            name = 'DistToClosL0.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['DistToClos0.'+str(j+19)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['DistToClos0.'+str(j+19)+'.'+str(k)] = 0
                
            name = 'DistToClosL1.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['DistToClos1.'+str(j+19)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['DistToClos1.'+str(j+19)+'.'+str(k)] = 0

            name = 'DistToClosL2.'+str(j)+'.'+str(k)
            if(name in analysisA.columns):
                analyseMe['DistToClos2.'+str(j+19)+'.'+str(k)] = analysisA[name]
            else:
                analyseMe['DistToClos2.'+str(j+19)+'.'+str(k)] = 0

            for k in range(300, 2050, 50):
                name = 'MaxDistanceInTolTime0.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['MaxDistanceInTolTime0.'+str(j)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['MaxDistanceInTolTime0.'+str(j)+'.'+str(k)] = 0
                    
                name = 'MaxDistanceInTolTimeL0.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['MaxDistanceInTolTime0.'+str(j+19)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['MaxDistanceInTolTime0.'+str(j+19)+'.'+str(k)] = 0

                name = 'MaxDistanceInTolTime1.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['MaxDistanceInTolTime1.'+str(j)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['MaxDistanceInTolTime1.'+str(j)+'.'+str(k)] = 0
                    
                name = 'MaxDistanceInTolTimeL1.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['MaxDistanceInTolTime1.'+str(j+19)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['MaxDistanceInTolTime1.'+str(j+19)+'.'+str(k)] = 0

                name = 'MaxDistanceInTolTime2.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['MaxDistanceInTolTime2.'+str(j)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['MaxDistanceInTolTime2.'+str(j)+'.'+str(k)] = 0
                    
                name = 'MaxDistanceInTolTimeL2.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['MaxDistanceInTolTime2.'+str(j+19)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['MaxDistanceInTolTime2.'+str(j+19)+'.'+str(k)] = 0

                name = 'DistBoneInTolTime0.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['DistBoneInTolTime.'+str(j)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['DistBoneInTolTime.'+str(j)+'.'+str(k)] = 0
                    
                name = 'DistBoneInTolTimeL0.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['DistBoneInTolTime.'+str(j+19)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['DistBoneInTolTime.'+str(j+19)+'.'+str(k)] = 0

                name = 'DistToClos0.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['DistToClos0.'+str(j)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['DistToClos0.'+str(j)+'.'+str(k)] = 0
                    
                name = 'DistToClos1.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['DistToClos1.'+str(j)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['DistToClos1.'+str(j)+'.'+str(k)] = 0

                name = 'DistToClos2.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['DistToClos2.'+str(j)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['DistToClos2.'+str(j)+'.'+str(k)] = 0


                name = 'DistToClosL0.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['DistToClos0.'+str(j+19)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['DistToClos0.'+str(j+19)+'.'+str(k)] = 0
                    
                name = 'DistToClosL1.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['DistToClos1.'+str(j+19)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['DistToClos1.'+str(j+19)+'.'+str(k)] = 0

                name = 'DistToClosL2.'+str(j)+'.'+str(k)
                if(name in analysisA.columns):
                    analyseMe['DistToClos2.'+str(j+19)+'.'+str(k)] = analysisA[name]
                else:
                    analyseMe['DistToClos2.'+str(j+19)+'.'+str(k)] = 0

    analyseMeAllPlanes = pd.DataFrame(columns = ['Config'])
        
    for i in range(0, 135):
        analyseMeAllPlanes.loc[i] = pd.Series({'Config' : i})
        for j in range(0, 38):
            analyseMeAllPlanes['Contact.'+str(j)] = analyseMe['Contact.'+str(j)]

            k = 0
            name = 'DistanceInTolTime.'+str(j)+'.'+str(k)
            val0 = analyseMe['DistToClos0.'+str(j)+'.'+str(k)].iloc[i]
            val1 = analyseMe['DistToClos1.'+str(j)+'.'+str(k)].iloc[i]
            val2 = analyseMe['DistToClos2.'+str(j)+'.'+str(k)].iloc[i]

            if((val0 <= val1) & (val0 <= val2)):
                analyseMeAllPlanes[name] = analyseMe['MaxDistanceInTolTime0.'+str(j)+'.'+str(k)]
            if((val1 <= val2) & (val1 <= val0)):
                analyseMeAllPlanes[name] = analyseMe['MaxDistanceInTolTime1.'+str(j)+'.'+str(k)]
            if((val2 <= val1) & (val2 <= val0)):
                analyseMeAllPlanes[name] = analyseMe['MaxDistanceInTolTime2.'+str(j)+'.'+str(k)]

            nameBoneDist = 'DistBoneInTolTime.'+str(j)+'.'+str(k)
            analyseMeAllPlanes[nameBoneDist] = analyseMe['DistBoneInTolTime.'+str(j)+'.'+str(k)]

            for k in range(300, 2050, 50):
                name = 'DistanceInTolTime.'+str(j)+'.'+str(k)

                nameBoneDist = 'DistBoneInTolTime.'+str(j)+'.'+str(k)
                analyseMeAllPlanes[nameBoneDist] = analyseMe['DistBoneInTolTime.'+str(j)+'.'+str(k)] 

                val0 = analyseMe['DistToClos0.'+str(j)+'.'+str(k)].iloc[i]
                val1 = analyseMe['DistToClos1.'+str(j)+'.'+str(k)].iloc[i]
                val2 = analyseMe['DistToClos2.'+str(j)+'.'+str(k)].iloc[i]

                if((val0 <= val1) & (val0 <= val2)):
                    analyseMeAllPlanes[name] = analyseMe['MaxDistanceInTolTime0.'+str(j)+'.'+str(k)]
                if((val1 <= val2) & (val1 <= val0)):
                    analyseMeAllPlanes[name] = analyseMe['MaxDistanceInTolTime1.'+str(j)+'.'+str(k)]
                if((val2 <= val1) & (val2 <= val0)):
                    analyseMeAllPlanes[name] = analyseMe['MaxDistanceInTolTime2.'+str(j)+'.'+str(k)]

    meanDistInTol = pd.DataFrame()

    for i in range(0, 135):
        progress(i+135, 5*135)
        #meanDistInTol.loc[i] = pd.Series({'Config' : i})
        for j in range(0, 38):
            k = 0
            name = 'DistanceInTolTime.'+str(j)+'.'+str(k)
            nbPlanes = 0
            meanDist = 0
            for m in range(0, 3):
                #print(m, j, analyseMe['TimeInTol'+str(m)+'.'+str(j)+'.'+str(k)].loc[i], np.isnan(analyseMe['TimeInTol'+str(m)+'.'+str(j)+'.'+str(k)].loc[i]))
                if(np.isnan(analyseMeAllPlanes['DistanceInTolTime.'+str(j)+'.'+str(k)].loc[i]) == False):
                    nbPlanes = nbPlanes + 1
                    meanDist = meanDist + analyseMeAllPlanes['DistanceInTolTime.'+str(j)+'.'+str(k)].iloc[i]
                    
            if(nbPlanes != 0):
                meanDistInTol.loc[i, name] = meanDist/nbPlanes
                nameBoneDist = 'DistBoneInTolTime.'+str(j)+'.'+str(k);
                meanDistInTol.loc[i, nameBoneDist] = analyseMeAllPlanes[nameBoneDist].iloc[i]
            else:
                meanDistInTol.loc[i, name] = 0

            for k in range(300, 2050, 50):
                name = 'DistanceInTolTime.'+str(j)+'.'+str(k)
                nbPlanes = 0
                meanDist = 0
                for m in range(0, 3):
                    #print(m, j, analyseMe['TimeInTol'+str(m)+'.'+str(j)+'.'+str(k)].loc[i], np.isnan(analyseMe['TimeInTol'+str(m)+'.'+str(j)+'.'+str(k)].loc[i]))
                    if(np.isnan(analyseMeAllPlanes['DistanceInTolTime.'+str(j)+'.'+str(k)].loc[i]) == False):
                        nbPlanes = nbPlanes + 1
                        meanDist = meanDist + analyseMeAllPlanes['DistanceInTolTime.'+str(j)+'.'+str(k)].iloc[i]
                        
                if(nbPlanes != 0):
                    meanDistInTol.loc[i, name] = meanDist/nbPlanes
                    nameBoneDist = 'DistBoneInTolTime.'+str(j)+'.'+str(k);
                    meanDistInTol.loc[i, nameBoneDist] = analyseMeAllPlanes[nameBoneDist].iloc[i]
                else:
                    meanDistInTol.loc[i, name] = 0

    meanDistPerTol = pd.DataFrame()
    meanDistHand = pd.DataFrame()

    nbPlanes = 0
    meanDistPhal = 0
    meanDistAllPhal = 0
    nbPhal = 0
    meanDistMorePhal = 0

    for i in range(0, 135):
        progress(i + 2*135, 5*135)
        
        k = 0
        name = 'DistanceInTolTime.'+str(k)
        nameBoneDist = 'DistBoneInTolTime.'+str(k)
        nbPhal = 0
        meanDistPhal = 0
        meanBoneDistPhal = 0
        for j in range(0, 38):
            if(meanDistInTol['DistanceInTolTime.'+str(j)+'.'+str(k)].iloc[i] != 0):
                meanDistPhal = meanDistPhal + meanDistInTol['DistanceInTolTime.'+str(j)+'.'+str(k)].iloc[i]
                meanBoneDistPhal = meanBoneDistPhal + meanDistInTol['DistBoneInTolTime.'+str(j)+'.'+str(k)].iloc[i]

                nbPhal = nbPhal + 1
                if(meanDistInTol['DistanceInTolTime.'+str(j)+'.'+str(k)].dropna().mean() > 0):
                    meanDistPerTol.loc[k, 'Phalanx.'+str(j)] = meanDistInTol['DistanceInTolTime.'+str(j)+'.'+str(k)].dropna().mean()   
                    meanDistPerTol.loc[k, 'BoneDist.'+str(j)] = meanDistInTol['DistBoneInTolTime.'+str(j)+'.'+str(k)].dropna().mean()

                else:
                    meanDistPerTol.loc[k, 'Phalanx.'+str(j)] = 0
                    meanDistPerTol.loc[k, 'BoneDist.'+str(j)] = 0

        if(nbPhal != 0):
            meanDistHand.loc[i, name] = meanDistPhal/nbPhal
            meanDistHand.loc[i, nameBoneDist] = meanBoneDistPhal/nbPhal

        for k in range(300, 2050, 50):
            name = 'DistanceInTolTime.'+str(k)
            nameBoneDist = 'DistBoneInTolTime.'+str(k)
            nbPhal = 0
            meanDistPhal = 0
            meanBoneDistPhal = 0
            for j in range(0, 38):
                if(meanDistInTol['DistanceInTolTime.'+str(j)+'.'+str(k)].iloc[i] != 0):
                    meanDistPhal = meanDistPhal + meanDistInTol['DistanceInTolTime.'+str(j)+'.'+str(k)].iloc[i]
                    meanBoneDistPhal = meanBoneDistPhal + meanDistInTol['DistBoneInTolTime.'+str(j)+'.'+str(k)].iloc[i]
                    nbPhal = nbPhal + 1
                    if(meanDistInTol['DistanceInTolTime.'+str(j)+'.'+str(k)].dropna().mean() > 0):
                        meanDistPerTol.loc[k, 'Phalanx.'+str(j)] = meanDistInTol['DistanceInTolTime.'+str(j)+'.'+str(k)].dropna().mean()   
                        meanDistPerTol.loc[k, 'BoneDist.'+str(j)] = meanDistInTol['DistBoneInTolTime.'+str(j)+'.'+str(k)].dropna().mean()
                    else:
                        meanDistPerTol.loc[k, 'Phalanx.'+str(j)] = 0
                        meanDistPerTol.loc[k, 'BoneDist.'+str(j)] = 0                    

            if(nbPhal != 0):
                meanDistHand.loc[i, name] = meanDistPhal/nbPhal
                meanDistHand.loc[i, nameBoneDist] = meanBoneDistPhal/nbPhal            

    if(write == str('Y')):
        meanDistPerTol.to_csv('./Analysis/TimeTolerance/'+ date + '/Planes/G-'+str(gamma)+'B-'+str(beta)+'-'+date+word+addword+'.csv', sep = ';')

    plt.figure(figsize=(20,10))
    for j in range(6, 19, 3):
        plt.plot(meanDistPerTol.index/1000, meanDistPerTol['Phalanx.'+str(j)], label = boneNames[j])
        
        plt.title('Tolerance (mm) function of Time Prediction Known (s), per Phalanx')
        plt.legend()
        
    if(write == str('Y')):
        plt.savefig('./Analysis/TimeTolerance/'+ date + '/Planes/G-'+str(gamma)+'B-'+str(beta)+'-R-FingerTips-'+date+word+addword+'.pdf')

    for j in range(25, 38, 3):
        plt.plot(meanDistPerTol.index/1000, meanDistPerTol['Phalanx.'+str(j)], label = boneNames[j])
        
        plt.title('Tolerance (mm) function of Time Prediction Known (s), per Phalanx')
        plt.legend()
        
    if(write == str('Y')):
        plt.savefig('./Analysis/TimeTolerance/'+ date + '/Planes/G-'+str(gamma)+'B-'+str(beta)+'-All-FingerTips-'+date+word+addword+'.pdf')

    plt.figure(figsize=(20,10))
    for j in range(6, 19, 3):
        plt.plot(meanDistPerTol.index/1000, meanDistPerTol['Phalanx.'+str(j)], label = 'Difference Predict/ContactPos.' + boneNames[j])
        plt.plot(meanDistPerTol.index/1000, meanDistPerTol['BoneDist.'+str(j)], label = 'Difference Pos/ContactPos.' + boneNames[j])
        
        plt.title('Distance between Prediction and Bones Position to Bone Final Contact Position, function of Time Prediction Known (s), per Phalanx')
        plt.legend()
        
    if(write == str('Y')):
        plt.savefig('./Analysis/TimeTolerance/'+ date + '/Planes/G-'+str(gamma)+'B-'+str(beta)+'-R-BoneAndPredict-FingerTips-'+date+word+addword+'.pdf')

    plt.figure(figsize=(20,10))
    for j in range(25, 38, 3):
        plt.plot(meanDistPerTol.index/1000, meanDistPerTol['Phalanx.'+str(j)], label = 'Difference Predict/ContactPos.' + boneNames[j])
        plt.plot(meanDistPerTol.index/1000, meanDistPerTol['BoneDist.'+str(j)], label = 'Difference Pos/ContactPos.' + boneNames[j])
        
        plt.title('Distance between Prediction and Bones Position to Bone Final Contact Position, function of Time Prediction Known (s), per Phalanx')
        plt.legend()
        
    if(write == str('Y')):
        plt.savefig('./Analysis/TimeTolerance/'+ date + '/Planes/G-'+str(gamma)+'B-'+str(beta)+'-L-BoneAndPredict-FingerTips-'+date+word+addword+'.pdf')


    plt.figure(figsize=(20,10))
    for j in range(25, 38, 3):
        plt.plot(meanDistPerTol.index/1000, meanDistPerTol['Phalanx.'+str(j)], label = boneNames[j])
        
        plt.title('Tolerance (mm) function of Time Prediction Known (s), per Phalanx')
        plt.legend()
        
    if(write == str('Y')):
        plt.savefig('./Analysis/TimeTolerance/'+ date + '/Planes/G-'+str(gamma)+'B-'+str(beta)+'-L-FingerTips-'+date+word+addword+'.pdf')


    plt.figure(figsize=(20,10))
    for j in range(0, 19):
        plt.plot(meanDistPerTol.index/1000, meanDistPerTol['Phalanx.'+str(j)], label = boneNames[j])
        
        plt.title('Tolerance (mm) function of Time Prediction Known (s), per Phalanx')
        plt.legend()
        
    if(write == str('Y')):
        plt.savefig('./Analysis/TimeTolerance/'+ date + '/Planes/G-'+str(gamma)+'B-'+str(beta)+'-R-AllFingers-'+date+word+addword+'.pdf')
                    
    plt.figure(figsize=(20,10))
    for j in range(0, 19):
        plt.plot(meanDistPerTol.index/1000, meanDistPerTol['Phalanx.'+str(j+19)], label = boneNames[j+19])
        
        plt.title('Tolerance (mm) function of Time Prediction Known (s), per Phalanx')
        plt.legend()
        
    if(write == str('Y')):
        plt.savefig('./Analysis/TimeTolerance/'+ date + '/Planes/G-'+str(gamma)+'B-'+str(beta)+'-L-AllFingers-'+date+word+addword+'.pdf')
         
    tasks = ['Touch', 'Raise', 'Pull', 'Push', 'Push Down']
    objects = ['obj0', 'obj1', 'obj2', 'obj3', 'obj4', 'obj5', 'obj6', 'Cube', 'Cylinder']
    scales = ['small', 'medium', 'large']

    config = 0
    for i in range(0, 135):
        progress(i + 3*135, 5*135)
        config = i
        for k in range(0, len(scales)):
            if((config < len(tasks)*len(objects)*(k+1)) & (config >= len(tasks)*len(objects)*(k))):
                taskToDo = tasks[config%len(tasks)]
                for j in range(0, len(objects)):
                    if((config < len(tasks)*(len(objects)*k + j+1)) & (config >= len(tasks)*(len(objects)*k + j))):
                        objToCatch = objects[j]
                objScale = scales[k]
                
        analyseMe.loc[i, 'Task'] = taskToDo
        analyseMe.loc[i, 'Obj'] = objToCatch
        analyseMe.loc[i, 'Scale'] = objScale
        
        meanDistInTol.loc[i, 'Task'] = taskToDo
        meanDistInTol.loc[i, 'Obj'] = objToCatch
        meanDistInTol.loc[i, 'Scale'] = objScale

        meanDistHand.loc[i, 'Task'] = taskToDo
        meanDistHand.loc[i, 'Obj'] = objToCatch
        meanDistHand.loc[i, 'Scale'] = objScale
        
        analyseMeAllPlanes.loc[i, 'Task'] = taskToDo
        analyseMeAllPlanes.loc[i, 'Obj'] = objToCatch
        analyseMeAllPlanes.loc[i, 'Scale'] = objScale
        

    meanDistPerTask = pd.DataFrame()
    meanDistPerObject = pd.DataFrame()
    meanDistPerScale = pd.DataFrame()

    for i in range(0, len(tasks)):
        condTask = meanDistHand['Task'] == tasks[i]
        for j in range(0, 38):
            meanDistPerTask.loc['0', tasks[i]] = meanDistHand[condTask]['DistanceInTolTime.0'].dropna().mean()
            for k in range(300, 2050, 50):
                meanDistPerTask.loc[k, tasks[i]] = meanDistHand[condTask]['DistanceInTolTime.'+str(k)].dropna().mean()
                
    for i in range(0, len(objects)):
        condObjects = meanDistHand['Obj'] == objects[i]
        for j in range(0, 38):
            meanDistPerObject.loc['0', objects[i]] = meanDistHand[condObjects]['DistanceInTolTime.0'].dropna().mean()   
            for k in range(300, 2050, 50):
                meanDistPerObject.loc[k, objects[i]] = meanDistHand[condObjects]['DistanceInTolTime.'+str(k)].dropna().mean()   

    for i in range(0, len(scales)):
        condScales = meanDistHand['Scale'] == scales[i]
        for j in range(0, 38):
            meanDistPerScale.loc['0', scales[i]] = meanDistHand[condScales]['DistanceInTolTime.0'].dropna().mean()
            for k in range(300, 2050, 50):
                meanDistPerScale.loc[k, scales[i]] = meanDistHand[condScales]['DistanceInTolTime.'+str(k)].dropna().mean()     

    if(write == str('Y')):
     	meanDistPerObject.to_csv('./Analysis/TimeTolerance/'+ date + '/Planes/G-'+str(gamma)+'B-'+str(beta)+'-perObject-'+date+word+addword+'.csv', sep = ';')
    if(write == str('Y')):
        meanDistPerTask.to_csv('./Analysis/TimeTolerance/'+ date + '/Planes/G-'+str(gamma)+'B-'+str(beta)+'-perTask-'+date+word+addword+'.csv', sep = ';')
    if(write == str('Y')):
        meanDistPerScale.to_csv('./Analysis/TimeTolerance/'+ date + '/Planes/G-'+str(gamma)+'B-'+str(beta)+'-perScale-'+date+word+addword+'.csv', sep = ';')
    if(write == str('Y')):
        meanDistHand.to_csv('./Analysis/TimeTolerance/'+ date + '/Planes/G-'+str(gamma)+'B-'+str(beta)+'-AllConfigTypes-'+date+word+addword+'.csv', sep = ';')


    boneDistPerTask = pd.DataFrame()
    boneDistPerObject = pd.DataFrame()
    boneDistPerScale = pd.DataFrame()

    for i in range(0, len(tasks)):
        condTask = meanDistHand['Task'] == tasks[i]
        for j in range(0, 38):
            boneDistPerTask.loc['0', tasks[i]] = meanDistHand[condTask]['DistBoneInTolTime.0'].dropna().mean()
            for k in range(300, 2050, 50):
                boneDistPerTask.loc[k, tasks[i]] = meanDistHand[condTask]['DistBoneInTolTime.'+str(k)].dropna().mean()
                
    for i in range(0, len(objects)):
        condObjects = meanDistHand['Obj'] == objects[i]
        for j in range(0, 38):
            boneDistPerObject.loc['0', objects[i]] = meanDistHand[condObjects]['DistBoneInTolTime.0'].dropna().mean()   
            for k in range(300, 2050, 50):
                boneDistPerObject.loc[k, objects[i]] = meanDistHand[condObjects]['DistBoneInTolTime.'+str(k)].dropna().mean()   

    for i in range(0, len(scales)):
        condScales = meanDistHand['Scale'] == scales[i]
        for j in range(0, 38):
            boneDistPerScale.loc['0', scales[i]] = meanDistHand[condScales]['DistBoneInTolTime.0'].dropna().mean()
            for k in range(300, 2050, 50):
                boneDistPerScale.loc[k, scales[i]] = meanDistHand[condScales]['DistBoneInTolTime.'+str(k)].dropna().mean()

    if(write == str('Y')):
        boneDistPerObject.to_csv('./Analysis/TimeTolerance/'+ date + '/Planes/G-'+str(gamma)+'B-'+str(beta)+'-perObject-BoneDist-'+date+word+addword+'.csv', sep = ';')
    if(write == str('Y')):
        boneDistPerTask.to_csv('./Analysis/TimeTolerance/'+ date + '/Planes/G-'+str(gamma)+'B-'+str(beta)+'-perTask-BoneDist-'+date+word+addword+'.csv', sep = ';')
    if(write == str('Y')):
        boneDistPerScale.to_csv('./Analysis/TimeTolerance/'+ date + '/Planes/G-'+str(gamma)+'B-'+str(beta)+'-perScale-BoneDist-'+date+word+addword+'.csv', sep = ';')

    progress(100, 100)
    print('\n')

read = 0
for read in range(0, len(list_beta)):
    gamma = list_gamma[read]
    beta = list_beta[read]
    if(write == str('Y')):
        shutil.move('./Analysis/TimeTolerance/G-'+str(gamma)+'B-'+str(beta)+'-'+ date + '-FullAnalysis-'+ word + '.csv', './Analysis/SingleParameterTimeTol/' + date + '-G-'+str(gamma)+'B-'+str(beta)+'-'+ date + '-FullAnalysis-'+ word + '.csv')
    if(write == str('Y')):
        shutil.move('./Analysis/TimeTolerance/G-'+str(gamma)+'B-'+str(beta)+'-'+ date + '-DescribeDf-'+ word + '.csv', './Analysis/SingleParameterTimeTol/'+date+'-G-'+str(gamma)+'B-'+str(beta)+'-'+ date + '-DescribeDf-'+ word + '.csv')


