import numpy as np
from scipy.stats import norm
from scipy.optimize import curve_fit

import pandas as pd
import matplotlib.pyplot as plt


emp_data = None
date = '09112020-1104'
emp_data = pd.read_excel('../Assets/Resources/DataSimulation/Speeds/' + date + '.xlsx')

positions = None
positions = pd.DataFrame()
positions['Time'] = emp_data['Time']
positions['Phalanx_0'] = emp_data['RightPos']

for i in range(1,19):
    positions['Phalanx_' + str(i) ] = emp_data['RightPos.' + str(i)]

speeds = None
filter_choice = 30
speeds = pd.DataFrame()
speeds['Time'] = positions['Time'][0:len(emp_data['Time']) - filter_choice]
for i in range(0, 19):
    for j in range(0, len(emp_data['Time']) - filter_choice):
        speeds['Phalanx_' + str(i)] =  (positions['Phalanx_' + str(i)][j+filter_choice] - positions['Phalanx_' + str(i) ][j])/(positions['Time'][j+filter_choice] - positions['Time'][j])

for i in range(0, 19):
    for j in range(0, len(emp_data['Time']) - filter_choice):
        speeds['Phalanx_' + str(i)].loc[j] = (positions['Phalanx_' + str(i)][j+filter_choice] - positions['Phalanx_' + str(i) ][j])/(positions['Time'][j+filter_choice] - positions['Time'][j])


threshold = 0.03 #m/s
v_start = abs(speeds['Phalanx_0'] >= threshold)
index_start = None
index_start = pd.DataFrame()
index_stop = None
index_stop = pd.DataFrame()
j = 0
k = 0

for i in range(0, len(speeds) - 1):
    if((v_start[i+1] == True) & (v_start[i] == False)):
        #index_start = index_start, i+1
        index_start[j] = i+1
    if((v_start[i+1] == False) & (v_start[i] == True)):
        index_stop[k] = i+1
    
for i in range(0, len(speeds) - 1):
    if((v_start[i+1] == True) & (v_start[i] == False)):
        #index_start = index_start, i+1
        j = j+1
    index_start.loc[j] = i+1
    if((v_start[i+1] == False) & (v_start[i] == True)):
        k = k+1
    index_stop.loc[k] = i+1 
    
index_start = index_start.transpose()
index_stop = index_stop.transpose()


window = 10

input_data = None
input_data = speeds['Phalanx_0'][index_start[0][0]:index_start[0][0]+window]

def jerk_model( time_step, a0, a1, a2):
    
    return a0 * ( ( time_step - a1)**2 ) * ( (time_step - a2)**2 )


init_vals = [0.1,0.1,0.1]
best_vals, covar = curve_fit( jerk_model, np.arange( window ), input_data)#, p0 = init_vals )
print( "best parameters 1: ", best_vals )


simulated_speed = jerk_model(np.arange(20), *best_vals)


plt.plot(index_start[0][0] + np.arange(window), input_data, label = 'Input')
#plt.plot(index_start[0][0] + np.arange(window), simulated_speed[index_start[0][0]:index_start[0][0]+window], label = 'Fit')
plt.plot(index_start[0][0] + np.arange(20), simulated_speed, label = 'Fit')


plt.legend()
plt.show()

v_stop = (abs(simulated_speed) < threshold)


index_predicted_stop = None
index_predicted_stop = pd.DataFrame()
k = 0

for i in range(0, 20 - 1):
    if((v_stop[i+1] == True) & (v_stop[i] == False)):
        index_predicted_stop[k] = i+1
    
for i in range(0, 20 - 1):
    if((v_stop[i+1] == True) & (v_stop[i] == False)):
        k = k+1
    index_predicted_stop.loc[k] = i+1

print("Predicted Movement Duration: " + str(speeds['Time'][index_start[0][0] + index_predicted_stop[0][0]] - speeds['Time'][index_start[0][0]]) )
print("Real Movement Duration: " + str( speeds['Time'][index_stop[0][0]] - speeds['Time'][index_start[0][0]]) )



def jerk_model_real( t, tf, xf):
    
    return (xf)/(tf) * (30*((t)/(tf))**2 - 60*((t)/(tf))**3 + 30*((t)/(tf))**4)


input_data_2 = None
window_2 = 50
input_data_2 = speeds['Phalanx_0'][index_start[1][0]:index_start[1][0]+window_2]
#xdata_2 = np.linspace(index_start[1][0], 1, index_start[1][0]+50)
#best_vals_2, covar = curve_fit( jerk_model_real, speeds['Time'][index_start[1][0]:index_start[1][0]+50], input_data_2)#np.arange(50), input_data_2)

best_vals_2, covar = curve_fit( jerk_model_real, np.arange(window_2), input_data_2)
print( "best parameters: ", best_vals_2 )

plt.plot(index_start[1][0] + np.arange(window_2), jerk_model_real(np.arange(window_2), *best_vals_2), 'r-', label = 'Fit')

plt.plot(input_data_2, label = 'Input')
plt.legend()
plt.show()

simulated_speed_2 =  jerk_model_real(np.arange(window_2), *best_vals_2)


v_stop_2 = simulated_speed_2 < threshold


index_predicted_stop_2 = None
index_predicted_stop_2 = pd.DataFrame()
k = 0

for i in range(0, window_2 - 1):
    if((v_stop_2[i+1] == True) & (v_stop_2[i] == False)):
        index_predicted_stop_2[k] = i+1
    
for i in range(0, window_2 - 1):
    if((v_stop_2[i+1] == True) & (v_stop_2[i] == False)):
        k = k+1
    index_predicted_stop_2.loc[k] = i+1



print("Predicted Movement Duration: " + str(speeds['Time'][250 + index_predicted_stop_2[0][0]] - speeds['Time'][index_start[1][0]]))
print("Real Movement Duration: " + str( speeds['Time'][index_stop[1][0]] - speeds['Time'][index_start[1][0]]) )
print("Difference Predicted/Real : "+ str(abs((speeds['Time'][250 + index_predicted_stop_2[0][0]] - speeds['Time'][index_start[1][0]])
                                              - (speeds['Time'][index_stop[1][0]] - speeds['Time'][index_start[1][0]]))) + " s")

def pos_jerk(t, xf):
    return xi + (xf - xi)*(10*((t)/(tf))**3 - 15*((t)/(tf))**4 + 6*((t)/(tf))**5)

xi = positions['Phalanx_0'][index_start[1][0]]
tf = (positions['Time'][250 + index_predicted_stop_2[0][0]] - positions['Time'][index_start[1][0]])
t = positions['Time'][index_start[1][0]:index_start[1][0]+window_2] - positions['Time'][index_start[1][0]]

best_vals_pos, covar = curve_fit(pos_jerk, t, positions['Phalanx_0'][index_start[1][0]:index_start[1][0]+window_2])
print( "best parameters: ", best_vals_pos )

predicted_pos = pos_jerk(t, best_vals_pos[0])

plt.plot(positions['Time'][index_start[1][0]:index_start[1][0]+window_2], xi + (best_vals_pos[0] - xi)*(10*(t/tf)**3 - 15*(t/tf)**4 + 6*(t/tf)**5), label = 'Fit')
#plt.plot(positions['Time'][index_start[1][0]:index_start[1][0]+window_2], predicted_pos, label = 'PredictedPos')
plt.plot(positions['Time'][index_start[1][0]:index_start[1][0]+window_2], positions['Phalanx_0'][index_start[1][0]:index_start[1][0]+window_2], label = 'Input')
plt.legend()
plt.show()


predicted_pos[index_start[1][0]] - positions['Phalanx_0'][index_start[1][0]]
print("Difference Position : " + str(abs((predicted_pos[index_start[1][0] + index_predicted_stop_2[0][0]] - positions['Phalanx_0'][index_stop[1][0]]) * 100)) + " cm")
