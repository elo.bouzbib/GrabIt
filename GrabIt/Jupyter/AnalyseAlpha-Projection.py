import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import shutil
import sys

# date = 'User1'
# word = 'graspContact'
# addword = 'test'
date = input("Enter User Name or Date:")
alpha = input("Enter Alpha:")
#word = input("Enter Word:")
word = ''
#addWord = input("Enter AddWord:")
addword = ''
projectionData = None

# stopPredict = input("Stop Predictions in Name? (Y/N)")
# if(stopPredict == str('Y')):
#     projectionData = pd.read_csv('../Assets/Resources/DataSimulation/AlgoHints/A-' + str(alpha) + '-' + word + date + '-stopPredictions.csv', sep = ";", decimal = '.')
# else:
projectionData = pd.read_csv('../Assets/Resources/DataSimulation/AlgoHints/A-' + str(alpha) + '-' + word + date + '.csv', sep = ";", decimal = '.')


#projectionData = pd.read_csv('../Assets/Resources/DataSimulation/fromQuest/'+ word + date + '-stopPredictions.csv', sep = ";", decimal = '.')
# projectionData = pd.read_csv('../Assets/Resources/DataSimulation/AlgoHints/A-' + str(alpha) + '-' + word + date + '-stopPredictions.csv', sep = ";", decimal = '.')

projectionData = projectionData.dropna()


projectionData['BoneName.0'] = projectionData['BoneName']
projectionData['BoneNameL.0'] = projectionData['BoneNameL']

projectionData['BonePos.0'] = projectionData['BonePos']
projectionData['BonePosL.0'] = projectionData['BonePosL']

projectionData['PosPred.0'] = projectionData['PosPred']

projectionData['PosPredL.0'] = projectionData['PosPredL']

projectionData['DistToPred.0'] = projectionData['DistToPred']

projectionData['DistToPred0L.0'] = projectionData['DistToPredL']

projectionData['BoneRadius.0'] = projectionData['BoneRadius']
projectionData['BoneContact.0'] = projectionData['BoneContact']

projectionData['BoneRadiusL.0'] = projectionData['BoneRadiusL']
projectionData['BoneContactL.0'] = projectionData['BoneContactL']

projectionData['ClosestPoint.0'] = projectionData['ClosestPoint']
projectionData['ClosestPointL.0'] = projectionData['ClosestPointL']

def progress(count, total, suffix=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', suffix))
    sys.stdout.flush()  # As suggested by Rom Ruben


for i in range(0, 135):
    cond1 = projectionData['Config'] == i
    projectionData = projectionData.drop(projectionData[projectionData['Config'] == i].index[0:2])

for j in range(0,19):
    projectionData.loc[projectionData['BoneContact.'+str(j)] == True, 'Touching.'+str(j)] = projectionData['BonePos.'+str(j)]
    projectionData.loc[projectionData['BoneContactL.'+str(j)] == True, 'TouchingL.'+str(j)] = projectionData['BonePosL.'+str(j)]

    
predictionsData = None
predictionsData = pd.DataFrame(columns = ['Alpha'])


for i in range(0,135):
    indexContact = 0
    indexPrediction = 0
    indexStart = 0
    indexTamere = 0

    indexCond = 0

    indexContactL = 0
    indexPredictionL = 0
    indexStartL = 0
    indexTamereL = 0
    
    progress(i, 135)

    if(len(projectionData[projectionData['Config'] == i]) != 0):
        cond1 = (projectionData['Config'] == i)
        predictionsData.loc[i] = pd.Series({'Alpha' : projectionData[cond1]['Alpha'].iloc[0]})

        for j in range(0, 19):                                                
            
            predictionsData.loc[i, 'Contact.'+str(j)] = len(projectionData[cond1]['Touching.'+str(j)].dropna()) != 0
            predictionsData.loc[i, 'ContactL.'+str(j)] = len(projectionData[cond1]['TouchingL.'+str(j)].dropna()) != 0

            if(len(projectionData[cond1]['Touching.'+str(j)].dropna()) != 0):
                indexContact = projectionData[cond1]['Touching.'+str(j)].dropna().index[0]
                # print('Phalanx ', str(j), 'Time ', "{:.3f}".format(projectionData['Time'][indexContact]))
                
                if(np.isnan(projectionData['Touching.'+str(j)][indexContact]) == False):
                    # print('PosAtContact:' , "{:.3f}".format(projectionData['Touching.'+str(j)][indexContact]),
                    #       'PosPRED-AtContact:' , "{:.3f}".format(projectionData['PosPred.'+str(j)][indexContact]))

                    
                    # plt.figure(figsize=(20,10))

                    indexCond = projectionData[cond1]['Time'].index

                    for k in range(50, 0, -5):
                        tolerance = k
                        projectionData.loc[1000*(abs(projectionData['PosPred.'+str(j)][indexCond] - projectionData['BonePos.'+str(j)][indexContact]) - projectionData['BoneRadius.'+str(j)]) <= tolerance, 'InGoodTol0.'+str(j)+'.'+str(k)] = True
                        
                        if(len(projectionData['InGoodTol0.'+str(j)+'.'+str(k)][indexCond].dropna()) != 0):
                            indexTamere = projectionData['InGoodTol0.'+str(j)+'.'+str(k)][indexCond].dropna().index
                            if(len(indexTamere) != 0):
                                if(indexContact in indexTamere):
                                    for q in range(0, len(indexTamere)):
                                        if(indexTamere[q] == indexContact):
    #                                        print('Success!')
                                            indexStart = indexTamere[q]
                                            for p in range(0, len(indexTamere)):
                                                if(indexTamere[q-p] == indexContact-p):
                                                    indexPrediction = indexTamere[q-p]

                                    if( (projectionData['Time'][indexContact] - projectionData['Time'][indexPrediction]) > 0 ):
                                        # print('Phalanx ', str(j), 'Tolerance ', k, 'IndexContact ', indexContact, 'IndexPrediction', indexPrediction , 'TimeContact', "{:.2f}".format(projectionData['Time'][indexContact]), 'TimeInTolerance', "{:.2f}".format(projectionData['Time'][indexPrediction]))
                                        # print('TimeKnown! ', "{:.2f}".format(projectionData['Time'][indexContact] - projectionData['Time'][indexPrediction]),
                                        #       'PosDifference0 ', "{:.3f}".format(1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred.'+str(j)][indexPrediction]) - projectionData['BoneRadius.'+str(j)][indexPrediction])),
                                        #      'PosClosestPred0 ', "{:.3f}".format(1000*(abs(projectionData['ClosestPoint.'+str(j)][indexPrediction] - projectionData['PosPred.'+str(j)][indexPrediction]) - projectionData['BoneRadius.'+str(j)][indexPrediction])))
                                        # # plt.vlines(projectionData['Time'][indexPrediction], 0, k, linestyles = 'dashed', colors = 'red', label = 'Prediction0, Tol.'+str(k))
                                        
                                        nameDist = 'DistInTol.'+str(j)+'.'+str(k)
                                        nameTime = 'TimeTol.'+str(j)+'.'+str(k)
                                        nameClos = 'DistToClos.'+str(j)+'.'+str(k)

                                        predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['Touching.'+str(j)][indexContact] - projectionData['PosPred.'+str(j)][indexPrediction]) - projectionData['BoneRadius.'+str(j)][indexPrediction])
                                        predictionsData.loc[i, nameTime] = projectionData['Time'][indexContact] - projectionData['Time'][indexPrediction]
                                        predictionsData.loc[i, nameClos] = 1000*(abs(projectionData['ClosestPoint.'+str(j)][indexPrediction] - projectionData['PosPred.'+str(j)][indexPrediction]) - projectionData['BoneRadius.'+str(j)][indexPrediction])

                                    else:
                                        pass
                                        # print('Config' + str(i), 'Phalanx. ' + str(j), 'NoPredictions in Tolerance ' + str(k), ' with Plane 0')
                                else:
                                    pass
                                    # print('Config' + str(i), 'Phalanx. ' + str(j), 'NoPredictions in Tolerance ' + str(k), ' with Plane 0')
                            else:
                                pass
                                # print('Config' + str(i), 'Phalanx. ' + str(j), 'NoPredictions in Tolerance ' + str(k), ' with Plane 0')
                        
                        
                        # plt.scatter(projectionData['Time'][indexCond], k*projectionData['InGoodTol0.'+str(j)+'.'+str(k)][indexCond], label = 'InGoodTol0.'+str(j)+'.'+str(k))
                        
                    # plt.vlines(projectionData['Time'][indexContact], 0, 53, linestyles = 'dashed', colors = 'cyan', label = 'Contact!')
                    # plt.scatter(projectionData['Time'][indexCond], projectionData['BoneContact.'+str(j)][indexCond], label = 'BoneContact')

                    # plt.title('Config' + str(i) + '; Phalanx ' + str(j) + projectionData['BoneName.'+str(j)].iloc[0])
                    # plt.legend()

                
                else:
                    pass
                    # print('Config', str(i), ' Fail!')
        
        for j in range(0, 19):
            if(len(projectionData[cond1]['TouchingL.'+str(j)].dropna()) != 0):
                indexContactL = projectionData[cond1]['TouchingL.'+str(j)].dropna().index[0]
                #print(projectionData['Time'][indexContact])
                # print('PhalanxL ', str(j), 'Time ', "{:.3f}".format(projectionData['Time'][indexContactL]))

                if(np.isnan(projectionData['TouchingL.'+str(j)][indexContactL]) == False):
                    # print('PosAtContactL:' , "{:.3f}".format(projectionData['TouchingL.'+str(j)][indexContactL]),
                    #       'PosPRED0-AtContactL:' , "{:.3f}".format(projectionData['PosPredL.'+str(j)][indexContactL]))

                    # print('Diff? (mm)',
                    #       'Plan0L: ', "{:.3f}".format(1000*(abs(projectionData['PosPredL.'+str(j)][indexContactL] - projectionData['BonePosL.'+str(j)][indexContactL]) - projectionData['BoneRadiusL.'+str(j)][indexContactL])),
                    #      'Plan0LClos: ', "{:.3f}".format(1000*(abs(projectionData['PosPredL.'+str(j)][indexContactL] - projectionData['ClosestPointL.'+str(j)][indexContactL]) - projectionData['BoneRadiusL.'+str(j)][indexContactL])))

                    # plt.figure(figsize=(20,10))

                    indexCond = projectionData[cond1]['Time'].index

                    for k in range(50, 0, -5):
                        tolerance = k
                        projectionData.loc[1000*(abs(projectionData['PosPredL.'+str(j)][indexCond] - projectionData['BonePosL.'+str(j)][indexContactL]) - projectionData['BoneRadiusL.'+str(j)]) <= tolerance, 'InGoodTolL0.'+str(j)+'.'+str(k)] = True
                        
                        if(len(projectionData['InGoodTolL0.'+str(j)+'.'+str(k)][indexCond].dropna()) != 0):
                            indexTamereL = projectionData['InGoodTolL0.'+str(j)+'.'+str(k)][indexCond].dropna().index
                            if(len(indexTamereL) != 0):
                                if(indexContactL in indexTamereL):
                                    for q in range(0, len(indexTamereL)): #Need to modify -> if indexContact in indexTamere
                                        if(indexTamereL[q] == indexContactL):
    #                                        print('Success!')
                                            indexStartL = indexTamereL[q]
                                            for p in range(0, len(indexTamereL)):
                                                if(indexTamereL[q-p] == indexContactL-p):
                                                    indexPredictionL = indexTamereL[q-p]

                                    if( (projectionData['Time'][indexContactL] - projectionData['Time'][indexPredictionL]) > 0 ):
                                        # print('PhalanxL ', str(j), 'Tolerance ', k, 'IndexContactL ', indexContactL, 'IndexPrediction', indexPredictionL, 'TimeContact', "{:.2f}".format(projectionData['Time'][indexContactL]), 'TimeInTolerance', "{:.2f}".format(projectionData['Time'][indexPredictionL]))
                                        # print('TimeKnownL! ', "{:.2f}".format(projectionData['Time'][indexContactL] - projectionData['Time'][indexPredictionL]),
                                        #       'PosDifferenceL ', "{:.3f}".format(1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL.'+str(j)][indexPredictionL]) - projectionData['BoneRadiusL.'+str(j)][indexPredictionL])),
                                        #      'PosClosestPredL ', "{:.3f}".format(1000*(abs(projectionData['ClosestPointL.'+str(j)][indexPredictionL] - projectionData['PosPredL.'+str(j)][indexPredictionL]) - projectionData['BoneRadiusL.'+str(j)][indexPredictionL])))
                                        # # plt.vlines(projectionData['Time'][indexPredictionL], 0, k, linestyles = 'dashed', colors = 'red', label = 'PredictionL0, Tol.'+str(k))
                                        
                                        nameDist = 'DistInTolL.'+str(j)+'.'+str(k)
                                        nameTime = 'TimeTolL.'+str(j)+'.'+str(k)
                                        nameClos = 'DistToClosL.'+str(j)+'.'+str(k)

                                        predictionsData.loc[i, nameDist] = 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL.'+str(j)][indexPredictionL]) - projectionData['BoneRadiusL.'+str(j)][indexPredictionL])
                                        predictionsData.loc[i, nameTime] = projectionData['Time'][indexContactL] - projectionData['Time'][indexPredictionL]
                                        predictionsData.loc[i, nameClos] = 1000*(abs(projectionData['ClosestPointL.'+str(j)][indexPredictionL] - projectionData['PosPredL.'+str(j)][indexPredictionL]) - projectionData['BoneRadiusL.'+str(j)][indexPredictionL])
    
                                        # predictionsData.loc[i] = pd.Series({ nameDist : 1000*(abs(projectionData['TouchingL.'+str(j)][indexContactL] - projectionData['PosPredL.'+str(j)][indexPredictionL]) - projectionData['BoneRadiusL.'+str(j)][indexPredictionL])})
                                        # predictionsData.loc[i] = pd.Series({ nameTime : projectionData['Time'][indexContactL] - projectionData['Time'][indexPredictionL]})
                                    else:
                                        pass
                                        # print('Config' + str(i), 'PhalanxL. ' + str(j), 'NoPredictionsL in Tolerance ' + str(k), ' with PlaneL 0')
                                else:
                                    pass
                                    # print('Config' + str(i), 'PhalanxL. ' + str(j), 'NoPredictionsL in Tolerance ' + str(k), ' with PlaneL 0')
                            else:
                                pass
                                # print('Config' + str(i), 'PhalanxL. ' + str(j), 'NoPredictions in Tolerance ' + str(k), ' with PlaneL 0')


                        
                        # plt.scatter(projectionData['Time'][indexCond], k*projectionData['InGoodTolL0.'+str(j)+'.'+str(k)][indexCond], label = 'InGoodTolL0.'+str(j)+'.'+str(k))
                        
                    # plt.vlines(projectionData['Time'][indexContactL], 0, 53, linestyles = 'dashed', colors = 'cyan', label = 'ContactL!')
                    # plt.scatter(projectionData['Time'][indexCond], projectionData['BoneContactL.'+str(j)][indexCond], label = 'BoneContact')

                    # plt.title('Config' + str(i) + '; Phalanx ' + str(j) + 'L-' + projectionData['BoneNameL.'+str(j)].iloc[0])
                    # plt.legend()


                else:
                    pass
                    # print('Config', str(i), ' Fail-L!')


predictionsData.to_csv('./Analysis/A-'+ str(projectionData['Alpha'].iloc[0]) + '-' + date + '-FullAnalysis-' + word + '.csv', sep = ';')
predictionsData.describe().to_csv('./Analysis/A-'+ str(projectionData['Alpha'].iloc[0]) + '-' + date + '-DescribeDf-' + word + '.csv', sep = ';')
predictionsData

shutil.move('../Assets/Resources/DataSimulation/AlgoHints/A-' + str(alpha) + '-' + word + date + '.csv', './Analysis/SimulationData/'+ date + '/A-' + str(alpha) + '-' + word + date + '.csv')

progress(100, 100)
print('\n')
